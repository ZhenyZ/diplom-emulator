#include "moduleconfigwindow.h"
#include "ui_moduleconfigwindow.h"

#include <moduledevice.h>
#include <core/modulebuilder.h>

#include <QDebug>

#include <QLabel>
#include <QTextEdit>
#include <QCheckBox>
#include <QComboBox>
#include <QStandardItemModel>
#include <QTableWidgetItem>

#include "moduleconfigdelegate.h"
#include "defaultconfigdelegate.h"

ModuleConfigWindow::ModuleConfigWindow(ModuleDevice* dev, ModuleBuilder* bld, QWidget *parent) :
	QWidget(parent),
	ui(new Ui::ModuleConfigWindow),
	device(dev),
	moduleBuilder(bld)
{
	this->config = new DeviceConfig();
	this->config->load(dev->getConfig()->save());
	ui->setupUi(this);

	ui->tableView->setItemDelegateForColumn(1, new ModuleConfigDelegate());
	ui->commonTableView->setItemDelegateForColumn(1, new DefaultConfigDelegate());

	setupModel();
	ui->tableView->setModel(itemModel);
	ui->commonTableView->setModel(defaultPropertiesModel);

	loadConfig();

	this->ui->caption1->setText(device->getName());
}

ModuleConfigWindow::~ModuleConfigWindow() {
	delete ui;
	delete config;
	delete itemModel;
}

void ModuleConfigWindow::slotTypeModelChanged(QModelIndex lt, QModelIndex, QVector<int> roles) {
	int row = lt.row();
	if (lt.column() != 1 || itemModel->item(row, 2) == nullptr) return;
	ModuleConfigField fld = lt.data(Qt::EditRole).value<ModuleConfigField>();
	switch (fld) {
		case ModuleConfigField::Boolean:
			itemModel->item(row, 2)->setData(QVariant(false));
		break;
		case ModuleConfigField::Hex:
			itemModel->item(row, 2)->setData(QVariant((qword)0));
		break;
		case ModuleConfigField::Integer:
			itemModel->item(row, 2)->setData(QVariant((int)0));
		break;
		case ModuleConfigField::String:
			itemModel->item(row, 2)->setData(QVariant(""));
		break;
	}
	QModelIndex left = itemModel->index(row, 2);
	emit signalModelChanged(left, left, roles);
}

void ModuleConfigWindow::setupModel() {
	QStringList headers;
	headers << "Имя" << "Тип" << "Значение";

	this->itemModel = new ModuleConfigModel();
	this->defaultPropertiesModel = new QStandardItemModel();
	defaultPropertiesModel->setColumnCount(2);
	itemModel->setColumnCount(3);
	itemModel->setHorizontalHeaderLabels(headers);
	defaultPropertiesModel->setHorizontalHeaderLabels({"Имя","Значение"});
	connect(
		itemModel, SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)),
		this, SLOT(slotTypeModelChanged(QModelIndex,QModelIndex,QVector<int>))
	);
	connect
		(this, SIGNAL(signalModelChanged(QModelIndex,QModelIndex,QVector<int>)),
		 itemModel, SIGNAL(signalForceChanged(QModelIndex,QModelIndex,QVector<int>)));

}

void ModuleConfigWindow::on_btnSave_clicked() {

	DeviceConfig * conf = new DeviceConfig();

	for (int i = 0; i < itemModel->rowCount(); ++i) {
		QString key = itemModel->item(i, 0)->data(Qt::EditRole).toString();
		ModuleConfigField mcf = itemModel->item(i, 1)->data(Qt::EditRole).value<ModuleConfigField>();
		QVariant value = itemModel->item(i, 2)->data(Qt::EditRole);

		switch (mcf) {
			case ModuleConfigField::Integer:
				conf->setProperty(key, (int)value.toInt());
			break;
			case ModuleConfigField::Hex:
				conf->setProperty(key, (qword)value.toUInt());
			break;
			case ModuleConfigField::Boolean:
				conf->setProperty(key, value.toBool());
			break;
			case ModuleConfigField::String:
				conf->setProperty(key, value.toString());
			break;
		}
	}

	for (int i = 0; i < defaultPropertiesModel->rowCount(); ++i) {
		QString key = defaultPropertiesModel->item(i, 0)->data().toString();
		QVariant value = defaultPropertiesModel->item(i, 1)->data(Qt::EditRole);
		if (key == "module_isBusBound") {
			conf->setProperty(key, value.toBool());
		} else
		if (key == "module_tickInterval") {
			conf->setProperty(key, value.toInt());
		}
	}

	device->getConfig()->load(conf->save());
	delete conf;
	moduleBuilder->save();
	this->close();
}

void ModuleConfigWindow::loadConfig() {
	if (!config->hasProperty("module_isBusBound")) {
		config->setProperty("module_isBusBound", false);
	}
	if (!config->hasProperty("module_tickInterval")) {
		config->setProperty("module_tickInterval", (int)1);
	}

	auto map = config->getPropertiesMap();
	itemModel->setRowCount(0);
	defaultPropertiesModel->setRowCount(0);

	int row = 0;
	int defaultRow = 0;

	for (auto it = map.begin(); it != map.end(); ++it) {
		if (it.key() == "module_isBusBound" || it.key() == "module_tickInterval") {
			QStandardItem* keyItem = new QStandardItem();
			keyItem->setData(it.key());
			if (it.key() == "module_isBusBound") {
				keyItem->setText("Привязать к шине");
			} else
			if (it.key() == "module_tickInterval") {
				keyItem->setText("Интервал опроса");
			}
			keyItem->setEditable(false);
			QStandardItem* valueItem = new QStandardItem();
			valueItem->setData(it.value(), Qt::EditRole);
			valueItem->setEditable(true);
			defaultPropertiesModel->setItem(defaultRow, 0, keyItem);
			defaultPropertiesModel->setItem(defaultRow, 1, valueItem);
			++defaultRow;
		} else {
			QStandardItem* nameItem = new QStandardItem();
			nameItem->setData(it.key(), Qt::EditRole);

			QStandardItem* valueItem = new QStandardItem;
			auto value = it.value();
			valueItem->setData(value, Qt::EditRole);

			QStandardItem* typeItem = new QStandardItem;
			ModuleConfigField fieldType;

			if (value.type() == QVariant::Type::UInt) {
				fieldType = ModuleConfigField::Hex;
			} else
			if (value.type() == QVariant::Type::Int) {
				fieldType = ModuleConfigField::Integer;
			} else
			if (value.type() == QVariant::Type::String) {
				fieldType = ModuleConfigField::String;
			} else
			if (value.type() == QVariant::Type::Bool) {
				fieldType = ModuleConfigField::Boolean;
			}
			typeItem->setData(QVariant::fromValue(fieldType), Qt::EditRole);
			itemModel->setItem(row, 0, nameItem);
			itemModel->setItem(row, 1, typeItem);
			itemModel->setItem(row, 2, valueItem);
			++row;
		}
	}
	ui->tableView->resizeColumnsToContents();
	ui->commonTableView->resizeColumnsToContents();
}

void ModuleConfigWindow::on_btnCancel_clicked() {
	this->close();
}

void ModuleConfigWindow::on_btnAdd_clicked() {
	QList<QStandardItem*> items;
	QStandardItem* nameItem = new QStandardItem;
	QStandardItem* valueItem = new QStandardItem;
	QStandardItem* typeItem = new QStandardItem;
	ModuleConfigField fieldType = ModuleConfigField::Integer;
	nameItem->setData(QString(""), Qt::EditRole);
	valueItem->setData((int)0, Qt::EditRole);
	typeItem->setData(QVariant::fromValue(fieldType), Qt::EditRole);
	items << nameItem << typeItem << valueItem;
	itemModel->appendRow(items);
	ui->tableView->resizeColumnsToContents();
	ui->commonTableView->resizeColumnsToContents();
}

void ModuleConfigWindow::on_btnRemove_clicked() {
	auto index = ui->tableView->currentIndex();
	if (index.isValid()) {
		itemModel->removeRow(index.row());
	}
	ui->tableView->resizeColumnsToContents();
	ui->commonTableView->resizeColumnsToContents();
}
