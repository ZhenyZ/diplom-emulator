#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "systembuilder.h"
#include "statisticswidget.h"

#include <core/repository/modulerepository.h>
#include <core/modulebuilder.h>
#include <core/compiledsystem.h>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	moduleBuilder = new ModuleBuilder();
	compiledSystem = new CompiledSystem();

	compiledSystemThread = new QThread();
	compiledSystem->moveToThread(compiledSystemThread);
	compiledSystemThread->start(QThread::Priority::LowPriority);

//	connect(this, SIGNAL(signalStartSystemThread(Priority)), compiledSystemThread, SLOT(start(Priority)));
//	connect(this, SIGNAL(signalStopSystemThread()), compiledSystemThread, SLOT(quit()));
//	emit signalStartSystemThread(QThread::Priority::LowPriority);

	connect(this, SIGNAL(signalStopSystem()), compiledSystem, SLOT(slotStop()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalStartSystem()), compiledSystem, SLOT(slotStart()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalRun()), compiledSystem, SLOT(slotResume()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalStep()), compiledSystem, SLOT(slotStep()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalReset()), compiledSystem, SLOT(slotReset()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalSuspend()), compiledSystem, SLOT(slotSuspend()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalSetModules(ModuleMap)), compiledSystem, SLOT(slotSetModules(ModuleMap)), Qt::QueuedConnection);
	connect(this, SIGNAL(signalEnableDebug()), compiledSystem, SLOT(slotAllowDebug()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalDisableDebug()), compiledSystem, SLOT(slotDenyDebug()), Qt::QueuedConnection);

	systemBuilder = new SystemBuilder();
	systemBuilder->setModuleBuilder(moduleBuilder);
	systemBuilder->setWindowFlags(systemBuilder->windowFlags() | Qt::Dialog);
	systemBuilder->setWindowModality(Qt::WindowModal);

	statisticsWidget = new StatisticsWidget();

	debugTimer = new QTimer();
	debugTimer->setSingleShot(false);
	debugTimer->setInterval(ui->debugDelaySpinbox->value());
	debugTimer->start();
	isRunning = false;
}

MainWindow::~MainWindow() {

	emit signalStopSystem();
	// Send signal
	QApplication::processEvents();
	// Switch threads for disconnect
	QApplication::processEvents();

	// Disconnect
//	emit signalStopSystemThread();
//	QApplication::processEvents();
	compiledSystemThread->quit();
	compiledSystemThread->wait();

	delete ui;
	if (moduleBuilder) {
		delete moduleBuilder;
	}
	delete compiledSystemThread;
	delete compiledSystem;
	if (systemBuilder) {
		delete systemBuilder;
	}
	if (statisticsWidget) {
		delete statisticsWidget;
	}
	debugTimer->deleteLater();
}

void MainWindow::debug(const QMap<QString, QString> & strs)
{
	//qDebug() << "MainWindow sigDebug with " << strs;
	if (ui->checkBox->isChecked()) {
		//for (auto x = strs.begin(); x != strs.end(); ++x) {
			this->ui->listWidget->addItem(strs["name"] + ": " + strs["message"]);
		//}
	}
}

void MainWindow::on_pushButton_clicked() {
	systemBuilder->show();
}

void MainWindow::on_pushButton_2_clicked()
{
	isRunning = false;
	disconnect(compiledSystem, SIGNAL(signalDebug(QMap<QString,QString>)), this, SLOT(debug(QMap<QString,QString>)));
	moduleBuilder->build();
	compiledSystem->setTickLength(moduleBuilder->getTickLengthMs());
	auto lst = moduleBuilder->getModules();
	for (auto item : lst) {
		QObject* obj = dynamic_cast<QObject*>(item);
		if (obj != nullptr ){
			obj->moveToThread(compiledSystemThread);
		}
	}
	emit signalSetModules(moduleBuilder->getModules());
	connect(compiledSystem,SIGNAL(signalDebug(const QMap<QString,QString>&)), this, SLOT(debug(const QMap<QString,QString>&)), Qt::QueuedConnection);
	emit signalStartSystem();
	qDebug() << "MainWindow onStart in thread " << this->thread();
	ui->btnPause->setEnabled(true);
	ui->btnRun->setEnabled(true);
	ui->pushButton_3->setEnabled(true);
	ui->pushButton_4->setEnabled(true);
	ui->pushButton_5->setEnabled(true);
	ui->pushButton_2->setEnabled(false);
	ui->btnReset->setEnabled(true);
}

void MainWindow::on_pushButton_3_clicked() {
	emit signalStep();
}

void MainWindow::on_pushButton_4_clicked()
{
	isRunning = false;
	if (ui->checkBox->isChecked()) {
		disconnect(debugTimer, SIGNAL(timeout()), this, SLOT(debugTimerTick()));
	}
	emit signalStopSystem();
	QApplication::processEvents();
}

void MainWindow::on_btnRun_clicked() {
	isRunning = true;
	if (ui->checkBox->isChecked()) {
		connect(debugTimer, SIGNAL(timeout()), this, SLOT(debugTimerTick()));
	} else {
		emit signalRun();
	}
}

void MainWindow::on_btnPause_clicked() {
	isRunning = false;
	if (ui->checkBox->isChecked()) {
		disconnect(debugTimer, SIGNAL(timeout()), this, SLOT(debugTimerTick()));
	} else {
		emit signalSuspend();
	}
}

void MainWindow::on_checkBox_toggled(bool checked) {
	if (checked) {
		if (isRunning) {
			emit signalSuspend();
		}
		connect(debugTimer, SIGNAL(timeout()), this, SLOT(debugTimerTick()));
		emit signalEnableDebug();
	} else  {
		if (isRunning) {
			emit signalRun();
		}
		disconnect(debugTimer, SIGNAL(timeout()), this, SLOT(debugTimerTick()));
		emit signalDisableDebug();
	}
	ui->debugDelaySpinbox->setEnabled(checked);
	ui->label->setEnabled(checked);
}

void MainWindow::on_pushButton_5_clicked() {
	statisticsWidget->setCompiledSystem(this->compiledSystem);
	statisticsWidget->show();
}

void MainWindow::keyPressEvent(QKeyEvent *e) {
	if ((e->key() == Qt::Key_F7)) {
		emit signalStep();
	}
}

void MainWindow::keyReleaseEvent(QKeyEvent *e) {
}

void MainWindow::closeEvent(QCloseEvent * e) {
	emit signalStopSystem();
	qDebug() << "Emit stop";
	QApplication::processEvents();
	qDebug() << "Emit stopthread";
	emit signalStopSystemThread();
	QApplication::processEvents();
	qDebug() << "done";
	delete moduleBuilder;
	moduleBuilder = nullptr;
	delete systemBuilder;
	systemBuilder = nullptr;
	delete statisticsWidget;
	statisticsWidget = nullptr;
	QMainWindow::closeEvent(e);
	QApplication::processEvents();
	QApplication::exit();
}

void MainWindow::on_btnReset_clicked() {
	emit signalReset();
}

void MainWindow::debugTimerTick()
{
	if (isRunning) {
		emit signalStep();
	}
}

void MainWindow::on_debugDelaySpinbox_valueChanged(int arg1) {
	debugTimer->setInterval(arg1);
}
