#ifndef DEFAULTCONFIGDELEGATE_H
#define DEFAULTCONFIGDELEGATE_H

#include <QStyledItemDelegate>

class DefaultConfigDelegate : public QStyledItemDelegate
{
	Q_OBJECT

public:
	DefaultConfigDelegate(QObject *parent = 0);

	QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
						  const QModelIndex &index) const override;

	void setEditorData(QWidget *editor, const QModelIndex &index) const override;
	void setModelData(QWidget *editor, QAbstractItemModel *model,
					  const QModelIndex &index) const override;

	virtual void updateEditorGeometry(QWidget *editor,
		const QStyleOptionViewItem &option, const QModelIndex &index) const override;

	virtual void paint(QPainter *painter, const QStyleOptionViewItem &option,
							 const QModelIndex &index) const override;

	virtual QString displayText(const QVariant &value, const QLocale &locale) const override;
};

#endif
