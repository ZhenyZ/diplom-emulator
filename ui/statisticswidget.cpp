#include "statisticswidget.h"
#include "ui_statisticswidget.h"

#include <core/compiledsystem.h>
#include <moduledevice.h>

StatisticsWidget::StatisticsWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::StatisticsWidget)
{
	ui->setupUi(this);
}

StatisticsWidget::~StatisticsWidget() {
	delete ui;
}

void StatisticsWidget::setCompiledSystem(CompiledSystem * system) {
	this->system = system;

	buildModulesList();
	this->ui->labelTickCount->setText(QString::number(system->getTickCount()));
	this->ui->labelTimeTotal->setText(QString::number(system->getTickCount() * system->getTickLength()) + " мс");
}

void StatisticsWidget::buildModulesList() {
	ui->tableWidget->setRowCount(0);
	QStringList header;
	header << "Модуль" << "Тип" << "Слот" << "Обращений" << "Доля";
	ui->tableWidget->setHorizontalHeaderLabels(header);
	auto modules = system->getModulesList();

	quint64 inputTotalCallCount = 0;
	quint64 outputTotalCallCount = 0;
	quint64 signalTotalCallCount = 0;

	for (auto dev: modules) {
		auto inputList = dev->getInputs();
		auto outputList = dev->getOutputs();
		auto signalList = dev->getSignalSources();
		for (auto it = inputList.begin(); it != inputList.end(); ++it) {
			inputTotalCallCount += (*it).callCount;
		}
		for (auto it = outputList.begin(); it != outputList.end(); ++it) {
			outputTotalCallCount += (*it).callCount;
		}
		for (auto it = signalList.begin(); it != signalList.end(); ++it) {
			signalTotalCallCount += (*it).callCount;
		}
	}

	for (auto dev: modules) {
		auto inputList = dev->getInputs();
		auto inputNames = dev->getInputNames();
		auto outputList = dev->getOutputs();
		auto outputNames = dev->getOutputNames();
		auto signalList = dev->getSignalSources();
		auto signalNames = dev->getSignalSourcesNames();

		for (auto it = inputList.begin(); it != inputList.end(); ++it) {
			if ((*it).callCount) {
				auto row = ui->tableWidget->rowCount();
				ui->tableWidget->insertRow(row);
				ui->tableWidget->setItem(row, 0, new QTableWidgetItem(dev->getName()));
				ui->tableWidget->setItem(row, 1, new QTableWidgetItem("Ввод"));
				ui->tableWidget->setItem(row, 2, new QTableWidgetItem(inputNames[it.key()]));
				auto callCount = (*it).callCount;
				auto callsItem = new QTableWidgetItem();
				callsItem->setData(Qt::DisplayRole, callCount);
				callsItem->setData(Qt::EditRole, callCount);
				ui->tableWidget->setItem(row, 3, callsItem);
				double percent = 100.0 * (double)((*it).callCount)/inputTotalCallCount;
				auto percentItem = new QTableWidgetItem();
				percentItem->setData(Qt::DisplayRole, percent);
				percentItem->setData(Qt::EditRole, percent);
				ui->tableWidget->setItem(row, 4, percentItem);
			}
		}
		for (auto it = outputList.begin(); it != outputList.end(); ++it) {
			if ((*it).callCount) {
				auto row = ui->tableWidget->rowCount();
				ui->tableWidget->insertRow(row);
				ui->tableWidget->setItem(row, 0, new QTableWidgetItem(dev->getName()));
				ui->tableWidget->setItem(row, 1, new QTableWidgetItem("Вывод"));
				ui->tableWidget->setItem(row, 2, new QTableWidgetItem(outputNames[it.key()]));
				auto callCount = (*it).callCount;
				auto callsItem = new QTableWidgetItem();
				callsItem->setData(Qt::DisplayRole, callCount);
				callsItem->setData(Qt::EditRole, callCount);
				ui->tableWidget->setItem(row, 3, callsItem);
				double percent = 100.0 * (double)((*it).callCount)/outputTotalCallCount;
				auto percentItem = new QTableWidgetItem();
				percentItem->setData(Qt::DisplayRole, percent);
				percentItem->setData(Qt::EditRole, percent);
				ui->tableWidget->setItem(row, 4, percentItem);
			}
		}
		for (auto it = signalList.begin(); it != signalList.end(); ++it) {
			if ((*it).callCount) {
				auto row = ui->tableWidget->rowCount();
				ui->tableWidget->insertRow(row);
				ui->tableWidget->setItem(row, 0, new QTableWidgetItem(dev->getName()));
				ui->tableWidget->setItem(row, 1, new QTableWidgetItem("Сигнал"));
				ui->tableWidget->setItem(row, 2, new QTableWidgetItem(signalNames[it.key()]));
				auto callCount = (*it).callCount;
				auto callsItem = new QTableWidgetItem();
				callsItem->setData(Qt::DisplayRole, callCount);
				callsItem->setData(Qt::EditRole, callCount);
				ui->tableWidget->setItem(row, 3, callsItem);
				double percent = 100.0 * (double)((*it).callCount)/signalTotalCallCount;
				auto percentItem = new QTableWidgetItem();
				percentItem->setData(Qt::DisplayRole, percent);
				percentItem->setData(Qt::EditRole, percent);
				ui->tableWidget->setItem(row, 4, percentItem);

			}
		}
	}

}
