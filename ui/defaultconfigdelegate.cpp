#include "defaultconfigdelegate.h"

#include <QTextEdit>
#include <QComboBox>
#include <QCheckBox>

#include <constants.h>

DefaultConfigDelegate::DefaultConfigDelegate(QObject *parent)
	: QStyledItemDelegate(parent)
{

}

void DefaultConfigDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option,
						 const QModelIndex &index) const
{
	QStyledItemDelegate::paint(painter, option, index);
}

QString DefaultConfigDelegate::displayText(const QVariant & value, const QLocale & locale) const {
	return value.toString();
}

QWidget *DefaultConfigDelegate::createEditor(QWidget *parent,
	const QStyleOptionViewItem &opt,
	const QModelIndex & index) const
{
	auto value = index.data();
	if (value.type() == QVariant::Type::Bool) {
		return new QCheckBox(parent);
	} else
	if (value.type() == QVariant::Type::Int) {
		return new QTextEdit(parent);
	}
	return nullptr;
}

void DefaultConfigDelegate::setEditorData(
		QWidget *editor,
		const QModelIndex &index) const
{
	auto itemValue = index.model()->data(index, Qt::EditRole);
	if (itemValue.type() == QVariant::Type::Bool) {
		QCheckBox* cb = static_cast<QCheckBox*>(editor);
		cb->setChecked(itemValue.toBool());
	} else
	if (itemValue.type() == QVariant::Type::Int) {
		QTextEdit* edit = static_cast<QTextEdit*>(editor);
		edit->setText(itemValue.toString());
	}
}

void DefaultConfigDelegate::setModelData(
		QWidget *editor,
		QAbstractItemModel *model,
		const QModelIndex &index) const
{
	auto itemValue = index.model()->data(index, Qt::EditRole);
	if (itemValue.type() == QVariant::Type::Bool) {
		QCheckBox* cb = static_cast<QCheckBox*>(editor);
		model->setData(index, QVariant::fromValue(cb->isChecked()), Qt::EditRole);
	} else
	if (itemValue.type() == QVariant::Type::Int) {
		QTextEdit* edit = static_cast<QTextEdit*>(editor);
		model->setData(index, QVariant::fromValue(edit->toPlainText().toInt()), Qt::EditRole);
	}
}

void DefaultConfigDelegate::updateEditorGeometry(QWidget *editor,
	const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
	editor->setGeometry(option.rect);
}

