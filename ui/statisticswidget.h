#ifndef STATISTICSWIDGET_H
#define STATISTICSWIDGET_H

#include <QWidget>

namespace Ui {
	class StatisticsWidget;
}

class CompiledSystem;

class StatisticsWidget : public QWidget
{
	Q_OBJECT

public:
	explicit StatisticsWidget(QWidget *parent = 0);
	~StatisticsWidget();
	void setCompiledSystem(CompiledSystem* system);

private:
	Ui::StatisticsWidget *ui;
	CompiledSystem* system = nullptr;

	void buildModulesList();
};

#endif // STATISTICSWIDGET_H
