#ifndef GENERICDATAFORM_H
#define GENERICDATAFORM_H

#include <QWidget>
#include <QMap>
#include <QString>
#include <functional>

namespace Ui {
	class GenericDataForm;
}

class ModuleDevice;

#include <core/modulebuilder.h>

class GenericDataForm : public QWidget
{
	Q_OBJECT

public:

	typedef QMap<quint64, QString> DescriptorMap;
	typedef std::function<DescriptorMap(ModuleDevice*)> DescriptorGetter;
	typedef std::function<QList<ModuleBuilder::Connector>(ModuleBuilder*, quint64)> ThisDeviceConnectorsGetter;
	typedef std::function<void(ModuleBuilder*, ModuleBuilder::Connector)> AddCallback;
	typedef std::function<void(ModuleBuilder*, ModuleBuilder::Connector)> RemoveCallback;

	explicit GenericDataForm(QWidget *parent = 0);
	~GenericDataForm();

	void setLabelThisDeviceConnectors(const QString & str);
	void setLabelExternalDeviceConnectors(const QString & str);

	void setThisDeviceConnectorMapGetterCallback(DescriptorGetter d) {
		this->descriptorGetter = d;
	}
	void setExternalDeviceConnectorMapGetterCallback(DescriptorGetter d) {
		this->externalDescriptorGetter = d;
	}

	void setThisDeviceConnectorsGetterCallback(ThisDeviceConnectorsGetter ge) {
		this->thisConnectorsGetter = ge;
	}

	void setAddCallback(AddCallback cb) {
		this->addCallback = cb;
	}

	void setRemoveCallback(RemoveCallback cb) {
		this->removeCallback = cb;
	}

	void setInputDirection(bool isInput = true) {
		this->isInputDirected = isInput;
	}

	void setModuleBuilder(ModuleBuilder* mb);
	void setModuleDeviceId(quint64 id);

private slots:
	void on_listThisDeviceConnectors_currentRowChanged(int currentRow);

	void on_btnClose_clicked();

	void on_cbDevices_currentIndexChanged(int index);

	void on_btnAdd_clicked();

	void on_btnRemove_clicked();

private:
	Ui::GenericDataForm *ui;

	bool isInputDirected = false;

	ModuleBuilder* builder = nullptr;
	ModuleDevice* device = nullptr;

	QList<quint64> deviceMappingExternal;
	QList<quint64> deviceMappingThis;
	QList<QPair<quint64, quint64>> connectionsMapping;

	quint64 deviceId;

	void reloadDevice();

	DescriptorGetter descriptorGetter = nullptr;
	DescriptorGetter externalDescriptorGetter = nullptr;
	ThisDeviceConnectorsGetter thisConnectorsGetter = nullptr;
	AddCallback addCallback = nullptr;
	RemoveCallback removeCallback = nullptr;
};

#endif // GENERICDATAFORM_H
