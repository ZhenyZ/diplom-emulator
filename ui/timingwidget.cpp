#include "timingwidget.h"
#include "ui_timingwidget.h"

#include <core/modulebuilder.h>

TimingWidget::TimingWidget(ModuleBuilder * s, QWidget *parent) :
	QWidget(parent),
	ui(new Ui::TimingWidget)
{
	ui->setupUi(this);
	builder = s;
}

TimingWidget::~TimingWidget()
{
	delete ui;
}

void TimingWidget::on_spinBox_valueChanged(int arg1)
{
	if (changing) {
		return;
	}
	changing = true;
	double result = (double)1000.0/(double)arg1;
	builder->setTickLengthMs(result);
	this->ui->doubleSpinBox_2->setValue(result);
	changing = false;
}

void TimingWidget::on_doubleSpinBox_2_valueChanged(double arg1)
{
	if (changing) {
		return;
	}
	changing = true;
	quint64 hertz = 1000 / arg1;
	builder->setTickLengthMs(arg1);
	ui->spinBox->setValue(hertz);
	changing = false;
}
