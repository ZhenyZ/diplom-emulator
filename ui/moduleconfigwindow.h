#ifndef MODULECONFIGWINDOW_H
#define MODULECONFIGWINDOW_H

#include <QWidget>
#include "moduleconfigmodel.h"

namespace Ui {
	class ModuleConfigWindow;
}

class ModuleDevice;
class DeviceConfig;
class ModuleBuilder;

class ModuleConfigWindow : public QWidget
{
	Q_OBJECT

public:
	explicit ModuleConfigWindow(ModuleDevice* dev, ModuleBuilder * bld, QWidget *parent = 0);
	~ModuleConfigWindow();

signals:
	void signalModelChanged(QModelIndex lt, QModelIndex rb, QVector<int>);

public slots:
	void slotTypeModelChanged(QModelIndex lt, QModelIndex rb, QVector<int>);

private slots:
	void on_btnSave_clicked();

	void on_btnCancel_clicked();

	void on_btnAdd_clicked();

	void on_btnRemove_clicked();

private:
	Ui::ModuleConfigWindow *ui = nullptr;
	ModuleDevice* device = nullptr;
	DeviceConfig* config = nullptr;
	ModuleBuilder* moduleBuilder = nullptr;
	void loadConfig();

	ModuleConfigModel* itemModel = nullptr;
	QStandardItemModel* defaultPropertiesModel = nullptr;

	void setupModel();

};

#endif // MODULECONFIGWINDOW_H
