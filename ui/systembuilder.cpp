#include "systembuilder.h"
#include "ui_systembuilder.h"

#include <QDebug>
#include <QMessageBox>
#include <QFileDialog>

#include "helper/genericformbuilder.h"

#include <core/modulebuilder.h>

#include "moduleconfigwindow.h"
#include "timingwidget.h"

SystemBuilder::SystemBuilder(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::SystemBuilder)
{
	ui->setupUi(this);
	repo = new ModuleRepository();
	repo->parseDir("./modules");
	for (auto mod : repo->getModulesList()) {
		ui->lstModules->addItem(mod->getName(), mod->getUID());
	}

	ui->lstBuildModules->setCurrentRow(-1);

}

SystemBuilder::~SystemBuilder() {
	delete repo;
	delete ui;
}

void SystemBuilder::setModuleBuilder(ModuleBuilder * mb) {
	this->moduleBuilder = mb;
	this->moduleBuilder->setRepository(this->repo);

	if (0 == mb->getModules().size()) {
		const QString filename = "piix4-x.vmc";
		QFile f;
		f.setFileName(filename);
		if (f.open(QFile::ReadOnly)) {
			f.close();
			this->moduleBuilder->loadFromFile(filename);
		} else {
			QMessageBox * b = new QMessageBox();
			b->setWindowTitle("Ошибка");
			b->setText("Не удалось загрузить список модулей по умолчанию");
			b->exec();
			delete b;
			return;
		}
	}
	this->createBuildModulesList();
}

void SystemBuilder::on_btnAddModuleToBuild_clicked() {
	auto index = ui->lstModules->currentIndex();
	if (index != -1) {
		auto item = ui->lstModules->itemData(index);
		auto mod = repo->getByUid(item.value<quint64>());
		moduleBuilder->addModule(mod);
		auto itemWidget = new QListWidgetItem(mod->getName());
		itemWidget->setData(Qt::UserRole, mod->getUID());
		ui->lstBuildModules->addItem(itemWidget);
	}
}

void SystemBuilder::on_btnLoadFromFile_clicked() {
	QFileDialog dlg;
	dlg.setFileMode(QFileDialog::FileMode::ExistingFile);
	dlg.setAcceptMode(QFileDialog::AcceptMode::AcceptOpen);
	dlg.setNameFilter("Файлы конфигурации ВМ (*.vmc)");
	if (dlg.exec()) {
		this->moduleBuilder->loadFromFile(dlg.selectedFiles().first());
		this->createBuildModulesList();
	}
}

void SystemBuilder::createBuildModulesList() {
	ui->lstBuildModules->clear();
	for (ModuleDevice* dev: moduleBuilder->getModules()) {
		auto itemWidget = new QListWidgetItem(dev->getName());
		itemWidget->setData(Qt::UserRole, dev->getUID());
		ui->lstBuildModules->addItem(itemWidget);
	}
}

quint64 SystemBuilder::getDeviceId(QListWidgetItem * item) {
	return item->data(Qt::UserRole).value<quint64>();
}

void SystemBuilder::on_btnSaveToFile_clicked() {
	QFileDialog dlg;
	dlg.setFileMode(QFileDialog::FileMode::AnyFile);
	dlg.setAcceptMode(QFileDialog::AcceptMode::AcceptSave);
	dlg.setConfirmOverwrite(true);
	dlg.setDefaultSuffix("vmc");
	dlg.setNameFilter("Файлы конфигурации ВМ (*.vmc)");
	if (dlg.exec()) {
		this->moduleBuilder->saveToFile(dlg.selectedFiles().first());
	}
}

void SystemBuilder::on_btnOutputPorts_clicked() {
	auto item =  ui->lstBuildModules->currentItem();
	if (item) {
		auto f = GenericFormBuilder::buildPortsOutputForm(this->moduleBuilder, getDeviceId(item));
		f->show();
	}
}

void SystemBuilder::on_btnInputSignals_clicked() {
	auto item =  ui->lstBuildModules->currentItem();
	if (item) {
		auto f = GenericFormBuilder::buildSignalsInputForm(this->moduleBuilder, getDeviceId(item));
		f->show();
	}
}

void SystemBuilder::on_btnInputPorts_clicked() {
	auto item =  ui->lstBuildModules->currentItem();
	if (item) {
		auto f = GenericFormBuilder::buildPortsInputForm(this->moduleBuilder, getDeviceId(item));
		f->show();
	}
}

void SystemBuilder::on_btnInputData_clicked() {
	auto item =  ui->lstBuildModules->currentItem();
	if (item) {
		auto f = GenericFormBuilder::buildDataInputForm(this->moduleBuilder, getDeviceId(item));
		f->show();
	}
}

void SystemBuilder::on_btnClose_clicked()
{
	this->close();
}

void SystemBuilder::on_btnOutputSignals_clicked() {
	auto item =  ui->lstBuildModules->currentItem();
	if (item) {
		auto f = GenericFormBuilder::buildSignalsOutputForm(this->moduleBuilder, getDeviceId(item));
		f->show();
	}
}

void SystemBuilder::on_btnOutputData_clicked() {
	auto item =  ui->lstBuildModules->currentItem();
	if (item) {
		auto f = GenericFormBuilder::buildDataOutputForm(this->moduleBuilder, getDeviceId(item));
		f->show();
	}
}

void SystemBuilder::on_btnModuleConfig_clicked() {
	auto item =  ui->lstBuildModules->currentItem();
	if (item) {
		auto dev = this->moduleBuilder->getModuleById(getDeviceId(item));
		auto f = new ModuleConfigWindow(dev, this->moduleBuilder);
		f->show();
	}
}

void SystemBuilder::on_btnRemoveModuleFromBuild_clicked() {
	auto item =  ui->lstBuildModules->currentItem();
	if (item) {
		auto mod = moduleBuilder->getModuleById(getDeviceId(item));
		moduleBuilder->removeModule(mod);
		this->createBuildModulesList();
	}
}

void SystemBuilder::on_btnCreateNew_clicked() {
	this->moduleBuilder->clear();
	this->createBuildModulesList();
}

void SystemBuilder::on_btnTimings_clicked()
{
	auto tw = new TimingWidget(moduleBuilder);
	tw->show();
}
