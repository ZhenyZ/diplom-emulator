#include "moduleconfigmodel.h"

ModuleConfigModel::ModuleConfigModel(QObject * parent)
	: QStandardItemModel(parent)
{
	connect(
		this, SIGNAL(signalForceChanged(QModelIndex,QModelIndex,QVector<int>)),
		this, SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>))
	);
}

ModuleConfigModel::ModuleConfigModel(int rows, int columns, QObject * parent)
	: QStandardItemModel(rows, columns, parent)
{

}


