#ifndef MODULECONFIGMODEL_H
#define MODULECONFIGMODEL_H

#include <QStandardItemModel>

class ModuleConfigModel : public QStandardItemModel
{
	Q_OBJECT
public:
	explicit ModuleConfigModel(QObject *parent = Q_NULLPTR);
	ModuleConfigModel(int rows, int columns, QObject *parent = Q_NULLPTR);
	virtual ~ModuleConfigModel() {}

signals:
	void signalForceChanged(QModelIndex lt, QModelIndex rb, QVector<int>);

};

#endif // MODULECONFIGMODEL_H
