#ifndef SYSTEMBUILDER_H
#define SYSTEMBUILDER_H

#include <QWidget>
#include <core/repository/modulerepository.h>

namespace Ui {
	class SystemBuilder;
}

class ModuleBuilder;
class CompiledSystem;

class QListWidgetItem;

class SystemBuilder : public QWidget
{
	Q_OBJECT

public:
	explicit SystemBuilder(QWidget *parent = 0);
	~SystemBuilder();

	void setModuleBuilder(ModuleBuilder* mb);
	ModuleBuilder* getModuleBuilder() {
		return this->moduleBuilder;
	}

private slots:
	void on_btnInputPorts_clicked();
	void on_btnAddModuleToBuild_clicked();
	void on_btnLoadFromFile_clicked();
	void on_btnSaveToFile_clicked();
	void on_btnOutputPorts_clicked();
	void on_btnInputSignals_clicked();
	void on_btnInputData_clicked();
	void on_btnClose_clicked();
	void on_btnOutputSignals_clicked();
	void on_btnOutputData_clicked();

	void on_btnModuleConfig_clicked();

	void on_btnRemoveModuleFromBuild_clicked();

	void on_btnCreateNew_clicked();

	void on_btnTimings_clicked();

private:
	Ui::SystemBuilder *ui;
	ModuleRepository* repo = nullptr;
	ModuleBuilder* moduleBuilder = nullptr;

	void createBuildModulesList();

	quint64 getDeviceId(QListWidgetItem* item);

};

#endif // SYSTEMBUILDER_H
