#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QMap>
#include <QString>
#include <QThread>
#include <QTimer>

#include <moduledevice.h>

class ModuleBuilder;
class CompiledSystem;

namespace Ui {
	class MainWindow;
}

class SystemBuilder;
class StatisticsWidget;

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

public slots:
	void debug(const QMap<QString, QString> & strs);

private slots:
	void on_pushButton_clicked();

	void on_pushButton_2_clicked();

	void on_pushButton_3_clicked();

	void on_pushButton_4_clicked();

	void on_btnRun_clicked();

	void on_btnPause_clicked();

	void on_checkBox_toggled(bool checked);

	void on_pushButton_5_clicked();

	virtual void keyPressEvent(QKeyEvent* e) override;
	virtual void keyReleaseEvent(QKeyEvent* e) override;
	virtual void closeEvent(QCloseEvent* e) override;

	void on_btnReset_clicked();

	void debugTimerTick();

	void on_debugDelaySpinbox_valueChanged(int arg1);

signals:
	void signalStartSystem();
	void signalStopSystem();
	void signalStep();
	void signalRun();
	void signalSuspend();
	void signalReset();
	void signalSetModules(ModuleMap);
	void signalEnableDebug();
	void signalDisableDebug();

	void signalStartSystemThread(QThread::Priority);
	void signalStopSystemThread();


private:
	Ui::MainWindow *ui;

	ModuleBuilder* moduleBuilder = nullptr;
	QThread * compiledSystemThread = nullptr;
	CompiledSystem* compiledSystem = nullptr;
	SystemBuilder* systemBuilder = nullptr;
	StatisticsWidget* statisticsWidget = nullptr;

	bool delayedDebug = false;
	bool isRunning = false;
	QTimer * debugTimer = nullptr;

};

#endif // MAINWINDOW_H
