#ifndef TIMINGWIDGET_H
#define TIMINGWIDGET_H

#include <QWidget>

namespace Ui {
	class TimingWidget;
}

class ModuleBuilder;

class TimingWidget : public QWidget
{
	Q_OBJECT

public:
	explicit TimingWidget(ModuleBuilder* s, QWidget *parent = 0);
	~TimingWidget();

private slots:
	void on_spinBox_valueChanged(int arg1);
	void on_doubleSpinBox_2_valueChanged(double arg1);
private:
	Ui::TimingWidget *ui;
	ModuleBuilder * builder = nullptr;
	bool changing = false;
};

#endif // TIMINGWIDGET_H
