#include "moduleconfigdelegate.h"

#include <QTextEdit>
#include <QComboBox>
#include <QCheckBox>

#include <constants.h>

ModuleConfigDelegate::ModuleConfigDelegate(QObject *parent)
	: QStyledItemDelegate(parent)
{

}

void ModuleConfigDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option,
						 const QModelIndex &index) const
{
	QStyledItemDelegate::paint(painter, option, index);
}

QString ModuleConfigDelegate::displayText(const QVariant & value, const QLocale & locale) const {
	ModuleConfigField val = value.value<ModuleConfigField>();
	if (val == ModuleConfigField::Integer) {
		return "int";
	} else
	if (val == ModuleConfigField::String) {
		return "string";
	} else
	if (val == ModuleConfigField::Boolean) {
		return "bool";
	} else
	if (val == ModuleConfigField::Hex) {
		return "hex";
	}
	return "unknown";
}

QWidget *ModuleConfigDelegate::createEditor(QWidget *parent,
	const QStyleOptionViewItem &opt,
	const QModelIndex & index) const
{
	auto value = index.data();
	QComboBox* cb = new QComboBox(parent);
	cb->addItem("bool");
	cb->addItem("string");
	cb->addItem("int");
	cb->addItem("hex");
	return cb;
}

void ModuleConfigDelegate::setEditorData(
		QWidget *editor,
		const QModelIndex &index) const
{
	auto itemValue = index.model()->data(index, Qt::EditRole);
	QComboBox* cb = static_cast<QComboBox*>(editor);
	ModuleConfigField w = itemValue.value<ModuleConfigField>();
	cb->setCurrentIndex((int)w);
}

void ModuleConfigDelegate::setModelData(
		QWidget *editor,
		QAbstractItemModel *model,
		const QModelIndex &index) const {
	QComboBox* cb = static_cast<QComboBox*>(editor);
	int selectedRow = cb->currentIndex();
	ModuleConfigField mcf = (ModuleConfigField)selectedRow;
	model->setData(index, QVariant::fromValue(mcf), Qt::EditRole);
}

void ModuleConfigDelegate::updateEditorGeometry(QWidget *editor,
	const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
	editor->setGeometry(option.rect);
}

