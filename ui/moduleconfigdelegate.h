#ifndef MODULECONFIGDELEGATE_H
#define MODULECONFIGDELEGATE_H

#include <QStyledItemDelegate>

enum class ModuleConfigField : int {
	Boolean = 0,
	String = 1,
	Integer = 2,
	Hex = 3
};

class ModuleConfigDelegate : public QStyledItemDelegate
{
	Q_OBJECT

public:
	ModuleConfigDelegate(QObject *parent = 0);

	QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
						  const QModelIndex &index) const override;

	void setEditorData(QWidget *editor, const QModelIndex &index) const override;
	void setModelData(QWidget *editor, QAbstractItemModel *model,
					  const QModelIndex &index) const override;

	virtual void updateEditorGeometry(QWidget *editor,
		const QStyleOptionViewItem &option, const QModelIndex &index) const override;

	virtual void paint(QPainter *painter, const QStyleOptionViewItem &option,
							 const QModelIndex &index) const override;

	virtual QString displayText(const QVariant &value, const QLocale &locale) const override;
};

Q_DECLARE_METATYPE(ModuleConfigField)

#endif // MODULECONFIGDELEGATE_H
