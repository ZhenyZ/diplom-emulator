#ifndef GENERICFORMBUILDER_H
#define GENERICFORMBUILDER_H

#include <QObject>
#include "../genericdataform.h"

#include <core/modulebuilder.h>
#include <moduledevice.h>

class GenericFormBuilder {
public:

	static GenericDataForm* buildDataInputForm(ModuleBuilder* moduleBuilder, quint64 deviceId) {
		auto f = new GenericDataForm();
		f->setModuleBuilder(moduleBuilder);
		f->setInputDirection(true);
		f->setLabelExternalDeviceConnectors("Шины данных - вывод");
		f->setLabelThisDeviceConnectors("Шины данных - ввод");
		f->setExternalDeviceConnectorMapGetterCallback([](ModuleDevice* dev) {
			return dev->getDataMapProvidedNames();
		});
		f->setThisDeviceConnectorMapGetterCallback([](ModuleDevice* dev) {
			return dev->getDataMapConsumedNames();
		});
		f->setThisDeviceConnectorsGetterCallback([](ModuleBuilder* builder, quint64 id) {
			return builder->getDataConsumedConnectorsByDeviceId(id);
		});
		f->setRemoveCallback([](ModuleBuilder* builder, ModuleBuilder::Connector conn) {
			builder->removeDataConnection(conn);
		});
		f->setAddCallback([](ModuleBuilder* builder, ModuleBuilder::Connector conn) {
			builder->addDataConnection(conn);
		});
		f->setModuleDeviceId(deviceId);
		return f;
	}

	static GenericDataForm* buildDataOutputForm(ModuleBuilder* moduleBuilder, quint64 deviceId) {
		auto f = new GenericDataForm();
		f->setModuleBuilder(moduleBuilder);
		f->setInputDirection(false);
		f->setLabelExternalDeviceConnectors("Шины данных - ввод");
		f->setLabelThisDeviceConnectors("Шины данных - вывод");
		f->setExternalDeviceConnectorMapGetterCallback([](ModuleDevice* dev) {
			return dev->getDataMapConsumedNames();
		});
		f->setThisDeviceConnectorMapGetterCallback([](ModuleDevice* dev) {
			return dev->getDataMapProvidedNames();
		});
		f->setThisDeviceConnectorsGetterCallback([](ModuleBuilder* builder, quint64 id) {
			return builder->getDataProvidedConnectorsByDeviceId(id);
		});
		f->setRemoveCallback([](ModuleBuilder* builder, ModuleBuilder::Connector conn) {
			builder->removeDataConnection(conn);
		});
		f->setAddCallback([](ModuleBuilder* builder, ModuleBuilder::Connector conn) {
			builder->addDataConnection(conn);
		});
		f->setModuleDeviceId(deviceId);
		return f;
	}

	static GenericDataForm* buildSignalsInputForm(ModuleBuilder* moduleBuilder, quint64 deviceId) {
		auto f = new GenericDataForm();
		f->setModuleBuilder(moduleBuilder);
		f->setInputDirection(true);
		f->setLabelExternalDeviceConnectors("Выходные сигналы");
		f->setLabelThisDeviceConnectors("Входные слоты");
		f->setExternalDeviceConnectorMapGetterCallback([](ModuleDevice* dev) {
			return dev->getSignalSourcesNames();
		});
		f->setThisDeviceConnectorMapGetterCallback([](ModuleDevice* dev) {
			return dev->getSignalReceiversNames();
		});
		f->setThisDeviceConnectorsGetterCallback([](ModuleBuilder* builder, quint64 id) {
			return builder->getSignalInputConnectorsByDeviceId(id);
		});
		f->setRemoveCallback([](ModuleBuilder* builder, ModuleBuilder::Connector conn) {
			builder->removeSignalConnection(conn);
		});
		f->setAddCallback([](ModuleBuilder* builder, ModuleBuilder::Connector conn) {
			builder->addSignalConnection(conn);
		});
		f->setModuleDeviceId(deviceId);
		return f;
	}

	static GenericDataForm* buildSignalsOutputForm(ModuleBuilder* moduleBuilder, quint64 deviceId) {
		auto f = new GenericDataForm();
		f->setModuleBuilder(moduleBuilder);
		f->setInputDirection(false);
		f->setLabelExternalDeviceConnectors("Входные слоты");
		f->setLabelThisDeviceConnectors("Выходные сигналы");
		f->setExternalDeviceConnectorMapGetterCallback([](ModuleDevice* dev) {
			return dev->getSignalReceiversNames();
		});
		f->setThisDeviceConnectorMapGetterCallback([](ModuleDevice* dev) {
			return dev->getSignalSourcesNames();
		});
		f->setThisDeviceConnectorsGetterCallback([](ModuleBuilder* builder, quint64 id) {
			return builder->getSignalOutputConnectorsByDeviceId(id);
		});
		f->setRemoveCallback([](ModuleBuilder* builder, ModuleBuilder::Connector conn) {
			builder->removeSignalConnection(conn);
		});
		f->setAddCallback([](ModuleBuilder* builder, ModuleBuilder::Connector conn) {
			builder->addSignalConnection(conn);
		});
		f->setModuleDeviceId(deviceId);
		return f;
	}

	static GenericDataForm* buildPortsInputForm(ModuleBuilder* moduleBuilder, quint64 deviceId) {
		auto f = new GenericDataForm();
		f->setModuleBuilder(moduleBuilder);
		f->setInputDirection(true);
		f->setLabelExternalDeviceConnectors("Порты вывода");
		f->setLabelThisDeviceConnectors("Порты ввода");
		f->setExternalDeviceConnectorMapGetterCallback([](ModuleDevice* dev) {
			return dev->getOutputNames();
		});
		f->setThisDeviceConnectorMapGetterCallback([](ModuleDevice* dev) {
			return dev->getInputNames();
		});
		f->setThisDeviceConnectorsGetterCallback([](ModuleBuilder* builder, quint64 id) {
			return builder->getPortInputConnectorsByDeviceId(id);
		});
		f->setRemoveCallback([](ModuleBuilder* builder, ModuleBuilder::Connector conn) {
			builder->removePortConnection(conn);
		});
		f->setAddCallback([](ModuleBuilder* builder, ModuleBuilder::Connector conn) {
			builder->addPortConnection(conn);
		});
		f->setModuleDeviceId(deviceId);
		return f;
	}

	static GenericDataForm* buildPortsOutputForm(ModuleBuilder* moduleBuilder, quint64 deviceId) {
		auto f = new GenericDataForm();
		f->setModuleBuilder(moduleBuilder);
		f->setInputDirection(false);
		f->setLabelExternalDeviceConnectors("Порты ввода");
		f->setLabelThisDeviceConnectors("Порты вывода");
		f->setExternalDeviceConnectorMapGetterCallback([](ModuleDevice* dev) {
			return dev->getInputNames();
		});
		f->setThisDeviceConnectorMapGetterCallback([](ModuleDevice* dev) {
			return dev->getOutputNames();
		});
		f->setThisDeviceConnectorsGetterCallback([](ModuleBuilder* builder, quint64 id) {
			return builder->getPortOutputConnectorsByDeviceId(id);
		});
		f->setRemoveCallback([](ModuleBuilder* builder, ModuleBuilder::Connector conn) {
			builder->removePortConnection(conn);
		});
		f->setAddCallback([](ModuleBuilder* builder, ModuleBuilder::Connector conn) {
			builder->addPortConnection(conn);
		});
		f->setModuleDeviceId(deviceId);
		return f;
	}

};

#endif // GENERICFORMBUILDER_H
