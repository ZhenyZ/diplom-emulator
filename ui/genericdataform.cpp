#include "genericdataform.h"
#include "ui_genericdataform.h"

#include <core/modulebuilder.h>
#include <moduledevice.h>

GenericDataForm::GenericDataForm(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::GenericDataForm)
{
	ui->setupUi(this);
}

GenericDataForm::~GenericDataForm() {
	delete ui;
}

void GenericDataForm::setLabelThisDeviceConnectors(const QString & str) {
	ui->labelThisDeviceConnectors->setText(str);
}

void GenericDataForm::setLabelExternalDeviceConnectors(const QString & str) {
	ui->labelExternalDeviceConnectors->setText(str);
}

void GenericDataForm::setModuleBuilder(ModuleBuilder* mb) {
	this->builder = mb;
}

void GenericDataForm::setModuleDeviceId(quint64 id) {
	this->deviceId = id;
	this->device = builder->getModuleById(id);
	reloadDevice();
}

void GenericDataForm::reloadDevice() {
	setWindowTitle(device->getName() + " - настройка");
	ui->lstConnections->clear();
	ui->listThisDeviceConnectors->clear();
	ui->cbDevices->clear();
	ui->cbExternalDeviceConnectors->clear();
	connectionsMapping.clear();
	deviceMappingThis.clear();
	if (builder->getModules().size()) {
		for (ModuleDevice* dev : builder->getModules()) {
			ui->cbDevices->addItem(dev->getName(), dev->getUID());
		}
		ui->cbDevices->setCurrentIndex(0);
	}
	DescriptorMap thisStrings = descriptorGetter(this->device);
	if (thisStrings.size()) {
		for (auto it = thisStrings.begin(); it != thisStrings.end(); ++it ) {
			ui->listThisDeviceConnectors->addItem(QString("%1").arg(it.key(), 4, 16, QChar('0')).toUpper() + " : " + it.value());
			deviceMappingThis.push_back(it.key());
		}
		ui->listThisDeviceConnectors->setCurrentRow(0);
	}
}

void GenericDataForm::on_listThisDeviceConnectors_currentRowChanged(int currentRow) {
	ui->lstConnections->clear();
	connectionsMapping.clear();
	if (currentRow != -1) {

		//auto ins = builder->getDataConsumedConnectorsByDeviceId(this->deviceId);
		QList<ModuleBuilder::Connector> ins = this->thisConnectorsGetter(this->builder, this->deviceId);
		if (ins.size()) {
			for (auto pin : ins) {
				quint64 pinIndex, otherPinIndex, otherDeviceId;
				if (this->isInputDirected) {
					pinIndex = pin.receiverSlotIndex;
					otherPinIndex = pin.senderSlotIndex;
					otherDeviceId = pin.senderId;
				} else {
					pinIndex = pin.senderSlotIndex;
					otherPinIndex = pin.receiverSlotIndex;
					otherDeviceId = pin.receiverId;
				}
				bool cond = pinIndex == deviceMappingThis[currentRow];
				if (cond) {
					auto dev = builder->getModuleById(this->isInputDirected ? pin.senderId : pin.receiverId);
					auto devNames = externalDescriptorGetter(dev);
					auto portName = devNames.find(otherPinIndex).value();
					ui->lstConnections->addItem(dev->getName() + ": " + QString("%1").arg(otherPinIndex, 4, 16, QChar('0')).toUpper() + " : " + portName);
					connectionsMapping.push_back({otherDeviceId, otherPinIndex});
				}
			}
		}
		ui->btnRemove->setEnabled(true);
	} else {
		ui->btnRemove->setEnabled(false);
		ui->btnAdd->setEnabled(false);
	}
}

void GenericDataForm::on_btnClose_clicked() {
	this->close();
}

void GenericDataForm::on_cbDevices_currentIndexChanged(int index) {
	ui->cbExternalDeviceConnectors->clear();
	deviceMappingExternal.clear();
	if (index != -1) {
		auto item = ui->cbDevices->itemData(index);
		auto dev = builder->getModuleById(item.value<quint64>());
		auto names = externalDescriptorGetter(dev);
		//auto names = dev->getDataMapProvidedNames();
		if (names.size()) {
			for (auto it = names.begin(); it != names.end(); ++it ) {
				ui->cbExternalDeviceConnectors->addItem(QString("%1").arg(it.key(), 4, 16, QChar('0')).toUpper() + " : " + it.value());
				deviceMappingExternal.push_back(it.key());
			}
			ui->cbExternalDeviceConnectors->setCurrentIndex(0);
		}
		ui->btnAdd->setEnabled(true);
	} else {
		ui->btnAdd->setEnabled(false);
	}
}

void GenericDataForm::on_btnAdd_clicked() {
	int extOutputIndex = ui->cbExternalDeviceConnectors->currentIndex();
	int intOutputIndex = ui->listThisDeviceConnectors->currentRow();
	if (extOutputIndex == -1 || intOutputIndex == -1) {
		return;
	}
	quint64 extUid = ui->cbDevices->currentData().value<quint64>();
	ModuleBuilder::Connector conn;
	conn.receiverId = this->isInputDirected ? this->deviceId : extUid;
	conn.receiverSlotIndex = this->isInputDirected ? deviceMappingThis[intOutputIndex] : deviceMappingExternal[extOutputIndex];
	conn.senderId = this->isInputDirected ? extUid : this->deviceId;
	conn.senderSlotIndex = this->isInputDirected ? deviceMappingExternal[extOutputIndex] : deviceMappingThis[intOutputIndex];

	this->addCallback(this->builder, conn);

	auto dev = builder->getModuleById(extUid);
	auto doun = externalDescriptorGetter(dev)[extOutputIndex]; //dev->getDataMapProvidedNames()[conn.senderSlotIndex];
	ui->lstConnections->addItem(dev->getName() + ": " + QString("%1").arg(extOutputIndex, 4, 16, QChar('0')).toUpper() + " : " + doun);

}

void GenericDataForm::on_btnRemove_clicked()
{
	auto listIndex = ui->lstConnections->currentRow();
	if (-1 != listIndex) {
		auto ins = thisConnectorsGetter(this->builder, this->deviceId);
		auto pinIndex = connectionsMapping.at(listIndex);
		auto thisIndex = this->deviceMappingThis[ui->listThisDeviceConnectors->currentRow()];
		if (ins.size()) {
			for (auto pin : ins) {
				bool cond = false;
				if (this->isInputDirected) {
					cond = (pin.senderId == pinIndex.first
							&& pin.senderSlotIndex == pinIndex.second
							&& pin.receiverSlotIndex == thisIndex);
				} else {
						cond = (pin.receiverId == pinIndex.first
								&& pin.receiverSlotIndex == pinIndex.second
								&& pin.senderSlotIndex == thisIndex);
				}
				if (cond) {
					removeCallback(this->builder, pin);
					break;
				}
			}
		}
	}
	this->on_listThisDeviceConnectors_currentRowChanged(ui->listThisDeviceConnectors->currentRow());
}
