#ifndef MODULENOTFOUNDEXCEPTION_H
#define MODULENOTFOUNDEXCEPTION_H

#include <QString>

class ModuleNotFoundException {
public:
	ModuleNotFoundException(QString reason = "") {
		this->reason = reason;
	}
	QString getReason() {
		return reason;
	}

private:
	QString reason;
};

#endif // MODULENOTFOUNDEXCEPTION_H
