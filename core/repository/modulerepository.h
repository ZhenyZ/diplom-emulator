#ifndef MODULEREPOSITORY_H
#define MODULEREPOSITORY_H

#include <moduledevice.h>

#include "modulenotfoundexception.h"

#include <QObject>

class ModuleRepository : public QObject
{
	Q_OBJECT
public:
	ModuleRepository(QObject* parent = nullptr);
	virtual ~ModuleRepository();

	void parseDir(const QString dirName);

	void registerModule(ModuleDevice* dev) {
		modules.insert(dev->getUID(), dev);
	}

	QList<ModuleDevice*> getModulesList() {
		return modules.values();
	}

	ModuleDevice * getByUid(const quint64 uid) throw(ModuleNotFoundException);

private:

	ModuleMap modules;
};

#endif // MODULEREPOSITORY_H
