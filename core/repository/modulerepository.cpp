#include "modulerepository.h"

#include <QDebug>
#include <QDir>
#include <QLibrary>
#include <QLibraryInfo>
#include <QApplication>
#include <QThread>

ModuleRepository::ModuleRepository(QObject * parent)
	: QObject(parent)
{

}

ModuleRepository::~ModuleRepository() {
	for (auto it = modules.begin(); it != modules.end(); ++it) {
		auto vm = it.value();
		auto vobj = dynamic_cast<QObject*>(vm);
		if (!vobj || (vobj->thread() == this->thread())) {
			delete it.value();
		}
	}
	modules.clear();
}

void ModuleRepository::parseDir(const QString dirName) {

	typedef ModuleDevice* (*ModuleCreateFunc)();

	QDir modulesDir(dirName);
	if (!modulesDir.exists()) {
		return;
	}

    modulesDir.setFilter(QDir::Files | QDir::Hidden);

	QFileInfoList filesList = modulesDir.entryInfoList();

	qDebug() << "Found " << filesList.size() << " files";

	for (auto it = filesList.begin(); it != filesList.end(); ++it) {
		auto filename = it->absoluteFilePath();
		qDebug() << "Parsing file " << filename;
		if (QLibrary::isLibrary(filename)) {
			QLibrary lib(filename);
			ModuleCreateFunc func = (ModuleCreateFunc)lib.resolve("createModule");
			if (func) {
				ModuleDevice* module = func();
				qDebug() << "Loading module " << module->getName();
				this->registerModule(module);
			} else {
				qDebug() << "File " << filename << " is not valid module library";
			}
		} else {
			qDebug() << "File " << filename << " is not a library";
		}
	}
}

ModuleDevice *ModuleRepository::getByUid(const quint64 uid) throw(ModuleNotFoundException) {
	auto it = modules.find(uid);
	if (it != modules.end()) {
		return it.value();
	}
	throw ModuleNotFoundException("UID not found");
}
