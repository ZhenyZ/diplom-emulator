#include "compiledsystem.h"

#include <moduledevice.h>
#include <QThread>
#include <QApplication>
#include <QDebug>

#include <loggable.h>

CompiledSystem::CompiledSystem(QObject *parent)
	: QObject(parent)
{
	tickLengthMs = 0;
	busTickCount = 0;
	busTickCountTotal = 0;
	isSuspended = true;
}

CompiledSystem::~CompiledSystem() {
}

void CompiledSystem::setModules(ModuleMap modules) {
	for (ModuleDevice* dev : modules) {
		Loggable* l = dynamic_cast<Loggable*>(dev);
		if (l != nullptr) {
			disconnect(l, SIGNAL(signalDebugOutput(const QMap<QString,QString>)));
		}
	}

	this->modules = modules;
	busModules.clear();
	for (ModuleDevice* dev : modules) {
		if (dev->isBusBound()) {
			busModules.push_back(dev);
		}
		Loggable* l = dynamic_cast<Loggable*>(dev);
		if (l != nullptr) {
			connect(l, SIGNAL(signalDebugOutput(const QMap<QString,QString>)), this, SLOT(slotDebug(const QMap<QString,QString>&)), Qt::QueuedConnection);
		}
	}

}

void CompiledSystem::setDebugEnabled(bool en)
{
	for (ModuleDevice* dev : this->modules) {
		Loggable* l = dynamic_cast<Loggable*>(dev);
		if (l != nullptr) {
			l->setDebugEnabled(en);
		}
	}
}

void CompiledSystem::processBus() {
	++busTickCount;
	++busTickCountTotal;
	int size = busModules.size();
	for (int i = 0; i < size; ++i) {
		auto dev = busModules[i];
		if (0 == (busTickCount % dev->getBusDivisor())) {
			dev->tick();
		}
	}
	if (busTickCount >= 0x100) {
		busTickCount = 0;
	}
}

void CompiledSystem::slotAllowDebug() {
	this->setDebugEnabled(true);
}

void CompiledSystem::slotDenyDebug() {
	this->setDebugEnabled(false);
}

void CompiledSystem::start() {
	busTickCount = 0;
	busTickCountTotal = 0;
	long cycles = 0;

	for (auto dev: this->modules) {
		dev->reloadConfig();
		dev->onBeforeStart();
	}

	while (!isStopped) {
		while (isSuspended && !isStepAllowed && !isStopped) {
			QApplication::processEvents();
			this->thread()->msleep(3);
		}
		isStepAllowed = false;
		if (isStopped) {
			break;
		}
		processBus();
		if (++cycles > 20) {
			QApplication::processEvents();
			cycles = 0;
		}
	}
}

void CompiledSystem::reset() {
	this->isStepAllowed = false;
	this->isSuspended = true;
	this->isStopped = true;
	this->isDebugAllowed = true;
	for (auto dev: modules) {
		dev->moduleReset();
	}
}

void CompiledSystem::slotDebug(const QMap<QString, QString> & val) {
	//qDebug() << "++ Compiled system : signal debug";
	emit signalDebug(val);
}

void CompiledSystem::slotSetModules(ModuleMap map) {
	setModules(map);
}

void CompiledSystem::slotReset() {
	reset();
}

void CompiledSystem::slotStart() {
	reset();
	for (auto dev: modules) {
		dev->createUi();
		dev->showUi();
	}
	this->isStopped = false;
	start();
}

void CompiledSystem::slotStop() {
	this->isStopped = true;
	for (auto dev: modules) {
//		dev->moduleReset();
		dev->hideUi();
		QApplication::processEvents();
		dev->moduleShutdown();
	}
}

void CompiledSystem::slotSuspend() {
	this->isSuspended = true;
}

void CompiledSystem::slotStep() {
	this->isStepAllowed = true;
}

void CompiledSystem::slotResume() {
	this->isSuspended = false;
}
