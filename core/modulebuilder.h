#ifndef MODULEBUILDER_H
#define MODULEBUILDER_H

#include <QObject>
#include <moduledevice.h>

class ModuleRepository;

class ModuleBuilder
{
public:

	struct Connector {
        qint64 senderSlotIndex;
        qint64 receiverSlotIndex;
        qint64 senderId;
        qint64 receiverId;
		bool operator == (const Connector &c2) {
			return (senderId == c2.senderId) && (senderSlotIndex == c2.senderSlotIndex) &&
					(receiverId == c2.receiverId) && (receiverSlotIndex == c2.receiverSlotIndex);
		}
	};

	void clear();

	void setRepository(ModuleRepository*r) {
		this->repository = r;
	}

	ModuleBuilder();
	~ModuleBuilder();

	bool saveToFile(const QString & filename);
	bool loadFromFile(const QString & filename);

	void addPortConnection(Connector & conn) { portConnectors.push_back(conn); }
	void removePortConnection(Connector & conn) { portConnectors.removeOne(conn); }

	void addSignalConnection(Connector & conn) { signalConnectors.push_back(conn); }
	void removeSignalConnection(Connector & conn) { signalConnectors.removeOne(conn); }

	void addDataConnection(Connector & conn) { dataConnectors.push_back(conn); }
	void removeDataConnection(Connector & conn) { dataConnectors.removeOne(conn); }

	void addModule(ModuleDevice* device) { devices.insert(device->getUID(), device); }
	void removeModule(ModuleDevice* device);
    ModuleDevice* getModuleById(quint64 id) { return devices[id]; }

	ModuleMap getModules() { return devices; }
	void setRootModule(int index) { rootModuleIndex = index; }

    QList<Connector> getPortInputConnectorsByDeviceId(quint64 id);
    QList<Connector> getPortOutputConnectorsByDeviceId(quint64 id);
    QList<Connector> getSignalInputConnectorsByDeviceId(quint64 id);
    QList<Connector> getSignalOutputConnectorsByDeviceId(quint64 id);
    QList<Connector> getDataProvidedConnectorsByDeviceId(quint64 id);
    QList<Connector> getDataConsumedConnectorsByDeviceId(quint64 id);

	ModuleDevice* build();

	void save();

	void setTickLengthMs(double msl);
	double getTickLengthMs();

private:
	QList<Connector> portConnectors;
	QList<Connector> signalConnectors;
	QList<Connector> dataConnectors;
	ModuleMap devices;
	int rootModuleIndex;

	ModuleRepository * repository;

	QString fileName;
	double tickLengthMs = 0.01;

};

#endif // MODULEBUILDER_H
