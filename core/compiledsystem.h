#ifndef COMPILEDSYSTEM_H
#define COMPILEDSYSTEM_H

#include <QObject>
#include <QMap>
#include <QString>

#include <moduledevice.h>

class CompiledSystem : public QObject
{
	Q_OBJECT
public:
	explicit CompiledSystem(QObject *parent = 0);
	~CompiledSystem();

	void setModules(ModuleMap lst);
	void setTickLength(double tickLengthMs) {
		this->tickLengthMs = tickLengthMs;
	}
	double getTickLength() const {
		return tickLengthMs;
	}

	void setDebugEnabled(bool en);

	const ModuleMap getModulesList() const {
		return modules;
	}

    quint64 getTickCount() const {
		return busTickCountTotal;
	}

private:

	ModuleMap modules;
	QList<ModuleDevice*> busModules;

	void processBus();

    quint64 busTickCount;
    quint64 busTickCountTotal;
	double tickLengthMs;

	void start();
	void reset();

	bool isSuspended;
	bool isStepAllowed;
	bool isStopped;
	bool isDebugAllowed;

signals:

	void signalDebug(const QMap<QString,QString> &val);

public slots:
	void slotDebug(const QMap<QString,QString> &val);

	void slotSetModules(ModuleMap);

	void slotReset();
	void slotStart();
	void slotStop();
	void slotStep();
	void slotResume();
	void slotSuspend();
	void slotAllowDebug();
	void slotDenyDebug();

};

#endif // COMPILEDSYSTEM_H
