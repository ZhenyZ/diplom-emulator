#include "modulebuilder.h"
#include "repository/modulerepository.h"

#include <fstream>
#include <QDataStream>
#include <QDebug>
#include <QFile>

void ModuleBuilder::clear()
{
	devices.clear();
	this->dataConnectors.clear();
	this->portConnectors.clear();
	this->signalConnectors.clear();
}

ModuleBuilder::ModuleBuilder() {
	rootModuleIndex = 0;
}

ModuleBuilder::~ModuleBuilder() {
	bool needSave = false;
	for (auto it = devices.begin(); it != devices.end(); ++it) {
		ModuleDevice * dev = repository->getByUid((*it)->getUID());
		DeviceConfig * config = dev->getConfig();
		if (config->isModified()) {
			needSave = true;
			break;
		}
	}
	if (needSave){
		save();
	}
}

void ModuleBuilder::save() {
	if (fileName.size()) {
		saveToFile(fileName);
	}
}

bool ModuleBuilder::saveToFile(const QString & filename) {
	this->fileName = filename;
	QFile file(filename);
	if (!file.open(QFile::WriteOnly)) {
		return false;
	}

	QDataStream stream(&file);

	stream << (qint64)devices.size();
	for (auto dev : devices) {
		stream << dev->getUID();
		stream << dev->getVersion();
		stream << dev->getUIDModifier();
		DeviceConfig* config = dev->getConfig();
		QByteArray configData = config->save();
		stream << configData;
	}
	stream << (qint64)portConnectors.size();
	for (Connector conn : portConnectors) {
		stream << conn.receiverId << conn.receiverSlotIndex
			 << conn.senderId << conn.senderSlotIndex;
	}
	stream << (qint64)signalConnectors.size();
	for (Connector conn : signalConnectors) {
		stream << conn.receiverId << conn.receiverSlotIndex
			 << conn.senderId << conn.senderSlotIndex;
	}
	stream << (qint64)dataConnectors.size();
	for (Connector conn : dataConnectors) {
		stream << conn.receiverId << conn.receiverSlotIndex
			 << conn.senderId << conn.senderSlotIndex;
	}
	file.close();
	return true;
}

bool ModuleBuilder::loadFromFile(const QString & filename) {
	this->fileName = filename;
	this->devices.clear();
	this->dataConnectors.clear();
	this->signalConnectors.clear();
	this->portConnectors.clear();

	QFile file(filename);
	if (!file.open(QFile::ReadOnly)) {
		return false;
	}

	QDataStream stream(&file);

	try {
		// Load devices from file and then fetch from repository
		qint64 size;
		stream >> size;

		quint64 devUid;
		quint64 devVersion;
		qint64 modifier;
		for (int i = 0; i < size; ++i) {
			stream >> devUid;
			stream >> devVersion;
			stream >> modifier;
			try {
				auto dev = repository->getByUid(devUid);
				devices.insert(dev->getUID(), dev);
				QByteArray configData;
				stream >> configData;
				dev->getConfig()->load(configData);
			} catch (ModuleNotFoundException ex) {
				return false;
			}
		}

		// Fetch port connectors
		stream >> size;
		for (qint64 i = 0; i < size; ++i) {
			Connector conn;
			stream >> conn.receiverId;
			stream >> conn.receiverSlotIndex;
			stream >> conn.senderId;
			stream >> conn.senderSlotIndex;
			portConnectors.push_back(conn);
		}
		// Fetch signal connectors
		stream >> size;
		for (qint64 i = 0; i < size; ++i) {
			Connector conn;
			stream >> conn.receiverId;
			stream >> conn.receiverSlotIndex;
			stream >> conn.senderId;
			stream >> conn.senderSlotIndex;
			signalConnectors.push_back(conn);
		}
		// Fetch data connectors
		stream >> size;
		for (qint64 i = 0; i < size; ++i) {
			Connector conn;
			stream >> conn.receiverId;
			stream >> conn.receiverSlotIndex;
			stream >> conn.senderId;
			stream >> conn.senderSlotIndex;
			dataConnectors.push_back(conn);
		}
	} catch (...) {
		return false;
	}

	return true;
}

void ModuleBuilder::removeModule(ModuleDevice * device) {
	//int index = devices.indexOf(device);
	auto uid = device->getUID();
	devices.remove(device->getUID());
	for (auto it = portConnectors.begin(); it != portConnectors.end();) {
		auto conn = *it;
		if (conn.receiverId == uid || conn.senderId == uid) {
			it = portConnectors.erase(it);
		} else {
			++it;
		}
	}
}

QList<ModuleBuilder::Connector> ModuleBuilder::getPortInputConnectorsByDeviceId(quint64 id) {
	QList<Connector> lst;
	for (Connector conn: this->portConnectors) {
		if (conn.receiverId == id) {
			lst.push_back(conn);
		}
	}
	return lst;
}

QList<ModuleBuilder::Connector> ModuleBuilder::getPortOutputConnectorsByDeviceId(quint64 id) {
	QList<Connector> lst;
	for (Connector conn: this->portConnectors) {
		if (conn.senderId == id) {
			lst.push_back(conn);
		}
	}
	return lst;
}

QList<ModuleBuilder::Connector> ModuleBuilder::getSignalOutputConnectorsByDeviceId(quint64 id) {
	QList<Connector> lst;
	for (Connector conn: this->signalConnectors) {
		if (conn.senderId == id) {
			lst.push_back(conn);
		}
	}
	return lst;
}

QList<ModuleBuilder::Connector> ModuleBuilder::getSignalInputConnectorsByDeviceId(quint64 id) {
	QList<Connector> lst;
	for (Connector conn: this->signalConnectors) {
		if (conn.receiverId == id) {
			lst.push_back(conn);
		}
	}
	return lst;
}

QList<ModuleBuilder::Connector> ModuleBuilder::getDataProvidedConnectorsByDeviceId(quint64 id) {
	QList<Connector> lst;
	for (Connector conn: this->dataConnectors) {
		if (conn.senderId == id) {
			lst.push_back(conn);
		}
	}
	return lst;
}

QList<ModuleBuilder::Connector> ModuleBuilder::getDataConsumedConnectorsByDeviceId(quint64 id) {
	QList<Connector> lst;
	for (Connector conn: this->dataConnectors) {
		if (conn.receiverId == id) {
			lst.push_back(conn);
		}
	}
	return lst;
}

ModuleDevice *ModuleBuilder::build() {
	// Connect ports
	for (Connector conn : portConnectors) {
		ModuleDevice::CompiledOutput & outs = devices[conn.senderId]->getPortOutput(conn.senderSlotIndex);
		ModuleDevice::CompiledInput & ins = devices[conn.receiverId]->getPortInput(conn.receiverSlotIndex);
		outs.target = ins.thisTarget;
		ins.source = outs.thisSource;
		outs.callCount = 0;
		ins.callCount = 0;
	}
	for (Connector conn : signalConnectors) {
		ModuleDevice::CompiledSignal & outs = devices[conn.senderId]->getSignalEmmitter(conn.senderSlotIndex);
		ModuleDevice::CompiledSignal & ins = devices[conn.receiverId]->getSignalReceiver(conn.receiverSlotIndex);
		outs.callback = ins.callback;
		outs.callCount = 0;
		ins.callCount = 0;
	}
	for (Connector conn : dataConnectors) {
		auto outs = devices[conn.senderId]->getDataBlockProvided(conn.senderSlotIndex);
		auto ins = devices[conn.receiverId]->getDataBlockConsumed(conn.receiverSlotIndex);
		ModuleDevice::ModuleMemPtr* sourcePtr = outs.first;
		ModuleDevice::ModuleMemPtr* destPtr = ins.first;
		qword** sourceSize = outs.second;
		qword** destSize = ins.second;
		*destPtr = *sourcePtr;
		**destPtr = **sourcePtr;
		*destSize = *sourceSize;
		**destSize = **sourceSize;
	}
	return nullptr;
}

void ModuleBuilder::setTickLengthMs(double msl) {
	this->tickLengthMs = msl;
}

double ModuleBuilder::getTickLengthMs() {
	return tickLengthMs;
}
