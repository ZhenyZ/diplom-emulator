#ifndef DEVICECONFIG_H
#define DEVICECONFIG_H

#include <QMap>
#include <QVariant>
#include <QByteArray>
#include <constants.h>

#include "deviceslib_global.h"

class DEVICESLIBSHARED_EXPORT DeviceConfig
{
public:
	DeviceConfig();

	QByteArray save();
	void load(const QByteArray & data);
	bool isModified();
	bool hasProperty(const QString & name);

	bool getBoolProperty(const QString & name);
	QString getStringProperty(const QString & name);
	int getIntProperty(const QString & name);
	qword getQWordProperty(const QString & name);

	void setProperty(const QString & name, bool value);
	void setProperty(const QString & name, QString value);
	void setProperty(const QString & name, int value);
	void setProperty(const QString & name, qword value);
	void removeProperty(const QString & name);

	const QMap<QString, QVariant> & getPropertiesMap() const;

private:
	QMap<QString, QVariant> properties;
	bool modified;
};

#endif // DEVICECONFIG_H
