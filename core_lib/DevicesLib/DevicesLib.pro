#-------------------------------------------------
#
# Project created by QtCreator 2017-05-13T20:42:56
#
#-------------------------------------------------

TARGET = EMUESDevicesLib
TEMPLATE = lib

QT += core gui widgets
QT -= opengl
CONFIG += c++14

DEFINES += DEVICESLIB_LIBRARY

SOURCES += \
    constants.cpp \
    deviceconfig.cpp \
    moduledevice.cpp \
    ui/moduledevicegui.cpp

HEADERS +=\
        deviceslib_global.h \
    constants.h \
    deviceconfig.h \
    moduledevice.h \
    ui/moduledevicegui.h

unix {
    target.path = /usr/local/lib/
    INSTALLS += target
}
