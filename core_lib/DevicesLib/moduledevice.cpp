#include "moduledevice.h"

#include <QDebug>
#include <QCryptographicHash>
#include <QByteArray>
#include <QDataStream>

ModuleDevice::ModuleDevice(const QString name, const QString publisher, quint64 version) {
	this->name = QString(name);
	this->publisher = QString(publisher);
	this->version = version;
	this->config = new DeviceConfig();
	this->uidModifier = 0;
}

ModuleDevice::~ModuleDevice() {
	delete config;
}

bool ModuleDevice::isBusBound() {
	return config->hasProperty("module_isBusBound") ? config->getBoolProperty("module_isBusBound") : false;
}

void ModuleDevice::createUi() {

}

void ModuleDevice::showUi() {

}

void ModuleDevice::hideUi() {

}

long ModuleDevice::getBusDivisor() {
	return busDivisor;
}

void ModuleDevice::reloadConfig() {
	busDivisor = config->hasProperty("module_tickInterval") ? config->getIntProperty("module_tickInterval") : 1;
}

ModuleDevice::InputMap & ModuleDevice::getInputs() {
	return inputFunctions;
}

ModuleDevice::DescriptorMap & ModuleDevice::getInputNames() {
	return inputFunctionNames;
}

ModuleDevice::OutputMap & ModuleDevice::getOutputs() {
	return outputFunctions;
}

ModuleDevice::DescriptorMap & ModuleDevice::getOutputNames() {
	return outputFunctionNames;
}

ModuleDevice::SignalMap & ModuleDevice::getSignalSources() {
	return signalEmitFunctions;
}

ModuleDevice::DescriptorMap & ModuleDevice::getSignalSourcesNames() {
	return signalEmitFunctionNames;
}

ModuleDevice::SignalMap & ModuleDevice::getSignalReceivers() {
	return signalReceiveFunctions;
}

ModuleDevice::DescriptorMap & ModuleDevice::getSignalReceiversNames() {
	return signalReceiveFunctionNames;
}

ModuleDevice::CompiledSignal & ModuleDevice::getSignalReceiver(quint64 index) {
	return signalReceiveFunctions[index];
}

ModuleDevice::CompiledSignal & ModuleDevice::getSignalEmmitter(quint64 index) {
	return signalEmitFunctions[index];
}

ModuleDevice::CompiledInput & ModuleDevice::getPortInput(quint64 index) {
	return inputFunctions[index];
}

ModuleDevice::CompiledOutput & ModuleDevice::getPortOutput(quint64 index) {
	return outputFunctions[index];
}

void ModuleDevice::registerEmitSignalFunction(quint64 index, const QString & name) {
	CompiledSignal s;
	s.callback = nullptr;
	s.callCount = 0;
	signalEmitFunctions.insert(index, s);
	signalEmitFunctionNames.insert(index, name);
}

void ModuleDevice::registerReceiveSignalFunction(quint64 index, const QString & name, ModuleDevice::SignalCallback f) {
	CompiledSignal s;
	s.callback = f;
	s.callCount = 0;
	signalReceiveFunctions.insert(index, s);
	signalReceiveFunctionNames.insert(index,name);
}

void ModuleDevice::registerOutput(quint64 index, const QString & name, ModuleDevice::OutputCallback f) {
	CompiledOutput ci;
	ci.thisSource = f;
	ci.target = nullptr;
	ci.callCount = 0;
	outputFunctions.insert(index,ci);
	outputFunctionNames.insert(index,name);
}

void ModuleDevice::registerInput(quint64 index, const QString & name, ModuleDevice::InputCallback f) {
	CompiledInput op;
	op.thisTarget = f;
	op.source = nullptr;
	op.callCount = 0;
	inputFunctions.insert(index,op);
	inputFunctionNames.insert(index,name);
}

void ModuleDevice::callSignalEmitByIndex(quint64 index) {
	CompiledSignal & semi = signalEmitFunctions[index];
	if (semi.callback) {
		++semi.callCount;
		semi.callback();
	}
}

void ModuleDevice::callSignalReceiveByIndex(quint64 index) {
	signalReceiveFunctions[index].callback();
}

void ModuleDevice::callInputByIndex(quint64 index, int size) {
	CompiledInput & ci = inputFunctions[index];
	if (ci.source) {
		++ci.callCount;
		ci.thisTarget(ci.source(size), size);
	}
}

void ModuleDevice::callOutputByIndex(quint64 index, int size) {
	CompiledOutput & out = outputFunctions[index];
	if (out.target) {
		++out.callCount;
		out.target(out.thisSource(size), size);
	}
}

qint64 ModuleDevice::getHash(const QString &str)
{
	QByteArray hash = QCryptographicHash::hash(
		QByteArray::fromRawData((const char*)str.utf16(), str.length()*2),
		QCryptographicHash::Md5
	);
	QDataStream stream(&hash, QIODevice::ReadOnly);
	qint64 a, b;
	stream >> a >> b;
	return a ^ b;
}

void ModuleDevice::registerDataMapProvider(quint64 index, const QString & name, DataMappingBlock blk) {
	dataMapProvided.emplace(index, blk);
	dataMapProvidedNames.insert(index, name);
}

void ModuleDevice::registerDataMapConsumer(quint64 index, const QString & name, DataMappingBlock blk) {
	dataMapConsumed.emplace(index, blk);
	dataMapConsumedNames.insert(index, name);
}

ModuleDevice::DataMappingBlock & ModuleDevice::getDataBlockProvided(quint64 index) {
	return dataMapProvided.at(index);
}

ModuleDevice::DataMappingBlock & ModuleDevice::getDataBlockConsumed(quint64 index) {
	return dataMapConsumed.at(index);
}

quint64 ModuleDevice::getUID() {
	qint64 result;
	if (uidModifier != 0) {
		result = getHash(name + publisher + QString::number(uidModifier));;
	} else {
		result = getHash(name + publisher);
	}
	return result;
}

void ModuleDevice::addUidModifier(qint64 modifier) {
	this->uidModifier = modifier;
}

qint64 ModuleDevice::getUIDModifier() {
	return uidModifier;
}

ModuleDevice::DataMapConsumed & ModuleDevice::getDataMapConsumed() {
	return dataMapConsumed;
}

ModuleDevice::DescriptorMap & ModuleDevice::getDataMapConsumedNames() {
	return dataMapConsumedNames;
}

ModuleDevice::DataMapProvided & ModuleDevice::getDataMapProvided() {
	return dataMapProvided;
}

ModuleDevice::DescriptorMap & ModuleDevice::getDataMapProvidedNames() {
	return dataMapProvidedNames;
}

DeviceConfig* ModuleDevice::getConfig() {
	return config;
}

void ModuleDevice::onBeforeStart() {

}

const QString & ModuleDevice::getName() {
	return name;
}

const QString & ModuleDevice::getPublisher() {
	return publisher;
}

quint64 ModuleDevice::getVersion() {
	return version;
}

