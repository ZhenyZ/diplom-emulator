#include "deviceconfig.h"

#include <QDataStream>
#include <QDebug>

DeviceConfig::DeviceConfig() {
	modified = false;
}

QByteArray DeviceConfig::save() {
	QByteArray result;
	QDataStream stream(&result, QIODevice::OpenModeFlag::WriteOnly);

	stream << (qint64)properties.size();

	for (auto it = properties.begin(); it != properties.end(); ++it) {
		stream << it.key();
		stream << it.value();
	}

	modified = false;
	return result;
}

void DeviceConfig::load(const QByteArray & data) {
	properties.clear();
	QDataStream stream(data);

	qint64 size;
	stream >> size;

	for (auto i = 0; i < size; ++i) {
		QString key;
		QVariant value;
		stream >> key;
		stream >> value;
		properties.insert(key, value);
	}
	modified = false;
}

bool DeviceConfig::isModified() {
	return modified;
}

bool DeviceConfig::hasProperty(const QString & name) {
	return (properties.find(name) != properties.end());
}

bool DeviceConfig::getBoolProperty(const QString & name) {
	return properties[name].toBool();
}

QString DeviceConfig::getStringProperty(const QString & name) {
	return properties[name].toString();
}

int DeviceConfig::getIntProperty(const QString & name) {
	return properties[name].toInt();
}

qword DeviceConfig::getQWordProperty(const QString & name) {
	return (qword)properties[name].toUInt();
}

void DeviceConfig::setProperty(const QString & name, bool value) {
	modified = true;
	properties[name] = QVariant((bool)value);
}

void DeviceConfig::setProperty(const QString & name, QString value) {
	modified = true;
	properties[name] = QVariant(value);
}

void DeviceConfig::setProperty(const QString & name, int value) {
	modified = true;
	properties[name] = QVariant((int)value);
}

void DeviceConfig::setProperty(const QString & name, qword value) {
	modified = true;
	properties[name] = QVariant((uint)value);
}

void DeviceConfig::removeProperty(const QString & name) {
	properties.remove(name);
}

const QMap<QString, QVariant> &DeviceConfig::getPropertiesMap() const {
	return this->properties;
}
