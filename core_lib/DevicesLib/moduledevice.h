#ifndef MODULEDEVICE_H
#define MODULEDEVICE_H

#include <memory>
#include <functional>

#include <QList>
#include <QMap>

#include <constants.h>
#include "deviceconfig.h"
#include "deviceslib_global.h"

class DEVICESLIBSHARED_EXPORT ModuleDevice
{
public:
	typedef byte** ModuleMemPtr;
	typedef std::pair<ModuleMemPtr*, qword**> DataMappingBlock;	// Direct device mapping

	typedef std::function<void()> SignalCallback;
	typedef std::function<dword(int lengthInBytes)> OutputCallback;
	typedef std::function<void(dword, int lengthInBytes)> InputCallback;

	typedef struct {
		OutputCallback source;		// External device getter
		InputCallback thisTarget;	// This device setter
		quint64 callCount;
	} CompiledInput;

	typedef struct {
		OutputCallback thisSource;	// This device getter
		InputCallback target;		// External device setter
		quint64 callCount;
	} CompiledOutput;

	typedef struct {
		SignalCallback callback;	// Signal receive slot
		quint64 callCount;
	} CompiledSignal;

	typedef QMap<quint64, CompiledInput> InputMap;
	typedef QMap<quint64, CompiledOutput> OutputMap;
	typedef QMap<quint64, CompiledSignal> SignalMap;
	typedef QMap<quint64, QString> DescriptorMap;
	typedef std::map<quint64, DataMappingBlock> DataMapProvided;
	typedef std::map<quint64, DataMappingBlock> DataMapConsumed;

	virtual ~ModuleDevice();
	DeviceConfig * getConfig();

	long getBusDivisor();

	virtual void reloadConfig();

	InputMap & getInputs();
	DescriptorMap & getInputNames();
	OutputMap & getOutputs();
	DescriptorMap & getOutputNames();
	SignalMap & getSignalSources();
	DescriptorMap & getSignalSourcesNames();
	SignalMap & getSignalReceivers();
	DescriptorMap & getSignalReceiversNames();
	DataMapConsumed & getDataMapConsumed();
	DescriptorMap & getDataMapConsumedNames();
	DataMapProvided & getDataMapProvided();
	DescriptorMap & getDataMapProvidedNames();

	CompiledSignal & getSignalReceiver(quint64 index);
	CompiledSignal & getSignalEmmitter(quint64 index);
	CompiledInput & getPortInput(quint64 index);
	CompiledOutput & getPortOutput(quint64 index);
	DataMappingBlock & getDataBlockProvided(quint64 index);
	DataMappingBlock & getDataBlockConsumed(quint64 index);

	bool isBusBound();
	virtual void tick() = 0;
	virtual void onBeforeStart();

	const QString & getName();
	const QString & getPublisher();
	quint64 getVersion();

	/**
	 * @brief Device must reset to initial state after this routine is called
	 */
	virtual void moduleReset() = 0;

	/**
	 * @brief Prepare for shutdown; free occupied memory, close ui, etc
	 */
	virtual void moduleShutdown() = 0;

	virtual void createUi();
	virtual void showUi();
	virtual void hideUi();

	quint64 getUID();
	void addUidModifier(qint64 modifier);
	qint64 getUIDModifier();

protected:

	ModuleDevice(const QString name, const QString publisher, quint64 version);

	void registerEmitSignalFunction(quint64 index, const QString & name);
	void registerReceiveSignalFunction(quint64 index, const QString & name, SignalCallback f);
	void registerOutput(quint64 index, const QString & name, OutputCallback f);
	void registerInput(quint64 index, const QString & name, InputCallback f);
	void registerDataMapProvider(quint64 index, const QString & name, DataMappingBlock blk);
	void registerDataMapConsumer(quint64 index, const QString & name, DataMappingBlock blk);

	void callSignalEmitByIndex(quint64 index);
	void callSignalReceiveByIndex(quint64 index);
	void callInputByIndex(quint64 index, int size);
	void callOutputByIndex(quint64 index, int size);

private:
	QString name;
	QString publisher;
	quint64 version;
	qint64 uidModifier;

	long busDivisor;
	DeviceConfig * config = nullptr;

	SignalMap signalEmitFunctions;
	DescriptorMap signalEmitFunctionNames;
	SignalMap signalReceiveFunctions;
	DescriptorMap signalReceiveFunctionNames;
	InputMap inputFunctions;
	DescriptorMap inputFunctionNames;
	OutputMap outputFunctions;
	DescriptorMap outputFunctionNames;
	DataMapConsumed dataMapConsumed;
	DescriptorMap dataMapConsumedNames;
	DataMapProvided dataMapProvided;
	DescriptorMap dataMapProvidedNames;

	qint64 getHash(const QString & str);

};

Q_DECLARE_METATYPE(ModuleDevice*)

typedef QMap<quint64, ModuleDevice*> ModuleMap;

#endif // MODULEDEVICE_H
