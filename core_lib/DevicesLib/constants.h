#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <QObject>

typedef quint8 byte;
typedef quint16 word;
typedef quint32 dword;
typedef quint64 qword;

#include "deviceslib_global.h"

class DEVICESLIBSHARED_EXPORT Constants
{
public:

	static dword
		// precompiled powers of 2
		pow2[32];

};


#endif // CONSTANTS_H
