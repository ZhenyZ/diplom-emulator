#ifndef MODULEDEVICEWITHGUI_H
#define MODULEDEVICEWITHGUI_H

class QWidget;
class QTextEdit;
class QTimer;
class ModuleDevice;
class QFile;

#include <QObject>
#include <QTextStream>
#include <deviceslib_global.h>

class DEVICESLIBSHARED_EXPORT ModuleDeviceGui : public QObject
{
	Q_OBJECT
public:
	ModuleDeviceGui(ModuleDevice* device, QObject * parent = nullptr);
	virtual ~ModuleDeviceGui();
	void setTitle(QString title);
	void setFilename(QString filename);

protected slots:
	void slotAddListWidget();
	void slotAddItemToListWidget(const QString &);
	void slotShowGui();
	void slotHideGui();
	void slotInit();
	void slotUpdateList();

private:
	QWidget* widget = nullptr;
	QTextEdit* listWidget = nullptr;
	QList<QString> storage;
	QList<QString> buffer;
	QTimer * updateTimer = nullptr;
	QString title;
	ModuleDevice* device = nullptr;
	QFile * file = nullptr;
	QTextStream stream;
};

#endif // MODULEDEVICEWITHGUI_H
