#include "moduledevicegui.h"

#include <QWidget>
#include <QListWidget>
#include <QBoxLayout>
#include <QApplication>
#include <QTextEdit>
#include <QThread>
#include <QTimer>
#include <QFile>

#include <QScrollBar>

#include <QFontDatabase>

#include <moduledevice.h>

ModuleDeviceGui::ModuleDeviceGui(ModuleDevice* dev, QObject* parent)
	:QObject(parent),
	device(dev)
{
	this->title = QString(dev->getName());
	this->moveToThread(QApplication::instance()->thread());
}

ModuleDeviceGui::~ModuleDeviceGui() {
	if (this->file && this->file->isOpen()) {
		this->file->flush();
		this->file->close();
	}
}

void ModuleDeviceGui::setTitle(QString title) {
	this->title = title;
	if (widget) {
		widget->setWindowTitle(title);
	}
}

void ModuleDeviceGui::setFilename(QString filename) {
	if (this->file && this->file->isOpen()) {
		this->file->close();
		delete this->file;
	}
	this->file = new QFile(filename);
	this->file->open(QIODevice::WriteOnly);
	this->stream.setDevice(this->file);
}

void ModuleDeviceGui::slotInit() {
	widget = new QWidget();
	widget->setWindowTitle(title);
	storage.reserve(2200);
	buffer.reserve(2000);
	updateTimer = new QTimer();
	updateTimer->setSingleShot(false);
	updateTimer->setInterval(50);
	connect(updateTimer, SIGNAL(timeout()), this, SLOT(slotUpdateList()));
	updateTimer->start();
}

void ModuleDeviceGui::slotAddListWidget() {
	QBoxLayout * vl = new QBoxLayout(QBoxLayout::Direction::TopToBottom, widget);
	listWidget = new QTextEdit();
	listWidget->setFont(QFontDatabase::systemFont(QFontDatabase::FixedFont));
	vl->addWidget(listWidget);
//	listWidget->setAutoScroll(true);
}

void ModuleDeviceGui::slotUpdateList() {
	bool writeToFile = this->file && this->file->isOpen();

	QList<QString> tempList;
	tempList.swap(buffer);
	storage.append(tempList);
	this->stream.flush();

//	if (listWidget && storage.size()) {
//		QString result = "";
////		for (int i = 0; i < 2200 && storage.size(); ++i) {
//		while (storage.size()) {
//			auto str = storage.takeFirst();
//			result += str;
//			if (!str.endsWith('\n')) {
//				result += "\n";
//			}
//		}
//		//listWidget->append(result);
//		if (writeToFile) {
//			this->stream << result;
//			this->stream.flush();
//		}
//		auto sb = listWidget->verticalScrollBar();
//		sb->setValue(sb->maximum());
//	}

}

void ModuleDeviceGui::slotAddItemToListWidget(const QString & str) {
	if (this->file) {
		this->stream << str;
		if (!str.endsWith('\n')) {
			this->stream << '\n';
		}
	}

	if (listWidget) {
		listWidget->append(str);
		if (!str.endsWith('\n') && !str.endsWith('\r')) {
			listWidget->append("\n");
		}
	}

	//	buffer.push_back(str);
}

void ModuleDeviceGui::slotShowGui() {
	widget->show();
}

void ModuleDeviceGui::slotHideGui() {
	widget->hide();
}


