#ifndef EMU_LOGGABLE_H
#define EMU_LOGGABLE_H

#include <QObject>
#include <QMap>
#include <QTextStream>
#include "debuglib_global.h"

class QFile;

class DEBUGLIBSHARED_EXPORT Loggable : public QObject
{
	Q_OBJECT
public:
	explicit Loggable(const QString & name, QObject * parent = 0);
	virtual ~Loggable();
	void setDebugEnabled(bool en = true);
	const QString & getName();
	void setFileName(const QString & fname);

protected:
	void doDebug(QMap<QString,QString> &param);
	bool isDebugEnabled();
	virtual void debug(const QString & str) = 0;

private:
	bool debugEnabled_;
	QString name_;
	QString filename;
	QFile * file = nullptr;
	QTextStream stream;

signals:
	void signalDebugOutput(const QMap<QString, QString> & params);
};

#endif // LOGGER_H
