#ifndef EMU_LOGGER_H
#define EMU_LOGGER_H

#include <QObject>
#include <QMap>
#include <QTextStream>

#include "debuglib_global.h"

class QFile;

class DEBUGLIBSHARED_EXPORT Logger : public QObject
{
	Q_OBJECT
public:
	explicit Logger( const QString fileName, QObject *parent = 0 );

protected:
	QFile *logFile;
	QTextStream streamFile;

signals:

public slots:
	virtual void slotDebugOutput( QMap<QString, QString> params ) = 0;
};

#endif // LOGGER_H
