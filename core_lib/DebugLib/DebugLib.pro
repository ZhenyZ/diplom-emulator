#-------------------------------------------------
#
# Project created by QtCreator 2017-05-13T20:45:32
#
#-------------------------------------------------

QT       -= gui

TARGET = EMUESDebugLib
TEMPLATE = lib

DEFINES += DEBUGLIB_LIBRARY

SOURCES += \
    loggable.cpp \
    logger.cpp

HEADERS +=\
        debuglib_global.h \
    loggable.h \
    logger.h

unix {
    target.path = /usr/local/lib/
    INSTALLS += target
}
