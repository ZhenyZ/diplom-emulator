#include <QFile>
#include <QTextStream>

#include "logger.h"

Logger::Logger( const QString fileName, QObject *parent ) : QObject( parent )
{
	logFile = new QFile();
	logFile->setFileName( fileName );
	logFile->open( QIODevice::WriteOnly );

	streamFile.setDevice( logFile );
}

