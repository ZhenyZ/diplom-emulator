#include <QFile>
#include <QTextStream>

#include "loggable.h"
#include <QDebug>

Loggable::Loggable( const QString & name, QObject * parent )
	: QObject(parent),
	  name_( name )
{
	this->file = new QFile("./debug.log");
	this->file->open(QIODevice::Append);
	this->stream.setDevice(this->file);
}

Loggable::~Loggable()
{
	if (this->file && this->file->isOpen()) {
		this->file->flush();
		this->file->close();
		this->file = nullptr;
	}
}

void Loggable::doDebug( QMap<QString, QString> & params ) {
	params.insert( "name", name_ );
	if (this->file) {
		if (params.value("type") == "device") {
//			this->stream << name_ << ": ";
		} else {
			auto str = params.value("message");
			this->stream << str;
			if (!str.endsWith('\n')) {
				this->stream << '\n';
			}
			this->stream.flush();

		}
	}
//	qDebug() << "Loggable emit sigDebug with " << params;
	emit signalDebugOutput( params );
}

void Loggable::setDebugEnabled(bool en) {
	debugEnabled_ = en;
}

const QString & Loggable::getName() {
	return name_;
}

void Loggable::setFileName(const QString & fname)
{
	this->filename = fname;
	if (this->file && this->file->isOpen()) {
		//this->file->flush();
		this->file->close();
		this->file = nullptr;
	}
	this->file = new QFile(filename);
	this->file->open(QIODevice::WriteOnly);
	this->stream.setDevice(this->file);
}

bool Loggable::isDebugEnabled() {
	return debugEnabled_;
}

