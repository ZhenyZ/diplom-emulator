#include "modulepic.h"

#include <QDebug>

#define RegisterInput(Port,Name,Func) registerInput(Port, Name, std::bind(Func, this, std::placeholders::_1, std::placeholders::_2));
#define RegisterOutput(Port,Name,Func) registerOutput(Port, Name, std::bind(Func, this, std::placeholders::_1));
#define RegisterSignal(Index,Name,Func) registerReceiveSignalFunction(Index, Name, std::bind(Func, this, Index));


ModulePIC::ModulePIC(QObject * parent)
	: Loggable("PIC", parent),
	ModuleDevice("8259 Interrupt Controller", "Eugene Shestakov", 12)

{
	init();
	initIo();
	cyclesSinceSent = 0;
}

ModulePIC::~ModulePIC() { }

void ModulePIC::tick() {
//	if (pendingInterrupts.size()) {
//		++cyclesSinceSent;
//		if (!pendingIntSent || cyclesSinceSent > 20) {
//			cyclesSinceSent = 0;
//			callSignalEmitByIndex(1);
//		} else {
//			checkInterrupts();
//		}
//	} else {
//		checkInterrupts();
//	}
	checkInterrupts();
}

void ModulePIC::debug(const QString & str)
{
	QMap<QString, QString> params;
	params.insert("type", "device");
	params.insert("message", str);
	doDebug(params);
//	qDebug() << "PIC: " << str;
}

/**
 * @brief Performs the initialization stage
 * @short Reset all pending interrupts and set masks and priorities to default
 */
void ModulePIC::init() {
	IRR = 0;
	ISR = 0;
	IMR = 0;
	InitMode = 0;
	ICW4go = 0;
	SNGL = 1;
	LTIM = 0;
	Vect0 = 0;
	AEOI = 0;
	ICWIndex = 0;
	pendingIntSent = false;
	interruptLevel = 0;
	interruptIndex = 0;
	for (int i = 0; i < 8; ++i)
		Prior[i] = 7-i;
	Poll = false;
}

void ModulePIC::initIo() {
	RegisterInput(0, "Set port 1", &ModulePIC::io_writePort1);
	RegisterInput(1, "Set port 2", &ModulePIC::io_writePort2);

	RegisterOutput(0, "Get status", &ModulePIC::io_read);
	RegisterOutput(1, "Get IMR", &ModulePIC::io_readIMR);
	RegisterOutput(2, "Get Interrupt level", &ModulePIC::io_getLevel);

	for (byte i = 0; i < 8; ++i) {
		RegisterSignal(i,"IR"+QString::number(i), &ModulePIC::signalLevelled);
	}
	registerEmitSignalFunction(SIGNAL_EMIT_RUN, "Run interrupt signal");
	registerEmitSignalFunction(SIGNAL_PENDING, "Pending interrupt signal");

	registerReceiveSignalFunction(8, "INT# ACK", std::bind(&ModulePIC::interruptAck, this));
	registerReceiveSignalFunction(9, "Reset IRQ1", std::bind(&ModulePIC::io_resetIrq1, this));
}

dword ModulePIC::io_getLevel(int) {
	return interruptIndex;
}

void ModulePIC::interrupt(int level) {
	bool append = true;
//	for (int i = 0; i < pendingInterrupts.size(); ++i) {
//		if (pendingInterrupts[i] == level) {
//			append = false;
//			break;
//		}
//	}
//	if (append) {
//		pendingInterrupts.push_back(level);
//	}
	if (level != 0) {
		debug("Sending PENDINT " + QString::number(level, 16) +"h");
	}
	pendingIntSent = true;
	callSignalEmitByIndex(SIGNAL_PENDING);
}

void ModulePIC::io_writePort1(dword value, int sz) {
	debug("Set port1 = " + QString::number(value, 16));
	if ((value & 0x10) == 0x10) {
		io_ICW(value, sz);
	} else {
		if ((value & 0x8) == 0) {
			io_OCW2(value, sz);
		} else {
			io_OCW3(value, sz);
		}
	}
}

void ModulePIC::io_writePort2(dword value, int sz) {
	debug("Set port2 = " + QString::number(value, 16));
	if (IsInit()) {
		io_ICW(value, sz);
	} else {
		io_OCW1(value, sz);
	}
}

/**
 * @brief Out the Initialization Command Word
 * @param Value
 */
void ModulePIC::io_ICW(dword Value, int) {
	++ICWIndex;
	debug("ICW " + QString::number(ICWIndex));
	switch (ICWIndex) {

		case 1:						//	ICW1: Sequense start
			ICW4go = (Value & 1);	//	ICW4 is present
			SNGL = (Value & 2);		//	Single PIC controller in system
			LTIM = (Value & 8);		//
			IMR = 0;
			if (!ICW4go) {
				AEOI = 0;
			}
		break;

		case 2:						//	ICW2
			Vect0 = Value;			//	Set vector offset
			if (SNGL) {
				if (!ICW4go)
					ICWIndex = 0;
				else
					ICWIndex = 3;
			}
		break;

		case 3:						//	ICW3: Not supported

		break;

		case 4:						//	ICW4: Sequense end
			AEOI = (Value & 2);		//	AutoEOI
			ICWIndex = 0;			//	Init sequence end
		break;
	}
}

/**
 * @brief Check for any allowed pending interrupts
 * @short Transfers interrupt level to chip if there is one
 */
void ModulePIC::checkInterrupts() {
	if (pendingIntSent) {
		return;
	}
	bool hasInterrupts = (IRR & (~IMR) & (~ISR));
	if (!hasInterrupts) {
		if (IRR) {
//			debug("CheckInt: check0 false: IRR = " + QString::number(IRR, 16) + "; ISR = " + QString::number(ISR, 16));
		}
		return;
	}

	int Index = -1, Pr = -1;
	for (int i = 0; i < 8; ++i) {
		if ((IRR & (~ISR) & Constants::pow2[i]) && (Prior[i] > Pr)) {
			Index = i;
			Pr = Prior[i];
		}
	}

	if (Index == -1) {
//		debug("CheckInt: index = -1; return");
		return;
	}

	if (Index != 0) {
		debug("PIC INT " + QString::number(Index));
	}

	int level = Index + Vect0;
	interruptIndex = level;
	interruptLevel = Index;

//	if (chipset->CPU->eflags.eflags.flag32 & FLAG_IF) {
//		chipset->CPU->interruptExternal(level);
//	chipset->signalFromPIC(level);

	interrupt(Index);

}

void ModulePIC::moduleReset() {
	init();
}

void ModulePIC::moduleShutdown()
{

}

bool ModulePIC::IsInit() { return (ICWIndex != 0); }

void ModulePIC::interruptAck() {
	debug("AckInterrupt");
	pendingIntSent = false;
//	if (!pendingInterrupts.size())  {
//		IRR = 0;
//		ISR = 0;
//		return;
//	}
	debug("Running interrupt " + QString::number(interruptIndex, 16)+"h");
	callOutputByIndex(2, 4);
	callSignalEmitByIndex(SIGNAL_EMIT_RUN);
	IRR &= ~Constants::pow2[interruptLevel];
	if (!AEOI) {
		ISR |= Constants::pow2[interruptLevel];
	}
}

/**
 * @brief Set OCW1 word
 * @param Value
 */
void ModulePIC::io_OCW1(dword Value, int) {
	debug("OCW 1");
	IMR = Value;
	checkInterrupts();
}

void ModulePIC::io_OCW2(dword value, int) {
	debug("OCW 2");

	// EOI
	if ((value & 0x20) == 0x20) {
		pendingIntSent = false;

		// End of Highest-prior. int
		if ((value & 0xC0) == 0) {
			int index = -1, pr = -1;
			for (int i = 0; i < 8; ++i) {
				if ((ISR & Constants::pow2[i]) && (Prior[i] > pr)) {
					index = i;
					pr = Prior[i];
				}
			}
			if (index != -1) {
				if (index != 0) {
					debug("EOI " + QString::number(index));
				}
				ISR &= ~Constants::pow2[index];
			}
		}
		if ((value & 0xC0) == 0x40) {
			int k = (value & 7);
			if (k != 0) {
				debug("EOI " + QString::number(k));
			}
			ISR &= ~Constants::pow2[k];
		}
	}

	// Cycl. prior. shift
	if ((value & 0xC0) == 0x80) {
		int q = Prior[0];
		for (int i = 0; i < 7; ++i) {
			Prior[i] = Prior[i+1];
		}
		Prior[7] = q;
	}

	// Cycl through Value & 7
	if ((value & 0xC0) == 0xC0) {
		int k = (value & 7);
		int q = 8;
		for (int i = k; i < 8; ++i) {
			Prior[i] = q--;
		}
		for (int i = 0; i < k; ++i) {
			Prior[i] = q--;
		}
	}
}

void ModulePIC::io_OCW3(dword Value, int) {
	debug("OCW 3");
	if (Value & 2) Poll = true;
	if ((Value & 7) == 2) ReadIRR = true;
	if ((Value & 7) == 3) ReadISR = true;
}

void ModulePIC::signalLevelled(dword level) {
	if (level != 0) {
		debug("Signal on line " + QString::number(level));
		debug("Already sent? " + QString::number(pendingIntSent));
	}
	if ((Constants::pow2[level] & IMR) & 0xFF) {
		if (level != 0) {
			debug("Signal is masked by IMR");
		}
	} else {
		if (level != 0) {
			debug("Signal not masked");
		}
	}

	IRR |= (Constants::pow2[level] & 0xFF);
		//checkInterrupts();
}

dword ModulePIC::io_read(int) {
	if (ReadISR) {
		debug("Read ISR");
		ReadISR = false;
		return ISR;
	}
	if (ReadIRR) {
		debug("Read IRR");
		ReadIRR = false;
		return IRR;
	}
	if (Poll) {
		debug("Read poll byte");
		Poll = false;
		int k = 0;
		if (IRR) {
			k = 0x80;

			int Index = -1, Pr = -1;
			for (int i = 0; i < 8; ++i) {
				if ((ISR & Constants::pow2[i]) && (Prior[i] > Pr)) {
					Index = i;
					Pr = Prior[i];
				}
			}
			return (k + Index);
		}
	}
	return 0;
}

dword ModulePIC::io_readIMR(int) {
	debug("Read IMR");
	return IMR;
}

void ModulePIC::io_resetIrq1() {
	debug("Reset IRQ1 status");
	IRR &= ((~Constants::pow2[1]) & 0xFF);
}

ModuleDevice* createModule() {
	return new ModulePIC();
}
