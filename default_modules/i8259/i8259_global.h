#ifndef I8259_GLOBAL_H
#define I8259_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(I8259_LIBRARY)
#  define I8259SHARED_EXPORT Q_DECL_EXPORT
#else
#  define I8259SHARED_EXPORT Q_DECL_IMPORT
#endif

#endif 
