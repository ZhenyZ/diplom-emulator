#ifndef MODULETIMER_H
#define MODULETIMER_H

#include <moduledevice.h>
#include <loggable.h>
#include <QList>

#include "i8259_global.h"

class I8259SHARED_EXPORT ModulePIC : public Loggable, public ModuleDevice
{
	Q_OBJECT
public:
	ModulePIC(QObject * parent = nullptr);
	virtual ~ModulePIC();

	virtual void tick() override;
	virtual void debug(const QString &str) override;
	virtual void moduleReset() override;
	virtual void moduleShutdown() override;

	static constexpr int
		SIGNAL_EMIT_RUN = 0,
		SIGNAL_PENDING = 1;

private:

	void init();
	void initIo();
	void checkInterrupts();

	bool IsInit();

	void io_writePort1(dword value, int);
	void io_writePort2(dword value, int);

	dword io_getLevel(int);

	dword io_read(int);
	dword io_readIMR(int);
	void io_resetIrq1();

	void io_ICW(dword Value, int);
	void io_OCW1(dword Value, int);
	void io_OCW2(dword Value, int);
	void io_OCW3(dword Value, int);

	void signalLevelled(dword value);
	void interrupt(int level);
	void interruptAck();

	byte
		IRR,
		ISR,
		IMR,
		Vect0;
	char
		Prior[8];
	byte
		InitMode,
		ICW4go,
		SNGL,
		LTIM,
		AEOI,
		ICWIndex;
	bool
		Poll,
		ReadIRR,
		ReadISR,
		pendingIntSent;

	dword cyclesSinceSent;

	dword interruptLevel;
	dword interruptIndex;

	QList<int> pendingInterrupts;

};

extern "C" I8259SHARED_EXPORT ModuleDevice* createModule();

#endif // MODULETIMER_H
