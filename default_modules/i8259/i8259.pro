QT -= gui

TARGET = i8259
TEMPLATE = lib
CONFIG += c++11

DEFINES += I8259_LIBRARY

INCLUDEPATH += $$PWD/../lib/

SOURCES += \ 
    modulepic.cpp

HEADERS += \ 
    i8259_global.h \
    modulepic.h

CONFIG(debug, debug|release) {
	LIBS += -L$$PWD/../lib/debug -lEMUESDebugLib -lEMUESDevicesLib
}
CONFIG(release, debug|release) {
	LIBS += -L$$PWD/../lib/release -lEMUESDebugLib -lEMUESDevicesLib
}

