#ifndef MC6845_GLOBAL_H
#define MC6845_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(MC6845_LIBRARY)
#  define MC6845SHARED_EXPORT Q_DECL_EXPORT
#else
#  define MC6845SHARED_EXPORT Q_DECL_IMPORT
#endif

#endif 
