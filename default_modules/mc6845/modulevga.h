#ifndef CONTROLLERVGA_H
#define CONTROLLERVGA_H

#include <QObject>
#include <QDebug>
#include <moduledevice.h>
#include <loggable.h>

#include "mc6845_global.h"

const int
	DISPLAY_MGA		= 0x1,
	DISPLAY_CGA		= 0x2,
	DISPLAY_EGA		= 0x3,
	DISPLAY_VGA		= 0x4,
	VideoMode_TEXT	= 50,
	VideoMode_GRAF	= 51;

class MC6845SHARED_EXPORT ModuleVGA : public Loggable, public ModuleDevice
{
	Q_OBJECT
public:
	ModuleVGA(QObject * parent = nullptr);
	virtual ~ModuleVGA();

	void io_selectRegister(dword value, int);
	void io_writeRegister(dword value, int);
	dword io_readRegister(int);
	void io_lightPenOff(dword, int);
	void io_lightPenOn(dword, int);
	void io_cursorOn(dword, int);
	void io_cursorOff(dword, int);

	void io_writeModeReg(dword value, int);
	dword io_readState(int);
	dword io_readStateMDA(int);

	void io_output(word port, dword value, int);
	dword io_input(word port, int);

	void init();
	void setupIO();

	void startup();

	virtual void tick() override;

	virtual void onBeforeStart() override;

	virtual void debug(const QString &str);
	virtual void moduleReset() override;
	virtual void moduleShutdown() override;

private:

	void callUpdateWidthHeight();

	const int INPUT_SELECT_REGISTER = 0;
	const int INPUT_WRITE_REGISTER = 1;
	const int INPUT_WRITE_MODE_REGISTER = 2;
	const int INPUT_DISABLE_LIGHT_PEN = 3;
	const int INPUT_ENABLE_LIGHT_PEN = 4;

	const int OUTPUT_READ_REGISTER = 0;
	const int OUTPUT_READ_STATE = 1;
	const int OUTPUT_READ_WIDTH = 2;
	const int OUTPUT_READ_HEIGHT = 3;

	const int SIGNAL_EMIT_DISPLAY_ON = 0;
	const int SIGNAL_EMIT_DISPLAY_OFF = 1;
	const int SIGNAL_EMIT_TEXT_MODE = 2;
	const int SIGNAL_EMIT_GRAPHICAL_MODE = 3;

	dword readHeight(int);
	dword readWidth(int);

	void initState();

	int type;
	dword baseAddress;
	dword memoryOffset;

	qword* memSizeTransit = nullptr;
	ModuleMemPtr memPointerTransit = nullptr;
	ModuleMemPtr memPointerCalculatedTransit = nullptr;
	qword* memSizeCalculatedTransit = nullptr;

	byte* videoMemory = nullptr;
	dword videoMemSize = 0;

	byte registers[256];

	byte registerIndex,
		 modeReg,
		 statusReg,
		 cursorOn,
		 penOn;

	dword
		width, height,
		oldWidth, oldHeight,
		cursorHi, cursorLo,
		cursorPos,
		cursorX, cursorY;
	int
		Mode,
		CV,
		FullScreen,
		State,
		CursorVisible,
		PenVisible,
		DisplayON,
		VSync,
		HSync,
		Graphics,
		Color;		// or monochrome
};

extern "C" MC6845SHARED_EXPORT ModuleDevice* createModule();

#endif // CONTROLLERVGA_H
