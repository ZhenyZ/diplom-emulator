QT -= gui

TARGET = mc6845
TEMPLATE = lib
CONFIG += c++11

DEFINES += MC6845_LIBRARY

INCLUDEPATH += $$PWD/../lib/

SOURCES += \ 
    modulevga.cpp

HEADERS += \ 
    mc6845_global.h \
    modulevga.h

CONFIG(debug, debug|release) {
	LIBS += -L$$PWD/../lib/debug -lEMUESDebugLib -lEMUESDevicesLib
}
CONFIG(release, debug|release) {
	LIBS += -L$$PWD/../lib/release -lEMUESDebugLib -lEMUESDevicesLib
}

