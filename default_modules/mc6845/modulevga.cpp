#include "modulevga.h"

ModuleVGA::ModuleVGA(QObject *parent)
	: Loggable("VGA", parent),
	ModuleDevice("VGA Controller", "Eugene Shestakov", 1)
{
	memPointerCalculatedTransit = new byte*[1];
	memSizeCalculatedTransit = new qword[1];
	memSizeCalculatedTransit[0] = 0;
	videoMemSize = 0x100000;
	videoMemory = new byte[videoMemSize];
	init();
	setupIO();
}

ModuleVGA::~ModuleVGA() {
	delete [] memPointerCalculatedTransit;
	delete [] memSizeCalculatedTransit;
	delete [] videoMemory;
}

void ModuleVGA::init() {
	initState();
}

void ModuleVGA::initState() {
	oldWidth = 42;
	oldHeight = 24;
	type = DISPLAY_MGA;
	for (int i = 0; i < 256; ++i) {
		registers[i] = 0;
	}
	registerIndex = 0;
	modeReg = 0;
	statusReg = 0;
	cursorOn = 1;
	penOn = 0;
	baseAddress = 0xB8000;
	Color = 0;
	cursorHi = 0;
	cursorLo = 0;
	cursorPos = 0;
	CursorVisible = 1;
	cursorX = 0;
	cursorY = 0;
	CV = 1;
	DisplayON = 1;
	FullScreen = 0;
	Graphics = 0;
	height = 25;
	width = 80;
	Mode = VideoMode_TEXT;
	HSync = 0;
	PenVisible = 0;
	State = 0;
	VSync = 0;
	memoryOffset = 0;
	FullScreen = false;
}

void ModuleVGA::setupIO() {
	registerInput(INPUT_SELECT_REGISTER, "Select register", std::bind(&ModuleVGA::io_selectRegister, this, std::placeholders::_1, std::placeholders::_2));
	registerInput(INPUT_WRITE_REGISTER, "Write to register", std::bind(&ModuleVGA::io_writeRegister, this, std::placeholders::_1, std::placeholders::_2));
	registerInput(INPUT_WRITE_MODE_REGISTER, "Write to Mode register", std::bind(&ModuleVGA::io_writeModeReg, this, std::placeholders::_1, std::placeholders::_2));
	registerInput(INPUT_DISABLE_LIGHT_PEN, "Disable light pen", std::bind(&ModuleVGA::io_lightPenOff, this, std::placeholders::_1, std::placeholders::_2));
	registerInput(INPUT_ENABLE_LIGHT_PEN, "Enable light pen", std::bind(&ModuleVGA::io_lightPenOn, this, std::placeholders::_1, std::placeholders::_2));

	registerOutput(OUTPUT_READ_REGISTER, "Read register", std::bind(&ModuleVGA::io_readRegister, this, std::placeholders::_1));
	registerOutput(OUTPUT_READ_STATE, "Read state", std::bind(&ModuleVGA::io_readState, this, std::placeholders::_1));
	registerOutput(OUTPUT_READ_WIDTH, "Read width", std::bind(&ModuleVGA::readWidth, this, std::placeholders::_1));
	registerOutput(OUTPUT_READ_HEIGHT, "Read height", std::bind(&ModuleVGA::readHeight, this, std::placeholders::_1));
	registerOutput(0x299, "Read state MDA", std::bind(&ModuleVGA::io_readStateMDA, this, std::placeholders::_1));

	registerEmitSignalFunction(SIGNAL_EMIT_DISPLAY_ON, "Switch display ON");
	registerEmitSignalFunction(SIGNAL_EMIT_DISPLAY_OFF, "Switch display OFF");
	registerEmitSignalFunction(SIGNAL_EMIT_TEXT_MODE, "Set text mode");
	registerEmitSignalFunction(SIGNAL_EMIT_GRAPHICAL_MODE, "Set graphical mode");

	ModuleDevice::DataMappingBlock sysMem = std::make_pair(&this->memPointerTransit, &memSizeTransit);
	registerDataMapConsumer(0, "System Memory", sysMem);
	ModuleDevice::DataMappingBlock videoMem = std::make_pair(&this->memPointerCalculatedTransit, &memSizeCalculatedTransit);
	registerDataMapProvider(0, "Video Memory Frame", videoMem);
}

void ModuleVGA::tick()
{

}

void ModuleVGA::io_writeModeReg(dword value, int) {
	oldWidth = width;
	oldHeight = height;
	if (value & 2) {
		Mode = VideoMode_GRAF;
		callSignalEmitByIndex(SIGNAL_EMIT_GRAPHICAL_MODE);
		if (value & 16) {
			width = 640;
			debug("Graphical Mode 640x200");
		}
		else {
			debug("Graphical Mode 320x200");
			width = 320;
		}
		height = 200;
		debug("Set base address 0xB8000");
		baseAddress = 0xB8000;
		*this->memSizeCalculatedTransit = 0x37FFF;
	}
	if (!(value & 2)) {
		callSignalEmitByIndex(SIGNAL_EMIT_TEXT_MODE);
		Mode = VideoMode_TEXT;
		if ((value & 1) == 1) {
			width = 80;
			debug("Text Mode 80x25");
		}
		else {
			debug("Text Mode 40x25");
			width = 40;
		}
		height = 25;
		debug("Set base address 0xB8000");
		baseAddress = 0xB8000;
		*this->memSizeCalculatedTransit = 0x37FFF;
	}
	if ((value & 8) != DisplayON) {
		if (!(value & 8)) {
			debug("Switch OFF");
			callSignalEmitByIndex(SIGNAL_EMIT_DISPLAY_OFF);
		}
		else {
			debug("Switch ON");
			callSignalEmitByIndex(SIGNAL_EMIT_DISPLAY_ON);
		}
	}
	callUpdateWidthHeight();
	*this->memPointerCalculatedTransit = *this->memPointerTransit + baseAddress + memoryOffset;
	DisplayON = (value & 8);
}


dword ModuleVGA::io_readState(int) {
	debug("Read state");
	VSync = (1 - VSync);
	return (VSync + ((1-CursorVisible) << 1) + (VSync << 3));
}

dword ModuleVGA::io_readStateMDA(int) {
	debug("Read state MDA");
	VSync = (1 - VSync);
	return (VSync + ((1-CursorVisible) << 1) + (VSync << 3));
}



void ModuleVGA::onBeforeStart() {
	*this->memSizeCalculatedTransit = 0x37FFF;
	*this->memPointerCalculatedTransit = *this->memPointerTransit + 0xB8000;
	callUpdateWidthHeight();
}

void ModuleVGA::io_selectRegister(dword index, int) {
	debug("Select register " + QString::number(index));
	registerIndex = index;
}

dword ModuleVGA::io_readRegister(int) {
	return registers[registerIndex];
}

void ModuleVGA::io_writeRegister(dword value, int) {
	debug("Write register [" + QString::number(registerIndex, 16) + "] = " + QString::number(value, 16));
	registers[registerIndex] = value;
	switch (registerIndex)
	{
		case 0x0A:
			cursorHi = (value & 0x0F);
			debug("Set cursor Hi = " + QString::number(cursorHi, 16));
		break;

		case 0x0B:
			cursorLo = (value & 0x0F);
			debug("Set cursor Lo = " + QString::number(cursorLo, 16));
		break;

		case 0x0C:
			memoryOffset &= 0x00FF;
			memoryOffset |= (value << 8);
			*this->memPointerCalculatedTransit = *this->memPointerTransit + baseAddress + memoryOffset;
			debug("Set address offset = " +	QString::number(baseAddress, 16));
		break;

		case 0x0D:
			memoryOffset &= 0xFF00;
			memoryOffset |= value;
			*this->memPointerCalculatedTransit = *this->memPointerTransit + baseAddress + memoryOffset;
			debug("Set address offset = " +	QString::number(baseAddress, 16));
		break;

		case 0x0E:
			cursorPos &= 0x00FF;
			cursorPos |= (value << 8);
			debug("Set cursor position = " + QString::number(cursorPos, 16));
		break;

		case 0x0F:
			cursorPos &= 0xFF00;
			cursorPos |= value;
			debug("Set cursor position = " + QString::number(cursorPos, 16));
		break;

		default:;
	}
}

void ModuleVGA::io_lightPenOff(dword e, int) {
	Q_UNUSED(e)
	penOn = 0;
}

void ModuleVGA::io_lightPenOn(dword e, int) {
	Q_UNUSED(e)
	penOn = 1;
}

void ModuleVGA::io_cursorOn(dword e, int) {
	Q_UNUSED(e)
	cursorOn = 1;
}

void ModuleVGA::io_cursorOff(dword e, int) {
	Q_UNUSED(e)
	cursorOn = 0;
}

void ModuleVGA::debug(const QString & str) {
	QMap<QString, QString> params;
	params.insert("type", "device");
	params.insert("message", str);
	doDebug(params);
}

void ModuleVGA::moduleReset() {
	initState();
}

void ModuleVGA::moduleShutdown() {

}

void ModuleVGA::callUpdateWidthHeight() {
//	if (oldWidth != width) {
		callOutputByIndex(OUTPUT_READ_HEIGHT, 4);
		oldWidth = width;
//	}
//	if (oldHeight != height) {
		callOutputByIndex(OUTPUT_READ_WIDTH, 4);
		oldHeight = height;
//	}
}

dword ModuleVGA::readHeight(int) {
	return height;
}

dword ModuleVGA::readWidth(int) {
	return width;
}

ModuleDevice* createModule() {
	return new ModuleVGA();
}
