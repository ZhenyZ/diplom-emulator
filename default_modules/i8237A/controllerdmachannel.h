#ifndef CONTROLLERDMACHANNEL_H
#define CONTROLLERDMACHANNEL_H

#include <QObject>
#include <constants.h>

class ModuleDMA;

class ControllerDMAChannel : public QObject
{
	Q_OBJECT
public:
	ControllerDMAChannel(word level, ModuleDMA*, QObject* parent = nullptr);
	virtual ~ControllerDMAChannel();

	void init();

	word BAR,			// Base Address
		 CAR,			// Current Address
		 BWCR,			// Base Word Count
		 CWCR,			// Current Word Count
		 ModeReg,
		 PageReg;

	byte getCWCR();
	byte getCAR();
	byte getPage();
	byte getPageHi();
	void setBWCR(byte);
	void setBAR(byte);
	void setPage(byte);
	void setPageHi(byte);

	void clearFlipFlop();

	word getInnerCWCR();
	word getInnerBWCR();
	word getInnerBAR();
	word getInnerCAR();
	word getInnerPage();

	void setInnerCAR(word value);
	void setInnerCWCR(word value);

	void setMode(byte);
	byte getMode();

	void reset();

	void DRQ();

	void setRQ(word line);
	void clearRQ(word line);

	void setSrcMem(byte** memory);
	void setDstMem(byte** memory);
	void setSrcMemSize(qword*);
	void setDstMemSize(qword*);
	void transfer();

	byte readSingleByte();
	void writeSingleByte(byte b);

private:

	ModuleDMA* masterDMA;

	byte** srcMem;
	byte** destMem;
	qword* srcMemSize;
	qword* dstMemSize;

	word inMask;
	word inShor;

	word channelLevel = 0;
	bool noMoreData;
};

#endif // CONTROLLERDMACHANNEL_H
