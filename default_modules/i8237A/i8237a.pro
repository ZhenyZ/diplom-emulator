QT += gui

TARGET = i8237a
TEMPLATE = lib
CONFIG += c++11

DEFINES += I8237_LIBRARY

INCLUDEPATH += $$PWD/../lib/

SOURCES += \ 
    moduledma.cpp \
    controllerdmachannel.cpp


HEADERS += \ 
    controllerdmachannel.h \
    moduledma.h \
    moduledma_global.h

CONFIG(debug, debug|release) {
	LIBS += -L$$PWD/../lib/debug -lEMUESDebugLib -lEMUESDevicesLib
}
CONFIG(release, debug|release) {
	LIBS += -L$$PWD/../lib/release -lEMUESDebugLib -lEMUESDevicesLib
}

