#ifndef I8237_GLOBAL_H
#define I8237_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(I8237_LIBRARY)
#  define I8237SHARED_EXPORT Q_DECL_EXPORT
#else
#  define I8237SHARED_EXPORT Q_DECL_IMPORT
#endif

#endif 
