#include "moduledma.h"
#include "controllerdmachannel.h"

#include <moduledevicegui.h>

#define RegisterInput(Port,Name,Func) registerInput(Port, Name, std::bind(Func, this, std::placeholders::_1, std::placeholders::_2));
#define RegisterInputParam(Port,Name,Func) registerInput(Port, Name, std::bind(Func, this, Port, std::placeholders::_1, std::placeholders::_2));
#define RegisterOutput(Port,Name,Func) registerOutput(Port, Name, std::bind(Func, this, std::placeholders::_1));
#define RegisterOutputParam(Port,Name,Func) registerOutput(Port, Name, std::bind(Func, this, Port, std::placeholders::_1));
#define RegisterSignalParam(Index,Name,Func) registerReceiveSignalFunction(Index, Name, std::bind(Func, this, Index, std::placeholders::_1));

ModuleDMA::ModuleDMA(QObject * parent)
	: Loggable("DMA Chip", parent),
	  ModuleDevice("8237A DMA Controller", "Eugene Shestakov", 1)
{
	this->cpuMemory = new byte*[1];
	this->cpuMemory[0] = nullptr;
	this->cpuMemSize = new qword[1];
	this->cpuMemSize[0] = 0;

	this->deviceMemory = new ModuleMemPtr[8];
	this->deviceMemSize = new qword*[8];
	for (int i = 0; i < 8; i++) {
		this->deviceMemory[i] =  new byte*[1];
		this->deviceMemory[i][0] = nullptr;
		this->deviceMemSize[i] = new qword[1];
		this->deviceMemSize[i][0] = 0x22222222;
	}
	init();
}

ModuleDMA::~ModuleDMA() {
	delete [] cpuMemory;
	delete [] cpuMemSize;
	for (int i = 0; i < 8; ++i) {
		delete [] deviceMemory[i];
		delete [] deviceMemSize[i];
	}
	delete [] deviceMemory;
	delete [] deviceMemSize;
}

void ModuleDMA::tick() {}

void ModuleDMA::showUi() {
	emit signalShowGui();
}

void ModuleDMA::hideUi() {
	emit signalHideGui();
}

void ModuleDMA::createUi() {
	initDebugWidget();
}

void ModuleDMA::init() {
	//	qDebug() << "DMA init;\n" << "CPU mem=" << *this->cpuMemory << "; memSizeCpu=" << *this->cpuMemSize;
	for (int i = 0; i < 8; i++) {
		Channels[i] = new ControllerDMAChannel(i, this);
		//connect(Channels[i], SIGNAL(signalDebugOutput(QMap<QString,QString>)), this, SIGNAL(signalDebugOutput(QMap<QString,QString>)));
		Channels[i]->setDstMem(this->cpuMemory);
		Channels[i]->setDstMemSize(this->cpuMemSize);
		Channels[i]->setSrcMem(this->deviceMemory[i]);
		Channels[i]->setSrcMemSize(this->deviceMemSize[i]);
//		qDebug() << "Dev " << i << " mem=" << this->deviceMemory[i]<< "; memSize=" << *this->deviceMemSize[i];
	}
	io_reset(0, 0);
	initIo();
}

void ModuleDMA::initIo() {
	RegisterInputParam(0x00, "Set BAR 0", &ModuleDMA::io_setBAR);
	RegisterInputParam(0x02, "Set BAR 1", &ModuleDMA::io_setBAR);
	RegisterInputParam(0x04, "Set BAR 2", &ModuleDMA::io_setBAR);
	RegisterInputParam(0x06, "Set BAR 3", &ModuleDMA::io_setBAR);
	RegisterInputParam(0x01, "Set BWCR 0", &ModuleDMA::io_setBWCR);
	RegisterInputParam(0x03, "Set BWCR 1", &ModuleDMA::io_setBWCR);
	RegisterInputParam(0x05, "Set BWCR 2", &ModuleDMA::io_setBWCR);
	RegisterInputParam(0x07, "Set BWCR 3", &ModuleDMA::io_setBWCR);
	RegisterInput(0x08, "Set Command register", &ModuleDMA::io_setCmdReg);
	RegisterInput(0x09, "Set Request register", &ModuleDMA::io_setReqReg);
	RegisterInput(0x0A, "Set Single mask", &ModuleDMA::io_setSingleMask);
	RegisterInput(0x0B, "Set Mode", &ModuleDMA::io_setMode);
	RegisterInput(0x0C, "Clear Flip-Flop", &ModuleDMA::io_clearFlipFlop);
	RegisterInput(0x0D, "Reset", &ModuleDMA::io_reset);
	RegisterInput(0x0E, "Clear mask", &ModuleDMA::io_clearMask);
	RegisterInput(0x0F, "Set mask", &ModuleDMA::io_setMask);
	RegisterInputParam(0x81, "Set page 0", &ModuleDMA::io_setPage);
	RegisterInputParam(0x82, "Set page 1", &ModuleDMA::io_setPage);
	RegisterInputParam(0x83, "Set page 2", &ModuleDMA::io_setPage);
	RegisterInputParam(0x87, "Set page 3", &ModuleDMA::io_setPage);
	RegisterInputParam(0x84, "Set page hi 0", &ModuleDMA::io_setPageHi);
	RegisterInputParam(0x85, "Set page hi 1", &ModuleDMA::io_setPageHi);
	RegisterInputParam(0x86, "Set page hi 2", &ModuleDMA::io_setPageHi);
	RegisterInputParam(0x88, "Set page hi 3", &ModuleDMA::io_setPageHi);

	RegisterOutputParam(0x00, "Get CAR 0", &ModuleDMA::io_getCAR);
	RegisterOutputParam(0x02, "Get CAR 1", &ModuleDMA::io_getCAR);
	RegisterOutputParam(0x04, "Get CAR 2", &ModuleDMA::io_getCAR);
	RegisterOutputParam(0x06, "Get CAR 3", &ModuleDMA::io_getCAR);
	RegisterOutputParam(0x01, "Get CWCR 0", &ModuleDMA::io_getCWCR);
	RegisterOutputParam(0x03, "Get CWCR 1", &ModuleDMA::io_getCWCR);
	RegisterOutputParam(0x05, "Get CWCR 2", &ModuleDMA::io_getCWCR);
	RegisterOutputParam(0x07, "Get CWCR 3", &ModuleDMA::io_getCWCR);
	RegisterOutput(0x08, "Get Status register", &ModuleDMA::io_getStatusReg);
	RegisterOutput(0x0D, "Get Work register A", &ModuleDMA::io_getWorkReg);
	RegisterOutputParam(0x81, "Get Page 0", &ModuleDMA::io_getPage);
	RegisterOutputParam(0x82, "Get Page 1", &ModuleDMA::io_getPage);
	RegisterOutputParam(0x83, "Get Page 2", &ModuleDMA::io_getPage);
	RegisterOutputParam(0x87, "Get Page 3", &ModuleDMA::io_getPage);
	RegisterOutputParam(0x84, "Get Page Hi 0", &ModuleDMA::io_getPageHi);
	RegisterOutputParam(0x85, "Get Page Hi 1", &ModuleDMA::io_getPageHi);
	RegisterOutputParam(0x86, "Get Page Hi 2", &ModuleDMA::io_getPageHi);
	RegisterOutputParam(0x88, "Get Page Hi 3", &ModuleDMA::io_getPageHi);

	for (int i = 0; i < 8; ++i) {
		registerInput(
				0xFF00 + i,
				"Write byte to channel " + QString::number(i),
				std::bind(&ModuleDMA::io_writeSingleByte, this, i, std::placeholders::_1, std::placeholders::_2)
		);
		registerOutput(
				0xFF00 + i,
				"Read byte from channel " + QString::number(i),
				std::bind(&ModuleDMA::io_readSingleByte, this, i, std::placeholders::_1)
		);
		registerEmitSignalFunction(0xFF00 + i, "Done channel " + QString::number(i));
		//registerReceiveSignalFunction(0xFF00 + i, "No mode data for channel " + QString::number(i), std::bind(&ControllerDMAChannel::noMoreData));
	}


	ModuleDevice::DataMappingBlock sysMem = std::make_pair(&this->cpuMemory, &this->cpuMemSize);
	registerDataMapConsumer(0, "CPU Host memory", sysMem);

	for (int i = 0; i < 8; ++i) {
		ModuleDevice::DataMappingBlock devMem = std::make_pair(&this->deviceMemory[i], &this->deviceMemSize[i]);
		registerDataMapConsumer(i+1, "Channel " + QString::number(i) + " device memory", devMem);
		registerReceiveSignalFunction(i, "DRQ channel " + QString::number(i), std::bind(&ControllerDMAChannel::DRQ, Channels[i]));
		registerEmitSignalFunction(i, "DACK channel " + QString::number(i));
	}

}

byte ModuleDMA::getChannelNo(word port) {
	switch (port) {

	case 0x00:
	case 0x01:
	case 0x87:
	case 0x88:
		return 0;

	case 0x02:
	case 0x03:
	case 0x83:
	case 0x86:
		return 1;

	case 0x04:
	case 0x05:
	case 0x81:
	case 0x84:
		return 2;

	case 0x06:
	case 0x07:
	case 0x82:
	case 0x85:
		return 3;

	default:
		debug("Port is out of DMA range");
		return 0;
	}
}

byte ModuleDMA::getControllerNo(word port) {
	if (port >= 0x89)	return 1;
	else return 0;
}

void ModuleDMA::io_setMode(dword value, int sizeInBytes) {
	byte channelNumber = value & 3;
	byte data = (value & 0xFC) >> 2;
	byte mode = data & 0x03;
	Channels[channelNumber]->setMode(data);
	if (data == 0x01) {
		debug("Channel " + QString::number(channelNumber) + " set direction = To Memory");
//		qDebug() << ("Channel " + QString::number(channelNumber) + " set direction = To Memory");
		Channels[channelNumber]->setSrcMem(deviceMemory[channelNumber]);
		Channels[channelNumber]->setDstMem(cpuMemory);
		Channels[channelNumber]->setSrcMemSize(deviceMemSize[channelNumber]);
		Channels[channelNumber]->setDstMemSize(cpuMemSize);
	} else
	if (data == 0x02) {
		debug("Channel " + QString::number(channelNumber) + " set direction = From Memory");
//		qDebug() << ("Channel " + QString::number(channelNumber) + " set direction = From Memory");
		Channels[channelNumber]->setDstMem(deviceMemory[channelNumber]);
		Channels[channelNumber]->setSrcMem(cpuMemory);
		Channels[channelNumber]->setDstMemSize(deviceMemSize[channelNumber]);
		Channels[channelNumber]->setSrcMemSize(cpuMemSize);
	}
}

void ModuleDMA::io_setBAR(word port, dword value, int sizeInBytes) {
	Channels[getChannelNo(port)]->setBAR(value);
}

void ModuleDMA::io_setBWCR(word port, dword value, int sizeInBytes) {
	Channels[getChannelNo(port)]->setBWCR(value);
}

void ModuleDMA::io_setPage(word port, dword value, int sizeInBytes) {
	auto channelNo = getChannelNo(port);
	Channels[channelNo]->setPage(value);
	debug("Write port " + QString::number(port, 16) + " value " + QString::number(value,16));
	debug("Set channel " + QString::number(channelNo) + " page to " + QString::number(Channels[channelNo]->PageReg, 16) );
}

void ModuleDMA::io_setPageHi(word port, dword value, int sizeInBytes) {
	auto channelNo = getChannelNo(port);
	Channels[channelNo]->setPageHi(value);
	debug("Write port " + QString::number(port, 16) + " value " + QString::number(value,16));
	debug("Set channel " + QString::number(channelNo) + " page to " + QString::number(Channels[channelNo]->PageReg, 16) );
}

void ModuleDMA::io_writeSingleByte(word index, dword value, int sizeInBytes) {
	Channels[index]->writeSingleByte(value);
}

dword ModuleDMA::io_readSingleByte(word index, int sizeInBytes)
{
	return Channels[index]->readSingleByte();
}

// TODO: complete
void ModuleDMA::io_dataComplete(word index, int sizeInBytes) {
	//Channels[index]->
}

dword ModuleDMA::io_getCAR(word port, int sizeInBytes) {
	return Channels[getChannelNo(port)]->getCAR();
}

dword ModuleDMA::io_getCWCR(word port, int sizeInBytes) {
	return Channels[getChannelNo(port)]->getCWCR();
}

dword ModuleDMA::io_getPage(word port, int sizeInBytes) {
	return Channels[getChannelNo(port)]->getPage();
}

dword ModuleDMA::io_getPageHi(word port, int sizeInBytes) {
	return Channels[getChannelNo(port)]->getPageHi();
}

void ModuleDMA::io_setCmdReg(dword value, int sizeInBytes) {
	CmdReg = value;
	debug("Set CommandReg = " + QString::number(value));
}

void ModuleDMA::checkDMARequest() {
	bool DMARequest = (ReqReg & (~MaskReg));
}

/**
 * Set Request register and then check for pending interupts
 */
void ModuleDMA::io_setReqReg(dword Value, int sizeInBytes) {
	byte a = ((Value & 4) >> 2);
	bool DMARequest;
	if (a) {
		ReqReg |= Constants::pow2[Value & 3];
		StatusReg |= Constants::pow2[(Value&3)+4];
	}
	else {
		ReqReg &= (~Constants::pow2[Value & 3]);
		StatusReg &= (~Constants::pow2[(Value&3)+4]);
	}
	checkDMARequest();
	debug("Set Req="+QString::number(Value & 0xFC, 16)+" on channel "+QString::number(Value & 3, 16));
}

/**
 * Reset "First-Last" latch for I/O routines
 */
void ModuleDMA::io_clearFlipFlop(dword e, int) {
	Q_UNUSED(e);
	debug("Clear flip-flop");
	for (int i = 0; i < 4; i++)
		Channels[i]->clearFlipFlop();
	return;
}

/**
 * Perform full register cleanup, reset "First-Last" latch
 * Devices connected to pins are being preserved from modification
 */
void ModuleDMA::io_reset(dword e, int sizeInBytes) {
	Q_UNUSED(e);
	for (int i = 0; i < 4; ++i) {
		Channels[i]->reset();
	}
	io_clearFlipFlop(e, sizeInBytes);
	TAR = 0;
	TWCR = 0;
	StatusReg = 0;
	CmdReg = 0;
	TempReg = 0;
	MaskReg = 0xFF;
	ReqReg = 0;
	debug("Reset");
}

/**
 * Clear mask on all PINS and then check for pending interrupts
 */
void ModuleDMA::io_clearMask(dword e, int sizeInBytes) {
	Q_UNUSED(e)
	MaskReg = 0;
	checkDMARequest();
	debug("Unmask all");
}

/**
 * Set all channels mask bit and then check for pending interrupts
 */
void ModuleDMA::io_setMask(dword value, int) {
	MaskReg = value;
	debug("Set mask=" + QString::number(value,16));
	checkDMARequest();
}

/**
 * Set specific channel mask bit and then check for pending interrupts
 */
void ModuleDMA::io_setSingleMask(dword Value, int sizeInBytes) {
	byte a = ((Value & 4) >> 2);
	if (a)
		MaskReg |= Constants::pow2[Value & 3];
	else
		MaskReg &= (~Constants::pow2[Value & 3]);
	checkDMARequest();
	debug("Set Mask = " + QString::number((Value & 0x4) >> 2, 16) + "h on channel " + QString::number(Value & 3, 16) + "h");
}

dword ModuleDMA::io_getStatusReg(int sizeInBytes) {
	debug("Get Status Register");
	return StatusReg;
}

dword ModuleDMA::io_getWorkReg(int sizeInBytes) {
	debug("Get Temp Reg");
	return TempReg;
}

void ModuleDMA::DACK_Channel(word level) {
	debug("DACK from channel " + QString::number(level));
	callSignalEmitByIndex(level);
}

void ModuleDMA::onBeforeStart()
{
	for (int i = 0; i < 8; i++) {
		Channels[i]->setDstMem(this->cpuMemory);
		Channels[i]->setDstMemSize(this->cpuMemSize);
		Channels[i]->setSrcMem(this->deviceMemory[i]);
		Channels[i]->setSrcMemSize(this->deviceMemSize[i]);
	}
}

void ModuleDMA::transferCompleteChannel(word channelIndex) {
	callSignalEmitByIndex(0xFF00 + channelIndex);
}

void ModuleDMA::RQ_Channel(word level) {
	debug("DRQ to channel " + QString::number(level));
	ReqReg |= Constants::pow2[level];
	if (!(MaskReg & Constants::pow2[level])) {
		Channels[level]->DRQ();
		ReqReg &= (~Constants::pow2[level]);
	}
}

void ModuleDMA::debug(const QString & str)
{
//	QMap<QString, QString> params;
//	params.insert("type", "device");
//	params.insert("message", str);
//	doDebug(params);
//	if (debugListWidget) {
//		debugListWidget->addItem(str);
//		debugListWidget->scrollToBottom();
//	}
	emit signalDebugToGui(str);
}

/**
 * Check all pending interrupts, register first interrupt found to be served
 * and then start transaction if CPU allows
 * @return flags set for produced channels
 * @warning Device buffer must be correct
 * @warning Memory address value is not checked for parity
 */
word ModuleDMA::proceed() {
	word result = 0;
	if (ReqReg & (~MaskReg)) {
		for (int i = 0; i < 4; i++) {
			if (ReqReg & Constants::pow2[i] & ((~MaskReg) & Constants::pow2[i])) {
				Channels[i]->DRQ();
				ReqReg &= (~Constants::pow2[i]);
				result |= Constants::pow2[i];
			}
		}
	}
	return result;
}

void ModuleDMA::initDebugWidget() {
//	debugWidget = new QWidget();
//	debugWidget->setWindowTitle("DMA");
//	QBoxLayout * vl = new QBoxLayout(QBoxLayout::Direction::TopToBottom, debugWidget);
//	debugListWidget = new QListWidget();
//	debugListWidget->setFont(QFontDatabase::systemFont(QFontDatabase::FixedFont));
//	vl->addWidget(debugListWidget);
//	debugListWidget->setAutoScroll(true);
	gui = new ModuleDeviceGui(this);
	gui->setTitle("8237A DMA Controller");
	gui->setFilename("debug.log");
	connect(this, SIGNAL(signalDebugToGui(QString)), gui, SLOT(slotAddItemToListWidget(QString)), Qt::QueuedConnection);
	connect(this, SIGNAL(signalShowGui()), gui, SLOT(slotShowGui()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalHideGui()), gui, SLOT(slotHideGui()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalInitGui()), gui, SLOT(slotInit()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalAddListWidget()), gui, SLOT(slotAddListWidget()), Qt::QueuedConnection);
	emit signalInitGui();
	emit signalAddListWidget();
}

void ModuleDMA::moduleReset() {
	io_reset(0, 0);
}

void ModuleDMA::moduleShutdown() {

}

ModuleDevice* createModule() {
	return new ModuleDMA();
}
