#ifndef MODULEDMA_H
#define MODULEDMA_H

#include <moduledevice.h>
#include <loggable.h>
#include <QDebug>

#include "i8237a_global.h"

class ControllerDMAChannel;
class QWidget;
class QListWidget;
class ModuleDeviceGui;

class I8237SHARED_EXPORT ModuleDMA : public Loggable, public ModuleDevice
{
	Q_OBJECT
public:
	ModuleDMA(QObject * parent = nullptr);
	virtual ~ModuleDMA();

	virtual void tick();

	virtual void showUi();
	virtual void hideUi();
	virtual void createUi();

	void RQ_Channel(word level);
	void DACK_Channel(word level);

	virtual void onBeforeStart() override;
	virtual void moduleReset() override;
	virtual void moduleShutdown() override;

	void transferCompleteChannel(word channelIndex);

signals:
	void signalDebugToGui(const QString &);
	void signalShowGui();
	void signalHideGui();
	void signalInitGui();
	void signalAddListWidget();

protected:
	virtual void debug(const QString &str);
	ModuleDeviceGui* gui = nullptr;
//	QWidget		* debugWidget = nullptr;
//	QListWidget	* debugListWidget = nullptr;

	void init();
	void initIo();

	ControllerDMAChannel* Channels[8];

	word proceed();

	// Common controller routines
	void io_setCmdReg(dword value, int sizeInBytes);
	void io_reset(dword e, int);
	void io_clearMask(dword e, int);
	void io_setMask(dword value, int);
	void io_setSingleMask(dword value, int);
	void io_clearFlipFlop(dword e, int);
	void io_setReqReg(dword Value, int sizeInBytes);

	void io_setMode(dword value, int);
	void io_setBAR(word port, dword value, int sizeInBytes);
	void io_setBWCR(word port, dword value, int sizeInBytes);
	void io_setPage(word port, dword value, int sizeInBytes);
	void io_setPageHi(word port, dword value, int sizeInBytes);

	void io_writeSingleByte(word index, dword value, int sizeInBytes);
	dword io_readSingleByte(word index, int sizeInBytes);
	void io_dataComplete(word index, int sizeInBytes);

	dword io_getCAR(word port, int sizeInBytes);
	dword io_getCWCR(word port, int sizeInBytes);
	dword io_getPage(word port, int sizeInBytes);
	dword io_getPageHi(word port, int sizeInBytes);

	// Input
	dword io_getStatusReg(int sizeInBytes);
	dword io_getWorkReg(int sizeInBytes);

//	byte* getMemoryPtr() { return memory; }
//	void setMemoryPtr(byte* mem) { memory = mem; }

private:

	void initDebugWidget();

	byte getChannelNo(word port);
	byte getControllerNo(word port);

	void checkDMARequest();

	word TAR,			// Temp address (current)
		 TWCR;			// Temp word count
	byte StatusReg,
		 CmdReg,
		 TempReg,
		 MaskReg,
		 ReqReg;

	ModuleMemPtr cpuMemory = nullptr;
	qword* cpuMemSize = nullptr;

	ModuleMemPtr* deviceMemory = nullptr;
	qword** deviceMemSize = nullptr;

};

extern "C" I8237SHARED_EXPORT ModuleDevice* createModule();

#endif // MODULEDMA_H
