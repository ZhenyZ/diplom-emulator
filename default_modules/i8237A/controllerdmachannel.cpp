#include "controllerdmachannel.h"
#include "moduledma.h"

ControllerDMAChannel::ControllerDMAChannel(word level, ModuleDMA* master, QObject * parent)
	: QObject(parent)
{
	this->channelLevel = level;
	this->masterDMA = master;
}

ControllerDMAChannel::~ControllerDMAChannel()
{

}

void ControllerDMAChannel::init() {

}

void ControllerDMAChannel::DRQ() {
	//debug("DACK");
	// TODO: Dataflow
	//myDev->dma_length = this->BWCR + 1;
	//myDev->DACK();
	masterDMA->DACK_Channel(this->channelLevel);
	transfer();
}

byte ControllerDMAChannel::getCAR() {
	byte a = ((BAR & inMask) >> (inShor));
	inMask = (~inMask);
	inShor = 8 - inShor;
	//debug("Get CAR ");
	return a;
}

byte ControllerDMAChannel::getCWCR() {
	byte a = ((CWCR & inMask) >> (inShor));
	inMask = (~inMask);
	inShor = 8 - inShor;
	//debug("Get CWCR ");
	return a;
}

void ControllerDMAChannel::setSrcMemSize(qword* size) {
	srcMemSize = size;
}

void ControllerDMAChannel::setDstMemSize(qword* size) {
	dstMemSize = size;
}

void ControllerDMAChannel::setSrcMem(byte** mem) {
	this->srcMem = mem;
}

void ControllerDMAChannel::setDstMem(byte** mem) {
	this->destMem = mem;
}


byte ControllerDMAChannel::getPage() {
	//debug("Get page lo");
	return (PageReg & 0xFF);
}

byte ControllerDMAChannel::getPageHi() {
	//debug("Get page hi");
	return ((PageReg & 0xFF00) >> 8);
}

byte ControllerDMAChannel::getMode() {
	return ModeReg;
}

void ControllerDMAChannel::setMode(byte value) {
	ModeReg = value;
}

void ControllerDMAChannel::setBAR(byte value) {
	BAR &= (~inMask);
	BAR += (value << inShor);
	CAR &= (~inMask);
	CAR += (value << inShor);
	inMask = (~inMask);
	inShor = 8 - inShor;
	//debug("Set BAR = " + QString::number(BAR, 16) + "h");
}

void ControllerDMAChannel::setBWCR(byte value) {
	BWCR &= (~inMask);
	BWCR += (value << inShor);
	CWCR &= (~inMask);
	CWCR += (value << inShor);
	inMask = (~inMask);
	inShor = 8 - inShor;
	//debug("Set BWCR = " + QString::number(BWCR, 16) + "h");
}

void ControllerDMAChannel::setPage(byte value) {
	PageReg &= 0xFF00;
	PageReg |= value;
}

void ControllerDMAChannel::setPageHi(byte value) {
	PageReg &= 0x00FF;
	PageReg |= (value << 8);
}

void ControllerDMAChannel::clearFlipFlop() {
	inMask = 0x00FF;
	inShor = 0;
	//debug("Clear flip-flop");
}

word ControllerDMAChannel::getInnerCWCR() {
	return this->CWCR;
}

word ControllerDMAChannel::getInnerPage() {
	return this->PageReg;
}


word ControllerDMAChannel::getInnerBWCR() {
	return this->BWCR;
}

word ControllerDMAChannel::getInnerBAR() {
	return this->BAR;
}

word ControllerDMAChannel::getInnerCAR() {
	return this->CAR;
}

void ControllerDMAChannel::setInnerCWCR(word val) {
	this->CWCR = val;
}

void ControllerDMAChannel::setInnerCAR(word val) {
	this->CAR = val;
}

void ControllerDMAChannel::reset() {
	BAR = 0;
	CAR = 0;
	BWCR = 0;
	CWCR = 0;
	PageReg = 0;
	ModeReg = 0;
	noMoreData = true;
}

void ControllerDMAChannel::setRQ(word line) {
	//debug("RQ on line " + QString::number(line));
	this->masterDMA->RQ_Channel(line);
}

//void ControllerDMAChannel::setDevice(DevicePC * c) {
//	this->myDev = c;
//}

void ControllerDMAChannel::transfer() {
	// TODO: Dataflow
//	qDebug() << "Starting transfer";
//	qDebug() << "Dest mem = " << destMem << " [" << *destMem << "] ";
//	qDebug() << "Src mem = " << srcMem << " [" << *srcMem << "] ";

	dword address = (getInnerPage() << 16) + getInnerCAR();
	dword curCWCR = getInnerCWCR();

	//byte* DevMemory = myDev->dma_data;;
	byte* devMemory = *srcMem;
	byte* targetPtr = *destMem;
	byte* memoryPtr = targetPtr + address; //masterDMA->getMemoryPtr() + Address;
	//dword size = myDev->dma_length;
	dword sizeBwcr = this->BWCR + 1;
	dword sizeDev = *srcMemSize;
	dword size = (sizeBwcr > sizeDev) ? sizeDev-1 : sizeBwcr;
	dword counter = 0;

//	qDebug() << "Transfer: ";
//	qDebug() << "Address		= " + QString::number(address, 16) + "h";
//	qDebug() << "Device datalen = " + QString::number(size, 16) + "h";
//	qDebug() << "DMA datalen	= " + QString::number(curCWCR,16) + "h";

	bool direction = !(getMode() & 0x8);
	int increment;
	if (direction) {
		if ((getMode() & 0x30) == 0x10) {
			increment = 1;
		}
		else {
			increment = 2;
		}
	}
	else {
		if ((getMode() & 0x30) == 0x10) {
			increment = -1;
		}
		else {
			increment = -2;
		}
	}

	dword mSize = (size*abs(increment) < curCWCR ? size : curCWCR);
	qDebug() << "Transferring " << mSize << " bytes from " << devMemory << " to " << memoryPtr;

	while (mSize != 0xFFFFFFFF) {
		*memoryPtr = *devMemory;
		memoryPtr += increment;
		devMemory += increment;
		address += increment;
		mSize--;
		counter++;
	}

	setInnerCWCR(mSize); // this->CWCR[Channel] = mSize;
	setInnerCAR(address); //this->CAR[Channel] = Address;
	qDebug() << "Transfer complete, " + QString::number(counter,16) + "h bytes sent";
	//*/
}

byte ControllerDMAChannel::readSingleByte() {
	dword address = (PageReg << 16) + CAR;
	++CAR;
	--CWCR;
	if (CWCR == 0xFFFF) {
		noMoreData = true;
		masterDMA->transferCompleteChannel(this->channelLevel);
	}
	return *(*srcMem + address);
}

void ControllerDMAChannel::writeSingleByte(byte b) {
	dword destAddress = (PageReg << 16) + CAR;
	*(*destMem + destAddress) = b;
	++CAR;
	--CWCR;
	if (CWCR == 0xFFFF) {
		qDebug() << "DMA stop transfer: all data received";
		noMoreData = true;
		masterDMA->transferCompleteChannel(this->channelLevel);
	}
}

