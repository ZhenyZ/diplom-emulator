#include "modulefdd.h"
#include "guiwrapper.h"

#include <QDebug>


ModuleFDD::ModuleFDD(QObject * parent)
	:Loggable("FDD", parent),
	ModuleDevice("Intel 8272A FDD Controller", "Eugene Shestakov", 1)
{
	setDebugEnabled(true);
	this->bufferSize = 5000000;
	this->buffer = new byte[this->bufferSize];

	this->bufferSizePtr = &this->bufferSize;

	this->bufferPtr = &this->buffer;

	init();
	initIo();
}

ModuleFDD::~ModuleFDD() {
	delete [] buffer;
}

/**
 * Clear all channel datas,
 * reset pending phase results,
 * all motors off, transfer stop.
 */
void ModuleFDD::reset() {
	setDebugEnabled(true);
	debug("Reset");
	phase = Phase_Null;
	motorON = 0;

	regStatus = 0;

	allowTransfer();
	setWriteRequired();
	setNoCommand();
	regStatus |= BIT_DMA_SET;

	regData = 0;
	workAllow = 0;
	useDMA = 1;
	command = CMD_NONE;
	ST0 = 0;
	ST1 = 0;
	ST2 = 0;
	ST3 = 0;

	head = 0;
	sector = 1;
	cylinder = 0;

}

/**
 * Writes control register byte, which bits are:
 *
 * 0 ==+-- Controller number
 * 1 ==+
 * 2 ----- Work allow (0 - reset)
 * 3 ----- DMA and PIC allow (0 - deny)
 * 4 ==+
 * 5   +-- Motor ON (bit4 - FDD0, ..., bit7  - FDD4
 * 6   |
 * 7 ==+
 */
void ModuleFDD::io_writeControlReg(dword Value, int) {
	setDebugEnabled(true);
	if (Value & 3)  { // Only first controller allowed.
		debug("Control reg: selected controller #" + QString::number(Value & 3) + " (wrong)");
		return;
	}
	motorON = (Value & 0x10);
	useDMA = ((Value & 0x08) >> 3);
	if (!workAllow && (Value & 0x04)) {
		workAllow = 0x04;
		qDebug() << "Reset FDD";
		reset();
		callSignalEmitByIndex(SIGNAL_EMIT_INT);
	}
	workAllow = (Value & 0x04);
//	debug("+ Write control reg:");
//	debug("|   Motor = " + QString::number(motorON));
//	debug("|   Allow = " + QString::number(workAllow));
//	debug("|   DMA   = " + QString::number(useDMA));

	debug("Write ControlReg " + QString::number(Value,16));

	ST0 |= 0xC0;

	if (!workAllow) {
		phase = Phase_Null;
	}
}

/**
 * Returns status register, bits are:
 *
 * 0 ==+
 * 1   +-- SEEK operation pending
 * 2   |
 * 3 ==+
 * 4 ----- READ or WRITE operation pending (1 = busy)
 * 5 ----- DMA mode (0 = no DMA)
 * 6 ----- (0 = CPU->FDD   |   1 = FDD->CPU )
 * 7 ----- Ready for next byte (0 = not ready)
 */
dword ModuleFDD::io_readStatusReg(int) {
	setDebugEnabled(true);
//	if (!workAllow) {
//		return 0;
//	}
//	byte result = (0x80 | ((!useDMA) << 5));
//	if ((phase == Phase_Exec_Read) || (phase == Phase_Result)) {
//		result |= 0x40;
//	}
//	if (phase != Phase_Null) {
//		result |= 0x10;
//	}
	debug("Read status reg: " + QString::number(regStatus, 16));
	return regStatus;
}

/**
	Write order byte or data byte when using no-dma mode
*/
void ModuleFDD::io_writeByte(dword Value, int) {
	qDebug() << "WR " << hex << Value;
	setDebugEnabled(true);
	debug("Write byte [ "+QString::number(Value) + " ]");
	if (!loaded) {
		return;
	}

	switch (phase) {
		case Phase_Result:		// No commands accepted
		case Phase_Exec_Read:
		case Phase_Exec_Seek:
			debug(" *** Write byte failed - pending operation ***");
		break;

		case Phase_Null:
			cmdNumber = 0;
			phase = Phase_Order;
			debug("Order started.");

		case Phase_Order:
			writeOrderByte(Value);
		break;

		case Phase_Exec_Write:
			debug("Write byte to disk");
			phase = Phase_Order;
			writeDataByte(Value);
		break;
	}

}


/**
 * Write command byte or command param
 * If command is finished, pass work to other functions
 * No param checks here are implemented except SEEK
 */
void ModuleFDD::writeOrderByte(byte Value) {
	setDebugEnabled(true);
	setBusyCommand();
//	qDebug() << "FDD Write Order " << Value;
	debug("Write Order byte [ "+QString::number(Value, 16) + " ]");

	// Maybe input done?
	if (command != CMD_NONE) {
		params[cmdNumber] = Value;
		cmdNumber++;
		debug("Write param byte #"+QString::number(cmdNumber) + "; total count=" + QString::number(CmdInputCount[command]));
		if (cmdNumber < CmdInputCount[command]) { // Input here is not done
			return;
		}
	}

	// Select new order or finish existing
	switch (command) {

		// -------------------------------------------------
		// No command is choosen, so let's choose now
		case CMD_NONE:
			command = Value;
			if (((Value & 0x1F) == 0x06) || (Value == 0x42) || (Value == 0x02)) {
				command &= 0x1F;
			}
			if (command <= 0x15) {
				debug("New command : " + CMDS[command]);
			} else {
				debug("New unknown command");
			}
			cmdNumber = 0;
			lastCommand = command;
			if (command == CMD_READ_ST0) {
				phase = Phase_Result;
				cmdCount = 2;
				cmdNumber = 0;
				command = CMD_NONE;
				params[0] = ST0;
				params[1] = cylinder;
				ST0 &= (~0xC0);
				allowTransfer();
				setReadRequired();
			} else {
				if ((command == 0x14) || (command == 0x94)) {
					phase = Phase_Result;
					cmdCount = 1;
					cmdNumber = 0;
					command = CMD_NONE;
					if (command) {
						params[0] = 0x10;
					} else {
						params[0] = 0x00;
					}
					allowTransfer();
					setReadRequired();
				}
			}
		return;

		case CMD_READ_ST3:
			phase = Phase_Result;
			cmdCount = 1;
			params[0] = ST3;
			cmdNumber = 0;
			setReadRequired();
		break;

		case CMD_SEEK_TRACK:
			phase = Phase_Null;
			cmdCount = 0;
			setWriteRequired();
			setNoCommand();
			if (params[1] > geometry.cylCount) {
				command = CMD_NONE;
				ST0 = 0xb10000000;
				debug("Track not found.");
				callSignalEmitByIndex(SIGNAL_EMIT_INT);
				return;
			} else {
				cylinder = params[1];
				callSignalEmitByIndex(SIGNAL_EMIT_INT);
			}
		break;

		case CMD_RECALIBRATE:
			cylinder = 0;
			phase = Phase_Null;
			setWriteRequired();
			setNoCommand();
			callSignalEmitByIndex(SIGNAL_EMIT_INT);
		break;

		case CMD_SET_MODE:
			phase = Phase_Null;
			setWriteRequired();
			setNoCommand();
		break;

		case CMD_PERPENDICULAR:
			phase = Phase_Null;
			setWriteRequired();
			setNoCommand();
		break;

		case CMD_CONFIGURE:
			phase = Phase_Null;
			setWriteRequired();
			setNoCommand();
		break;

		case CMD_READ_SECTOR:
			debug("Read sector");
			phase = Phase_Exec_Read;
			setReadRequired();
			DACK();
		break;

		default:
			debug("Disk: command unknown.");
			command = CMD_NONE;
			phase = Phase_Null;
			setReadRequired();
			return;
	}
	debug("Input done for command ["+CMDS[command]+"]");
	command = CMD_NONE;
}

void ModuleFDD::writeDataByte(byte Value) {
	debug("!Write Data byte [unsupp]");
	Q_UNUSED(Value)
}

void ModuleFDD::receiveByteFromDma(dword , int)
{

}

dword ModuleFDD::sendByteToDma(int) {
	return outputByte;
}

void ModuleFDD::setDmaTerminal()
{
	this->isDmaTerminal = true;
}


/**
 * Read data byte when using no-dma mode or result byte in result phase
 */
dword ModuleFDD::io_readByte(int) {
	setDebugEnabled(true);
	debug("Read byte");
	// Are we reading result or data?
	if (phase == Phase_Result) {	// Result read
		debug("Read result byte");
		byte Value = params[cmdNumber];
		cmdNumber++;
		debug(QString::number(cmdNumber) + " bytes read, " + QString::number(cmdCount) + " bytes total");
		if (cmdNumber == cmdCount) { // Input here is not done
			debug("Result read done.");
			phase = Phase_Null;
			allowTransfer();
			setWriteRequired();
			setNoCommand();
		}
		return Value;
	}

	// Otherwise data byte read
	return 0;
}

/**
 * Load diskette from file
 */
void ModuleFDD::load() {
	setDebugEnabled(true);
	diskFile.setFileName(filename);

	if (diskFile.open(QIODevice::ReadOnly)) {
		switch (diskFile.size()) {
			case 2949120: geometry.cylCount = 80; geometry.headCount = 1; geometry.sectPerTrack = 36; break;
			case 1474560: geometry.cylCount = 80; geometry.headCount = 1; geometry.sectPerTrack = 18; break;
			case 1228800: geometry.cylCount = 80; geometry.headCount = 1; geometry.sectPerTrack = 15; break;
			case 737280:  geometry.cylCount = 80; geometry.headCount = 1; geometry.sectPerTrack = 9; break;
			case 368640: geometry.cylCount = 40; geometry.headCount = 1; geometry.sectPerTrack = 9; break;
			case 327680: geometry.cylCount = 40; geometry.headCount = 1; geometry.sectPerTrack = 8; break;
			case 184320: geometry.cylCount = 40; geometry.headCount = 0; geometry.sectPerTrack = 9; break;
			case 163840: geometry.cylCount = 40; geometry.headCount = 0; geometry.sectPerTrack = 8; break;
			default: {
				geometry.cylCount = 80;
				geometry.headCount = 1;
				geometry.sectPerTrack = 18;
			}
		}
		geometry.totalSize = diskFile.size();
		loaded = true;
		if (this->diskData != nullptr) {
			delete [] diskData;
		}
		diskDataLength = diskFile.size();
		diskData = new byte[diskDataLength];
		diskFile.read((char*)diskData, diskDataLength);

		debug("Loaded disk from file " + filename + " successfully.");
	}
	else {
		loaded = false;
		debug("Error load disk from file " + filename + ".");
	}
	sector = 1;
	head = 0;
	cylinder = 0;
	diskFile.close();
}

void ModuleFDD::eject() {
	this->loaded = false;
}

void ModuleFDD::moduleReset()
{
	DeviceConfig * config = getConfig();
	if (config->hasProperty("filename")) {
		filename = config->getStringProperty("filename");
	} else {
		qDebug() << "Setting filename";
		config->setProperty("filename", "");
	}

	reset();
	load();
}

void ModuleFDD::moduleShutdown() {
	getConfig()->setProperty("filename", this->filename);
}

void ModuleFDD::init() {
	setDebugEnabled(true);
	reset();
}

void ModuleFDD::initIo() {
	registerInput(PORT_IN_WRITE_CONTROL_REGISTER, "Write control register", std::bind(&ModuleFDD::io_writeControlReg, this, std::placeholders::_1, std::placeholders::_2));
	registerInput(
				PORT_IN_WRITE_BYTE,
				"Write byte",
				std::bind(
					&ModuleFDD::io_writeByte,
					this,
					std::placeholders::_1,
					std::placeholders::_2
				)
	);
	registerInput(PORT_IN_DMA, "Receive byte from DMA", std::bind(&ModuleFDD::receiveByteFromDma, this, std::placeholders::_1, std::placeholders::_2));

	registerOutput(PORT_OUT_READ_STATUS, "Read status register", std::bind(&ModuleFDD::io_readStatusReg, this, std::placeholders::_1));
	registerOutput(PORT_OUT_READ_BYTE, "Read byte", std::bind(&ModuleFDD::io_readByte, this, std::placeholders::_1));
	registerOutput(PORT_OUT_DMA, "Send byte to DMA", std::bind(&ModuleFDD::sendByteToDma, this, std::placeholders::_1));

	registerEmitSignalFunction(SIGNAL_EMIT_DMA, "Data ready");
	registerEmitSignalFunction(SIGNAL_EMIT_INT, "Interrupt");
	registerEmitSignalFunction(SIGNAL_EMIT_NO_MORE_DATA, "DMA No more data");

	registerReceiveSignalFunction(SIGNAL_DMA_DACK, "Start Transfer", std::bind(&ModuleFDD::DACK, this));
	registerReceiveSignalFunction(SIGNAL_DMA_TERMINAL, "DMA Count 0", std::bind(&ModuleFDD::setDmaTerminal, this));

	DataMappingBlock blk = std::make_pair(&bufferPtr, &bufferSizePtr);
	registerDataMapProvider(0, "Data buffer", blk);
}

void ModuleFDD::DACK() {
	setDebugEnabled(true);
	debug("DACK");
	if (command == CMD_READ_SECTOR) {
		startReadSector();
	}
	debug("OK Read sector");
	phase = Phase_Result;
	cmdCount = 7;
	params[5] = params[4];
	params[0] = ST0;
	params[1] = ST1;
	params[2] = ST2;
	params[3] = cylinder;
	params[4] = head;
	params[6] = sector;
	cmdNumber = 0;
	callSignalEmitByIndex(SIGNAL_EMIT_INT);
}

/**
 * Reads C:H:S from DiskFile
 * Disk must be loaded into memory using Load() function
 */
void ModuleFDD::startReadSector() {
	setDebugEnabled(true);
	qDebug() << "FDD : start read sector";

	ST1 = 0;
	this->isDmaTerminal = false;
//	dword DataLen
//	dword DataLen = this->myChannelDMA->getInnerCWCR();
//	dword DataLen = this->expectedDataLen;

//	qword expectedDataLen = this->bufferSize;


	byte sectorSizeCode = params[4];
	qword sectorSize = SECTOR_SIZE_TABLE[sectorSizeCode];

	cylinder = params[1];
	head = params[2];
	sector = params[3];

	sectorAddress = cylinder * (geometry.headCount+1) * geometry.sectPerTrack +
					head * geometry.sectPerTrack +
					(sector - 1);
	currentAddress = sectorAddress * sectorSize;

	currentByteInSector = 0;
	maxTransferLength = geometry.sectPerTrack*sectorSize;
	transferredCount = 0;

	qDebug() << "FDD: Calculated address for S=" << sector << " C=" << cylinder << " H=" << head << " is " << sectorAddress;

	do {
		outputByte = diskData[currentAddress];
		callOutputByIndex(PORT_OUT_DMA, 1);
		++currentAddress;
		++transferredCount;
		++currentByteInSector;
		if (currentByteInSector == sectorSize) {
			currentByteInSector = 0;
			++sector;
			if (sector > geometry.sectPerTrack) {
				--sector;
				isDmaTerminal = true;
				qDebug() << "FDD Terminating early: max_sector reached";
			}
		}
	} while (transferredCount < maxTransferLength && !isDmaTerminal);

	qDebug() << "Transferred " << transferredCount << " bytes total; final C=" << cylinder << " H=" << head << " S=" << sector;

	ST0 = (head << 2);
	ST2 = 0;
	ST3 = 0x20 | (params[0] & 4) | (head << 2);

	qDebug() << "ST0 " << bin << ST0 << " ST2 " << bin << ST2 << " ST3 " << bin << ST3;

//	callSignalEmitByIndex(SIGNAL_DMA_DACK);
}

void ModuleFDD::debug(const QString & str) {
	QMap<QString, QString> params;
	params.insert("type", "device");
	params.insert("message", str);
	doDebug(params);
	qDebug() << "FDD: " << str;
}

void ModuleFDD::setWriteRequired() {
	this->regStatus &= BIT_DIO_WRITE_REQUIRED;
}

void ModuleFDD::setReadRequired() {
	this->regStatus |= BIT_DIO_READ_REQUIRED;
}

void ModuleFDD::setBusyCommand() {
	this->regStatus |= BIT_CMD_BUSY_SET;
}

void ModuleFDD::setNoCommand() {
	this->regStatus &= BIT_CMD_BUSY_CLEAR;
}

void ModuleFDD::allowTransfer() {
	this->regStatus |= BIT_ALLOW_TRANSFER_SET;
}

void ModuleFDD::restrictTransfer() {
	this->regStatus &= BIT_ALLOW_TRANSFER_CLEAR;
}

void ModuleFDD::tick() {
	qDebug() << "FDD Tick in thread " << this->thread();
}

void ModuleFDD::reloadConfig() {
	ModuleDevice::reloadConfig();
	auto config = getConfig();
	if (config->hasProperty("filename")) {
		filename = config->getStringProperty("filename");
	} else {
		qDebug() << "Setting filename";
		config->setProperty("filename", "");
	}

	load();
}

ModuleDevice* createModule() {
	auto f = new ModuleFDD();
	Q_ASSERT(dynamic_cast<ModuleFDD*>(f));
	qDebug() << "PTR=" << (void*)f;
	return f;
}

void ModuleFDD::hideUi() {
	emit signalHideGui();
}

void ModuleFDD::createUi() {
	initGui();
}

void ModuleFDD::slotLoadFile(const QString & fileName) {
	this->filename = fileName;
	this->load();
}

void ModuleFDD::slotUpdateStatusInfo() {
	if (!isDebugEnabled())
		return;
	QMap<QString, QString> vals;
	vals.insert("status", this->loaded ? "Not loaded" : "Ready");
	vals.insert("cylinder", QString("%1").arg(this->cylinder));
	vals.insert("head", QString("%1").arg(this->head));
	vals.insert("sector", QString("%1").arg(this->sector));
	vals.insert("filename", this->filename);
	vals.insert("size", QString("%1").arg(this->diskDataLength));
	emit signalUpdateWidget(vals);
}

void ModuleFDD::showUi() {
	emit signalShowGui();
}

void ModuleFDD::initGui()
{
	guiWrapper = new GuiWrapper();
	connect(this, SIGNAL(signalInitGui()), guiWrapper, SLOT(slotInit()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalUpdateWidget(QMap<QString,QString>)), guiWrapper, SIGNAL(signalUpdateStatus(QMap<QString,QString>)), Qt::QueuedConnection);
	connect(this, SIGNAL(signalShowGui()), guiWrapper, SIGNAL(signalShow()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalHideGui()), guiWrapper, SIGNAL(signalHide()), Qt::QueuedConnection);
	connect(guiWrapper, SIGNAL(signalLoadDisk(QString)), this, SLOT(slotLoadFile(QString)), Qt::QueuedConnection);

	connect(guiWrapper, SIGNAL(signalRequestUpdateStatus()), this, SLOT(slotUpdateStatusInfo()), Qt::QueuedConnection);

	emit signalInitGui();
}

