#ifndef GUIWRAPPER_H
#define GUIWRAPPER_H

#include <QString>
#include <QMap>
#include <QObject>
#include <QWidget>

class DiskStatusForm;

class GuiWrapper : public QObject
{
	Q_OBJECT
public:
	explicit GuiWrapper(QObject *parent = 0);
	virtual ~GuiWrapper();

signals:
	void signalShow();
	void signalHide();
	void signalUpdateStatus(QMap<QString,QString> data);
	void signalRequestUpdateStatus();

	void signalLoadDisk(const QString & filename);

public slots:
	void slotInit();

private:
	DiskStatusForm* statusWidget = nullptr;
	QTimer* refreshTimer = nullptr;

public slots:
};

#endif // GUIWRAPPER_H
