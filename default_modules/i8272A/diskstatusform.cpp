#include "diskstatusform.h"
#include "ui_diskstatusform.h"

#include <QFileDialog>

DiskStatusForm::DiskStatusForm(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::DiskStatusForm)
{
	ui->setupUi(this);

}

DiskStatusForm::~DiskStatusForm()
{
	delete ui;
}

void DiskStatusForm::slotUpdateInfo(QMap<QString, QString> params) {
	ui->labelCylinder->setText(params.value("cylinder"));
	ui->labelHead->setText(params.value("head"));
	ui->labelSector->setText(params.value("sector"));
	ui->labelSize->setText(params.value("size"));
	ui->labelStatus->setText(params.value("status"));
	ui->editFileName->setText(params.value("filename"));
}

void DiskStatusForm::on_buttonOpenFile_clicked() {
	QFileDialog dlg;
	dlg.setFileMode(QFileDialog::FileMode::ExistingFile);
	dlg.setAcceptMode(QFileDialog::AcceptMode::AcceptOpen);
	dlg.setNameFilter("Any disk files (*.*)");
	if (dlg.exec()) {
		emit signalLoadDisk(dlg.selectedFiles().first());
	}
}
