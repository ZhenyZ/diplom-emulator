#ifndef DISKSTATUSFORM_H
#define DISKSTATUSFORM_H

#include <QWidget>
#include <QMap>
#include <QString>

namespace Ui {
	class DiskStatusForm;
}

class DiskStatusForm : public QWidget
{
	Q_OBJECT

public:
	explicit DiskStatusForm(QWidget *parent = 0);
	~DiskStatusForm();

public slots:
	void slotUpdateInfo(QMap<QString,QString> params);

signals:
	void signalLoadDisk(const QString & filename);

private slots:
	void on_buttonOpenFile_clicked();

private:
	Ui::DiskStatusForm *ui;
};

#endif // DISKSTATUSFORM_H
