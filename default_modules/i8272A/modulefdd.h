#ifndef CONTROLLERFDD_H
#define CONTROLLERFDD_H

#include <QObject>
#include <QFile>
#include <QByteArray>
#include <QDebug>

#include <moduledevice.h>
#include <loggable.h>

#include "i8272a_global.h"

class GuiWrapper;

class I8272ASHARED_EXPORT ModuleFDD : public Loggable, public ModuleDevice
{
	Q_OBJECT
public:

	static constexpr dword
		BIT_ALLOW_TRANSFER_SET = 0b10000000,
		BIT_ALLOW_TRANSFER_CLEAR = ~BIT_ALLOW_TRANSFER_SET,
		BIT_CMD_BUSY_SET = 0b00010000,
		BIT_CMD_BUSY_CLEAR = ~BIT_CMD_BUSY_SET,
		BIT_DIO_WRITE_REQUIRED = 0b10111111,
		BIT_DIO_READ_REQUIRED = ~BIT_DIO_WRITE_REQUIRED,
		BIT_DMA_SET = 0b00100000,
		BIT_DMA_CLEAR = ~BIT_DMA_SET;


	ModuleFDD(QObject* parent = nullptr);
	virtual ~ModuleFDD();

	virtual void tick() override;

	virtual void reloadConfig() override;

	void io_writeControlReg(dword Value, int);
	dword io_readStatusReg(int);
	void io_writeByte(dword Value, int);
	dword io_readByte(int);

	void load();
	void eject();

	virtual void moduleReset() override;
	virtual void moduleShutdown() override;

	virtual void debug(const QString & str) override;

	virtual void showUi() override;
	virtual void hideUi() override;
	virtual void createUi() override;

	void receiveByteFromDma(dword value, int);
	dword sendByteToDma(int);

signals:
	void signalShowGui();
	void signalHideGui();
	void signalInitGui();
	void signalUpdateWidget(QMap<QString,QString>);

public slots:
	void slotLoadFile(const QString & fileName);
	void slotUpdateStatusInfo();

protected:


private:

	qword bufferSize = 0;
	byte* buffer = nullptr;

	ModuleMemPtr bufferPtr = nullptr;
	qword* bufferSizePtr = nullptr;

	byte* diskData = nullptr;
	qword diskDataLength = 0;

	QString filename;

	void setWriteRequired();
	void setReadRequired();
	void setBusyCommand();
	void setNoCommand();
	void allowTransfer();
	void restrictTransfer();

	static constexpr int
		SIGNAL_EMIT_INT = 0,
		SIGNAL_EMIT_DMA = 1,
		SIGNAL_EMIT_NO_MORE_DATA = 2,

		SIGNAL_DMA_DACK = 0,
		SIGNAL_DMA_TERMINAL = 1,

		PORT_IN_WRITE_CONTROL_REGISTER = 0,
		PORT_IN_WRITE_BYTE = 1,
		PORT_IN_DMA = 2,

		PORT_OUT_READ_STATUS = 0,
		PORT_OUT_READ_BYTE = 1,
		PORT_OUT_DMA = 2;

	enum PhaseStatus {
		Phase_Null = 0,
		Phase_Order = 1,
		Phase_Exec_Read = 2,
		Phase_Exec_Write = 3,
		Phase_Exec_Seek = 4,
		Phase_Result = 5};

	virtual void DACK();

	int dma_length;
	byte* dma_data;
	byte outputByte;

	void init();
	void initIo();

	void startReadSector();
	void finishReadSector();

	byte regStatus,
		 regData,
		 workAllow,
		 useDMA,
		 ST0,
		 ST1,
		 ST2,
		 ST3,
		 motorON;

	PhaseStatus phase;		// order, exec, result
	byte command,	// command name
		 lastCommand,
		 cmdNumber,	// current byte index
		 cmdCount;	// current command params count
	byte params[20],	// command bytes themself

		 cylinder,	//
		 head,		//	Current
		 sector;		//

//	ControllerDMA*	myDMA;
//	ChannelDMA*		myDMAChannel;
//	ControllerPIC*	myPIC;

	bool loaded = false;

	void reset();
	void writeOrderByte(byte Value);
	void writeDataByte(byte Value);

	void setDmaTerminal();

	bool isDmaTerminal = false;

	void initGui();


	QFile diskFile;

	struct {
		int  headCount,
			 cylCount,
			 sectPerTrack;
		long totalSize;
	} geometry;

	const dword
		BufferLimit = 5338192;

	const short
		SECTOR_SIZE_TABLE[4] =
			{ 128, 256, 512, 1024 };

	static constexpr byte
		CMD_NONE			= 0xFF,
		CMD_READ_TRACK		= 0x02,
		CMD_SET_MODE		= 0x03,
		CMD_READ_ST3		= 0x04,
		CMD_WRITE_SECTOR	= 0x05,
		CMD_READ_SECTOR		= 0x06,
		CMD_RECALIBRATE		= 0x07,
		CMD_READ_ST0		= 0x08,
		CMD_FORMAT_TRACK	= 0x0D,
		CMD_SEEK_TRACK		= 0x0F,
		CMD_PERPENDICULAR	= 0x12,
		CMD_CONFIGURE		= 0x13
	;

	const QString
		CMDS[64] = {
		"UNKNOWN",
		"UNKNOWN",
		"Read track",
		"Set Mode",
		"Read ST3",
		"Write sector",
		"Read sector",
		"Recalibrate",
		"Read ST0",
		"UNKNOWN",
		"UNKNOWN",
		"UNKNOWN",
		"UNKNOWN",
		"Format track",
		"UNKNOWN",
		"Seek track",
		"UNKNOWN",	//x10
		"UNKNOWN",
		"Perpendicular mode",
		"Configure",
		"LOCK",
		{""}
		};

	// count of command bytes for each command
	// corresponding to its index
	const byte CmdInputCount[64] = {
		0, 0, 8, 2, 1, 8, 8, 1, 2, 0, 0, 0, 0, 5, 0, 2,
		0, 0, 1, 3
	};

	qword currentByteInSector = 0;
	qword maxTransferLength = 0;
	qword sectorAddress = 0;
	qword currentAddress = 0;
	qword transferredCount = 0;

	GuiWrapper * guiWrapper = nullptr;

};

extern "C" I8272ASHARED_EXPORT ModuleDevice* createModule();

#endif // CONTROLLERFDD_H
