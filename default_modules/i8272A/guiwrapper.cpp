#include "guiwrapper.h"
#include "diskstatusform.h"

#include <QApplication>
#include <QTimer>

GuiWrapper::GuiWrapper(QObject *parent) : QObject(parent)
{
	this->moveToThread(QApplication::instance()->thread());
}

GuiWrapper::~GuiWrapper() {
	delete refreshTimer;
	delete statusWidget;
}

void GuiWrapper::slotInit() {
	refreshTimer = new QTimer();
	refreshTimer->setSingleShot(false);
	refreshTimer->setInterval(250);

	connect(refreshTimer, SIGNAL(timeout()), this, SIGNAL(signalRequestUpdateStatus()), Qt::QueuedConnection);

	statusWidget = new DiskStatusForm();
	connect(this, SIGNAL(signalHide()), statusWidget, SLOT(hide()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalShow()), statusWidget, SLOT(show()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalUpdateStatus(QMap<QString,QString>)), statusWidget, SLOT(slotUpdateInfo(QMap<QString,QString>)), Qt::QueuedConnection);
	connect(statusWidget, SIGNAL(signalLoadDisk(QString)), this, SIGNAL(signalLoadDisk(QString)), Qt::QueuedConnection);

	refreshTimer->start();
}
