QT += gui widgets

TARGET = i8272a
TEMPLATE = lib
CONFIG += c++11

DEFINES += I8272A_LIBRARY

INCLUDEPATH += $$PWD/../lib/

SOURCES += \ 
    modulefdd.cpp \
    diskstatusform.cpp \
    guiwrapper.cpp


HEADERS += \ 
    modulefdd.h \
    i8272a_global.h \
    diskstatusform.h \
    guiwrapper.h

CONFIG(debug, debug|release) {
	LIBS += -L$$PWD/../lib/debug -lEMUESDebugLib -lEMUESDevicesLib
}
CONFIG(release, debug|release) {
	LIBS += -L$$PWD/../lib/release -lEMUESDebugLib -lEMUESDevicesLib
}

FORMS += \
    diskstatusform.ui
