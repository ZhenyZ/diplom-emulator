#ifndef I8272A_GLOBAL_H
#define I8272A_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(I8272A_LIBRARY)
#  define I8272ASHARED_EXPORT Q_DECL_EXPORT
#else
#  define I8272ASHARED_EXPORT Q_DECL_IMPORT
#endif

#endif 
