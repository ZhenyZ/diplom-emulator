QT       -= gui

TARGET = i440lx
TEMPLATE = lib

INCLUDEPATH += $$PWD/../lib/

CONFIG(debug, debug|release) {
	LIBS += -L$$PWD/../lib/debug -lEMUESDebugLib -lEMUESDevicesLib
}
CONFIG(release, debug|release) {
	LIBS += -L$$PWD/../lib/release -lEMUESDebugLib -lEMUESDevicesLib
}

DEFINES += I440LX_LIBRARY

SOURCES += i440lx.cpp \
    sis20496.cpp \
    devicepci.cpp

HEADERS += i440lx.h\
        i440lx_global.h \
    sis20496.h \
    devicepci.h

