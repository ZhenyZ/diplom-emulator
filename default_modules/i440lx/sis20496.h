#ifndef SIS20496_H
#define SIS20496_H

#include "devicepci.h"

class sis20496 : public DevicePCI
{
public:
	sis20496();
	virtual ~sis20496();

	virtual void readValue();
	virtual void writeValue();


	struct DeviceHeader {
		DefaultPciHeader defaultHeader;
		byte cpuConfig;
		byte dramConfig;
		word cacheConfig;
		word shadowConfig;
		byte cacheableControl;
		byte C496AddressDecoder;
		byte dramBoundary[8];
		word exclusive0;
		word exclusive1;
		word exclusive2;
		byte pciKeybConfig;
		byte outPinConfig;
		word ideVesaBusConfig;
		byte sramRemapConfig;
		byte ioTrapsConfig;
		word ioTrapBase0;
		word ioTrapBase1;
		word ide0config;
		word ide1config;
		word exclusive3;
		byte edoDramConfig;
		byte miscControl;
		word assymDramConfig;
	} deviceHeader;

	union {
		DeviceHeader header;
		byte rawData[256];
	} dataSpace;

	QString getRegName();

private:
	void debug(const QString & str);

};

#endif // SIS20496_H
