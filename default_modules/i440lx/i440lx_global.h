#ifndef I440LX_GLOBAL_H
#define I440LX_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(I440LX_LIBRARY)
#  define I440LXSHARED_EXPORT Q_DECL_EXPORT
#else
#  define I440LXSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // I440LX_GLOBAL_H
