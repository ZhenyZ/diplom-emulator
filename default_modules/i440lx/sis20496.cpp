#include "sis20496.h"
#include <QDebug>

sis20496::sis20496()
	: DevicePCI()
{
	deviceHeader.defaultHeader.vendorId = 0x8086;
	deviceHeader.defaultHeader.deviceId = 0x0496;
	deviceHeader.defaultHeader.commandReg = 0x0007;
	deviceHeader.defaultHeader.statusReg = 0x0280;
	deviceHeader.defaultHeader.revisionId = 0x02;
	deviceHeader.defaultHeader.devClassCodeLo = 0x06;
	deviceHeader.defaultHeader.devClassCodeMd = 0x00;
	deviceHeader.defaultHeader.devClassCodeHi = 0x00;
	deviceHeader.defaultHeader.reserved0C0D = 0;
	deviceHeader.defaultHeader.devHeaderType = 0x00;
	deviceHeader.defaultHeader.bar0 = 0;
	deviceHeader.defaultHeader.bar1 = 0;
	deviceHeader.defaultHeader.bar2 = 0;
	deviceHeader.defaultHeader.bar3 = 0;
	deviceHeader.defaultHeader.bar4 = 0;
	deviceHeader.defaultHeader.bar5 = 0;
	deviceHeader.defaultHeader.BIST = 0;
	deviceHeader.defaultHeader.cardbusCISPointer = 0;
	deviceHeader.defaultHeader.capPointer = 0;
	deviceHeader.defaultHeader.expansionRomBaseAddress = 0;
	deviceHeader.defaultHeader.intLine = 0;
	deviceHeader.defaultHeader.intPin = 0;
	deviceHeader.defaultHeader.subsystemId = 0;
	deviceHeader.defaultHeader.subsystemVendorId = 0;

	deviceHeader.cpuConfig = 0x00;
	deviceHeader.dramConfig = 0x00;
	deviceHeader.cacheConfig = 0x0000;
	deviceHeader.shadowConfig = 0x0000;
	deviceHeader.cacheableControl = 0x00;
	deviceHeader.C496AddressDecoder = 0x00;
	deviceHeader.dramBoundary[8] = {0};
	deviceHeader.exclusive0 = 0x0000;
	deviceHeader.exclusive1 = 0x0000;
	deviceHeader.exclusive2 = 0x0000;
	deviceHeader.pciKeybConfig = 0x00;
	deviceHeader.outPinConfig = 0x00;
	deviceHeader.ideVesaBusConfig = 0x0000;
	deviceHeader.sramRemapConfig = 0x00;
	deviceHeader.ioTrapsConfig = 0x00;
	deviceHeader.ioTrapBase0 = 0x0000;
	deviceHeader.ioTrapBase1 = 0x0000;
	deviceHeader.ide0config = 0x0000;
	deviceHeader.ide1config = 0x0000;
	deviceHeader.exclusive3 = 0x0000;
	deviceHeader.edoDramConfig = 0x00;
	deviceHeader.miscControl = 0x00;
	deviceHeader.assymDramConfig = 0x0000;
	dataSpace.header = deviceHeader;
}

sis20496::~sis20496() {

}



void sis20496::readValue() {
	if (funcNo == 0) {
		data = *reinterpret_cast<word*>(dataSpace.rawData + regNo);
		if (regNo < 0x40) {
			qDebug() << "Read " << getRegNameGeneric() << ": " << hex << data;
		} else {
			qDebug() << "Read " << getRegName() << ": " << hex << data;
		}
	} else {
		debug("Unknown read func" + QString::number(funcNo, 16));
	}
}

void sis20496::writeValue() {
	if (funcNo == 0) {
		*reinterpret_cast<dword*>(dataSpace.rawData + regNo) = data;
//		dataSpace.rawData[regNo] = data;
		if (regNo < 0x40) {
			qDebug() << "Write " << getRegNameGeneric() << ": " << hex << data;
		} else {
			qDebug() << "Write " << getRegName() << ": " << hex << data;
		}
	} else {
		debug("Unknown read func" + QString::number(funcNo, 16));
	}
}

QString sis20496::getRegName() {
	switch (regNo) {
		case 0x40:
			return "CpuConfig";
		break;
		case 0x41:
			return "DramConfig";
		break;
		case 0x42:
		case 0x43:
			return "CacheConfig";
		break;
		case 0x44:
		case 0x45:
			return "ShadowConfig";
		break;
		case 0x46:
			return "CacheableControl";
		break;
		case 0x47:
			return "Decoder";
		break;
	}
	return "hz";
}

void sis20496::debug(const QString & str)
{
	qDebug() << "SIS Host: " << str;
}
