#include "devicepci.h"

#include <QDebug>

DevicePCI::DevicePCI()
{
	data = 0;
	funcNo = 0;
	regNo = 0;
}

dword DevicePCI::io_readData(int sizeInBytes) {
	readValue();
	qDebug() << "PCI: data " << " size " << sizeInBytes << " = " << data;
	if (sizeInBytes == 4) {
		return data;
	}
	if (sizeInBytes == 2) {
		return (data & 0xFFFF0000) >> 16;
	}
	return (data & 0xFF) >> 24;
}

dword DevicePCI::io_readDataOffs8(int sizeInBytes) {
	readValue();
	if (sizeInBytes == 2) {
		qDebug() << "PCI: data+8 " << " size " << sizeInBytes << " = " << ((data & 0xFFFF00) >> 8);
		return (data & 0x00FFFF00) >> 8;
	}
	qDebug() << "PCI: data+8 " << " size " << sizeInBytes << " = " << ((data & 0xFF) >> 16);
	return (data & 0xFF) >> 16;
}

dword DevicePCI::io_readDataOffs16(int sizeInBytes) {
	readValue();
	if (sizeInBytes == 2) {
		qDebug() << "PCI: data+16 " << " size " << sizeInBytes << " = " << (data & 0xFFFF);
		return (data & 0x0000FFFF);
	}
	qDebug() << "PCI: data+16 " << " size " << sizeInBytes << " = " << ((data & 0xFF00) >> 16);
	return (data & 0xFF00) >> 8;
}

dword DevicePCI::io_readDataOffs24(int sizeInBytes) {
	readValue();
	qDebug() << "PCI: data+24 " << " size " << sizeInBytes << " = " << (data & 0xFF);
	return (data & 0xFF);
}

void DevicePCI::io_writeData(dword d, int sizeInBytes) {
	if (sizeInBytes == 4) {
		data = d;
	} else
	if (sizeInBytes == 2) {
		data &= 0x0000FFFF;
		data |= ((word)(d & 0xFFFF) << 16);
	} else
	if (sizeInBytes == 1) {
		data &= 0x00FFFFFF;
		data |= ((byte)(d & 0xFF) << 24);
	}
	writeValue();
	qDebug() << "PCI: Write data " << hex << d << " size " << sizeInBytes;
}

void DevicePCI::io_writeDataOffs8(dword d, int sizeInBytes) {
	if (sizeInBytes == 2) {
		data &= 0xFF0000FF;
		data |= ((word)(d & 0xFFFF) << 8);
	} else
	if (sizeInBytes == 1) {
		data &= 0xFF00FFFF;
		data |= ((byte)(d & 0xFF) << 16);
	}
	writeValue();
	qDebug() << "PCI: Write data+8 " << hex << d << " size " << sizeInBytes;
}

void DevicePCI::io_writeDataOffs16(dword d, int sizeInBytes) {
	if (sizeInBytes == 2) {
		data &= 0xFFFF0000;
		data |= ((word)(d & 0xFFFF));
	} else
	if (sizeInBytes == 1) {
		data &= 0xFFFF00FF;
		data |= ((byte)(d & 0xFF) << 8);
	}
	writeValue();
	qDebug() << "PCI: Write data+16 " << hex << d << " size " << sizeInBytes;
}

void DevicePCI::io_writeDataOffs24(dword d, int sizeInBytes) {
	if (sizeInBytes == 1) {
		data &= 0xFFFFFF00;
		data |= ((byte)(d & 0xFF));
	}
	writeValue();
	qDebug() << "PCI: Write data+24 " << hex << d << " size " << sizeInBytes;
}

QString DevicePCI::getRegNameGeneric()
{
	switch (regNo) {
		case 0x00:
		case 0x01:
			return "VID";
		break;
		case 0x02:
		case 0x03:
			return "Device ID";
		break;
		case 0x04:
		case 0x05:
			return "CommandReg";
		break;
		case 0x06:
		case 0x07:
			return "StatusReg";
		break;
		case 0x08:
			return "Revision";
		break;
		case 0x09:
		case 0x0A:
		case 0x0B:
			return "devClassCode";
		break;
		case 0x0E:
			return "HeaderType";
		break;
		case 0x0F:
			return "BIST";
		break;
		case 0x10:
		case 0x11:
		case 0x12:
		case 0x13:
			return "BAR0";
		break;
		case 0x14:
		case 0x15:
		case 0x16:
		case 0x17:
			return "BAR1";
		break;
		case 0x18:
		case 0x19:
		case 0x1A:
		case 0x1B:
			return "BAR2";
		break;
		case 0x1C:
		case 0x1D:
		case 0x1E:
		case 0x1F:
			return "BAR3";
		break;
		case 0x20:
		case 0x21:
		case 0x22:
		case 0x23:
			return "BAR4";
		break;
		case 0x24:
		case 0x25:
		case 0x26:
		case 0x27:
			return "BAR5";
		break;
		case 0x28:
		case 0x29:
		case 0x2A:
		case 0x2B:
			return "BACardbus CIS pointer";
		break;
		case 0x2C:
		case 0x2D:
			return "Subsystem VENID";
		break;
		case 0x2E:
		case 0x2F:
			return "Subsystem ID";
		break;
		case 0x30:
		case 0x31:
		case 0x32:
		case 0x33:
			return "Expansion ROM base address";
		break;
		case 0x34:
			return "Capabilities ptr";
		break;
		case 0x3C:
			return "Interrut Line";
		break;
		case 0x3D:
			return "Interrupt Pin";
		break;
		case 0x3E:
			return "Min_Gnt";
		break;
		case 0x3F:
			return "Max_Lat";
		break;
	}
	return "hz";
}
