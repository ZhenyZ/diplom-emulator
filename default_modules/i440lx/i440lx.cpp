#include "i440lx.h"
#include <QDebug>
#include "sis20496.h"

I440lx::I440lx(QObject * parent)
	: Loggable("440lx", parent),
	ModuleDevice("i440lx PAC", "Eugene Shestakov", 10001)
{
	host = new sis20496();
	init();
	ioAddressIndex = 0;
	ioDataIndex = 0;
	currentDevice.dev = nullptr;
}

void I440lx::init() {
	registerInput(0, "Write Addr", std::bind(&I440lx::io_writeAddress, this, std::placeholders::_1, std::placeholders::_2));
	registerInput(1, "Write Data 32", std::bind(&I440lx::io_writeData, this, std::placeholders::_1, std::placeholders::_2));
	registerInput(2, "Write Data+8", std::bind(&I440lx::io_writeDataOffs8, this, std::placeholders::_1, std::placeholders::_2));
	registerInput(3, "Write Data+16", std::bind(&I440lx::io_writeDataOffs16, this, std::placeholders::_1, std::placeholders::_2));
	registerInput(4, "Write Data+24", std::bind(&I440lx::io_writeDataOffs24, this, std::placeholders::_1, std::placeholders::_2));
	registerOutput(0, "Read Addr", std::bind(&I440lx::io_readAddress, this, std::placeholders::_1));
	registerOutput(1, "Read Data 32", std::bind(&I440lx::io_readData, this, std::placeholders::_1));
	registerOutput(2, "Read Data+8", std::bind(&I440lx::io_readDataOffs8, this, std::placeholders::_1));
	registerOutput(3, "Read Data+16", std::bind(&I440lx::io_readDataOffs16, this, std::placeholders::_1));
	registerOutput(4, "Read Data+24", std::bind(&I440lx::io_readDataOffs24, this, std::placeholders::_1));
	registerEmitSignalFunction(SIGNAL_RESET_HARD, "Hard reset signal");
	registerEmitSignalFunction(SIGNAL_RESET_SOFT, "Soft reset signal");
	registerEmitSignalFunction(SIGNAL_RESET_SYSTEM, "Total system reset signal");
}

ModuleDevice * createModule() {
	return new I440lx();
}


I440lx::~I440lx() {
}


dword I440lx::io_readAddress(int) {
	return address;
}

void I440lx::io_writeAddress(dword d, int) {
	address = d;
	currentDevice = getDeviceInfo();
}

dword I440lx::io_readData(int sizeInBytes) {
	if (currentDevice.dev != nullptr) {
		return currentDevice.dev->io_readData(sizeInBytes);
	}
	qDebug() << "Read from unknown device";
	return 0xFFFFFFFF;
}

dword I440lx::io_readDataOffs8(int sizeInBytes) {
	if (currentDevice.dev != nullptr) {
		return currentDevice.dev->io_readDataOffs8(sizeInBytes);
	}
	qDebug() << "Read from unknown device";
	return 0xFFFFFFFF;
}

dword I440lx::io_readDataOffs16(int sizeInBytes) {
	if (currentDevice.dev != nullptr) {
		return currentDevice.dev->io_readDataOffs16(sizeInBytes);
	}
	qDebug() << "Read from unknown device";
	return 0xFFFFFFFF;
}

dword I440lx::io_readDataOffs24(int sizeInBytes) {
	if (currentDevice.dev != nullptr) {
		return currentDevice.dev->io_readDataOffs24(sizeInBytes);
	}
	qDebug() << "Read from unknown device";
	return 0xFFFFFFFF;
}

void I440lx::io_writeData(dword d, int sizeInBytes) {
	if (currentDevice.dev != nullptr) {
		return currentDevice.dev->io_writeData(d, sizeInBytes);
	}
	qDebug() << "Write to unknown device";
}

void I440lx::io_writeDataOffs8(dword d, int sizeInBytes) {
	if (currentDevice.dev != nullptr) {
		return currentDevice.dev->io_writeDataOffs8(d, sizeInBytes);
	}
	qDebug() << "Write to unknown device";
}

void I440lx::io_writeDataOffs16(dword d, int sizeInBytes) {
	if (currentDevice.dev != nullptr) {
		return currentDevice.dev->io_writeDataOffs16(d, sizeInBytes);
	}
	qDebug() << "Write to unknown device";
}

void I440lx::io_writeDataOffs24(dword d, int sizeInBytes) {
	if (currentDevice.dev != nullptr) {
		return currentDevice.dev->io_writeDataOffs24(d, sizeInBytes);
	}
	qDebug() << "Write to unknown device";
}

void I440lx::io_writeResetControl(dword, int) {

}

void I440lx::debug(const QString & str) {
	QMap<QString, QString> params;
	params.insert("type", "device");
	params.insert("message", str);
	doDebug(params);
}

void I440lx::moduleReset() {
	currentDevice.busNumber = 0;
	currentDevice.configEnable = 0;
	currentDevice.deviceNumber = 0;
	currentDevice.functionNumber = 0;
	currentDevice.registerNumber = 0;
}

void I440lx::moduleShutdown() {
	delete host;
}

I440lx::DeviceInfo I440lx::getDeviceInfo() {
	DeviceInfo result;
	result.busNumber = ((address & BIT_BUS_NUMBER) >> BIT_BUS_NUMBER_SHIFT);
	result.configEnable = (address & BIT_CONFIG_ENABLE);
	result.deviceNumber = ((address & BIT_DEVICE_NUMBER) >> BIT_DEVICE_NUMBER_SHIFT);
	result.functionNumber = ((address & BIT_FUNCTION_NUMBER) >> BIT_FUNCTION_NUMBER_SHIFT);
	result.registerNumber = ((address & BIT_REGISTER_NUMBER) >> BIT_REGISTER_NUMBER_SHIFT);

	qDebug() << "Select Device: Bus=" << hex << result.busNumber << ", Dev=" << hex << result.deviceNumber
			 << ", Func=" << hex << result.functionNumber << ", Reg=" << hex << result.registerNumber;
	if (result.busNumber == 0) {
		if (result.deviceNumber == 0) {
			qDebug() << "Select HOST device";
			result.dev = host;
			host->setFunction(result.functionNumber);
			host->setRegister(result.registerNumber);
		} else {
			result.dev = nullptr;
			data = 0xFFFFFFFF;
		}
	} else {
		data = 0xFFFFFFFF;
		result.dev = nullptr;
	}

	return result;
}

void I440lx::tick() {

}
