#ifndef I440LX_H
#define I440LX_H

#include "i440lx_global.h"
#include <moduledevice.h>
#include <loggable.h>

class sis20496;
class DevicePCI;

class I440LXSHARED_EXPORT I440lx : public Loggable, public ModuleDevice
{
	Q_OBJECT
public:

	static constexpr quint64
		SIGNAL_RESET_SOFT = 0,
		SIGNAL_RESET_HARD = 1,
		SIGNAL_RESET_SYSTEM = 2;

	I440lx(QObject * parent = nullptr);
	virtual ~I440lx();

	dword io_readAddress(int sizeInBytes);
	void io_writeAddress(dword, int sizeInBytes);

	dword io_readData(int sizeInBytes);
	dword io_readDataOffs8(int sizeInBytes);
	dword io_readDataOffs16(int sizeInBytes);
	dword io_readDataOffs24(int sizeInBytes);
	void io_writeData(dword, int sizeInBytes);
	void io_writeDataOffs8(dword, int sizeInBytes);
	void io_writeDataOffs16(dword, int sizeInBytes);
	void io_writeDataOffs24(dword, int sizeInBytes);
	void io_writeResetControl(dword, int);

	void init();

	virtual void tick() override;
	virtual void debug(const QString &str) override;
	virtual void moduleReset() override;
	virtual void moduleShutdown() override;

	struct DeviceInfo {
		dword busNumber;
		dword deviceNumber;
		dword functionNumber;
		dword registerNumber;
		dword configEnable;
		DevicePCI * dev;
	};

private:

	int ioAddressIndex;
	int ioDataIndex;
	dword address;
	dword data;

	static constexpr dword
		BIT_CONFIG_ENABLE = 0x80000000,
		BIT_BUS_NUMBER = 0x00FF0000,
		BIT_BUS_NUMBER_SHIFT = 16,
		BIT_DEVICE_NUMBER = 0x0000F800,
		BIT_DEVICE_NUMBER_SHIFT = 11,
		BIT_FUNCTION_NUMBER = 0x00000700,
		BIT_FUNCTION_NUMBER_SHIFT = 8,
		BIT_REGISTER_NUMBER = 0x000000FC,
		BIT_REGISTER_NUMBER_SHIFT = 2;

	DeviceInfo getDeviceInfo();
	DeviceInfo currentDevice;

	sis20496 * host = nullptr;

};

extern "C" I440LXSHARED_EXPORT ModuleDevice * createModule();


#endif // I440LX_H
