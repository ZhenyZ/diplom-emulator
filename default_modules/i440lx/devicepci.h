#ifndef DEVICEPCI_H
#define DEVICEPCI_H

#include <constants.h>

class DevicePCI
{
public:
	DevicePCI();

	struct DefaultPciHeader {
		word vendorId;
		word deviceId;
		word commandReg;
		word statusReg;
		byte revisionId;
		byte devClassCodeLo;
		byte devClassCodeMd;
		byte devClassCodeHi;
		word reserved0C0D;
		byte devHeaderType;
		byte BIST;
		dword bar0;
		dword bar1;
		dword bar2;
		dword bar3;
		dword bar4;
		dword bar5;
		dword cardbusCISPointer;
		word subsystemVendorId;
		word subsystemId;
		dword expansionRomBaseAddress;
		byte capPointer;
		byte reserved3537[3];
		dword reserved383B;
		byte intLine;
		byte intPin;
		byte min_Gnt;
		byte max_Lat;
	};

	void setFunction(dword funcNo) {
		this->funcNo = funcNo;
	}

	void setRegister(dword regNo) {
		this->regNo = regNo;
	}

	virtual void readValue() = 0;
	virtual void writeValue() = 0;

	dword io_readData(int sizeInBytes);
	dword io_readDataOffs8(int sizeInBytes);
	dword io_readDataOffs16(int sizeInBytes);
	dword io_readDataOffs24(int sizeInBytes);
	void io_writeData(dword, int sizeInBytes);
	void io_writeDataOffs8(dword, int sizeInBytes);
	void io_writeDataOffs16(dword, int sizeInBytes);
	void io_writeDataOffs24(dword, int sizeInBytes);

protected:
	dword funcNo;
	dword regNo;

	dword data;

	QString getRegNameGeneric();

};

#endif // DEVICEPCI_H
