QT += core gui widgets

CONFIG += c++14

TARGET = i486cpu
TEMPLATE = lib

DEFINES += CPU_I486_LIBRARY

INCLUDEPATH += $$PWD/../lib/


SOURCES += \ 
    ui/cpuguiwrapper.cpp \
    ui/status.cpp \
    cpuintelx86.cpp \
    cpuintelx86_alu.cpp \
    cpuintelx86_decoder.cpp \
    cpuintelx86data.cpp \
    cpuintelx86debug.cpp \
    cpuintelx86instructions.cpp \
    cpuintelx86protection.cpp \
    cpuintelx86stack.cpp \
    cpuintelx86strings.cpp \
    cpuintelx86transfers.cpp \
    loggerintelx86.cpp \
    intel387x.cpp \
    cpuintelx86_alu_shifts.cpp \
    cpuintelx86_alu_logical.cpp \
    cpuintelx86_alu_arithm.cpp \
    cpuintelx86_alu_templates.cpp


CONFIG(debug, debug|release) {
	LIBS += -L$$PWD/../lib/debug -lEMUESDebugLib -lEMUESDevicesLib
}
CONFIG(release, debug|release) {
	LIBS += -L$$PWD/../lib/release -lEMUESDebugLib -lEMUESDevicesLib
}


HEADERS += cpuintelx86_global.h \
    ui/cpuguiwrapper.h \
    ui/status.h \
    cpuintelx86.h \
    cpuintelx86_alu.h \
    cpuintelx86_decoder.h \
    cpuintelx86_types.h \
    loggerintelx86.h \
    intel387x.h

FORMS += ui/status.ui


