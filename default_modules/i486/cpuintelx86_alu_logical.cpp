#include "cpuintelx86_alu.h"

template<typename T>
T CPUIntelx86_ALU::Xor(T op1, T op2) {
	flags->eflags.flag32 &= CLEAR_OC;
	T result = (op1 ^ op2);
	setFlagsSZP<T>(result);
	return result;
}

template<typename T>
T CPUIntelx86_ALU::Or(T op1, T op2) {
	flags->eflags.flag32 &= CLEAR_OC;
	T result = (op1 | op2);
	setFlagsSZP<T>(result);
	return result;
}

template<typename T>
T CPUIntelx86_ALU::And(T op1, T op2) {
	flags->eflags.flag32 &= CLEAR_OC;
	T result = (op1 & op2);
	setFlagsSZP<T>(result);
	return result;
}
