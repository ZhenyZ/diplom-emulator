#ifndef CPUIntelx86_ALU_TEMPLATES
#define CPUIntelx86_ALU_TEMPLATES

#include "cpuintelx86_alu.h"

template<>
inline word CPUIntelx86_ALU::getCFMask() {
	return 0xFF00;
}

template <>
inline dword CPUIntelx86_ALU::getCFMask() {
	return 0xFFFF0000;
}

template <>
inline qword CPUIntelx86_ALU::getCFMask() {
	return 0xFFFFFFFF00000000;
}

template<>
inline word CPUIntelx86_ALU::getTypeMask() {
	return 0xFFFF;
}

template<>
inline dword CPUIntelx86_ALU::getTypeMask() {
	return 0xFFFFFFFF;
}

template<>
inline byte CPUIntelx86_ALU::getTypeMask() {
	return 0xFF;
}


template<>
inline byte CPUIntelx86_ALU::getMostSignificantBitValue() {
	return 0x80;
}

template<>
inline word CPUIntelx86_ALU::getMostSignificantBitValue() {
	return 0x8000;
}

template<>
inline dword CPUIntelx86_ALU::getMostSignificantBitValue() {
	return 0x80000000;
}

template<>
inline byte CPUIntelx86_ALU::getRclCount(byte sourceCount) {
	return (sourceCount & 0x1F) % 9;
}

template<>
inline word CPUIntelx86_ALU::getRclCount(byte sourceCount) {
	return (sourceCount & 0x1F) % 17;
}

template<>
inline dword CPUIntelx86_ALU::getRclCount(byte sourceCount) {
	return sourceCount & 0x1F;
}

template<>
inline byte CPUIntelx86_ALU::getRolCount(byte sourceCount) {
	return sourceCount % 8;
}

template<>
inline word CPUIntelx86_ALU::getRolCount(byte sourceCount) {
	return sourceCount % 16;
}

template<>
inline dword CPUIntelx86_ALU::getRolCount(byte sourceCount) {
	return sourceCount % 32;
}

template<>
inline byte CPUIntelx86_ALU::getShiftCount() {
	return 7;
}

template<>
inline word CPUIntelx86_ALU::getShiftCount() {
	return 15;
}

template<>
inline dword CPUIntelx86_ALU::getShiftCount() {
	return 31;
}

template<>
inline byte CPUIntelx86_ALU::getRegisterBeforeMul() {
	return regAccumulator->word.byte.low8;
}

template<>
inline word CPUIntelx86_ALU::getRegisterBeforeMul() {
	return regAccumulator->word.reg16;
}

template<>
inline dword CPUIntelx86_ALU::getRegisterBeforeMul() {
	return regAccumulator->dword.reg32;
}

template<>
inline void CPUIntelx86_ALU::setRegistersAfterMul(qword result) {
	regAccumulator->dword.reg32 = (dword)(result & 0xFFFFFFFF);
	regData->dword.reg32 = (dword)((qword)((result & 0xFFFFFFFF00000000) >> 32));
}

template<>
inline void CPUIntelx86_ALU::setRegistersAfterMul(dword result) {
	regAccumulator->word.reg16 = (word)(result & 0xFFFF);
	regData->word.reg16 =  (word)((dword)((result & 0xFFFF0000) >> 16));
}

template<>
inline void CPUIntelx86_ALU::setRegistersAfterMul(word result) {
	regAccumulator->word.reg16 = result;
}

#endif

