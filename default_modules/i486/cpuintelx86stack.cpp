#include "cpuintelx86.h"
#include "cpuintelx86_alu.h"
#include <QDebug>

/**
 * @brief CPU_86::getNextPushESP
 * @return
 */
dword CPUIntelx86::getNextPushESP(Instruction& instr){
	dword addr_base = (this->*getSegmentAddress)(REGISTER_SS);
	if (!operOK) return 0;
	dword addr = 0;

	int stackAddrSize = getAddrSize(REGISTER_SS);
	int sp_decrement;
	if ((stackAddrSize) || (instr.szOperands == sz32)) {
		sp_decrement = 4;
	} else {
		sp_decrement = 2;
	}
	if (0 == stackAddrSize) {
		addr = getReg16(REGISTER_ESP) - sp_decrement;
		addr &= 0xFFFF;
	} else {
		addr = getReg32(REGISTER_ESP) - sp_decrement;
	}

	if (!isInLimits(REGISTER_SS, addr_base+addr, sp_decrement)) {
		fault_SS(instr, 0);
		return 0;
	}

	if (0 == stackAddrSize) {
		registers[REGISTER_ESP].word.reg16 -= sp_decrement;
	} else {
		registers[REGISTER_ESP].dword.reg32 -= sp_decrement;
	}

	return addr_base + addr;
}

/**
 * @brief CPUIntelx86::getNextPopESP
 * @return
 */
dword CPUIntelx86::getNextPopESP(Instruction& instr) {
	dword addr_base = (this->*getSegmentAddress)(REGISTER_SS);
	if (!operOK) return 0;

	dword addr = 0;

	int stackAddrSize = getAddrSize(REGISTER_SS);
	int sp_increment;
	if ((stackAddrSize) || (instr.szOperands == sz32)) {
		sp_increment = 4;
	} else {
		sp_increment = 2;
	}
	if (0 == stackAddrSize) {
		addr = getReg16(REGISTER_ESP);
	} else {
		addr = getReg32(REGISTER_ESP);
	}

	if (!isInLimits(REGISTER_SS, addr_base + addr, sp_increment)) {
		fault_SS(instr, 0);
		return 0;
	}

	if (0 == stackAddrSize) {
		registers[REGISTER_ESP].word.reg16 += sp_increment;
	} else {
		registers[REGISTER_ESP].dword.reg32 += sp_increment;
	}

	return addr_base + addr;
}

byte CPUIntelx86::pop8(Instruction & instr) {
	currentCyclesTaken += 5;
	dword addr = getNextPopESP(instr);
	if (!operOK) return 0;
	byte result = 0;
	if (instr.szOperands == sz32) {
		result = (getDWordR(addr, REGISTER_SS) & 0xFF);
	} else {
		result = (getWordR(addr, REGISTER_SS) & 0xFF);
	}
	return result;
}

word CPUIntelx86::pop16(Instruction& instr) {
	currentCyclesTaken += 5;
	dword addr = getNextPopESP(instr);
	if (!operOK) return 0;
	word result = 0;
	if (instr.szOperands == sz32) {
		result = (getDWordR(addr, REGISTER_SS) & 0xFFFF);
	} else {
		result = getWordR(addr, REGISTER_SS);
	}
	return result;
}

dword CPUIntelx86::pop32(Instruction& instr) {
	currentCyclesTaken += 5;
	dword addr = getNextPopESP(instr);
	if (!operOK) return 0;
	dword result = getDWordR(addr, REGISTER_SS);
	return result;
}

void CPUIntelx86::push8(Instruction & instr, const byte value) {
	currentCyclesTaken += 5;
	dword addr = getNextPushESP(instr); if (!operOK) return;
	if ((instr.szOperands == sz32) || getOpSize(REGISTER_SS)) {
		setDWordR(addr, alu->byte2s32(value), REGISTER_SS);
	}
	else { // 32-bit
		setWordR(addr, alu->byte2s16(value), REGISTER_SS);
	}
}

void CPUIntelx86::push16(Instruction& instr, const word value) {
	currentCyclesTaken += 5;
	dword addr = getNextPushESP(instr); if (!operOK) return;
	if ((instr.szOperands == sz32) || getOpSize(REGISTER_SS)) {
		setDWordR(addr, alu->word2s32(value), REGISTER_SS);
	}
	else {
		setWordR(addr, value, REGISTER_SS);
	}
}

void CPUIntelx86::push32(Instruction & instr, const dword value) {
	currentCyclesTaken += 5;
	dword addr = getNextPushESP(instr); if (!operOK) return;
	setDWordR(addr, value, REGISTER_SS);
}

void CPUIntelx86::pusha_16(Instruction& instr) {
	cyclesTotal += 47;
	word oldSP = getReg16(REGISTER_ESP);
	dword addr = (this->*getSegmentAddress)(REGISTER_SS);

	if (0 == SRegRights[REGISTER_SS].opSize) { // 16-bit stack segment
		addr += getReg16(REGISTER_ESP);
		if (!isInLimits(REGISTER_SS, addr-16, 16)) {
			operOK = 0;
			fault_SS(instr, 0);
			return;
		}
	}
	else {
		addr += getReg32(REGISTER_ESP);
		if (!isInLimits(REGISTER_SS, addr-32, 32)) {
			operOK = 0;
			fault_SS(instr, 0);
			return;
		}
	}

	operOK = 1;
	push16(instr, getReg16(REGISTER_EAX));
	push16(instr, getReg16(REGISTER_ECX));
	push16(instr, getReg16(REGISTER_EDX));
	push16(instr, getReg16(REGISTER_EBX));
	push16(instr, oldSP);
	push16(instr, getReg16(REGISTER_EBP));
	push16(instr, getReg16(REGISTER_ESI));
	push16(instr, getReg16(REGISTER_EDI));
}

void CPUIntelx86::pusha_32(Instruction & instr) {
	cyclesTotal += 63;

	dword oldESP = getReg32(REGISTER_ESP);
	dword addr = (this->*getSegmentAddress)(REGISTER_SS);

	if (0 == SRegRights[REGISTER_SS].opSize) { // 16-bit stack segment
		addr += getReg16(REGISTER_ESP);
	}
	else {
		addr += getReg32(REGISTER_ESP);
	}
	if (!isInLimits(REGISTER_SS, addr-32, 32)) {
		operOK = 0;
		fault_SS(instr, 0);
		return;
	}

	operOK = 1;
	push32(instr, getReg32(REGISTER_EAX));
	push32(instr, getReg32(REGISTER_ECX));
	push32(instr, getReg32(REGISTER_EDX));
	push32(instr, getReg32(REGISTER_EBX));
	push32(instr, oldESP);
	push32(instr, getReg32(REGISTER_EBP));
	push32(instr, getReg32(REGISTER_ESI));
	push32(instr, getReg32(REGISTER_EDI));
}

void CPUIntelx86::do_pusha(Instruction & instr) {
	debug_op0();
	if (instr.szOperands == sz32) {
		pusha_32(instr);
	} else {
		pusha_16(instr);
	}
}

void CPUIntelx86::popa_16(Instruction & instr) {
	cyclesTotal += 49;

	dword C = (this->*getSegmentAddress)(REGISTER_SS);
	if (0 == SRegRights[REGISTER_SS].opSize) { // 16-bit stack segment
		C += getReg16(REGISTER_ESP);
		if (!isInLimits(REGISTER_SS, C, 16)) {
			operOK = 0;
			fault_SS(instr, 0);
			return;
		}
	}
	else {
		C += getReg32(REGISTER_ESP);
		if (!isInLimits(REGISTER_SS, C, 32)) {
			operOK = 0;
			fault_SS(instr, 0);
			return;
		}
	}
	operOK = 1;
	setReg16(REGISTER_EDI, pop16(instr));
	setReg16(REGISTER_ESI, pop16(instr));
	setReg16(REGISTER_EBP, pop16(instr));
	pop16(instr);
	setReg16(REGISTER_EBX, pop16(instr));
	setReg16(REGISTER_EDX, pop16(instr));
	setReg16(REGISTER_ECX, pop16(instr));
	setReg16(REGISTER_EAX, pop16(instr));
}

void CPUIntelx86::popa_32(Instruction & instr) {
	dword C = (this->*getSegmentAddress)(REGISTER_SS);
	if (0 == SRegRights[REGISTER_SS].opSize) { // 16-bit stack segment
		C += getReg16(REGISTER_ESP);
		if (!isInLimits(REGISTER_SS, C-32, 32)) {
			fault_SS(instr, 0);
			operOK = 0;
			return;
		}
	}
	else {
		C += getReg32(REGISTER_ESP);
		if (!isInLimits(REGISTER_SS, C-32, 32)) {
			fault_SS(instr, 0);
			return;
		}
	}
	operOK = 1;
	setReg32(REGISTER_EDI, pop32(instr));
	setReg32(REGISTER_ESI, pop32(instr));
	setReg32(REGISTER_EBP, pop32(instr));
	pop32(instr);
	setReg32(REGISTER_EBX, pop32(instr));
	setReg32(REGISTER_EDX, pop32(instr));
	setReg32(REGISTER_ECX, pop32(instr));
	setReg32(REGISTER_EAX, pop32(instr));
}

void CPUIntelx86::do_popa(Instruction & instr) {
	debug_op0();

	if (instr.szOperands == sz32) {
		popa_32(instr);
	} else {
		popa_16(instr);
	}
}

void CPUIntelx86::push_flags(Instruction & instr) {
	cyclesTotal += 20;
	operOK = 0;
	if (instr.szOperands == sz16) {
		push16(instr, eflags.wflag.f16);
	} else {
		push32(instr, eflags.eflags.flag32);
	}
}

void CPUIntelx86::pop_flags(Instruction& instr) {
	cyclesTotal += 25;
	operOK = 0;
	if (instr.szOperands == sz16) {
		word A = pop16(instr);
		if (!operOK) return;
		eflags.wflag.f16 = A;
	} else {
		dword C = pop32(instr);
		if (!operOK) return;
		eflags.eflags.flag32 = C;
	}
}


void CPUIntelx86::do_pushf(Instruction& instr) {
	cyclesTotal += 9;
	debug_op0();
	push_flags(instr);
}

void CPUIntelx86::do_popf(Instruction& instr) {
	cyclesTotal += 9;
	debug_op0();
	pop_flags(instr);
}
