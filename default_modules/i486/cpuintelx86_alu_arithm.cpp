#include "cpuintelx86_alu.h"

template<typename opType, typename opTypeNext>
opType CPUIntelx86_ALU::add(opType operand1, opType operand2) {
	opTypeNext resultExt = (opTypeNext)operand1 + (opTypeNext)operand2;
	opType result = (resultExt & getTypeMask<opType>());

	setFlagOF<opType>(operand1, operand2, result);
	setFlagAF(operand1, operand2, (opType)(resultExt & getTypeMask<opType>()));
	setFlagCF<opTypeNext>(resultExt);
	setFlagsSZP<opType>(result);
	return result;
}

template<typename opType, typename opTypeNext>
opType CPUIntelx86_ALU::sub(opType operand1, opType operand2) {
	opTypeNext resultExt = (opTypeNext)operand1 - (opTypeNext)operand2;
	opType result = (resultExt & getTypeMask<opType>());
	setFlagsACOSub<opType, opTypeNext>(operand1, operand2, resultExt);
	setFlagsSZP<opType>(result);
	return result;
}

template<typename T, typename TNext>
T CPUIntelx86_ALU::adc(T op1, T op2) {
	return add<T, TNext>(op1, (op2 + (flags->eflags.flag32 & FLAG_CF)));
}

template<typename T, typename TNext>
T CPUIntelx86_ALU::sbb(T op1, T op2) {
	T tempOp2 = op2 + ((flags->eflags.flag32 & FLAG_CF) & getTypeMask<T>());
	return sub<T, TNext>(op1, tempOp2);
}

template<typename T, typename TNext>
void CPUIntelx86_ALU::cmp(T op1, T op2) {
	sub<T, TNext>(op1, op2);
}

template<typename OperandType, typename ResultType>
ResultType CPUIntelx86_ALU::mul_2(OperandType operand1, OperandType operand2) {
	ResultType result = (ResultType)operand1 * (ResultType)operand2;
	if (result & getCFMask<ResultType>()) {
		flags->eflags.flag32 |= FLAG_OF;
		flags->eflags.flag32 |= FLAG_CF;
	} else {
		 flags->eflags.flag32 &= FLAG_OF_CLEAR;
		 flags->eflags.flag32 &= FLAG_CF_CLEAR;
	}
	setFlagPF<OperandType>(result & getTypeMask<OperandType>());
	return result;
}

template<typename OperandType, typename ResultType>
void CPUIntelx86_ALU::mul_1(OperandType operand) {
	OperandType operandRegister = getRegisterBeforeMul<OperandType>();
	ResultType result = mul_2<OperandType, ResultType>(operandRegister, operand);
	setRegistersAfterMul<ResultType>(result);
}

template<typename OperandType, typename ResultType>
ResultType CPUIntelx86_ALU::imul_2(OperandType operand1, OperandType operand2) {
	auto signedOperand1 = unsignedToSigned(operand1);
	auto signedOperand2 = unsignedToSigned(operand2);
	auto result = mulSigned(signedOperand1, signedOperand2);
	if (result & getCFMask<ResultType>()) {
		flags->eflags.flag32 |= FLAG_OF;
		flags->eflags.flag32 |= FLAG_CF;
	} else {
		 flags->eflags.flag32 &= FLAG_OF_CLEAR;
		 flags->eflags.flag32 &= FLAG_CF_CLEAR;
	}
	setFlagPF<OperandType>(result & getTypeMask<OperandType>());
	return result;
}

template<typename OperandType, typename ResultType>
void CPUIntelx86_ALU::imul_1(OperandType operand) {
	OperandType operandRegister = getRegisterBeforeMul<OperandType>();
	ResultType result = imul_2<OperandType, ResultType>(operandRegister, operand);
	setRegistersAfterMul<ResultType>(result);
}



