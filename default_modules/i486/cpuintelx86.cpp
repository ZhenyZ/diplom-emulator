#include "cpuintelx86.h"
#include "cpuintelx86_decoder.h"
#include "loggerintelx86.h"
#include <QThread>
#include <QApplication>
#include <QWidget>
#include <QBoxLayout>
#include <QFontDatabase>
#include <QDebug>
#include <moduledevicegui.h>

#include "ui/cpuguiwrapper.h"

constexpr CPUIntelx86::RegName CPUIntelx86::RegTable8[8];

CPUIntelx86::CPUIntelx86(QObject *parent)
	: Loggable("CPU", parent),
	  ModuleDevice("Intel 80486 CPU", "Eugene Shestakov", 1)
{
	qword memSize = 0x200000;
	DeviceConfig * config = getConfig();
	if (config->hasProperty(CONFIG_MEMORY_SIZE)) {
		memSize = config->getQWordProperty(CONFIG_MEMORY_SIZE);
	} else {
		config->setProperty(CONFIG_MEMORY_SIZE, memSize);
	}
	hasPendingInterrupts = false;

	this->eMemSize = memSize;
	this->memory = new byte[eMemSize];
	alu = new CPUIntelx86_ALU(this, &eflags, &registers[REGISTER_EAX], &registers[REGISTER_EDX]);
	x387 = new Intel387x(this);
	decoder = new CPUIntelx86_decoder(this, this);

	connect(decoder, SIGNAL(signalDebugOutput(const QMap<QString,QString> &)), this, SLOT(slotDebugCPU(const QMap<QString,QString> &)), Qt::QueuedConnection);
	connect(this, SIGNAL(signalSelfDebugRedirect(QMap<QString,QString>)), this, SLOT(slotDebugCPU(const QMap<QString,QString> &)), Qt::QueuedConnection);
	setDebugEnabled();

	initIO();
}

void CPUIntelx86::hideUi() {
	emit signalHideGui();
}

void CPUIntelx86::createUi() {
	initDebugWidget();
}

void CPUIntelx86::showUi() {
	emit signalShowGui();
}

void CPUIntelx86::initDebugWidget() {
	gui = new ModuleDeviceGui(this);
	gui->setFilename("debug.log");
	connect(this, SIGNAL(signalInitGui()), gui, SLOT(slotInit()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalAddListWidget()), gui, SLOT(slotAddListWidget()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalDebugToGui(QString)), gui, SLOT(slotAddItemToListWidget(QString)), Qt::QueuedConnection);
	connect(this, SIGNAL(signalShowGui()), gui, SLOT(slotShowGui()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalHideGui()), gui, SLOT(slotHideGui()), Qt::QueuedConnection);

	guiWrapper = new CpuGuiWrapper();
	connect(this, SIGNAL(signalInitGui()), guiWrapper, SLOT(slotInit()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalUpdateStatusWidget(QMap<QString,QString>)), guiWrapper, SIGNAL(signalUpdateStatus(QMap<QString,QString>)), Qt::QueuedConnection);
	connect(this, SIGNAL(signalShowGui()), guiWrapper, SIGNAL(signalShow()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalHideGui()), guiWrapper, SIGNAL(signalHide()), Qt::QueuedConnection);
	connect(guiWrapper, SIGNAL(signalPullCX()), this, SLOT(slotPullCX()), Qt::QueuedConnection);

	connect(guiWrapper, SIGNAL(signalRequestUpdateStatus()), this, SLOT(slotUpdateStatusInfo()), Qt::QueuedConnection);

	emit signalInitGui();
	emit signalAddListWidget();
}

CPUIntelx86::~CPUIntelx86() {
	delete [] memory;
	delete [] eMemSizePtr;
}

void CPUIntelx86::resetHard() {
	reset();
}

byte *CPUIntelx86::getMemory() {
	return memory;
}

void CPUIntelx86::slotUpdateStatusInfo() {
	if (isDebugEnabled())
		updateStatusInfo();
}

void CPUIntelx86::slotReset()
{
	reset();
}

void CPUIntelx86::slotPullCX()
{
	registers[REGISTER_ECX].word.reg16 = 1;
}

void CPUIntelx86::slotSetDebug(bool b) {
	this->setDebugEnabled( b );
}

void CPUIntelx86::reboot() {
	qDebug() << "REBOOT issued";
	halted = false;

	decoder->setEIP( 0x0000FFF0 );
	decoder->setCS( 0xFFFF0000 );

	operatingMode = opModeReal;

	cpl = 0;

	flagGP = 0;
	flagSS = 0;

	setSegment = &CPUIntelx86::setSegmentReal;
	getSegment = &CPUIntelx86::getSegmentReal;
	getSegmentAddress = &CPUIntelx86::getSegmentAddressReal;
	getByte = &CPUIntelx86::getByteFlat;
	getWord = &CPUIntelx86::getWordFlat;
	getDWord = &CPUIntelx86::getDWordFlat;
	setByte = &CPUIntelx86::setByteFlat;
	setWord = &CPUIntelx86::setWordFlat;
	setDWord = &CPUIntelx86::setDWordFlat;

	eflags.eflags.flag32 = 0x00000002;

	for (int i = 0; i < 8; i++) {
		registers[i].dword.reg32 = 0x00000000;
		cRegisters[0] = 0x00000000;
	}

	for (int i = 0; i < 6; i++) {
		decoder->setOpSize(i, sz16);
		decoder->setAddressSize(i, sz16);
		SRegisters[i] = 0;
		SRegAddress[i] = 0x00000000;
		SRegRights[i].isRealModed = true;
		SRegRights[i].conforming = 0;
		SRegRights[i].dpl = 0;
		SRegRights[i].execute = 1;
		SRegRights[i].limit = 0xFFFF;
		SRegRights[i].maxAddress = 0xFFFF;
		SRegRights[i].minAddress = 0x00000000;
		SRegRights[i].opSize = 0;
		SRegRights[i].read = 1;
		SRegRights[i].system = 0;
		SRegRights[i].type = 0;
		SRegRights[i].write = 1;
	}
	SRegAddress[REGISTER_CS] = 0xFFFFF000;
	SRegRights[REGISTER_CS].maxAddress = 0xFFFF0000;
	SRegRights[REGISTER_CS].maxAddress = 0xFFFFFFFF;
	SRegRights[REGISTER_CS].limit = 0xFFFF;
	SRegisters[REGISTER_CS] = 0xF000;
	registers[REGISTER_EDX].dword.reg32 = 0x00000501;

	GDTR.Base = 0;
	GDTR.Limit = 0xFFFF;
	IDTR.Base = 0;
	IDTR.Limit = 0xFFFF;
	LDTR.base = 0;
	LDTR.limit = 0xFFFF;
	LDTR.selector = 0;
	LDTR.info.writable = 1;
	LDTR.info.readable = 1;
	LDTR.info.present = 1;
	taskRegister.base = 0;
	taskRegister.limit = 0xFFFF;
	taskRegister.selector = 0;
	taskRegister.info.present = 1;
	taskRegister.info.readable = 1;
	taskRegister.info.writable = 1;
}

/**
 * @brief Perform reset procedure as specified by RESET pin pulse
 */
void CPUIntelx86::reset() {
	qDebug() << "RESET issued";
	reboot();

	memset(memory, 0, sizeof(byte)*eMemSize);

	DeviceConfig * config = getConfig();
	QString biosFileName = "./systems/pc/BIOS/Award 286 BIOS (merged roms).bin";
	if (config->hasProperty(CONFIG_BIOS_FILENAME)) {
		biosFileName = config->getStringProperty(CONFIG_BIOS_FILENAME);
	} else {
		config->setProperty(CONFIG_BIOS_FILENAME, biosFileName);
	}

	this->loadBIOS(biosFileName);
}

void CPUIntelx86::tick() {
	if (halted) {
		QApplication::processEvents();
		return;
	}
	decoder->decodeNextInstruction();
}

void CPUIntelx86::initIO() {
	for (int i = 0; i < 0x1FFF; ++i) {
		registerOutput(
				i,
				"Port " + QString("%1").arg((ushort)i, (int)4, (int)16, QChar('0')).toUpper() + " OUT",
				std::bind(&CPUIntelx86::ioFuncOutput, this, std::placeholders::_1 )
		);
		registerInput(
				i,
				"Port " + QString("%1").arg((ushort)i, (int)4, (int)16,QChar('0')).toUpper() + " IN",
				std::bind(&CPUIntelx86::ioFuncInput, this, std::placeholders::_1, std::placeholders::_2)
		);
	}

	registerInput(
			0x50000,
			"Interrupt data",
			std::bind(&CPUIntelx86::setInputInterrupt, this, std::placeholders::_1, std::placeholders::_2)
	);

	registerReceiveSignalFunction(0, "Interrupt signal", std::bind(&CPUIntelx86::callInterruptSignal, this));
	registerReceiveSignalFunction(1, "Reboot signal", std::bind(&CPUIntelx86::reboot, this));
	registerReceiveSignalFunction(2, "Reset hard signal", std::bind(&CPUIntelx86::resetHard, this));
	registerReceiveSignalFunction(3, "External pending interrupt", std::bind(&CPUIntelx86::ioInterruptPending, this));

	registerEmitSignalFunction(0, "Interrupt acknowledge");

	this->eMemSizePtr = new qword*[1];
	this->eMemSizePtr[0] = &this->eMemSize;
	this->memPtr = &memory;
	DataMappingBlock blk = std::make_pair(&memPtr, this->eMemSizePtr);
	registerDataMapProvider(0, "Memory", blk);
	qDebug() << "CPU memBlock = " << *memPtr;
}

void CPUIntelx86::moduleReset() {
	resetHard();
}

void CPUIntelx86::moduleShutdown() {
	halted = true;
	emit signalHideGui();
}

void CPUIntelx86::processOutput(word port, dword data, int sizeInBytes) {
	portData = data;
	callOutputByIndex(port, sizeInBytes);
}

dword CPUIntelx86::processInput(word port, int sizeInBytes) {
	callInputByIndex(port, sizeInBytes);
	return portData;
}

void CPUIntelx86::ioFuncInput(dword data, int sizeInBytes) {
	ioInputSize = sizeInBytes;
	portData = data;
}

dword CPUIntelx86::ioFuncOutput(int size) {
	ioOutputSize = size;
	return portData;
}

void CPUIntelx86::setInputInterrupt(dword value, int) {
	interruptData = value;
}

void CPUIntelx86::callInterruptSignal() {
	signalExternalInterrupt(interruptData);
}

void CPUIntelx86::ioAckInterrupt() {
	this->hasPendingInterrupts = false;
	callSignalEmitByIndex(0);
	this->hasPendingInterrupts = false;
}

dword CPUIntelx86::getSegmentAddressExt(const dword selector) {
	return (this->*getSegmentAddress)(selector);
}

void CPUIntelx86::ioInterruptPending() {
	if (this->hasPendingInterrupts) {
		return;
	}
	this->hasPendingInterrupts = true;
	if (this->eflags.eflags.flag32 & FLAG_IF) {
		ioAckInterrupt();
	}
}


///**
// * @brief Start the execution
// */
//void CPUIntelx86::slotExecute()
//{
//	this->reset();

//	this->loadBIOS("./systems/pc/BIOS/PCXTBIOS.BIN");

//	memory[0xB8001] = 0x07;
//	memory[0xB8000] = 'A';
//	memory[0xB8003] = 0x07;
//	memory[0xB8002] = 'B';
//	memory[0xB8005] = 0x07;
//	memory[0xB8004] = 'C';

//	long cyclesLast = 0;
//	long cyclesNew = 0;

//	long cyclesAdditional = 0;

//	while ( !terminateNeeded_ ) {

//#ifdef ENABLE_DEBUG
//		if ( isDebugEnabled() )
//			this->thread()->msleep(1);
//#endif


//		if (cyclesNew - cyclesAdditional > 1000) {
//			QApplication::processEvents();
//			cyclesAdditional = cyclesNew;
//		}

//		cyclesNew = cyclesTotal;
//		if (cyclesNew - cyclesLast > 20)
//			chipset->proceedRQ();
//		while (cyclesNew - cyclesLast > 20) {
//			chipset->PIT->tick();
//			cyclesLast += 20;
//		}


//#ifdef ENABLE_DEBUG
//		if ( isDebugEnabled() )
//			this->updateStatusInfo();
//#endif

//		if (suspended_) {
//			while (suspended_ && !runningAllowed_) {
//				QApplication::processEvents();
//				this->thread()->msleep(20);
//			}
//			if (suspended_ && runningAllowed_) runningAllowed_ = false;
//		}

//		decoder->decodeNextInstruction();

//	}

//	emit signalCPUTerminated();
//}


void CPUIntelx86::updateStatusInfo() {
	if (!isDebugEnabled())
		return;
	QMap<QString, QString> vals;
	for (int i = 0; i < 8; i++) {
		vals.insert(RegStrings32[i+8].toLower(), QString("%1").arg((ulong)registers[i].dword.reg32, (int)8, (int)16, QChar('0')));
	}
	for (int i = 0; i < 6; i++) {
		vals.insert(SRegStrings[i].toLower(), QString("%1").arg((ushort)SRegisters[i], (int)4, (int)16, QChar('0')));
	}
	//CZSOPAID
	QString flags = "";
	flags.append(QString::number(eflags.eflags.flag32 & FLAG_CF ? 1 : 0));
	flags.append(QString::number(eflags.eflags.flag32 & FLAG_ZF ? 1 : 0));
	flags.append(QString::number(eflags.eflags.flag32 & FLAG_SF ? 1 : 0));
	flags.append(QString::number(eflags.eflags.flag32 & FLAG_OF ? 1 : 0));
	flags.append(QString::number(eflags.eflags.flag32 & FLAG_PF ? 1 : 0));
	flags.append(QString::number(eflags.eflags.flag32 & FLAG_AF ? 1 : 0));
	flags.append(QString::number(eflags.eflags.flag32 & FLAG_IF ? 1 : 0));
	flags.append(QString::number(eflags.eflags.flag32 & FLAG_DF ? 1 : 0));
	vals.insert("flags", flags);
	emit signalUpdateStatusWidget(vals);
}

ModuleDevice* createModule() {
	return new CPUIntelx86();
}

template<> dword CPUIntelx86::extractBitValue(dword& src) {
	return src;
}

template<> word CPUIntelx86::extractBitValue(dword& src) {
	return (word)(src & 0x0000FFFF);
}

template<> byte CPUIntelx86::extractBitValue(dword& src) {
	return (byte)(src & 0x000000FF);
}

