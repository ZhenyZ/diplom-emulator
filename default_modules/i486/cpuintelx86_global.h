#ifndef I486_GLOBAL_H
#define I486_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(CPU_I486_LIBRARY)
#  define I486SHARED_EXPORT Q_DECL_EXPORT
#else
#  define I486SHARED_EXPORT Q_DECL_IMPORT
#endif

#endif
