#ifndef CPUINTELX86_DECODER_H
#define CPUINTELX86_DECODER_H

#include <QObject>
#include <QMap>
#include <QString>

#include <loggable.h>
#include <constants.h>

#include "cpuintelx86_types.h"

class CPUIntelx86;

enum Size {sz8 = -1, sz16 = 0, sz32 = 1};
enum OperandType {opReg, opMem, opImm, opSReg, opCReg};

struct Instruction {
	int cyclesTotal;			//	CPU cycles taken
	byte opcode;

	bool overrideOpSize;
	bool overrideAddrSize;
	Size szOperands;				//	operands size
	Size szAddress;				//	address ptr size

	dword srcAddr, dstAddr;		//	operand addresses (reg index / mem addr)
	dword srcValue, dstValue;		//	operand values
	OperandType typeDst, typeSrc;	//	operands kind (mem / reg / imm)
	byte len;					//	total instruction size in bytes
	bool executable;				//	false -> cannot execute

	int segmentPrefix;			//	override segment prefix
	bool segmentOverride;

	repeatPrefix repPrefix;			//	for string instructions
	byte mod, reg, rm;			//	mod/reg/rm optional byte
	dword immediate;				//	imm8/16/32 value
	int extensionIndex;			//	only for debug purpose

	int reverseDirection;
	int isWord;
	dword uoffset;
	qint64 soffset;
};

struct DebugClosure {
	OperandType ot;
	Size opSize;
	dword value;
};

enum class OpExtensionArithmetic {
	Add,
	Or,
	Adc,
	Sub,
	Sbb,
	Xor,
	And,
	Cmp
};

class CPUIntelx86_decoder : public Loggable
{
	Q_OBJECT
	friend class CPUIntelx86;

public:
	CPUIntelx86_decoder(CPUIntelx86* myCPU, QObject* parent = 0);

	void setOpSize(int segment, Size sz);
	void setAddressSize(int segment, Size sz);

	void setEIP(dword newEIP) {oldEIP = EIP = newEIP;}
	void setCS(dword newCS) {oldCS = CS; CS = newCS;}

	void decodeNextInstruction();
	void decodeExtension();
	void decodeExtension_grp00();
	void decodeExtension_grp01();
	void decodeFPU();

	void advance() {oldEIP = EIP; oldCS = CS;}
	void rollback() {EIP = oldEIP; CS = oldCS;}

	dword EIP;
	dword CS;

private:
	Instruction currentInstruction;
	bool checkCanExecute(int len);

	void swapDstSrc();

	byte nextByte();
	word nextWord();
	dword nextDWord();

	void getRM();
	void loadRM16();
	void loadRM32();

	Size getOperandSize();
	Size getOperandSize(int segment);
	void loadImmediateValue(int isSignExtendedByte);
	void loadImmediateOffset(int isSignExtendedByte);
	void executeArithmetic1Accumulator(OpExtensionArithmetic operation);
	void executeArithmetic1RegisterMemory(OpExtensionArithmetic operation);

	dword oldEIP;
	dword oldCS;
	CPUIntelx86* CPU;

	Size addressSize[8];	// addr-size of all segments
	Size operandSize[8];	// op-size of all segments
	byte* memory;

	void debug( const QString & val );
	void debug_critical(const QString val);

};

#endif // CPUINTELX86_DECODER_H
