#ifndef CPUIntelx86_ALU_Shifts
#define CPUIntelx86_ALU_Shifts

#include "cpuintelx86_alu.h"

template<typename sz>
sz CPUIntelx86_ALU::shift(sz operand, byte shiftCount, opShiftType kind) {
	sz result = operand;
	switch (kind) {
		case stSAL:
		case stSHL:
			result = shl<sz>(operand, shiftCount);
		break;
		case stSHR:
			result = shr<sz>(operand, shiftCount);
		break;
		case stSAR:
			result = sar<sz>(operand, shiftCount);
		break;
		case stROL:
			result = rol<sz>(operand, shiftCount);
		break;
		case stRCL:
			result = rcl<sz>(operand, shiftCount);
		break;
		case stROR:
			result = ror<sz>(operand, shiftCount);
		break;
		case stRCR:
			result = rcr<sz>(operand, shiftCount);
		break;
	}
	return result;
}

template<typename sz>
sz CPUIntelx86_ALU::rcr(sz operand, byte shiftCount) {
	sz result = operand;
	sz isOverflow = 0;
	sz resultCF = 0;
	sz sourceCF = flags->eflags.flag32 & FLAG_CF;
	sz tempReg = 0;
	shiftCount = getRclCount<sz>(shiftCount);
	if (shiftCount) {
		flags->eflags.flag32 &= FLAG_CF_CLEAR;
		if (shiftCount) {
			for (int i = shiftCount; i > 0; i--) {
				tempReg = sourceCF;
				sourceCF = (result & 1);
				result = result >> 1;
				result |= (tempReg << getShiftCount<sz>());
			}
			resultCF = sourceCF;
		}
		flags->eflags.flag32 |= resultCF;
		isOverflow = (((result & getMostSignificantBitValue<sz>()) >> getShiftCount<sz>()) ^ resultCF);
		if ((shiftCount == 1)) {
			if (isOverflow) {
				flags->eflags.flag32 |= FLAG_OF;
			} else {
				flags->eflags.flag32 &= FLAG_OF_CLEAR;
			}
		}
	}
	return result;
}

template<typename sz>
sz CPUIntelx86_ALU::ror(sz operand, byte shiftCount)
{
	sz result = operand;
	sz isOverflow = 0;
	sz resultCF = 0;
	sz sourceCF = 0;
	shiftCount = shiftCount & 0x1F;
	shiftCount = getRolCount<sz>(shiftCount);
	if (shiftCount) {
		flags->eflags.flag32 &= FLAG_CF_CLEAR;
		if (shiftCount) {
			for (byte i = shiftCount; i > 0; i--) {
				sourceCF = (result & 1);
				result = result >> 1;
				result |= (sourceCF << getShiftCount<sz>());
			}
			resultCF = sourceCF;
			isOverflow = (((result & getMostSignificantBitValue<sz>()) >> getShiftCount<sz>()) ^ resultCF);
		}
		flags->eflags.flag32 |= resultCF;
		isOverflow = (((result & getMostSignificantBitValue<sz>()) >> getShiftCount<sz>()) ^ resultCF);
		if ((shiftCount == 1)) {
			if (isOverflow) {
				flags->eflags.flag32 |= FLAG_OF;
			} else {
				flags->eflags.flag32 &= FLAG_OF_CLEAR;
			}
		}
	}
	return result;
}


template <typename sz> sz CPUIntelx86_ALU::rcl(sz operand, byte shiftCount) {
	sz result = operand;
	sz isOverflow = 0;
	sz resultCF = 0;
	sz sourceCF = flags->eflags.flag32 & FLAG_CF;
	sz tempReg = 0;
	shiftCount = getRclCount<sz>(shiftCount);
	if (shiftCount) {
		flags->eflags.flag32 &= FLAG_CF_CLEAR;
		if (shiftCount) {
			for (int i = shiftCount; i > 0; i--) {
				tempReg = sourceCF;
				sourceCF = ((result & getMostSignificantBitValue<sz>()) >> getShiftCount<sz>());
				result = (result << 1);
				result |= tempReg;
			}
			resultCF = sourceCF;
		}
		flags->eflags.flag32 |= resultCF;
		isOverflow = (((result & getMostSignificantBitValue<sz>()) >> getShiftCount<sz>()) ^ resultCF);
		if ((shiftCount == 1)) {
			if (isOverflow) {
				flags->eflags.flag32 |= FLAG_OF;
			} else {
				flags->eflags.flag32 &= FLAG_OF_CLEAR;
			}
		}
	}
	return result;
}

template <typename sz> sz CPUIntelx86_ALU::rol(sz operand, byte shiftCount) {
	sz result = operand;
	sz isOverflow = 0;
	sz resultCF = 0;
	sz sourceCF = 0;
	shiftCount = shiftCount & 0x1F;
	shiftCount = getRolCount<sz>(shiftCount);
	if (shiftCount) {
		flags->eflags.flag32 &= FLAG_CF_CLEAR;
		if (shiftCount) {
			for (int i = shiftCount; i > 0; i--) {
				sourceCF = ((result & getMostSignificantBitValue<sz>()) >> getShiftCount<sz>());
				result = (result << 1);
				result |= sourceCF;
			}
			resultCF = sourceCF;
		}
		flags->eflags.flag32 |= resultCF;
		isOverflow = (((result & getMostSignificantBitValue<sz>()) >> getShiftCount<sz>()) ^ resultCF);
		if ((shiftCount == 1)) {
			if (isOverflow) {
				flags->eflags.flag32 |= FLAG_OF;
			} else {
				flags->eflags.flag32 &= FLAG_OF_CLEAR;
			}
		}
	}
	return result;
}

template <typename sz> sz CPUIntelx86_ALU::sar(sz operand, byte shiftCount) {
	sz result = operand;
	sz resultCF = 0;
	sz tempReg = 0;
	shiftCount = (shiftCount & 0x1F);
	if (shiftCount) {
		flags->eflags.flag32 &= FLAG_CF_CLEAR;
		tempReg = (operand & getMostSignificantBitValue<sz>());
		for (int i = shiftCount; i > 0; i--) {
			resultCF = (result & 1);
			result = (result >> 1);
			result |= tempReg;
		}
		flags->eflags.flag32 |= resultCF;
		if ((shiftCount == 1)) {
			flags->eflags.flag32 &= FLAG_OF_CLEAR;
		}
	}
	setFlagsSZP<sz>(result);
	return result;
}

template <typename sz> sz CPUIntelx86_ALU::shr(sz operand, byte shiftCountOrig) {
	sz result = operand;
	sz isOverflow = 0;
	sz resultCF = 0;
	byte shiftCount = (shiftCountOrig & 0x1F);
	if (shiftCount) {
		flags->eflags.flag32 &= FLAG_CF_CLEAR;
		--shiftCount;
		result = operand >> shiftCount;
		resultCF = (result & 1);
		result = result >> 1;
		isOverflow = (operand & getMostSignificantBitValue<sz>());
		flags->eflags.flag32 |= resultCF;
		if (!shiftCount) {
			if (isOverflow) {
				flags->eflags.flag32 |= FLAG_OF;
			} else {
				flags->eflags.flag32 &= FLAG_OF_CLEAR;
			}
		}
	}
	setFlagsSZP<sz>(result);
	return result;
}

template <typename sz> sz CPUIntelx86_ALU::shl(sz operand, byte shiftCountOrig) {
	sz result = operand;
	sz isOverflow = 0;
	sz resultCF = 0;
	byte shiftCount = (shiftCountOrig & 0x1F);
	if (shiftCount) {
		flags->eflags.flag32 &= FLAG_CF_CLEAR;
		--shiftCount;
		result = (operand << shiftCount);
		resultCF = ((result & getMostSignificantBitValue<sz>()) >> getShiftCount<sz>());
		result = result << 1;
		isOverflow = ((operand ^ result) & getMostSignificantBitValue<sz>());
		flags->eflags.flag32 |= resultCF;
		if (!shiftCount) {
			if (isOverflow) {
				flags->eflags.flag32 |= FLAG_OF;
			} else {
				flags->eflags.flag32 &= FLAG_OF_CLEAR;
			}
		}
	}
	setFlagsSZP<sz>(result);
	return result;
}

#endif
