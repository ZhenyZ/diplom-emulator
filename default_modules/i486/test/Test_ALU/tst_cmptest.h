#ifndef TST_CPU_CMP_TEST_H
#define TST_CPU_CMP_TEST_H

#include <QString>
#include <QtTest>
#include <QDebug>

#include "../../cpuintelx86.h"
#include "../../cpuintelx86_types.h"

class CpuCmpTest : public QObject
{
	Q_OBJECT

public:
	CpuCmpTest();
	friend class CPUIntelx86;
	friend class CPUIntelx86_ALU;

	CPUIntelx86 * cpu = nullptr;

private Q_SLOTS:
	void initTestCase();
	void cleanupTestCase();

	void testCmp16data();
	void testCmp16();
	void testTest16data();
	void testTest16();
};

#endif // TST_ALUTEST_H
