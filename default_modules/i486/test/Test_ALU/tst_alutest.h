#ifndef TST_ALUTEST_H
#define TST_ALUTEST_H

#include <QString>
#include <QtTest>
#include <QDebug>

#include "../../cpuintelx86.h"
#include "../../cpuintelx86_types.h"

class aluTest : public QObject
{
	Q_OBJECT

public:
	aluTest();
	friend class CPUIntelx86;
	friend class CPUIntelx86_ALU;

	CPUIntelx86 * cpu = nullptr;

private Q_SLOTS:
	void initTestCase();
	void cleanupTestCase();

	void testAdd8_data();
	void testAdd8();
	void testAdd16_data();
	void testAdd16();
	void testAdd32_data();
	void testAdd32();

	void testSub8_data();
	void testSub8();
	void testSub16_data();
	void testSub16();
	void testSub32_data();
	void testSub32();

	void testShl8_data();
	void testShl8();
	void testShl16_data();
	void testShl16();
	void testShl32_data();
	void testShl32();

	void testShr8_data();
	void testShr8();
	void testShr16_data();
	void testShr16();
	void testShr32_data();
	void testShr32();

	void testSar8_data();
	void testSar8();
	void testSar16_data();
	void testSar16();
	void testSar32_data();
	void testSar32();

	void testMul_data();
	void testMul();
	void testMulAx_data();
	void testMulAx();
	void testIMul_data();
	void testIMul();

	void testRcr8_data();
	void testRcr8();

	void testRcl8_data();
	void testRcl8();

	void testRor8_data();
	void testRor8();

	void testRol8_data();
	void testRol8();
};

#endif // TST_ALUTEST_H
