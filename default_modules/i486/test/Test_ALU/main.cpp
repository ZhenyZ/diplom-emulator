#include "tst_alutest.h"
#include "tst_cmptest.h"

#include <QMap>
#include <QString>
#include <QMetaType>

Q_DECLARE_METATYPE(Instruction)

int main(int argc, char *argv[]) {
	qRegisterMetaType<QMap<QString,QString>>();
	aluTest alutest;
	QTest::qExec(&alutest, argc, argv);
	CpuCmpTest cmptest;
	QTest::qExec(&cmptest, argc, argv);
}
