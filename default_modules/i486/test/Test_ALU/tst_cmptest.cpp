#include "tst_cmptest.h"
#include <QMap>
#include <QString>
#include <QMetaType>

Q_DECLARE_METATYPE(Instruction)

CpuCmpTest::CpuCmpTest() {
}

void CpuCmpTest::initTestCase() {
	cpu = new CPUIntelx86();
}

void CpuCmpTest::cleanupTestCase() {
	delete cpu;
}

void CpuCmpTest::testCmp16data() {
}

void CpuCmpTest::testCmp16() {
	Instruction instruction;
	instruction.dstAddr = CPUIntelx86::REGISTER_EAX;
	instruction.srcAddr = CPUIntelx86::REGISTER_EBX;
	instruction.szOperands = sz16;
	instruction.srcValue = 0xAA;
	instruction.dstValue = 0x55;
	auto src = instruction.srcValue;
	auto dst = instruction.dstValue;
	cpu->setReg16(instruction.srcAddr, instruction.srcValue);
	cpu->setReg16(instruction.dstAddr, instruction.dstValue);
	cpu->do_arithm_rm_r(instruction, OpExtensionArithmetic::Cmp);
	QVERIFY2(cpu->getReg16(instruction.srcAddr) == src, "Source register changed its value");
	QVERIFY2(cpu->getReg16(instruction.dstAddr) == dst, "Destination register changed its value");
}

void CpuCmpTest::testTest16data() {

}

void CpuCmpTest::testTest16() {

}

#include "moc_tst_cmptest.cpp"

