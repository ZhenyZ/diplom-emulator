#include "tst_alutest.h"

void aluTest::testAdd8_data() {
	QTest::addColumn<dword>("eflags_initial");
	QTest::addColumn<byte>("operand1");
	QTest::addColumn<byte>("operand2");
	QTest::addColumn<byte>("expectedResult");
	QTest::addColumn<dword>("eflags_final");
	QTest::newRow("add8_1") << (dword)0x00 << (byte)0x00 << (byte)0x00 << (byte)0x00 << (dword)(FLAG_ZF | FLAG_PF);
	QTest::newRow("add8_2") << (dword)0x00 << (byte)0x01 << (byte)0x01 << (byte)0x02 << (dword)0x00;
	QTest::newRow("add8_3") << (dword)0x00 << (byte)0xFC << (byte)0x03 << (byte)0xFF << (dword)(FLAG_SF | FLAG_PF);
	QTest::newRow("add8_4") << (dword)0x00 << (byte)0xFC << (byte)0x04 << (byte)0x00 << (dword)(FLAG_ZF | FLAG_PF | FLAG_CF | FLAG_AF);
	QTest::newRow("add8_5") << (dword)0x00 << (byte)0xFF <<(byte) 0xFF << (byte)0xFE << (dword)(FLAG_SF | FLAG_CF | FLAG_AF);
}

void aluTest::testAdd8() {
	QFETCH(dword, eflags_initial);
	QFETCH(byte, operand1);
	QFETCH(byte, operand2);
	QFETCH(byte, expectedResult);
	QFETCH(dword, eflags_final);
	cpu->eflags.eflags.flag32 = eflags_initial;
	byte result = cpu->alu->add<byte, word>(operand1, operand2);
	QVERIFY2(result == expectedResult, "Results don't match");
	QVERIFY2(cpu->eflags.eflags.flag32 == eflags_final, "Flags error");
}

void aluTest::testAdd16_data() {
	QTest::addColumn<dword>("eflags_initial");
	QTest::addColumn<word>("operand1");
	QTest::addColumn<word>("operand2");
	QTest::addColumn<word>("expectedResult");
	QTest::addColumn<dword>("eflags_final");
	QTest::newRow("add16_1") << (dword)0x00 << (word)0x00 << (word)0x00 << (word)0x00 << (dword)(FLAG_ZF | FLAG_PF);
	QTest::newRow("add16_2") << (dword)0x00 << (word)0x01 << (word)0x01 << (word)0x02 << (dword)0x00;
	QTest::newRow("add_*FC-and-*03_exp-*FF+SP") << (dword)0x00 << (word)0xFFFC << (word)0x0003 << (word)0xFFFF << (dword)(FLAG_SF | FLAG_PF);
	QTest::newRow("add_*FC-and-*04_exp-0+ZPC") << (dword)0x00 << (word)0xFFFC << (word)0x0004 << (word)0x00 << (dword)(FLAG_ZF | FLAG_PF | FLAG_CF);
	QTest::newRow("add_*FF-and-*FF_exp-*FE+CS") << (dword)0x00 << (word)0xFFFF << (word)0xFFFF << (word)0xFFFE << (dword)(FLAG_SF | FLAG_CF);
}

void aluTest::testAdd16() {
	QFETCH(dword, eflags_initial);
	QFETCH(word, operand1);
	QFETCH(word, operand2);
	QFETCH(word, expectedResult);
	QFETCH(dword, eflags_final);
	cpu->eflags.eflags.flag32 = eflags_initial;
	word result = cpu->alu->add<word, dword>(operand1, operand2);
	QVERIFY2(result == expectedResult, "Results don't match");
	QVERIFY2(cpu->eflags.eflags.flag32 == eflags_final, "Flags error");
}

void aluTest::testAdd32_data() {
	QTest::addColumn<dword>("eflags_initial");
	QTest::addColumn<dword>("operand1");
	QTest::addColumn<dword>("operand2");
	QTest::addColumn<dword>("expectedResult");
	QTest::addColumn<dword>("eflags_final");
	QTest::newRow("add_0-and-0_exp-0") << (dword)0x00 << (dword)0x00 << (dword)0x00 << (dword)0x00 << (dword)(FLAG_ZF | FLAG_PF);
	QTest::newRow("add_1-and-1_exp-2") << (dword)0x00 << (dword)0x01 << (dword)0x01 << (dword)0x02 << (dword)0x00;
	QTest::newRow("add_*FC-and-*03_exp-*FF+SP") << (dword)0x00 << (dword)0xFFFFFFFC << (dword)0x0003 << (dword)0xFFFFFFFF << (dword)(FLAG_SF | FLAG_PF);
	QTest::newRow("add_*FC-and-*04_exp-0+ZPC") << (dword)0x00 << (dword)0xFFFFFFFC << (dword)0x0004 << (dword)0x00 << (dword)(FLAG_ZF | FLAG_PF | FLAG_CF);
	QTest::newRow("add_*FF-and-*FF_exp-*FE+CS") << (dword)0x00 << (dword)0xFFFFFFFF << (dword)0xFFFFFFFF << (dword)0xFFFFFFFE << (dword)(FLAG_SF | FLAG_CF);
}

void aluTest::testAdd32() {
	QFETCH(dword, eflags_initial);
	QFETCH(dword, operand1);
	QFETCH(dword, operand2);
	QFETCH(dword, expectedResult);
	QFETCH(dword, eflags_final);
	cpu->eflags.eflags.flag32 = eflags_initial;
	dword result = cpu->alu->add<dword, qword>(operand1, operand2);
	QVERIFY2(result == expectedResult, "Results don't match");
	QVERIFY2(cpu->eflags.eflags.flag32 == eflags_final, "Flags error");
}

void aluTest::testSub8_data() {
	QTest::addColumn<dword>("eflags_initial");
	QTest::addColumn<byte>("operand1");
	QTest::addColumn<byte>("operand2");
	QTest::addColumn<byte>("expectedResult");
	QTest::addColumn<dword>("eflags_final");
	QTest::newRow("sub_0_0_exp_0+ZP") << (dword)0x00 << (byte)0x00 << (byte)0x00 << (byte)0x00 << (dword)(FLAG_ZF | FLAG_PF);
	QTest::newRow("sub_1_1_exp_0+ZP") << (dword)0x00 << (byte)0x01 << (byte)0x01 << (byte)0x00 << (dword)(FLAG_ZF | FLAG_PF);
	QTest::newRow("sub_1_2_exp_FF+SPCA") << (dword)0x00 << (byte)0x01 << (byte)0x02 << (byte)0xFF << (dword)(FLAG_SF | FLAG_CF | FLAG_PF | FLAG_AF);
}

void aluTest::testSub8() {
	QFETCH(dword, eflags_initial);
	QFETCH(byte, operand1);
	QFETCH(byte, operand2);
	QFETCH(byte, expectedResult);
	QFETCH(dword, eflags_final);
	cpu->eflags.eflags.flag32 = eflags_initial;
	byte result = cpu->alu->sub<byte, word>(operand1, operand2);
	QVERIFY2(result == expectedResult, "Results don't match");
	QVERIFY2(cpu->eflags.eflags.flag32 == eflags_final, "Flags error");
}

void aluTest::testSub16_data() {
	QTest::addColumn<dword>("eflags_initial");
	QTest::addColumn<word>("operand1");
	QTest::addColumn<word>("operand2");
	QTest::addColumn<word>("expectedResult");
	QTest::addColumn<dword>("eflags_final");
	QTest::newRow("sub_0_0_exp_0+ZP") << (dword)0x00 << (word)0x00 << (word)0x00 << (word)0x00 << (dword)(FLAG_ZF | FLAG_PF);
	QTest::newRow("sub_1_1_exp_0+ZP") << (dword)0x00 << (word)0x01 << (word)0x01 << (word)0x00 << (dword)(FLAG_ZF | FLAG_PF);
	QTest::newRow("sub_1_2_exp_FF+SPCA") << (dword)0x00 << (word)0x01 << (word)0x02 << (word)0xFFFF << (dword)(FLAG_SF | FLAG_CF | FLAG_PF);
}

void aluTest::testSub16() {
	QFETCH(dword, eflags_initial);
	QFETCH(word, operand1);
	QFETCH(word, operand2);
	QFETCH(word, expectedResult);
	QFETCH(dword, eflags_final);
	cpu->eflags.eflags.flag32 = eflags_initial;
	word result = cpu->alu->sub<word, dword>(operand1, operand2);
	QVERIFY2(result == expectedResult, "Results don't match");
	QVERIFY2(cpu->eflags.eflags.flag32 == eflags_final, "Flags error");
}

void aluTest::testSub32_data() {
	QTest::addColumn<dword>("eflags_initial");
	QTest::addColumn<dword>("operand1");
	QTest::addColumn<dword>("operand2");
	QTest::addColumn<dword>("expectedResult");
	QTest::addColumn<dword>("eflags_final");
	QTest::newRow("sub_0_0_exp_0+ZP") << (dword)0x00 << (dword)0x00 << (dword)0x00 << (dword)0x00 << (dword)(FLAG_ZF | FLAG_PF);
	QTest::newRow("sub_1_1_exp_0+ZP") << (dword)0x00 << (dword)0x01 << (dword)0x01 << (dword)0x00 << (dword)(FLAG_ZF | FLAG_PF);
	QTest::newRow("sub_1_2_exp_FF+SPCA") << (dword)0x00 << (dword)0x01 << (dword)0x02 << (dword)0xFFFFFFFF << (dword)(FLAG_SF | FLAG_CF | FLAG_PF);
}

void aluTest::testSub32() {
	QFETCH(dword, eflags_initial);
	QFETCH(dword, operand1);
	QFETCH(dword, operand2);
	QFETCH(dword, expectedResult);
	QFETCH(dword, eflags_final);
	cpu->eflags.eflags.flag32 = eflags_initial;
	dword result = cpu->alu->sub<dword, qword>(operand1, operand2);
	QVERIFY2(result == expectedResult, "Results don't match");
	QVERIFY2(cpu->eflags.eflags.flag32 == eflags_final, "Flags error");
}

void aluTest::testMul_data() {
	QTest::addColumn<dword>("eflags_initial");
	QTest::addColumn<word>("operand1");
	QTest::addColumn<word>("operand2");
	QTest::addColumn<dword>("expectedResult");
	QTest::addColumn<dword>("eflags_final");
	QTest::newRow("mul_0_0_exp_0+P") << (dword)0x00 << (word)0x00 << (word)0x00 << (dword)0x00 << (dword)(FLAG_PF);
	QTest::newRow("mul_1_1_exp_1") << (dword)0x00 << (word)0x01 << (word)0x01 << (dword)0x01 << (dword)(0x00);
	QTest::newRow("mul_FFFE_FFFC_exp_FFFA0008+OC") << (dword)0x00 << (word)0xFFFC << (word)0xFFFE << (dword)0xFFFA0008 << (dword)(FLAG_OF | FLAG_CF);
}

void aluTest::testMul() {
	QFETCH(dword, eflags_initial);
	QFETCH(word, operand1);
	QFETCH(word, operand2);
	QFETCH(dword, expectedResult);
	QFETCH(dword, eflags_final);
	cpu->eflags.eflags.flag32 = eflags_initial;
	dword result = cpu->alu->mul_2<word, dword>(operand1, operand2);
	QVERIFY2(result == expectedResult, "Results don't match");
	QVERIFY2(cpu->eflags.eflags.flag32 == eflags_final, "Flags error");
}

void aluTest::testMulAx_data() {
	QTest::addColumn<dword>("eflags_initial");
	QTest::addColumn<word>("ax");
	QTest::addColumn<word>("operand");
	QTest::addColumn<word>("expectedAx");
	QTest::addColumn<word>("expectedDx");
	QTest::addColumn<dword>("eflags_final");
	QTest::newRow("mul_0:0_exp_0:0+P") << (dword)0x00 << (word)0x00 << (word)0x00 << (word)0x00 << (word)0x00 << (dword)(FLAG_PF);
	QTest::newRow("mul_1:1_exp_0:1") << (dword)0x00 << (word)0x01 << (word)0x01 << (word)0x01 << (word)0x00 << (dword)(0x00);
	QTest::newRow("mul_FFFE:FFFC_exp_0008:FFFA+OC") << (dword)0x00 << (word)0xFFFC << (word)0xFFFE << (word)0x0008 << (word)(0xFFFA) << (dword)(FLAG_OF | FLAG_CF);
}

void aluTest::testMulAx() {
	QFETCH(dword, eflags_initial);
	QFETCH(word, ax);
	QFETCH(word, operand);
	QFETCH(word, expectedAx);
	QFETCH(word, expectedDx);
	QFETCH(dword, eflags_final);
	cpu->eflags.eflags.flag32 = eflags_initial;
	cpu->setReg16(CPUIntelx86::REGISTER_EAX, ax);
	cpu->alu->mul_1<word, dword>(operand);

	QVERIFY2(expectedAx == cpu->getReg16(CPUIntelx86::REGISTER_EAX), "Results don't match");
	QVERIFY2(expectedDx == cpu->getReg16(CPUIntelx86::REGISTER_EDX), "Results don't match");
	QVERIFY2(cpu->eflags.eflags.flag32 == eflags_final, "Flags error");
}

void aluTest::testIMul_data() {
	QTest::addColumn<dword>("eflags_initial");
	QTest::addColumn<word>("operand1");
	QTest::addColumn<word>("operand2");
	QTest::addColumn<dword>("expectedResult");
	QTest::addColumn<dword>("eflags_final");
	QTest::newRow("imul_0_0_exp_0+P") << (dword)0x00 << (word)0x00 << (word)0x00 << (dword)0x00 << (dword)(FLAG_PF);
	QTest::newRow("imul_1_1_exp_1") << (dword)0x00 << (word)0x01 << (word)0x01 << (dword)0x01 << (dword)(0x00);
	QTest::newRow("imul_FFFE:FFFC_exp_0000:0008") << (dword)0x00 << (word)0xFFFC << (word)0xFFFE << (dword)0x00000008 << (dword)0x00;
	QTest::newRow("imul_7FFE:7FFC_exp_3FFD:0008+OC") << (dword)0x00 << (word)0x7FFE << (word)0x7FFC << (dword)0x3FFD0008 << (dword)(FLAG_OF | FLAG_CF);
}

void aluTest::testIMul() {
	QFETCH(dword, eflags_initial);
	QFETCH(word, operand1);
	QFETCH(word, operand2);
	QFETCH(dword, expectedResult);
	QFETCH(dword, eflags_final);
	cpu->eflags.eflags.flag32 = eflags_initial;
	dword result = cpu->alu->imul_2<word, dword>(operand1, operand2);
	QVERIFY2(result == expectedResult, "Results don't match");
	QVERIFY2(cpu->eflags.eflags.flag32 == eflags_final, "Flags error");
}

