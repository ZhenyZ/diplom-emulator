QT += testlib gui widgets

TARGET = tst_alutest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += ../../
INCLUDEPATH += ../../../lib/

#LIBS += -L/../../../lib/ -lEMUESDebugLib -lEMUESDevicesLib

SOURCES += tst_alutest.cpp \
    tst_alutest_shifts.cpp \
	tst_alutest_arithm.cpp \
	tst_cmptest.cpp \
    main.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/./ -li486cpu
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/./ -li486cpu
else:unix: LIBS += -L$$PWD/./ -li486cpu

INCLUDEPATH += $$PWD/../../
DEPENDPATH += $$PWD/../../

HEADERS += \
    tst_alutest.h \
	tst_cmptest.h

