#include "tst_alutest.h"

void aluTest::testShl8_data() {
	QTest::addColumn<dword>("eflags_initial");
	QTest::addColumn<byte>("operand");
	QTest::addColumn<byte>("shiftCount");
	QTest::addColumn<byte>("expectedResult");
	QTest::addColumn<dword>("eflags_final");
	QTest::newRow("shl_0-by-0_exp+ZP-S") << (dword)0xFF << (byte)0x00 << (byte)0x00 << (byte)0x00 << (dword)(0xFF & FLAG_SF_CLEAR);
	QTest::newRow("shl_0-by-0_exp+ZP") << (dword)0x00 << (byte)0x00 << (byte)0x00 << (byte)0x00 << (dword)(FLAG_ZF | FLAG_PF);
	QTest::newRow("shl_FF-by-0_exp-FF+SP") << (dword)0x00 << (byte)0xFF << (byte)0x00 << (byte)0xFF << (dword)(FLAG_SF | FLAG_PF);
	QTest::newRow("shl_40-by-1_exp-80+SO") << (dword)0x00 << (byte)0x40 << (byte)0x01 << (byte)0x80 << (dword)(FLAG_SF | FLAG_OF);
	QTest::newRow("shl_1-by-7_exp-80+S") << (dword)0x00 << (byte)0x01 << (byte)0x07 << (byte)0x80 << (dword)(FLAG_SF);
	QTest::newRow("shl_1-by-8_exp-00+PCZ") << (dword)0x00 << (byte)0x01 << (byte)0x08 << (byte)0x00 << (dword)(FLAG_ZF | FLAG_CF | FLAG_PF);
}

void aluTest::testShl8() {
	QFETCH(dword, eflags_initial);
	QFETCH(byte, operand);
	QFETCH(byte, shiftCount);
	QFETCH(byte, expectedResult);
	QFETCH(dword, eflags_final);
	cpu->eflags.eflags.flag32 = eflags_initial;
	byte result = cpu->alu->shl<byte>(operand, shiftCount);
	QVERIFY2(result == expectedResult, "Results don't match");
	QVERIFY2(cpu->eflags.eflags.flag32 == eflags_final, "Flags error");
}

void aluTest::testShl16_data() {
	QTest::addColumn<dword>("eflags_initial");
	QTest::addColumn<word>("operand");
	QTest::addColumn<byte>("shiftCount");
	QTest::addColumn<word>("expectedResult");
	QTest::addColumn<dword>("eflags_final");
	QTest::newRow("shl_0-by-0_exp+ZP-S")		<< (dword)0xFF << (word)0x0000 << (byte)0x00 << (word)0x00 << (dword)(0xFF & FLAG_SF_CLEAR);
	QTest::newRow("shl_0-by-0_exp+ZP")			<< (dword)0x00 << (word)0x0000 << (byte)0x00 << (word)0x00 << (dword)(FLAG_ZF | FLAG_PF);
	QTest::newRow("shl_FFFF-by-0_exp-FFFF+SP")	<< (dword)0x00 << (word)0xFFFF << (byte)0x00 << (word)0xFFFF << (dword)(FLAG_SF | FLAG_PF);
	QTest::newRow("shl_1-by-15_exp-8000+S")		<< (dword)0x00 << (word)0x0001 << (byte)15 << (word)0x8000 << (dword)(FLAG_SF);
	QTest::newRow("shl_1-by-16_exp-0000+PCZ")	<< (dword)0x00 << (word)0x0001 << (byte)16 << (word)0x0000 << (dword)(FLAG_ZF | FLAG_CF | FLAG_PF);
}

void aluTest::testShl16() {
	QFETCH(dword, eflags_initial);
	QFETCH(word, operand);
	QFETCH(byte, shiftCount);
	QFETCH(word, expectedResult);
	QFETCH(dword, eflags_final);
	cpu->eflags.eflags.flag32 = eflags_initial;
	word result = cpu->alu->shl<word>(operand, shiftCount);
	QVERIFY2(result == expectedResult, "Results don't match");
	QVERIFY2(cpu->eflags.eflags.flag32 == eflags_final, "Flags error");
}

void aluTest::testShl32_data() {
	QTest::addColumn<dword>("eflags_initial");
	QTest::addColumn<dword>("operand");
	QTest::addColumn<byte>("shiftCount");
	QTest::addColumn<dword>("expectedResult");
	QTest::addColumn<dword>("eflags_final");
	QTest::newRow("shl_0-by-0_exp+ZP-S")		<< (dword)0xFF << (dword)0x00 << (byte)0x00 << (dword)0x00 << (dword)(0xFF & FLAG_SF_CLEAR);
	QTest::newRow("shl_0-by-0_exp+ZP")			<< (dword)0x00 << (dword)0x00 << (byte)0x00 << (dword)0x00 << (dword)(FLAG_ZF | FLAG_PF);
	QTest::newRow("shl_*FF-by-0_exp-*FF+SP")	<< (dword)0x00 << (dword)0xFFFFFFFF << (byte)0x00 << (dword)0xFFFFFFFF << (dword)(FLAG_SF | FLAG_PF);
	QTest::newRow("shl_1-by-31_exp-80000000+S")	<< (dword)0x00 << (dword)0x0001 << (byte)31 << (dword)0x80000000 << (dword)(FLAG_SF);
	QTest::newRow("shl_1-by-32_exp-1")			<< (dword)0x00 << (dword)0x0001 << (byte)32 << (dword)0x0001 << (dword)(0x00);
}

void aluTest::testShl32() {
	QFETCH(dword, eflags_initial);
	QFETCH(dword, operand);
	QFETCH(byte, shiftCount);
	QFETCH(dword, expectedResult);
	QFETCH(dword, eflags_final);
	cpu->eflags.eflags.flag32 = eflags_initial;
	dword result = cpu->alu->shl<dword>(operand, shiftCount);
	QVERIFY2(result == expectedResult, "Results don't match");
	QVERIFY2(cpu->eflags.eflags.flag32 == eflags_final, "Flags error");
}

void aluTest::testShr8_data() {
	QTest::addColumn<dword>("eflags_initial");
	QTest::addColumn<byte>("operand");
	QTest::addColumn<byte>("shiftCount");
	QTest::addColumn<byte>("expectedResult");
	QTest::addColumn<dword>("eflags_final");
	QTest::newRow("shr_0-by-0_exp-Z") << (dword)0xFF << (byte)0x00 << (byte)0x00 << (byte)0x00 << (dword)(0xFF & FLAG_SF_CLEAR);
	QTest::newRow("shr_0-by-0_exp+ZP") << (dword)0x00 << (byte)0x00 << (byte)0x00 << (byte)0x00 << (dword)(FLAG_ZF | FLAG_PF);
	QTest::newRow("shr_FF-by-0_exp-FF+SP") << (dword)0x00 << (byte)0xFF << (byte)0x00 << (byte)0xFF << (dword)(FLAG_SF | FLAG_PF);
	QTest::newRow("shr_0x80-by-7_exp-1") << (dword)0x00 << (byte)0x80 << (byte)0x07 << (byte)0x01 << (dword)0x00;
	QTest::newRow("shr_0x80-by-8_exp-00+PCZ") << (dword)0x00 << (byte)0x80 << (byte)0x08 << (byte)0x00 << (dword)(FLAG_ZF | FLAG_CF | FLAG_PF);
}

void aluTest::testShr8() {
	QFETCH(dword, eflags_initial);
	QFETCH(byte, operand);
	QFETCH(byte, shiftCount);
	QFETCH(byte, expectedResult);
	QFETCH(dword, eflags_final);
	cpu->eflags.eflags.flag32 = eflags_initial;
	byte result = cpu->alu->shr<byte>(operand, shiftCount);
	QVERIFY2(result == expectedResult, "Results don't match");
	QVERIFY2(cpu->eflags.eflags.flag32 == eflags_final, "Flags error");
}

void aluTest::testShr16_data() {
	QTest::addColumn<dword>("eflags_initial");
	QTest::addColumn<word>("operand");
	QTest::addColumn<byte>("shiftCount");
	QTest::addColumn<word>("expectedResult");
	QTest::addColumn<dword>("eflags_final");
	QTest::newRow("shr_0-by-0_no_flags_clear") << (dword)0xFF << (word)0x00 << (byte)0x00 << (word)0x00 << (dword)(0xFF & FLAG_SF_CLEAR);
	QTest::newRow("shr_0-by-0_no_flags_set") << (dword)0x00 << (word)0x00 << (byte)0x00 << (word)0x00 << (dword)(FLAG_ZF | FLAG_PF);
	QTest::newRow("shr_FFFF-by-0_exp-FFFF+SP") << (dword)0x00 << (word)0xFFFF << (byte)0x00 << (word)0xFFFF << (dword)(FLAG_SF | FLAG_PF);
	QTest::newRow("shr_0x80-by-15_exp-1") << (dword)0x00 << (word)0x8000 << (byte)15 << (word)0x0001 << (dword)0x00;
	QTest::newRow("shr_0x80-by-16_exp-0+PCZ") << (dword)0x00 << (word)0x8000 << (byte)16 << (word)0x0000 << (dword)(FLAG_ZF | FLAG_CF | FLAG_PF);
}

void aluTest::testShr16() {
	QFETCH(dword, eflags_initial);
	QFETCH(word, operand);
	QFETCH(byte, shiftCount);
	QFETCH(word, expectedResult);
	QFETCH(dword, eflags_final);
	cpu->eflags.eflags.flag32 = eflags_initial;
	word result = cpu->alu->shr<word>(operand, shiftCount);
	QVERIFY2(result == expectedResult, "Results don't match");
	QVERIFY2(cpu->eflags.eflags.flag32 == eflags_final, "Flags error");
}

void aluTest::testShr32_data() {
	QTest::addColumn<dword>("eflags_initial");
	QTest::addColumn<dword>("operand");
	QTest::addColumn<byte>("shiftCount");
	QTest::addColumn<dword>("expectedResult");
	QTest::addColumn<dword>("eflags_final");
	QTest::newRow("shr_0-by-0_no_flags_clear") << (dword)0xFF << (dword)0x00 << (byte)0x00 << (dword)0x00 << (dword)(0xFF & FLAG_SF_CLEAR);
	QTest::newRow("shr_0-by-0_no_flags_set") << (dword)0x00 << (dword)0x00 << (byte)0x00 << (dword)0x00 << (dword)(FLAG_ZF | FLAG_PF);
	QTest::newRow("shr_*FF-by-0_exp-*FF+SP") << (dword)0x00 << (dword)0xFFFFFFFF << (byte)0x00 << (dword)0xFFFFFFFF << (dword)(FLAG_SF | FLAG_PF);
	QTest::newRow("shr_80*-by-31_exp-1") << (dword)0x00 << (dword)0x80000000 << (byte)31 << (dword)0x01 << (dword)0x00;
	QTest::newRow("shr_80*-by-32_exp-80*+S") << (dword)0x00 << (dword)0x80000000 << (byte)32 << (dword)0x80000000 << (dword)(FLAG_SF);
}

void aluTest::testShr32() {
	QFETCH(dword, eflags_initial);
	QFETCH(dword, operand);
	QFETCH(byte, shiftCount);
	QFETCH(dword, expectedResult);
	QFETCH(dword, eflags_final);
	cpu->eflags.eflags.flag32 = eflags_initial;
	dword result = cpu->alu->shr<dword>(operand, shiftCount);
	QVERIFY2(result == expectedResult, "Results don't match");
	QVERIFY2(cpu->eflags.eflags.flag32 == eflags_final, "Flags error");
}

void aluTest::testSar8_data() {
	QTest::addColumn<dword>("eflags_initial");
	QTest::addColumn<byte>("operand");
	QTest::addColumn<byte>("shiftCount");
	QTest::addColumn<byte>("expectedResult");
	QTest::addColumn<dword>("eflags_final");
	QTest::newRow("sar_0-by-0_exp-S") << (dword)0xFF << (byte)0x00 << (byte)0x00 << (byte)0x00 << (dword)(0xFF & FLAG_SF_CLEAR);
	QTest::newRow("sar_0-by-0_exp+ZP") << (dword)0x00 << (byte)0x00 << (byte)0x00 << (byte)0x00 << (dword)(FLAG_ZF | FLAG_PF);
	QTest::newRow("sar_FF-by-0_exp-FF+SP") << (dword)0x00 << (byte)0xFF << (byte)0x00 << (byte)0xFF << (dword)(FLAG_SF | FLAG_PF);
	QTest::newRow("sar_0x80-by-7_exp-FF+SP") << (dword)0x00 << (byte)0x80 << (byte)0x07 << (byte)0xFF << (dword)(FLAG_SF | FLAG_PF);
	QTest::newRow("sar_0x40-by-7_exp-0+PCZ") << (dword)0x00 << (byte)0x40 << (byte)0x07 << (byte)0x00 << (dword)(FLAG_ZF | FLAG_CF | FLAG_PF);
}

void aluTest::testSar8() {
	QFETCH(dword, eflags_initial);
	QFETCH(byte, operand);
	QFETCH(byte, shiftCount);
	QFETCH(byte, expectedResult);
	QFETCH(dword, eflags_final);
	cpu->eflags.eflags.flag32 = eflags_initial;
	byte result = cpu->alu->sar<byte>(operand, shiftCount);
	QVERIFY2(result == expectedResult, "Results don't match");
	QVERIFY2(cpu->eflags.eflags.flag32 == eflags_final, "Flags error");
}

void aluTest::testSar16_data() {
	QTest::addColumn<dword>("eflags_initial");
	QTest::addColumn<word>("operand");
	QTest::addColumn<byte>("shiftCount");
	QTest::addColumn<word>("expectedResult");
	QTest::addColumn<dword>("eflags_final");
	QTest::newRow("sar_0-by-0_no_flags_clear") << (dword)0xFF << (word)0x00 << (byte)0x00 << (word)0x00 << (dword)(0xFF & FLAG_SF_CLEAR);
	QTest::newRow("sar_0-by-0_no_flags_set") << (dword)0x00 << (word)0x00 << (byte)0x00 << (word)0x00 << (dword)(FLAG_ZF | FLAG_PF);
	QTest::newRow("sar_FFFF-by-0_exp-FFFF+SP") << (dword)0x00 << (word)0xFFFF << (byte)0x00 << (word)0xFFFF << (dword)(FLAG_SF | FLAG_PF);
	QTest::newRow("sar_0x80-by-15_exp-FF+SP") << (dword)0x00 << (word)0x8000 << (byte)15 << (word)0xFFFF << (dword)(FLAG_SF | FLAG_PF);
	QTest::newRow("sar_0x40-by-15_exp-0+PCZ") << (dword)0x00 << (word)0x4000 << (byte)15 << (word)0x0000 << (dword)(FLAG_ZF | FLAG_CF | FLAG_PF);
}

void aluTest::testSar16() {
	QFETCH(dword, eflags_initial);
	QFETCH(word, operand);
	QFETCH(byte, shiftCount);
	QFETCH(word, expectedResult);
	QFETCH(dword, eflags_final);
	cpu->eflags.eflags.flag32 = eflags_initial;
	word result = cpu->alu->sar<word>(operand, shiftCount);
	QVERIFY2(result == expectedResult, "Results don't match");
	QVERIFY2(cpu->eflags.eflags.flag32 == eflags_final, "Flags error");
}

void aluTest::testSar32_data() {
	QTest::addColumn<dword>("eflags_initial");
	QTest::addColumn<dword>("operand");
	QTest::addColumn<byte>("shiftCount");
	QTest::addColumn<dword>("expectedResult");
	QTest::addColumn<dword>("eflags_final");
	QTest::newRow("sar_0-by-0_no_flags_clear") << (dword)0xFF << (dword)0x00 << (byte)0x00 << (dword)0x00 << (dword)(0xFF & FLAG_SF_CLEAR);
	QTest::newRow("sar_0-by-0_no_flags_set") << (dword)0x00 << (dword)0x00 << (byte)0x00 << (dword)0x00 << (dword)(FLAG_ZF | FLAG_PF);
	QTest::newRow("sar_*FF-by-0_exp-*FF+SP") << (dword)0x00 << (dword)0xFFFFFFFF << (byte)0x00 << (dword)0xFFFFFFFF << (dword)(FLAG_SF | FLAG_PF);
	QTest::newRow("sar_80*-by-31_exp-FFFFFFFF+PS") << (dword)0x00 << (dword)0x80000000 << (byte)31 << (dword)0xFFFFFFFF << (dword)(FLAG_SF | FLAG_PF);
	QTest::newRow("sar_40*-by-31_exp-0+ZPC") << (dword)0x00 << (dword)0x40000000 << (byte)31 << (dword)0 << (dword)(FLAG_PF | FLAG_ZF | FLAG_CF);
}

void aluTest::testSar32() {
	QFETCH(dword, eflags_initial);
	QFETCH(dword, operand);
	QFETCH(byte, shiftCount);
	QFETCH(dword, expectedResult);
	QFETCH(dword, eflags_final);
	cpu->eflags.eflags.flag32 = eflags_initial;
	dword result = cpu->alu->sar<dword>(operand, shiftCount);
	QVERIFY2(result == expectedResult, "Results don't match");
	QVERIFY2(cpu->eflags.eflags.flag32 == eflags_final, "Flags error");
}

void aluTest::testRcr8_data() {
	QTest::addColumn<dword>("eflags_initial");
	QTest::addColumn<byte>("operand");
	QTest::addColumn<byte>("shiftCount");
	QTest::addColumn<byte>("expectedResult");
	QTest::addColumn<dword>("eflags_final");
	QTest::newRow("rcr_-C+0-by-0_exp-0") << (dword)0xFF << (byte)0x00 << (byte)0x00 << (byte)0x00 << (dword)(0xFF);
	QTest::newRow("rcr_-C+FF-by-0_exp-FF") << (dword)0x00 << (byte)0xFF << (byte)0x00 << (byte)0xFF << (dword)(0x00);
	QTest::newRow("rcr_-C+0x80-by-7_exp-1-C") << (dword)0x00 << (byte)0x80 << (byte)0x07 << (byte)0x01 << (dword)(0);
	QTest::newRow("rcr_+C+0x80-by-7_exp-3-C") << (dword)(FLAG_CF) << (byte)0x80 << (byte)0x07 << (byte)0x3 << (dword)(0x00);
	QTest::newRow("rcr_-C+0xAA-by-4_exp-4A+C") << (dword)0x00 << (byte)0xAA << (byte)0x04 << (byte)0x4A << (dword)(FLAG_CF);
	QTest::newRow("rcr_+C+0xAA-by-4_exp-5A+C") << (dword)(FLAG_CF) << (byte)0xAA << (byte)0x04 << (byte)0x5A << (dword)(FLAG_CF);
}

void aluTest::testRcr8() {
	QFETCH(dword, eflags_initial);
	QFETCH(byte, operand);
	QFETCH(byte, shiftCount);
	QFETCH(byte, expectedResult);
	QFETCH(dword, eflags_final);
	cpu->eflags.eflags.flag32 = eflags_initial;
	byte result = cpu->alu->rcr<byte>(operand, shiftCount);
	QVERIFY2(result == expectedResult, "Results don't match");
	QVERIFY2(cpu->eflags.eflags.flag32 == eflags_final, "Flags error");
}

void aluTest::testRcl8_data() {
	QTest::addColumn<dword>("eflags_initial");
	QTest::addColumn<byte>("operand");
	QTest::addColumn<byte>("shiftCount");
	QTest::addColumn<byte>("expectedResult");
	QTest::addColumn<dword>("eflags_final");
	QTest::newRow("rcl_-C+0-by-0_exp-0") << (dword)0xFF << (byte)0x00 << (byte)0x00 << (byte)0x00 << (dword)(0xFF);
	QTest::newRow("rcl_-C+FF-by-0_exp-FF") << (dword)0x00 << (byte)0xFF << (byte)0x00 << (byte)0xFF << (dword)(0x00);
	QTest::newRow("rcl_-C+0x80-by-7_exp-0x20-C") << (dword)0x00 << (byte)0x80 << (byte)0x07 << (byte)0x20 << (dword)(0);
	QTest::newRow("rcl_+C+0x80-by-7_exp-0x60-C") << (dword)(FLAG_CF) << (byte)0x80 << (byte)0x07 << (byte)0x60 << (dword)(0x00);
	QTest::newRow("rcl_-C+0xAA-by-4_exp-A5-C") << (dword)0x00 << (byte)0xAA << (byte)0x04 << (byte)0xA5 << (dword)(0x00);
	QTest::newRow("rcl_+C+0xAA-by-5_exp-5A+C") << (dword)(FLAG_CF) << (byte)0xAA << (byte)0x05 << (byte)0x5A << (dword)(FLAG_CF);
}

void aluTest::testRcl8() {
	QFETCH(dword, eflags_initial);
	QFETCH(byte, operand);
	QFETCH(byte, shiftCount);
	QFETCH(byte, expectedResult);
	QFETCH(dword, eflags_final);
	cpu->eflags.eflags.flag32 = eflags_initial;
	byte result = cpu->alu->rcl<byte>(operand, shiftCount);
	QVERIFY2(result == expectedResult, "Results don't match");
	QVERIFY2(cpu->eflags.eflags.flag32 == eflags_final, "Flags error");
}

void aluTest::testRol8_data() {
	QTest::addColumn<dword>("eflags_initial");
	QTest::addColumn<byte>("operand");
	QTest::addColumn<byte>("shiftCount");
	QTest::addColumn<byte>("expectedResult");
	QTest::addColumn<dword>("eflags_final");
	QTest::newRow("rol_0-by-0_exp-0")			<< (dword)0xFF << (byte)0x00 << (byte)0x00 << (byte)0x00 << (dword)(0xFF);
	QTest::newRow("rol_FF-by-0_exp-FF")			<< (dword)0x00 << (byte)0xFF << (byte)0x00 << (byte)0xFF << (dword)(0x00);
	QTest::newRow("rol_0x80-by-7_exp-0x40-C")	<< (dword)0x00 << (byte)0x80 << (byte)0x7 << (byte)0x40 << (dword)(0);
	QTest::newRow("rol_0x80-by-1_exp-0x01+CO")	<< (dword)(0x00) << (byte)0x80 << (byte)0x01 << (byte)0x01 << (dword)(FLAG_CF | FLAG_OF);
	QTest::newRow("rol_0xAA-by-4_exp-AA-C")		<< (dword)0x00 << (byte)0xAA << (byte)0x04 << (byte)0xAA << (dword)(0x00);
	QTest::newRow("rol_0xAA-by-5_exp-55+C")		<< (dword)(FLAG_CF) << (byte)0xAA << (byte)0x05 << (byte)0x55 << (dword)(FLAG_CF);
}

void aluTest::testRol8() {
	QFETCH(dword, eflags_initial);
	QFETCH(byte, operand);
	QFETCH(byte, shiftCount);
	QFETCH(byte, expectedResult);
	QFETCH(dword, eflags_final);
	cpu->eflags.eflags.flag32 = eflags_initial;
	byte result = cpu->alu->rol<byte>(operand, shiftCount);
	QVERIFY2(result == expectedResult, "Results don't match");
	QVERIFY2(cpu->eflags.eflags.flag32 == eflags_final, "Flags error");
}

void aluTest::testRor8_data() {
	QTest::addColumn<dword>("eflags_initial");
	QTest::addColumn<byte>("operand");
	QTest::addColumn<byte>("shiftCount");
	QTest::addColumn<byte>("expectedResult");
	QTest::addColumn<dword>("eflags_final");
	QTest::newRow("ror_0-by-0_exp-0") << (dword)0xFF << (byte)0x00 << (byte)0x00 << (byte)0x00 << (dword)(0xFF);
	QTest::newRow("ror_FF-by-0_exp-FF") << (dword)0x00 << (byte)0xFF << (byte)0x00 << (byte)0xFF << (dword)(0x00);
	QTest::newRow("ror_0x80-by-7_exp-0x01-C") << (dword)0x00 << (byte)0x80 << (byte)0x07 << (byte)0x01 << (dword)(0);
	QTest::newRow("ror_0x40-by-7_exp-0x80+C") << (dword)(0x00) << (byte)0x40 << (byte)0x07 << (byte)0x80 << (dword)(FLAG_CF);
	QTest::newRow("ror_0xAA-by-4_exp-AA+C") << (dword)0x00 << (byte)0xAA << (byte)0x04 << (byte)0xAA << (dword)(FLAG_CF);
	QTest::newRow("ror_0xAA-by-5_exp-55-C") << (dword)(FLAG_CF) << (byte)0xAA << (byte)0x05 << (byte)0x55 << (dword)(0x00);
}

void aluTest::testRor8() {
	QFETCH(dword, eflags_initial);
	QFETCH(byte, operand);
	QFETCH(byte, shiftCount);
	QFETCH(byte, expectedResult);
	QFETCH(dword, eflags_final);
	cpu->eflags.eflags.flag32 = eflags_initial;
	byte result = cpu->alu->ror<byte>(operand, shiftCount);
	QVERIFY2(result == expectedResult, "Results don't match");
	QVERIFY2(cpu->eflags.eflags.flag32 == eflags_final, "Flags error");
}
