#include "cpuintelx86.h"
#include "cpuintelx86_alu.h"
#include "cpuintelx86_types.h"

void CPUIntelx86::do_ProceedString(Instruction &instr) {
	switch (instr.opcode) {
		case 0x6C: strOp = soiINSB; break;
		case 0x6D: strOp = soiINSW; break;
		case 0x6E: strOp = soiOUTSB; break;
		case 0x6F: strOp = soiOUTSW; break;
		case 0xA4: strOp = soiMOVSB; break;
		case 0xA5: strOp = soiMOVSW; break;
		case 0xA6: strOp = soiCMPSB; break;
		case 0xA7: strOp = soiCMPSW; break;
		case 0xAA: strOp = soiSTOSB; break;
		case 0xAB: strOp = soiSTOSW; break;
		case 0xAC: strOp = soiLODSB; break;
		case 0xAD: strOp = soiLODSW; break;
		case 0xAE: strOp = soiSCASB; break;
		case 0xAF: strOp = soiSCASW; break;
	}
	strOpPrepare(instr);
	strOpProceed(instr);
}

void CPUIntelx86::strOpDebug(Instruction &instr) {
	debug_op2({opMem, instr.szOperands, stringDest(0, instr)}, {opMem, instr.szOperands, stringSource(0, instr)});
}

void CPUIntelx86::strOpPrepare(Instruction &instr) {
	cyclesTotal += 4;

	strOpSrcBase = (this->*getSegmentAddress)(instr.segmentPrefix);
	strOpDstBase = (this->*getSegmentAddress)(REGISTER_ES);
	strOpRep = (instr.repPrefix != repNone);

	switch (strOp) {
		case soiCMPSB:
		case soiMOVSB:
		case soiINSB:
		case soiLODSB:
		case soiSCASB:
		case soiSTOSB:
		case soiOUTSB:
			instr.szOperands = sz8;
		break;
		default:
		break;
	}

	if (instr.szAddress == sz16) {
		strOpCtr = getReg16(REGISTER_ECX);
		strOpDst = getReg16(REGISTER_EDI);
		strOpSrc = getReg16(REGISTER_ESI);
	}
	else {
		strOpCtr = getReg32(REGISTER_ECX);
		strOpDst = getReg32(REGISTER_EDI);
		strOpSrc = getReg32(REGISTER_ESI);
	}

	strOpDebug(instr);
}

void CPUIntelx86::strOpProceed(Instruction &instr) {
	operOK = 1;
	bool condPrefix = false;
	if ((strOp == soiCMPSB) || (strOp == soiCMPSW) || (strOp == soiSCASB) || (strOp == soiSCASW))
		condPrefix = true;

	do {
		if (instr.szOperands == sz8) {
			strOpExecute8(instr);
		} else {
			strOpExecute16(instr);
		}
		if (!operOK) break;
		if (instr.repPrefix == repNone) break;
		strOpCtr--;
		if (instr.szAddress == sz16) {
			strOpCtr &= 0xFFFF;
		}

		if (strOpCtr == 0) break;
		if (condPrefix) {
			dword flg = (eflags.eflags.flag32 & FLAG_ZF);
			if ((instr.repPrefix == repEqual) && (0 == flg)) {
				break;
			}
			if ((instr.repPrefix == repNonEqual) && (1 == flg)) {
				break;
			}
		}
	} while (strOpCtr != 0);

	if (operOK) {
		if (instr.szAddress == sz16) {
			setReg16(REGISTER_ECX, (strOpCtr & 0xFFFF));
			setReg16(REGISTER_ESI, (strOpSrc & 0xFFFF));
			setReg16(REGISTER_EDI, (strOpDst & 0xFFFF));
		} else {
			setReg32(REGISTER_ECX, strOpCtr);
			setReg32(REGISTER_ESI, strOpSrc);
			setReg32(REGISTER_EDI, strOpDst);
		}
	}

	instr.repPrefix = repNone;
}

void CPUIntelx86::strOpExecute8(Instruction &instr) {
	int opLen = 1;
	byte src8;
	byte dst8;

	switch (strOp) {
		case soiMOVSB:
			src8 = getByteR(stringSource(opLen, instr), instr.segmentPrefix); if (!operOK) break;
			setByteR(stringDest(opLen, instr), src8, REGISTER_ES);
		break;

		case soiCMPSB:
			src8 = getByteR(stringSource(opLen, instr), instr.segmentPrefix); if (!operOK) break;
			dst8 = getByteR(stringDest(opLen, instr), REGISTER_ES); if (!operOK) break;
			alu->cmp<byte, word>(src8, dst8);
		break;

		case soiLODSB:
			src8 = getByteR(stringSource(opLen, instr), instr.segmentPrefix); if (!operOK) break;
			setReg8(REGISTER_EAX, src8);
		break;

		case soiSTOSB:
			setByteR(stringDest(opLen, instr), getReg8(REGISTER_EAX), REGISTER_ES);
			//strOpNextDI(1);
		break;

		case soiSCASB:
			src8 = getReg8(REGISTER_EAX);
			dst8 = getByteR(stringDest(opLen, instr), REGISTER_ES); if(!operOK) break;
			alu->cmp<byte, word>(src8, dst8);
			//strOpNextDI(1);
		break;

		case soiOUTSB:
			src8 = getByteR(stringSource(opLen, instr), instr.segmentPrefix); if (!operOK) break;
			processOutput(getReg16(REGISTER_EDX), src8, 1);
			//strOpNextSI(1);
		break;

		case soiINSB:
			//strOpNextDI(1);
		break;

		default:
			stop();
			debug_log("String 8-bit expected, got 16-bit");
	}
}

void CPUIntelx86::strOpExecute16(Instruction &instr) {
	int opLen;
	if (instr.szOperands == sz16) {
		opLen = 2;
		word src16;
		word dst16;

		switch (strOp)
		{
			case soiMOVSW:
				src16 = getWordR(stringSource(opLen, instr), instr.segmentPrefix); if (!operOK) break;
				setWordR(stringDest(opLen, instr), src16, REGISTER_ES);
				//strOpNext(2);

			break;

			case soiCMPSW:
				src16 = getWordR(stringSource(opLen, instr), instr.segmentPrefix); if (!operOK) break;
				dst16 = getWordR(stringDest(opLen, instr), REGISTER_ES); if (!operOK) break;
				alu->cmp<word, dword>(src16, dst16);
				//strOpNext(2);
			break;

			case soiLODSW:
				src16 = getWordR(stringSource(opLen, instr), instr.segmentPrefix); if (!operOK) break;
				setReg16(REGISTER_EAX, src16);
				//strOpNextSI(2);
			break;

			case soiSTOSW:
				setWordR(stringDest(opLen, instr), getReg16(REGISTER_EAX), REGISTER_ES);
				//strOpNextDI(2);
			break;

			case soiSCASW:
				src16 = getReg16(REGISTER_EAX);
				dst16 = getWordR(stringDest(opLen, instr), REGISTER_ES); if(!operOK) break;
				alu->cmp<word, dword>(src16, dst16);
				//strOpNextDI(2);
			break;

			case soiOUTSW:
				src16 = getWordR(stringSource(opLen, instr), instr.segmentPrefix); if (!operOK) break;
				// TODO: Lost bytes
				processOutput(getReg16(REGISTER_EDX), src16, 2);
				strOpNextSI(2, instr);
			break;

			case soiINSW:
				stringDest(opLen, instr);
			break;

			default:
				stop();
				debug_log("String 16-bit expected, got 8/32-bit");
		}
		//strOpNext(2);
	} else {
		dword src32;
		dword dst32;
		opLen = 4;

		switch (strOp)
		{
			case soiMOVSW:
				src32 = getDWordR(stringSource(opLen, instr), instr.segmentPrefix); if (!operOK) break;
				setDWordR(stringDest(opLen, instr), src32, REGISTER_ES);
				//strOpNext(4);
			break;

			case soiCMPSW:
				src32 = getDWordR(stringSource(opLen, instr), instr.segmentPrefix); if (!operOK) break;
				dst32 = getDWordR(stringDest(opLen, instr), REGISTER_ES); if (!operOK) break;
				alu->cmp<dword, qword>(src32, dst32);
				//strOpNext(4);
			break;

			case soiLODSW:
				src32 = getDWordR(stringSource(opLen, instr), instr.segmentPrefix); if (!operOK) break;
				setReg32(REGISTER_EAX, src32);
				//strOpNextSI(4);
			break;

			case soiSTOSW:
				setDWordR(stringDest(opLen, instr), getReg32(REGISTER_EAX), REGISTER_ES);
				//strOpNextDI(4);
			break;

			case soiSCASW:
				src32 = getReg32(REGISTER_EAX);
				dst32 = getDWordR(stringDest(opLen, instr), REGISTER_ES); if(!operOK) break;
				alu->cmp<dword, qword>(src32, dst32);
				//strOpNextDI(4);
			break;

			case soiOUTSW:
			// TODO: Lost bytes
				src32 = getDWordR(stringSource(opLen, instr), instr.segmentPrefix); if (!operOK) break;
				processOutput(getReg32(REGISTER_EDX), src32, 4);
				//strOpNextSI(4);
			break;

			case soiINSW:
				stringDest(opLen, instr);
				//strOpNextDI(4);
			break;

			default:
				stop();
				debug_log("String 32-bit expected, got 8/16-bit");
		}

	}
}

void CPUIntelx86::strOpNext(int count, Instruction &instr) {
	strOpNextSI(count, instr);
	strOpNextDI(count, instr);
}

void CPUIntelx86::strOpNextSI(int count, Instruction &instr) {
	if (0 == (eflags.eflags.flag32 & FLAG_DF))
		strOpSrc += count;
	else
		strOpSrc -= count;
	if (instr.szAddress == sz16) {
		strOpSrc &= 0xFFFF;
	}

}

void CPUIntelx86::strOpNextDI(int count, Instruction &instr) {
	if (0 == (eflags.eflags.flag32 & FLAG_DF)) {
		strOpDst += count;
	} else {
		strOpDst -= count;
	}
	if (instr.szAddress == sz16) {
		strOpDst &= 0xFFFF;
	}

}

dword CPUIntelx86::stringDest(int count, Instruction &instr) {
	dword address = strOpDstBase + strOpDst;
	strOpNextDI(count, instr);
	return address;
}

dword CPUIntelx86::stringSource(int count, Instruction &instr) {
	dword address = strOpSrcBase + strOpSrc;
	strOpNextSI(count, instr);
	return address;
}

