#ifndef CPUINTELX86_ALU_H
#define CPUINTELX86_ALU_H


#include "cpuintelx86_types.h"
#include "cpuintelx86_decoder.h"
#include "cpuintelx86_global.h"

class CPUIntelx86;

const byte shlmask8[9] = {
	0, 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01
};
const word shlmask16[9] = {
	0, 0x8000, 0x4000, 0x2000, 0x1000, 0x0800, 0x0400, 0x0200, 0x0100
};
const dword shlmask32[9] = {
	0, 0x80000000, 0x40000000, 0x20000000, 0x10000000, 0x08000000, 0x04000000, 0x02000000, 0x01000000
};

const byte shrmask8[9] = {
	0, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80
};
const word shrmask16[17] = {
	0, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x0100, 0x0200, 0x0400, 0x0800, 0x1000,
	0x2000, 0x4000, 0x8000
};

const dword shrmask32[33] = {
	0,
	0x01, 0x02, 0x04, 0x08,
	0x10, 0x20, 0x40, 0x80,
	0x0100, 0x0200, 0x0400, 0x0800,
	0x1000,	0x2000, 0x4000, 0x8000,
	0x100000, 0x200000, 0x400000, 0x800000,
	0x10000000, 0x20000000, 0x40000000, 0x80000000
};

constexpr dword
	CLEAR_OC	= (~(FLAG_OF | FLAG_CF));

enum opShiftType {stROL=0, stROR=1, stRCL=2, stRCR=3, stSHL=4, stSHR=5, stSAL=6, stSAR=7};

class I486SHARED_EXPORT CPUIntelx86_ALU
{
public:

	CPUIntelx86_ALU(CPUIntelx86 *myCPU, FlagsRegister *flags, Register* regAccumulator, Register* regData);

	template<typename opType> inline opType getTypeMask();

	template<typename T> T Xor(T op1, T op2);
	template<typename T> T Or(T op1, T op2);
	template<typename T> T And(T op1, T op2);

	qint8	byte2s8(byte b);
	qint16	byte2s16(byte b);
	qint32	byte2s32(byte b);
	qint16	word2s16(word w);
	qint32	word2s32(word w);
	qint32	dword2s32(dword d);

	template <typename sz> sz shift(sz operand, byte shiftCount, opShiftType kind);

	template <typename opsize> opsize inline getRclCount(byte sourceCount);
	template <typename opsize> opsize inline getRolCount(byte sourceCount);
	template <typename opsize> opsize inline getShiftCount();
	template <typename opsize> opsize inline getMostSignificantBitValue();

	template <typename sz> sz shl(sz operand, byte shiftCount);
	template <typename sz> sz shr(sz operand, byte shiftCount);
	template <typename sz> sz sar(sz operand, byte shiftCount);
	template <typename sz> sz rol(sz operand, byte shiftCount);
	template <typename sz> sz rcl(sz operand, byte shiftCount);
	template <typename sz> sz ror(sz operand, byte shiftCount);
	template <typename sz> sz rcr(sz operand, byte shiftCount);

	template<typename opType, typename opTypeNext> opType add(opType operand1, opType operand2);
	template<typename opType, typename opTypeNext> opType sub(opType operand1, opType operand2);
	template<typename T, typename TNext> T adc(T op1, T op2);
	template<typename T, typename TNext> T sbb(T op1, T op2);
	template<typename T, typename TNext> void cmp(T op1, T op2);

	template<typename OperandType> inline OperandType getRegisterBeforeMul();
	template<typename OperandType> inline void setRegistersAfterMul(OperandType result);
	template<typename OperandType, typename ResultType>	ResultType mul_2(OperandType operand1, OperandType operand2);
	template<typename OperandType, typename ResultType>	void mul_1(OperandType operand);

	qint8 unsignedToSigned(byte val);
	qint16 unsignedToSigned(word val);
	qint32 unsignedToSigned(dword val);
	qint16 mulSigned(qint8 val1, qint8 val2);
	qint32 mulSigned(qint16 val1, qint16 val2);
	qint64 mulSigned(qint32 val1, qint32 val2);

	template<typename OperandType, typename ResultType> ResultType imul_2(OperandType operand1, OperandType operand2);
	template<typename OperandType, typename ResultType>	void imul_1(OperandType operand);

	void div_8(Instruction & instr, byte divider);
	void div_16(Instruction & instr, word divider);
	void div_32(Instruction & instr, dword divider);
	void idiv_8(Instruction & instr, byte divider);
	void idiv_16(Instruction & instr, word divider);
	void idiv_32(Instruction & instr, dword divider);

private:

	CPUIntelx86* myCPU = nullptr;
	FlagsRegister* flags = nullptr;
	Register* regAccumulator = nullptr;
	Register* regData = nullptr;

	template <typename sz> inline sz getCFMask();
	template <typename sz> void setFlagCF(const sz& value) {
		flags->eflags.flag32 &= FLAG_CF_CLEAR;
		if (value & getCFMask<sz>()) {
			flags->eflags.flag32 |= FLAG_CF;
		}
	}

	template <typename sz>
	void setFlagOF(sz op1, sz op2, sz result) {
		flags->eflags.flag32 &= FLAG_OF_CLEAR;
		if ((result ^ op1) & (result ^ op2) & getMostSignificantBitValue<sz>()) {
			flags->eflags.flag32 |= FLAG_OF;
		}
	}

	template <typename sz>
	void setFlagOFSub(sz op1, sz op2, sz result) {
		flags->eflags.flag32 &= FLAG_OF_CLEAR;
		if ((op1 ^ result) & (op1 ^ op2) & getMostSignificantBitValue<sz>()) {
			flags->eflags.flag32 |= FLAG_OF;
		}
	}

	void setFlagAF(byte op1, byte op2, byte result) {
		flags->eflags.flag32 &= FLAG_AF_CLEAR;
		if ((op1 ^ op2 ^ result) & 0x10) {
			flags->eflags.flag32 |= FLAG_AF;
		}
	}

	void setFlagAF(word, word, word) { }
	void setFlagAF(dword, dword, dword) { }

	template <typename opType, typename opTypeNext>
	void setFlagsACOSub(opType op1, opType op2, opTypeNext result) {
		setFlagCF<opTypeNext>(result);
		setFlagOFSub<opType>(op1, op2, (opType)(result & getTypeMask<opType>()));
		setFlagAF(op1, op2, (opType)(result & getTypeMask<opType>()));
	}

	template<typename T> inline void setFlagZF(T result) {
		flags->eflags.flag32 &= FLAG_ZF_CLEAR;
		if (0 == result) {
			flags->eflags.flag32 |= FLAG_ZF;
		}
	}

	template<typename opType>
	void setFlagSF(opType result) {
		flags->eflags.flag32 &= FLAG_SF_CLEAR;
		if (result & getMostSignificantBitValue<opType>()) {
			flags->eflags.flag32 |= FLAG_SF;
		}
	}

	template<typename T> void setFlagPF(T val) {
		flags->eflags.flag32 &= FLAG_PF_CLEAR;
		if (getFlagPF(val)) {
			flags->eflags.flag32 |= FLAG_PF;
		}
	}

	template<typename T> void setFlagsSZP(T op) {
		setFlagSF<T>(op);
		setFlagZF<T>(op);
		setFlagPF<T>(op);
	}

	byte getFlagPF(byte result) {
		return (!Parity[result]);
	}

	byte getFlagPF(word result) {
		return getFlagPF( (byte)((result & 0xFF00) >> 8)) & getFlagPF( (byte)(result & 0xFF) );
	}

	byte getFlagPF(dword result) {
		return getFlagPF( (word)((result & 0xFFFF0000) >> 16)) & getFlagPF( (word)(result & 0xFFFF) );
	}

	const byte shlmask8[9] = {
		0, 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01
	};
	const word shlmask16[9] = {
		0, 0x8000, 0x4000, 0x2000, 0x1000, 0x0800, 0x0400, 0x0200, 0x0100
	};
	const dword shlmask32[9] = {
		0, 0x80000000, 0x40000000, 0x20000000, 0x10000000, 0x08000000, 0x04000000, 0x02000000, 0x01000000
	};

	const byte shrmask8[9] = {
		0, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80
	};
	const word shrmask16[17] = {
		0, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x0100, 0x0200, 0x0400, 0x0800, 0x1000,
		0x2000, 0x4000, 0x8000
	};

	const dword shrmask32[33] = {
		0,
		0x01, 0x02, 0x04, 0x08,
		0x10, 0x20, 0x40, 0x80,
		0x0100, 0x0200, 0x0400, 0x0800,
		0x1000,	0x2000, 0x4000, 0x8000,
		0x100000, 0x200000, 0x400000, 0x800000,
		0x10000000, 0x20000000, 0x40000000, 0x80000000
	};


	friend class CPUIntelx86;
};

#ifndef CPUIntelx86_ALU_Shifts
#include "cpuintelx86_alu_shifts.cpp"
#endif

#include "cpuintelx86_alu_logical.cpp"

#include "cpuintelx86_alu_arithm.cpp"

#ifndef CPUIntelx86_ALU_TEMPLATES
#include "cpuintelx86_alu_templates.cpp"
#endif

#endif // CPUINTELX86_ALU_H
