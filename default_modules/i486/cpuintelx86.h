#ifndef CPUINTELX86_H
#define CPUINTELX86_H

#include <QObject>
#include "cpuintelx86_types.h"
#include "cpuintelx86_decoder.h"
#include "cpuintelx86_global.h"
#include "cpuintelx86_alu.h"
#include "intel387x.h"

#include <moduledevice.h>
#include <loggable.h>
#include <QListWidget>

class CPUIntelx86_decoder;
class ModuleDeviceGui;
class CpuGuiWrapper;

class QWidget;

enum debugKind {dkRegReg, dkRegMem};

class I486SHARED_EXPORT CPUIntelx86 : public Loggable, public ModuleDevice
{
	Q_OBJECT
	friend class CPUIntelx86_decoder;
	friend class CPUIntelx86_ALU;

public:

	enum OperatingMode {
		opModeReal,
		opModeProtected,
		opModeV8086
	};

	struct FarPointer {
		dword address;
		word selector;
	};

	// GDT, IDT registers
	struct rDTR {
		word Limit;
		dword Base;
	};

	struct rLDTR {
		word limit;
		dword base;
		word selector;
		DescriptorInfo info;
	};

	// Segment rights descriptor
	struct SegAccessRight{
		bool isRealModed;
		int type;		//	Segment / task switcher / trap / gate descriptor
		int system;		//	Never mind
		dword limit;		//	High granulared space <<4 or non-granulared x1
		int dpl;		//	Descriptor Priveledge Level
		int opSize;		//	0 - 16-bit, 1 - 32-bit
		int read;		//	1 - can be read from
		int write;		//	1 - can be written to
		int execute;	//	1 - can be executed (for decoder)
		int conforming;	//	1 - required stack switch
		dword minAddress;	//	real bounds,
		dword maxAddress;	//	calculated in-time
	};

	static constexpr dword
		// Base registers
		REGISTER_EAX	= 0,
		REGISTER_ECX	= 1,
		REGISTER_EDX	= 2,
		REGISTER_EBX	= 3,
		REGISTER_ESP	= 4,
		REGISTER_EBP	= 5,
		REGISTER_ESI	= 6,
		REGISTER_EDI	= 7,
		NO_REGISTER		= 8,

		// Segment registers
		REGISTER_ES		= 0,
		REGISTER_CS		= 1,
		REGISTER_SS		= 2,
		REGISTER_DS		= 3,
		REGISTER_FS		= 4,
		REGISTER_GS		= 5,
		NO_SREGISTER	= 6;

	static constexpr dword
		BIT_PAGING_ENABLE	= 0x80000000,
		BIT_CR_PAGE_4K		= 0x00000010,
		BIT_PM_ENABLE		= 0x00000001;

	static constexpr dword
		BITS_IOPL = 0b0011000000000000,
		BITS_IOPL_SHIFT = 12;



	// Proceeder of short 8-bit arrangement
	struct RegName {
		word Reg, Shor;
		word Mask;
	};

	static constexpr RegName RegTable8[8] =
		{{REGISTER_EAX, 0, 0x00FF},
		 {REGISTER_ECX, 0, 0x00FF},
		 {REGISTER_EDX, 0, 0x00FF},
		 {REGISTER_EBX, 0, 0x00FF},
		 {REGISTER_EAX, 8, 0xFF00},
		 {REGISTER_ECX, 8, 0xFF00},
		 {REGISTER_EDX, 8, 0xFF00},
		 {REGISTER_EBX, 8, 0xFF00}};

	CPUIntelx86(QObject *parent = 0);
	virtual ~CPUIntelx86();

	virtual void tick() override;
	void resetHard();

	byte* getMemory();
	void loadBIOS(const QString FileName);

	bool canExecute(const dword address, const int instructionLength);

	void opArithmeticRM(Instruction i);
	void reboot();

	void signalExternalInterrupt(word level);

	FlagsRegister eflags;

	void stop();

	virtual void moduleReset() override;
	virtual void moduleShutdown() override;
	virtual void showUi() override;
	virtual void hideUi() override;
	virtual void createUi() override;

signals:
	void signalCPUStopped();
	void signalCPUSuspended();

	void signalDebugToGui(const QString &);
	void signalShowGui();
	void signalHideGui();
	void signalInitGui();
	void signalAddListWidget();
	void signalUpdateStatusWidget(QMap<QString,QString> vals);
	void signalSelfDebugRedirect(QMap<QString,QString> vals);

public slots:
	void slotUpdateStatusInfo();
	void slotReset();
	void slotPullCX();

	void slotSetDebug(bool b);
	void slotDebugCPU(const QMap<QString, QString> & params);

private:

	const QString
		CONFIG_MEMORY_SIZE = "mem_size",
		CONFIG_BIOS_FILENAME = "bios_file_name";

	void initDebugWidget();
	void initIO();
	ModuleDeviceGui* gui= nullptr;
	CpuGuiWrapper* guiWrapper = nullptr;

	bool halted;
	qword eMemSize;
	qword** eMemSizePtr = nullptr;

	byte* memory = nullptr;
	byte* biosMemory = nullptr;
	qword biosMemSize = 0;
	byte** memPtr = nullptr;

	CPUIntelx86_ALU* alu = nullptr;
	Intel387x* x387 = nullptr;
	CPUIntelx86_decoder* decoder = nullptr;

	Register registers[8];
	SegAccessRight SRegRights[6]; // Segment security descriptors (shadowed)
	dword SRegAddress[6]; // Preloaded segment address provider
	word SRegisters[6];	 // Segment registers

	dword cRegisters[8]; // Control regs CR0..CR7
	dword tRegisters[8]; // Debug regs TR0..TR7

	byte cpl; // current priviledge level

	rDTR GDTR; // table registers
	rLDTR LDTR;
	rLDTR taskRegister;
	rDTR IDTR;

	int flagGP; // Flag to handle double faults
	int flagSS; // The same for stack
	int flagPF;

	int currentCyclesTaken;
	long cyclesTotal;

	void reset();

	void processOutput(word port, dword data, int sizeInBytes);
	dword processInput(word port, int sizeInBytes);
	void ioFuncInput(dword data, int size);
	dword ioFuncOutput(int size);
	void setInputInterrupt(dword value, int);
	void callInterruptSignal();
	void ioInterruptPending();
	void ioAckInterrupt();
	dword portData;
	int ioInputSize;
	int ioOutputSize;
	word interruptData;

	bool hasPendingInterrupts;

	void debug_log(const QString msg, QString opNameOverride = "");
	virtual void debug( const QString &str ) override;

	void debug_rm(Instruction &instr, QString opNameOverride = "");
	void debug_jump(jumpEval, bool testResult, qint32 displacement);
	void debug_op0(QString opNameOverride = "");
	void debug_op1(DebugClosure op1, QString opNameOverride = "");
	void debug_op2(DebugClosure op1, DebugClosure op2, QString opNameOverride = "");


	bool isInLimits(const word selector, const dword address, int size) const;

	byte readByte(word segment, dword address);
	word readWord(word segment, dword address);
	dword readDWord(word segment, dword address);

	void writeByte(word segment, dword address, byte value);
	void writeWord(word segment, dword address, word value);
	void writeDWord(word segment, dword address, dword value);

	byte byte_at(dword addr) const;
	word word_at(dword addr) const;
	dword dword_at(dword addr) const;
	void set_byte_at(dword addr, byte value);
	void set_word_at(dword addr, word value);
	void set_dword_at(dword addr, dword value);

	byte getReg8(const dword Reg) const;
	word getReg16(const dword Reg) const;
	dword getReg32(const dword Reg) const;
	void setReg8(const dword Reg, const byte Byte);
	void setReg16(const dword Reg, const word Word);
	void setReg32(const dword Reg, const dword DWord);


	FarPointer readFarPointer(dword address, bool is32bit);
	void writeFarPointer(dword address, FarPointer ptr);

	void push8(Instruction & instr, const byte Value);
	void push16(Instruction & instr, const word Value);
	void push32(Instruction & instr, const dword Value);
	byte pop8(Instruction & instr);
	word pop16(Instruction & instr);
	dword pop32(Instruction & instr);

	dword getNextPushESP(Instruction & instr);
	dword getNextPopESP(Instruction & instr);


	dword getSegmentAddressReal(const dword Selector);
	dword getSegmentReal(const dword Selector);
	void setSegmentReal(const dword Selector, const dword Value);
	dword getSegmentAddressProtected(const dword Selector);
	void getSegmentProtected(const dword Selector, const dword Value);

	byte getByteFlat(const dword Address, const word Segment);
	word getWordFlat(const dword Address, const word Segment);
	dword getDWordFlat(const dword Address, const word Segment);
	void setByteFlat(const dword Address, const byte Byte, const word Segment);
	void setWordFlat(const dword Address, const word Word, const word Segment);
	void setDWordFlat(const dword Address, const dword DWord, const word Segment);

	int getOpSize(const word selector) const;
	int getOpSize() const;
	int getAddrSize(const word selector) const;

	void pusha_16(Instruction & instr);
	void pusha_32(Instruction& instr);
	void popa_16(Instruction& instr);
	void popa_32(Instruction& instr);

	template<typename T> T extractBitValue(dword& src);


	template<typename T, typename TNext> T do_arithmetic(Instruction &instr, OpExtensionArithmetic operation);

	void do_imul_tri(Instruction &instr);

	void do_jumpcc(jumpEval jumpKind, int isLong, dword disp, Instruction & instr);
	void do_arithm_rm_r(Instruction &instr, OpExtensionArithmetic operation);
	void do_pushSReg(Instruction & instr, const word selector);
	void do_popSReg(Instruction & instr, const word selector);
	void do_pushImm(Instruction & instr);

	void do_pusha(Instruction & instr);
	void do_popa(Instruction & instr);

	void do_daa();
	void do_das();
	void do_aaa();
	void do_aad(byte base);
	void do_aas();

	void do_bswap(byte reg);
	void do_bt(Instruction &instr);
	void do_bts(Instruction &instr);
	void do_bsf(Instruction &instr);
	void do_setcc(Instruction &instr, byte code);
	void do_movcc(Instruction &instr, byte code);

	void do_inc_reg(Instruction & instr);
	void do_dec_reg(Instruction & instr);

	void do_cli();
	void do_sti();
	byte getIOPL();

	void do_push_reg(Instruction & instr);
	void do_pop_reg(Instruction & instr);

	void enter_c8(Instruction & instr, word locSize, byte lexLevel);
	void leave_c9(Instruction & instr);

	void do_bound(Instruction &instr);
	void do_arpl(Instruction &instr);

	void loadLdtr(Instruction &instr);
	void loadTaskRegister(Instruction &instr);

	void do_xchg_ax_reg(Instruction & instr, byte reg);
	void do_cwd();
	void do_cbw();

	void do_mov_reg_imm(Instruction &instr);
	void do_mov_rm_r(Instruction &instr, bool debug);

	void do_mov_sr_rm(Instruction &instr);

	void do_jumpn_rel(Instruction &instr);

	void do_out_imm8_ax(Instruction & instr);
	void do_in_ax_imm8(Instruction & instr);

	void iret(Instruction & instr);
	void iretReal(Instruction & instr);
	void iretProtected(Instruction & instr);

	void retf();
	void retfReal(dword popCount);
	void retfProtected(dword popCount);

	void callf(const word &newCS, const dword &newEIP);
	void callfReal(Instruction & instr, const word &newCS, const dword &newEIP);
	void callfProtected(Instruction & instr, const word &newCS, const dword &newEIP);

	void jmpf(const word &newCS, const dword &newEIP);
	void jmpfReal(const word &newCS, const dword &newEIP);
	void jmpfProtected(const word &newCS, const dword &newEIP);

	void loadInstructionOperands(Instruction &instr);
	void loadInstructionOperand(Instruction &instr, OperandType ot, dword &value, dword &address);
	qword readMemory64(Instruction & instr);
	void saveInstructionDestination(Instruction &instr, dword value);
	void saveInstructionDestination64(Instruction & instr, qword value);

	qint32 getDisplacement(dword value, int isLong);

	void interruptInternal(word level, int isSoftware);

	bool operOK;
	OperatingMode operatingMode;

	typedef dword(CPUIntelx86::*pGetSegment)(const dword);
	typedef dword(CPUIntelx86::*pGetSegmentAddress)(const dword);
	typedef void(CPUIntelx86::*pSetSegment)(const dword, const dword);

	dword getSegmentAddressExt(const dword selector);

	pGetSegment	getSegment;
	pSetSegment	setSegment;
	pGetSegmentAddress getSegmentAddress;

	dword readSegmentAddressExt(const dword selector);

	bool isInCallLimits(dword newEIP, DescriptorInfo &descr);
	void do_retn_imm16(Instruction &instr);
	void do_retn(Instruction & instr);
	DescriptorInfo loadDescriptorInfo(const dword &selector);
	gateInfo loadGateInfo(DescriptorInfo &descr);
	void checkStackSize(dword count);
	void debug_insert_base(QMap<QString, QString> &params, QString opNameOverride = "");
	void debug_insert_closure(QMap<QString, QString> &params, DebugClosure dc, int index);
	void do_mov_rm_sr(Instruction &instr);
	void do_mov_reg_imm_ext(Instruction &instr, byte opExtension);
	void do_out_dx_ax(Instruction & instr);
	void do_in_ax_dx(Instruction & instr);
	void do_arithmetic_ext(Instruction &instr, byte opExtension);
	void inc_dec_rm(Instruction &instr, byte opIndex);
	void calln_abs_rm(Instruction &instr);
	void callf_abs_rm(Instruction &instr);
	void jmpn_abs_rm(Instruction &instr);
	void jmpf_abs_rm(Instruction &instr);
	void push_rm(Instruction &instr);

	bool flagTestCZ();
	bool flagTestCP();
	bool flagTestCC();
	bool flagTestCO();
	bool flagTestCS();
	bool flagTestCA();
	bool flagTestCAE();
	bool flagTestCB();
	bool flagTestCBE();
	bool flagTestCG();
	bool flagTestCGE();
	bool flagTestCL();
	bool flagTestCLE();

// *********************************
// *   String block
	enum StrOpIndex {
		soiMOVSB,
		soiSCASB,
		soiCMPSB,
		soiLODSB,
		soiSTOSB,
		soiOUTSB,
		soiINSB,
		soiMOVSW,
		soiSCASW,
		soiCMPSW,
		soiLODSW,
		soiSTOSW,
		soiOUTSW,
		soiINSW
	};
	StrOpIndex strOp;
	bool strOpWr,		// Operation requires write
		 strOpFl,		// Operation requires set EFLAGS
		 strOpRep;		// Req. repeat
	dword strOpSrc, strOpDst;
	dword strOpSrcBase, strOpDstBase;
	dword strOpCtr;
	void do_ProceedString(Instruction &instr);
	void strOpDebug(Instruction &instr);
	void strOpPrepare(Instruction &instr);
	void strOpProceed(Instruction &instr);
	void strOpExecute8(Instruction &instr);
	void strOpExecute16(Instruction &instr);
	void strOpNext(int count, Instruction &);
	void strOpNextSI(int count, Instruction &);
	void strOpNextDI(int count, Instruction &);
	dword stringDest(int count, Instruction &instr);
	dword stringSource(int count, Instruction & instr);
// *
// *********************************


	void do_shift_rm(Instruction &instr, byte shiftCount);
	void do_shrd(Instruction & instr, byte shiftCount);
	void do_shld(Instruction & instr, byte shiftCount);
	void do_calln_rel(Instruction & instr, qint32 disp);
	void do_unary_ext(Instruction &instr);
	void fault_GP(Instruction & instr, const dword Code);
	void fault_DE(Instruction & instr);
	void exception_BR(Instruction & instr);
	void fault_UD(Instruction & instr);
	void fault_NP(Instruction & instr, const dword Code);
	void fault_SS(Instruction & instr, const dword Code);
	void fault_PF(Instruction & instr, const dword code);
	void intProtected(Instruction & instr, dword Number, int isSoftware);
	void intReal(Instruction & instr, const dword Number, int isSoftware);
	void push_flags(Instruction & instr);
	void pop_flags(Instruction & instr);
	DescriptorInfo loadInterruptDescriptorInfo(const word &selector);
	void do_loop(loopKind lc, Instruction &instr);
	void do_retf(Instruction & instr);
	void do_retf_imm16(Instruction & instr);
	void updateStatusInfo();
	void do_binary_ext_imm(Instruction &instr);
	void do_xchg_rm_r(Instruction &instr);
	void do_test_rm_r(Instruction &instr);
	void do_lea(Instruction &instr);
	void do_load_eff_seg(Instruction &instr, word selector);
	void loadSegmentFarPointer(word sreg, Instruction &instr);
	void do_pushf(Instruction & instr);
	void do_popf(Instruction & instr);
	int check_CPL(const word required);
	void checkOperatingMode();
	void checkPagingMode();
	void setSegmentProtected(const dword Register, const dword Value);

	struct PageDirEntryInfo {
		dword address;
		dword addressPte;
		dword addressPde;
		dword present;
		dword rw;
		dword user;
		dword is4M;
		bool ok;
		dword accessed;
		dword dirty;
	};

	PageDirEntryInfo loadPageDirEntry(dword address);
	dword getPDEAddress(dword address);
	dword getPTEAddress(dword address);

	dword pageDirAddress;
	dword pageDir[1024];
	dword pageTableAddress;
	dword pageTable[1024];
	bool isTable4k;
	dword MSR[256][2];

	typedef byte(CPUIntelx86::*pGetByte)(const dword, const word);
	typedef word(CPUIntelx86::*pGetWord)(const dword, const word);
	typedef dword(CPUIntelx86::*pGetDWord)(const dword, const word);
	typedef void(CPUIntelx86::*pSetByte)(const dword, const byte, const word);
	typedef void(CPUIntelx86::*pSetWord)(const dword, const word, const word);
	typedef void(CPUIntelx86::*pSetDWord)(const dword, const dword, const word);

	byte getByteR(const dword addr, const word seg) {
		return (this->*getByte)(addr, seg);
	}
	word getWordR(const dword addr, const word seg) {
		return (this->*getWord)(addr, seg);
	}
	dword getDWordR(const dword addr, const word seg) {
		return (this->*getDWord)(addr, seg);
	}

	void setByteR(const dword addr, const byte val, const word seg) {
		(this->*setByte)(addr, val, seg);
	}
	void setWordR(const dword addr, const word val, const word seg) {
		(this->*setWord)(addr, val, seg);
	}
	void setDWordR(const dword addr, const dword val, const word seg) {
		(this->*setDWord)(addr, val, seg);
	}

	pGetByte getByte;
	pSetByte setByte;
	pGetWord getWord;
	pSetWord setWord;
	pGetDWord getDWord;
	pSetDWord setDWord;

	byte getBytePaged(const dword Address, const word Segment);
	word getWordPaged(const dword Address, const word Segment);
	dword getDWordPaged(const dword Address, const word Segment);
	void setBytePaged(const dword Address, const byte Byte, const word Segment);
	void setWordPaged(const dword Address, const word Word, const word Segment);
	void setDWordPaged(const dword Address, const dword DWord, const word Segment);

	dword getBaseAddressForPage(const dword address, bool write);

	static constexpr dword
		BIT_PAGE_PRESENT = 0b00000001,
		BIT_PAGE_WRITEABLE = 0b00000010,
		BIT_PAGE_USER = 0b00000100,
		BIT_PAGE_ACCESSED_SET = 0b00100000,
		BIT_PAGE_ACCESSED_CLEAR = ~BIT_PAGE_ACCESSED_SET,
		BIT_PAGE_DIRTY_SET = 0b01000000,
		BIT_PAGE_DIRTY_CLEAR = ~BIT_PAGE_DIRTY_SET,
		BIT_PAGE_SIZE = 0b10000000,
		BIT_PAGE_GLOBAL = 0x0100,

		BIT_PAGE_PDE_ADDRESS = 0b11111111110000000000000000000000,
		BIT_PAGE_PDE_SHIFT = 22,
		BIT_PAGE_PTE_ADDRESS = 0b00000000001111111111000000000000,
		BIT_PAGE_PTE_SHIFT = 12,
		BIT_PAGE_OFFSET = 0b00000000000000000000111111111111;

	void do_lar(Instruction &instr);
	void do_lsl(Instruction &instr);
	void do_mov_creg_rm(Instruction &instr);

	void do_smsw(Instruction &instr);
	void do_lmsw(Instruction &instr);
	void do_pop_rm(Instruction &instr);
	void do_xlat(Instruction &instr);
	void verr(Instruction & instr);
	void verw(Instruction & instr);

	friend class aluTest;
	friend class CpuCmpTest;
};

extern "C" I486SHARED_EXPORT ModuleDevice* createModule();


#endif // CPUINTELX86_H

