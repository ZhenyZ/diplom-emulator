#include "cpuintelx86.h"
#include "loggerintelx86.h"

void CPUIntelx86::slotDebugCPU(const QMap<QString, QString> & params)
{
	if (!isDebugEnabled()) {
		return;
	}
	int opCount = params.value("operandcount", "0").toInt();

	QString s;
	QTextStream stream(&s);

	stream << right << qSetFieldWidth(8) << qSetPadChar('0') << hex << params.value("address").toUInt() << qSetFieldWidth(3) << qSetPadChar(' ') << " ";
	stream << left << qSetFieldWidth(2) << qSetPadChar('0') << hex << params.value("opcode").toInt() << qSetFieldWidth(3) << qSetPadChar(' ') << " ";
	stream << left << qSetPadChar(' ') << qSetFieldWidth(8) << params.value("opname");

	QString operand1, operand2;
	int op1value, op2value;
	if (opCount == 1) {
		QString otype1 = params.value("operand1type");
		int oval1 = params.value("operand1value").toInt();
		if (otype1 == "register" || otype1 == "sregister" || otype1 == "cregister") {
			if (otype1 == "register") {
				if (params.value("operand1size") == "8")
					operand1 = RegStrings[oval1];
				else if (params.value("operand1size") == "16")
					operand1 = RegStrings[oval1+8];
				else if (params.value("operand1size") == "32")
					operand1 = RegStrings32[oval1+8];
			}
			if (otype1 == "sregister") {
				operand1 = SRegStrings[oval1];
			}
			if (otype1 == "cregister") {
				operand1 = cRegStrings[oval1];
			}
			stream << right << qSetPadChar(' ') << qSetFieldWidth(8) << operand1<< qSetPadChar(' ') << qSetFieldWidth(0);
		} else {
			if (params.value("operand1type") == "memory") {
				stream << left << qSetPadChar(' ') << qSetFieldWidth(1) << "[" << right << qSetFieldWidth(6) << qSetPadChar('0') << hex << oval1 << qSetFieldWidth(1) << "]"<< qSetPadChar(' ') << qSetFieldWidth(0);
			}
			else {
				stream << right << qSetPadChar('0') << qSetFieldWidth(8) << hex << oval1<< qSetPadChar(' ') << qSetFieldWidth(0);
			}
		}
	}

	if (opCount == 2) {
		QString otype1 = params.value("operand1type");
		if (otype1 == "register" || otype1 == "sregister" || otype1 == "cregister") {
			int oval1 = params.value("operand1value").toInt();
			if (otype1 == "register") {
				if (params.value("operand1size") == "8")
					operand1 = RegStrings[oval1];
				if (params.value("operand1size") == "16")
					operand1 = RegStrings[oval1+8];
				if (params.value("operand1size") == "32")
					operand1 = RegStrings32[oval1+8];
			}
			if (otype1 == "sregister") {
				operand1 = SRegStrings[oval1];
			}
			if (otype1 == "cregister") {
				operand1 = cRegStrings[oval1];
			}
			stream << right << qSetPadChar(' ') << qSetFieldWidth(8) << operand1 << qSetPadChar(' ') << qSetFieldWidth(0);
		} else {
			int padLen1 = params.value("operand1size", "16").toInt() / 4;
			int preLen1 = 8 - padLen1;
			op1value = params.value("operand1value").toInt();
			if (params.value("operand1type") == "memory") {
				stream << left << qSetPadChar(' ') << qSetFieldWidth(1) << "[" << right << qSetFieldWidth(6) << qSetPadChar('0') << hex << op1value << qSetFieldWidth(1) << "]" << qSetPadChar(' ') << qSetFieldWidth(0);
			}
			else {
				stream << left << qSetPadChar(' ') << qSetFieldWidth(preLen1) << "" << right << qSetPadChar('0') << qSetFieldWidth(padLen1) << hex << op1value << qSetPadChar(' ') << qSetFieldWidth(0);
			}
		}

		stream << qSetFieldWidth(2) << ", " << left << qSetFieldWidth(0);


		QString otype2 = params.value("operand2type");
		if (otype2 == "register" || otype2 == "sregister" || otype2 == "cregister") {
			int oval2 = params.value("operand2value").toInt();
			if (otype2 == "register") {
				if (params.value("operand2size") == "8")
					operand2 = RegStrings[oval2];
				if (params.value("operand2size") == "16")
					operand2 = RegStrings[oval2+8];
				if (params.value("operand2size") == "32")
					operand2 = RegStrings32[oval2+8];
			}
			if (otype2 == "sregister") {
				operand1 = SRegStrings[oval2];
			}
			if (otype2 == "cregister") {
				operand2 = cRegStrings[oval2];
			}
			stream << left << qSetPadChar(' ') << qSetFieldWidth(8) << operand2 << qSetPadChar(' ') << qSetFieldWidth(0);
		} else {
			int padLen2 = params.value("operand2size", "16").toInt() / 4;
			op2value = params.value("operand2value").toInt();
			if (params.value("operand2type") == "memory") {
				stream << left << qSetPadChar(' ') << qSetFieldWidth(1) << "[" << qSetFieldWidth(6) << qSetPadChar('0') << right << hex << op2value << qSetFieldWidth(1) << "]" << qSetPadChar(' ') << qSetFieldWidth(0);
			}
			else {
				stream << right << qSetPadChar('0') << qSetFieldWidth(padLen2) << hex << op2value << qSetPadChar(' ') << qSetFieldWidth(0);
			}
		}


	}

	stream << qSetFieldWidth(0);
	stream.flush();

//	if (debugListWidget) {
//		debugListWidget->addItem(s);
//		debugListWidget->scrollToBottom();
//	}
	emit signalDebugToGui(s);
}

void CPUIntelx86::debug(const QString &str) {
	QMap<QString, QString> params;
	params.insert("opname", str);
//	doDebug( params );
	//slotDebugCPU(params);
	emit signalSelfDebugRedirect(params);

}

void CPUIntelx86::debug_log(const QString msg, QString opNameOverride) {

	//if (!debugEnabled) return;
	QMap<QString, QString> params;
	params.insert("type", "cpu");
	params.insert("operandcount", "0");
	debug_insert_base(params, opNameOverride);
	params.remove("opname");
	params.insert("opname", msg);
//	emit signalDebugOutput(params);
//	doDebug( params );
	//slotDebugCPU(params);
	emit signalSelfDebugRedirect(params);
}


#ifdef ENABLE_DEBUG
void CPUIntelx86::debug_rm(Instruction &instr, QString opNameOverride)
{
	if ( !isDebugEnabled() ) return;
	DebugClosure c1, c2;

	c1.opSize = instr.szOperands;
	c2.opSize = instr.szOperands;

	c1.ot = instr.typeDst;
	c2.ot = instr.typeSrc;

	if (instr.isWord && instr.typeDst == opReg && instr.szOperands == sz32) {
		c1.value = instr.dstAddr;
	} else {
		c1.value = instr.dstAddr;
	}

	if (instr.isWord && instr.typeSrc == opReg && instr.szOperands == sz32) {
		c2.value = instr.srcAddr;
	} else {
		c2.value = instr.srcAddr;
	}

	if (c2.ot == opImm) {
		c2.value = instr.srcValue;
	}

	debug_op2(c1, c2, opNameOverride);
}
#else
void CPUIntelx86::debug_rm(Instruction&) {
}

#endif

#ifdef ENABLE_DEBUG
void CPUIntelx86::debug_jump(jumpEval jumpKind, bool testResult, qint32 displacement)
{

	if ( !isDebugEnabled() ) return;
	QString str = "";
	switch (jumpKind) {
		case jC: str = "JC"; break;
		case jNC: str = "JNC"; break;
		case jO: str = "JO"; break;
		case jNO: str = "JNO"; break;
		case jZ: str = "JZ"; break;
		case jNZ: str = "JNZ"; break;
		case jP: str = "JP"; break;
		case jNP: str = "JNP"; break;
		case jA: str = "JA"; break;
		case jS: str = "JS"; break;
		case jNS: str = "JNS"; break;
		case jG: str = "JG"; break;
		case jGE: str = "JGE"; break;
		case jBE: str = "JBE"; break;
		case jLE: str = "JLE"; break;
		case jL: str = "JL"; break;
		case jCXZ: str = "JCXZ"; break;
		case jECXZ: str = "JECXZ"; break;
	}
	if (testResult)
		str += "+";
	else
		str += "-";
	if (0 == getOpSize())
		displacement &= 0xFFFF;

	QMap<QString, QString> params;
	params.insert("operandcount", "1");

	params.insert("operand1type", "immediate");
	params.insert("operand1size", "16");
	params.insert("operand1value", QString::number(displacement));

	debug_insert_base(params);
//	emit signalDebugOutput(params);

	//debuglog->addDebug(str, QString::number(displacement,16)+"h");
//	doDebug( params );
	//slotDebugCPU(params);
	emit signalSelfDebugRedirect(params);

}
#else
void CPUIntelx86::debug_jump(jumpEval, bool, qint32){

}

#endif

#ifdef ENABLE_DEBUG
void CPUIntelx86::debug_op0(QString opNameOverride)
{
	if ( !isDebugEnabled() ) return;
	QMap<QString, QString> params;

	params.insert("operandcount", "0");
	debug_insert_base(params, opNameOverride);

//	emit signalDebugOutput(params);
//	doDebug( params );
	emit signalSelfDebugRedirect(params);
}
#else
void CPUIntelx86::debug_op0() {}
#endif

#ifdef ENABLE_DEBUG
void CPUIntelx86::debug_insert_closure(QMap<QString, QString> &params, DebugClosure dc, int index) {
	if ( !isDebugEnabled() ) return;
	QString str;
	switch (dc.ot) {
		case opReg: str = "register"; break;
		case opSReg: str = "sregister"; break;
		case opMem: str = "memory"; break;
		case opImm: str = "immediate"; break;
		case opCReg: str = "cregister"; break;
	}
	params.insert("operand"+QString::number(index)+"type", str);
	switch (dc.opSize) {
		case sz8: str = "8"; break;
		case sz16: str = "16"; break;
		case sz32: str = "32"; break;
	}
	params.insert("operand"+QString::number(index)+"size", str);
	params.insert("operand"+QString::number(index)+"value", QString::number(dc.value));
}
#else
void CPUIntelx86::debug_insert_closure(QMap<QString, QString> &, DebugClosure, int) {}
#endif

#ifdef ENABLE_DEBUG
void CPUIntelx86::debug_insert_base(QMap<QString, QString> &params, QString opNameOverride) {
	if ( !isDebugEnabled() ) return;
	params.insert("type", "cpu");


	if (decoder->currentInstruction.opcode == 0x0F) {
		params.insert("opcode", QString::number((decoder->currentInstruction.opcode << 8) | (decoder->currentInstruction.extensionIndex)));
	} else {
		params.insert("opcode", QString::number(decoder->currentInstruction.opcode));
	}

	if (opNameOverride == "") {
		if (decoder->currentInstruction.szOperands == sz16 || decoder->currentInstruction.szOperands == sz8) {
			if (OpcodeNames16[decoder->currentInstruction.opcode] != "*")
				params.insert("opname", OpcodeNames16[decoder->currentInstruction.opcode]);
			else {
				params.insert("opname", OpcodeNames16[decoder->currentInstruction.extensionIndex]);
			}
		} else {
			if (OpcodeNames32[decoder->currentInstruction.opcode] != "*")
				params.insert("opname", OpcodeNames32[decoder->currentInstruction.opcode]);
			else {
				params.insert("opname", OpcodeNames32[decoder->currentInstruction.extensionIndex]);
			}
		}
	}
	else {
		params.insert("opname", opNameOverride);
	}
	params.insert("address", QString::number((quint32)(decoder->oldCS + decoder->oldEIP)));
}
#else
void CPUIntelx86::debug_insert_base(QMap<QString, QString> &) {
}

#endif

#ifdef ENABLE_DEBUG
void CPUIntelx86::debug_op1(DebugClosure op1, QString opNameOverride)
{
	if ( !isDebugEnabled() ) return;
	QMap<QString, QString> params;

	params.insert("operandcount", "1");
	debug_insert_closure(params, op1, 1);
	debug_insert_base(params, opNameOverride);

//	doDebug( params );
	//slotDebugCPU(params);
	emit signalSelfDebugRedirect(params);
//	emit signalDebugOutput(params);
}

#else
void CPUIntelx86::debug_op1(DebugClosure) {}
#endif

#ifdef ENABLE_DEBUG
void CPUIntelx86::debug_op2(DebugClosure op1, DebugClosure op2, QString opNameOverride)
{
	if ( !isDebugEnabled() ) return;

	QMap<QString, QString> params;
	params.insert("operandcount", "2");

	debug_insert_closure(params, op1, 1);
	debug_insert_closure(params, op2, 2);
	debug_insert_base(params, opNameOverride);

//	doDebug( params );
	//slotDebugCPU(params);
	emit signalSelfDebugRedirect(params);
//	emit signalDebugOutput(params);
}

#else
void CPUIntelx86::debug_op2(DebugClosure, DebugClosure) {}
#endif
