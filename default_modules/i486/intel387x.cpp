#include "intel387x.h"

#include <QDebug>

Intel387x::Intel387x(CPUIntelx86* cpu)
{
	this->cpu = cpu;
	invalidElement.type = dtExtended;
	invalidElement.data.realExtended = 0.00L;
}

void Intel387x::finit() {
	stackTop = 0;
	for (int i = 0; i < 8; ++i) {
		stack[i].data.realExtended = 0.00L;
		stack[i].type = dtExtended;
		tags[i] = TAG_EMPTY;
	}
	controlReg = 0;
	statusReg = 0;
	tagReg = 0;
	instructionPointer = 0;
	dataPointer = 0;
}

void Intel387x::fldz() {
	StackElement el;
	el.data.realExtended = 0.00L;
	el.type = dtExtended;
	push(el);
}

void Intel387x::fstReg(byte index, bool doPop) {
	if (stackTop - index < 0) {
		qDebug() << "FPU: FST Stack underflow2";
		return;
	}
	stack[index].data = stack[stackTop].data;
	stack[index].type = stack[stackTop].type;
	if (stackTop < 0 && doPop) {
		qDebug() << "FPU: FST Stack underflow";
		return;
	}
	pop();
}

Intel387x::StackElement Intel387x::fstRm32(bool doPop) {
	StackElement result;
	result.data = stack[stackTop].data;
	result.type = stack[stackTop].type;
	convertStackElementToSingle(result);
	if (doPop) {
		if (stackTop < 0) {
			qDebug() << "FPU: FST r/m32 Stack underflow 2";
			return result;
		}
		pop();
	}
	return result;
}

Intel387x::StackElement Intel387x::fstRm64(bool doPop) {
	StackElement result;
	result.data = stack[stackTop].data;
	result.type = stack[stackTop].type;
	convertStackElementToDouble(result);
	if (doPop) {
		if (stackTop < 0) {
			qDebug() << "FPU: FST r/m64 Stack underflow 2";
			return result;
		}
		pop();
	}
	return result;
}

void Intel387x::fld64(qword data) {
	StackElement el;
	el.type = dtDouble;
	el.data.realDouble = data;
	push(el);
}

Intel387x::StackElement Intel387x::fstRm80(bool doPop) {
	StackElement result;
	result.data = stack[stackTop].data;
	result.type = stack[stackTop].type;
	convertStackElementToExt(result);
	if (doPop) {
		if (stackTop < 0) {
			qDebug() << "FPU: FST r/m80 Stack underflow 2";
			return result;
		}
		pop();
	}
	return result;
}

void Intel387x::push(Intel387x::StackElement el) {
	stack[stackTop++].data.realExtended = 0.00L;
	if (stackTop < 7) {
		++stackTop;
	} else {
		qDebug() << "FPU Push: stack overflow";
	}
}

Intel387x::StackElement Intel387x::pop() {
	if (stackTop == 0) {
		qDebug() << "FPU: Stack underflow.";
		return stack[0];
	}
	return stack[stackTop--];
}

void Intel387x::convertStackElementToExt(Intel387x::StackElement & el) {
	switch (el.type) {
		case dtDouble:
			el.data.realExtended = (long double)el.data.realDouble;
		break;
		case dtLong:
			el.data.realExtended = (long double)el.data.intLong;
		break;
		case dtShort:
			el.data.realExtended = (long double)el.data.intShort;
		break;
		case dtWord:
			el.data.realExtended = (long double)el.data.intWord;
		break;
		case dtPackedDecimal:
			qDebug() << "FPU: Convert BCD to Ext not supported";
		break;
		case dtSingle:
			el.data.realExtended = (long double)el.data.realSingle;
		break;
	}
	el.type = dtExtended;
}

void Intel387x::convertStackElementToDouble(Intel387x::StackElement & el) {
	switch (el.type) {
		case dtSingle:
			el.data.realDouble = (double)el.data.realSingle;
		break;
		case dtLong:
			el.data.realDouble = (double)el.data.intLong;
		break;
		case dtShort:
			el.data.realDouble = (double)el.data.intShort;
		break;
		case dtWord:
			el.data.realDouble = (double)el.data.intWord;
		break;
		case dtPackedDecimal:
			qDebug() << "FPU: Convert BCD to Single not supported";
		break;
		case dtExtended:
			el.data.realDouble = (double)el.data.realExtended;
		break;
	}
	el.type = dtDouble;
}

void Intel387x::convertStackElementToSingle(Intel387x::StackElement & el) {
	switch (el.type) {
		case dtDouble:
			el.data.realSingle = (float)el.data.realDouble;
		break;
		case dtLong:
			el.data.realSingle = (float)el.data.intLong;
		break;
		case dtShort:
			el.data.realSingle = (float)el.data.intShort;
		break;
		case dtWord:
			el.data.realSingle = (float)el.data.intWord;
		break;
		case dtPackedDecimal:
			qDebug() << "FPU: Convert BCD to Single not supported";
		break;
		case dtExtended:
			el.data.realSingle = (float)el.data.realExtended;
		break;
	}
	el.type = dtSingle;
}

