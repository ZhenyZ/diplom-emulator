#include "cpuintelx86.h"
#include "cpuintelx86_decoder.h"
#include "cpuintelx86_types.h"
#include "cpuintelx86_alu.h"

#include <QDebug>

const word
	segRMBaseA[8] = {CPUIntelx86::REGISTER_DS, CPUIntelx86::REGISTER_DS, CPUIntelx86::REGISTER_SS, CPUIntelx86::REGISTER_SS,
					CPUIntelx86::REGISTER_DS, CPUIntelx86::REGISTER_DS, CPUIntelx86::REGISTER_SS, CPUIntelx86::REGISTER_DS},
	segRMBaseB[8] = {CPUIntelx86::REGISTER_DS, CPUIntelx86::REGISTER_DS, CPUIntelx86::REGISTER_SS, CPUIntelx86::REGISTER_SS,
					CPUIntelx86::REGISTER_DS, CPUIntelx86::REGISTER_DS, CPUIntelx86::REGISTER_DS, CPUIntelx86::REGISTER_DS};

CPUIntelx86_decoder::CPUIntelx86_decoder(CPUIntelx86 *myCPU, QObject *parent)
	: Loggable( "Decoder", parent )
{
	this->CPU = myCPU;
	for	(int i = 0; i < 8; i++) {
		addressSize[i] = sz16;
		operandSize[i] = sz16;
	}
	this->memory = CPU->getMemory();
}

/**
 * @brief Set current CS op-size for faster execution
 * @param sz - default op-size of CS: 0 = 16-bit; 1 = 32-bit
 */
void CPUIntelx86_decoder::setOpSize(int segment, Size sz) {
	operandSize[segment] = sz;
}

/**
 * @brief Set current CS addr-size for faster execution
 * @param sz - default addr-size of CS: 0 = 16-bit; 1 = 32-bit
 */
void CPUIntelx86_decoder::setAddressSize(int segment, Size sz) {
	addressSize[segment] = sz;
}

/**
 * @brief Fetches next instruction and calls processor specific function
 * @short Performs all access checks
 */
void CPUIntelx86_decoder::decodeNextInstruction()
{
	byte opcode;

	bool isPrefix = true;
	int	prefixLen = 0;

	currentInstruction.executable = true;
	currentInstruction.len = 0;
	currentInstruction.segmentPrefix = CPUIntelx86::REGISTER_DS;
	currentInstruction.segmentOverride = false;
	currentInstruction.repPrefix = repNone;

	currentInstruction.overrideOpSize = false;
	currentInstruction.szOperands = (Size)operandSize[CPUIntelx86::REGISTER_CS];
	currentInstruction.overrideAddrSize = false;
	currentInstruction.szAddress = (Size)addressSize[CPUIntelx86::REGISTER_CS];

	// Decode prefixes ----
	while (isPrefix) {
		opcode = nextByte();
		currentInstruction.len++;
		switch (opcode) {
			case 0x26:
				currentInstruction.segmentPrefix = CPUIntelx86::REGISTER_ES;
				currentInstruction.segmentOverride = true;
				CPU->debug_op0("ES:");
				prefixLen++;
			continue;

			case 0x2E:
				currentInstruction.segmentPrefix = CPUIntelx86::REGISTER_CS;
				currentInstruction.segmentOverride = true;
				CPU->debug_op0("CS:");
				prefixLen++;
			continue;

			case 0x36:
				currentInstruction.segmentPrefix = CPUIntelx86::REGISTER_SS;
				currentInstruction.segmentOverride = true;
				CPU->debug_op0("SS:");
				prefixLen++;
			continue;

			case 0x3E:
				currentInstruction.segmentPrefix = CPUIntelx86::REGISTER_DS;
				currentInstruction.segmentOverride = true;
				CPU->debug_op0("DS:");
				prefixLen++;
			continue;

			case 0x64:
				currentInstruction.segmentPrefix = CPUIntelx86::REGISTER_FS;
				currentInstruction.segmentOverride = true;
				CPU->debug_op0("FS:");
				prefixLen++;
			continue;

			case 0x65:
				currentInstruction.segmentPrefix = CPUIntelx86::REGISTER_GS;
				currentInstruction.segmentOverride = true;
				CPU->debug_op0("GS:");
				prefixLen++;
			continue;

			case 0x66:
				currentInstruction.overrideOpSize = true;
				if (currentInstruction.szOperands == sz16) {
					currentInstruction.szOperands = sz32;
				} else {
					currentInstruction.szOperands = sz16;
				}
				prefixLen++;
			continue;

			case 0x67:
				currentInstruction.overrideAddrSize = true;
				if (currentInstruction.szAddress == sz16) {
					currentInstruction.szAddress = sz32;
				} else {
					currentInstruction.szAddress = sz16;
				}
				prefixLen++;
			continue;

			case 0xF0:
				CPU->debug_op0("LOCK:");
				prefixLen++;
			continue;

			case 0xF2:
				currentInstruction.repPrefix = repNonEqual;
				CPU->debug_op0("REPNZ");
				prefixLen++;
			continue;

			case 0xF3:
				currentInstruction.repPrefix = repEqual;
				CPU->debug_op0("REPZ");
				prefixLen++;
			continue;

			default:
				isPrefix = false;

		}
	} // ---- Prefix end
	// Here we got clean instruction opcode with all prefixes set

	currentInstruction.opcode = opcode;
	//EIP += prefixLen;

	byte imm8;	// immediate 8-bit value for some operations
	word imm16;
	dword imm32;
	byte opExtension;

	word ptrCS;
	dword ptrEIP;

	byte signExt = 0;
	OperandType tempOperandType;

	//	Now known things: op_size, addr_size, segment, opcode
	//	Decode instruction itself
	switch (opcode) {

		case 0x00:	// ADD
		case 0x01:
		case 0x02:
		case 0x03:
			executeArithmetic1RegisterMemory(OpExtensionArithmetic::Add);
		break;
		case 0x04:	// AL / AX / EAX
		case 0x05:
			executeArithmetic1Accumulator(OpExtensionArithmetic::Add);
		break;
		case 0x06:
			CPU->do_pushSReg(currentInstruction, CPUIntelx86::REGISTER_ES);
		break;
		case 0x07:
			CPU->do_popSReg(currentInstruction, CPUIntelx86::REGISTER_ES);
		break;
		case 0x08:	// OR
		case 0x09:
		case 0x0A:
		case 0x0B:
			executeArithmetic1RegisterMemory(OpExtensionArithmetic::Or);
		break;
		case 0x0C:
		case 0x0D:
			executeArithmetic1Accumulator(OpExtensionArithmetic::Or);
		break;
		case 0x0E:
			CPU->do_pushSReg(currentInstruction, CPUIntelx86::REGISTER_CS);
		break;
		case 0x0F:
			decodeExtension();
		break;
		case 0x10:	// ADC
		case 0x11:
		case 0x12:
		case 0x13:
			executeArithmetic1RegisterMemory(OpExtensionArithmetic::Adc);
		break;
		case 0x14:
		case 0x15:
			executeArithmetic1Accumulator(OpExtensionArithmetic::Adc);
		break;
		case 0x16: CPU->do_pushSReg(currentInstruction, CPUIntelx86::REGISTER_SS); break;
		case 0x17: CPU->do_popSReg(currentInstruction, CPUIntelx86::REGISTER_SS); break;
		case 0x18:	// SBB
		case 0x19:
		case 0x1A:
		case 0x1B:
			executeArithmetic1RegisterMemory(OpExtensionArithmetic::Sbb);
		break;
		case 0x1C:
		case 0x1D:
			executeArithmetic1Accumulator(OpExtensionArithmetic::Sbb);
		break;
		case 0x1E:
			CPU->do_pushSReg(currentInstruction, CPUIntelx86::REGISTER_DS);
		break;
		case 0x1F:
			CPU->do_popSReg(currentInstruction, CPUIntelx86::REGISTER_DS);
		break;
		case 0x20:	// AND
		case 0x21:
		case 0x22:
		case 0x23:
			executeArithmetic1RegisterMemory(OpExtensionArithmetic::And);
		break;

		case 0x24:
		case 0x25:
			executeArithmetic1Accumulator(OpExtensionArithmetic::And);
		break;
		case 0x27: CPU->do_daa(); break;

		case 0x28:	// SUB
		case 0x29:
		case 0x2A:
		case 0x2B:
			executeArithmetic1RegisterMemory(OpExtensionArithmetic::Sub);
		break;

		case 0x2C:
		case 0x2D:
			executeArithmetic1Accumulator(OpExtensionArithmetic::Sub);
		break;

		case 0x2F: CPU->do_das(); break;

		case 0x30:	// XOR
		case 0x31:
		case 0x32:
		case 0x33:
			executeArithmetic1RegisterMemory(OpExtensionArithmetic::Xor);
		break;

		case 0x34:
		case 0x35:
			executeArithmetic1Accumulator(OpExtensionArithmetic::Xor);
		break;

		case 0x37: CPU->do_aaa(); break;

		case 0x38:	// CMP
		case 0x39:
		case 0x3A:
		case 0x3B:
			executeArithmetic1RegisterMemory(OpExtensionArithmetic::Cmp);
		break;
		case 0x3C:
		case 0x3D:
			executeArithmetic1Accumulator(OpExtensionArithmetic::Cmp);
		break;

		case 0x3F: CPU->do_aas(); break;

		case 0x40:
		case 0x41:
		case 0x42:
		case 0x43:
		case 0x44:
		case 0x45:
		case 0x46:
		case 0x47:
			currentInstruction.reg = (opcode & 0x7);
			CPU->do_inc_reg(currentInstruction);
		break;

		case 0x48:
		case 0x49:
		case 0x4A:
		case 0x4B:
		case 0x4C:
		case 0x4D:
		case 0x4E:
		case 0x4F:
			currentInstruction.reg = (opcode & 0x7);
			CPU->do_dec_reg(currentInstruction);
		break;

		case 0x50:
		case 0x51:
		case 0x52:
		case 0x53:
		case 0x54:
		case 0x55:
		case 0x56:
		case 0x57:
			currentInstruction.reg = (opcode & 0x7);
			CPU->do_push_reg(currentInstruction);
		break;

		case 0x58:
		case 0x59:
		case 0x5A:
		case 0x5B:
		case 0x5C:
		case 0x5D:
		case 0x5E:
		case 0x5F:
			currentInstruction.reg = (opcode & 0x7);
			CPU->do_pop_reg(currentInstruction);
		break;

		case 0x60:
			CPU->do_pusha(currentInstruction);
		break;
		case 0x61:
			CPU->do_popa(currentInstruction);
		break;

		case 0x62:
			currentInstruction.isWord = 1;
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			this->getRM();
			if (currentInstruction.executable) {
				CPU->do_bound(currentInstruction);
			}
		break;

		case 0x63:
			currentInstruction.szOperands = sz16;
			this->getRM();
			if (currentInstruction.executable) {
				CPU->do_arpl(currentInstruction);
			}
		break;

		case 0x68:
		case 0x6A:
			currentInstruction.isWord = !(opcode & 0x02);
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize(CPUIntelx86::REGISTER_CS);
				loadImmediateValue(0);
			} else {
				if (!currentInstruction.isWord) {
					currentInstruction.srcValue = nextByte();
				} else {
					loadImmediateValue(0);
				}
			}

			CPU->do_pushImm(currentInstruction);
		break;

		case 0x6B:
			signExt = 1;
		case 0x69:
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize(CPUIntelx86::REGISTER_CS);
			}
			this->getRM();
			tempOperandType = currentInstruction.typeSrc;
			imm32 = currentInstruction.srcValue;
			this->loadImmediateValue(signExt);
			currentInstruction.immediate = currentInstruction.srcValue;
			currentInstruction.typeSrc = tempOperandType;
			qSwap(currentInstruction.srcValue, imm32);
			if (currentInstruction.executable) {
				CPU->do_imul_tri(currentInstruction);
			}
		break;


		case 0x70:
		case 0x71:
		case 0x72:
		case 0x73:
		case 0x74:
		case 0x75:
		case 0x76:
		case 0x77:
		case 0x78:
		case 0x79:
		case 0x7A:
		case 0x7B:
		case 0x7C:
		case 0x7D:
		case 0x7E:
		case 0x7F:
			imm8 = nextByte();
			if (currentInstruction.executable) {
				CPU->do_jumpcc((jumpEval)(opcode & 0x0F), 0, imm8, currentInstruction);
			}
		break;

		case 0x80:
		case 0x81:
		case 0x82:
		case 0x83:
			signExt = (opcode & 0x2);
			currentInstruction.isWord = (opcode & 0x1);
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			this->getRM();
			if (!currentInstruction.executable) {
				break;
			}
			loadImmediateValue(signExt);
			if (currentInstruction.executable) {
				CPU->do_binary_ext_imm(currentInstruction);
			}
		break;


		case 0x84:
		case 0x85:
			currentInstruction.isWord = (opcode & 0x1);
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}

			this->getRM();
			if (currentInstruction.executable) {
				CPU->do_test_rm_r(currentInstruction);
			}
		break;

		case 0x86:
		case 0x87:
			currentInstruction.isWord = (opcode & 0x1);
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}

			this->getRM();
			if (currentInstruction.executable) {
				CPU->do_xchg_rm_r(currentInstruction);
			}
		break;

		case 0x88:
		case 0x89:
		case 0x8A:
		case 0x8B:
			//	First determine sizing of operands
			currentInstruction.reverseDirection = opcode & 0x2;
			currentInstruction.isWord = opcode & 0x1;
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}

			this->getRM();

			// If 8-bit operand, then opSize doesnt matter

			if (currentInstruction.executable) {
				if (currentInstruction.reverseDirection) {
					swapDstSrc();
				}
				CPU->do_mov_rm_r(currentInstruction, true);
			}
		break;

		case 0x8C:
			currentInstruction.isWord = 1;
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			this->getRM();
			if (currentInstruction.executable) {
				CPU->do_mov_rm_sr(currentInstruction);
			}
		break;

		case 0x8D:
			currentInstruction.isWord = 1;
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			this->getRM();

			swapDstSrc();

			if (currentInstruction.executable) {
				CPU->do_lea(currentInstruction);
			}

		break;

		case 0x8E:
			currentInstruction.isWord = 1;
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			this->getRM();
			swapDstSrc();
			if (currentInstruction.executable) {
				CPU->do_mov_sr_rm(currentInstruction);
			}
		break;

		case 0x8F:
			currentInstruction.isWord = 1;
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			this->getRM();
			if (currentInstruction.executable) {
				CPU->do_pop_rm(currentInstruction);
			}
		break;

		case 0x90:
			currentInstruction.cyclesTotal++;
			CPU->debug_op0("NOP");
		break;

		case 0x91:
		case 0x92:
		case 0x93:
		case 0x94:
		case 0x95:
		case 0x96:
		case 0x97:
			CPU->do_xchg_ax_reg(currentInstruction, (opcode & 0x7));
		break;

		case 0x98: CPU->do_cbw(); break;
		case 0x99: CPU->do_cwd(); break;

		case 0x9A:
			if (sz16 == operandSize[CPUIntelx86::REGISTER_CS]) {
				ptrEIP = nextWord();
			} else {
				ptrEIP = nextDWord();
			}
			if (currentInstruction.executable) {
				ptrCS = nextWord();
				if (currentInstruction.executable) {
					CPU->callf(ptrCS, ptrEIP);
				}
			}
		break;
		case 0x9B:
			CPU->debug_op0("WAIT:");
		break;
		case 0x9C: CPU->do_pushf(currentInstruction); break;
		case 0x9D: CPU->do_popf(currentInstruction); break;

		case 0x9E:
			currentInstruction.cyclesTotal += 2;
			CPU->debug_op0("SAHF");
			CPU->eflags.wflag.flags.Flags_Lo = CPU->registers[CPUIntelx86::REGISTER_EAX].word.byte.high8;
		break;

		case 0x9F:
			currentInstruction.cyclesTotal += 6;
			CPU->debug_op0("LAHF");
			CPU->registers[CPUIntelx86::REGISTER_EAX].word.byte.high8 = CPU->eflags.wflag.flags.Flags_Lo;
		break;

		case 0xA0:
		case 0xA1:
		case 0xA2:
		case 0xA3:
			currentInstruction.reverseDirection = opcode & 2;
			currentInstruction.isWord = opcode & 1;

			currentInstruction.typeDst = opReg;
			currentInstruction.dstAddr = CPUIntelx86::REGISTER_EAX;

			loadImmediateOffset(0);
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			if (currentInstruction.executable) {
				imm32 = CPU->getSegmentAddressExt(currentInstruction.segmentPrefix);
				imm32 += currentInstruction.uoffset;

				currentInstruction.typeSrc = opMem;
				currentInstruction.srcAddr = imm32;

				if (currentInstruction.reverseDirection) {
					swapDstSrc();
				}

				CPU->do_mov_rm_r(currentInstruction, true);
			}
		break;

		case 0xA8:
		case 0xA9:
			currentInstruction.isWord = (opcode & 0x01);
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}

			loadImmediateValue(0);

			CPU->debug_op2({opReg, currentInstruction.szOperands, CPUIntelx86::REGISTER_EAX}, {opImm, currentInstruction.szOperands, currentInstruction.srcValue});

			if (currentInstruction.executable) {
				if (currentInstruction.szOperands == sz8) {
					CPU->alu->And<byte>(CPU->getReg8(CPUIntelx86::REGISTER_EAX), currentInstruction.srcValue);
				}
				else {
					if (currentInstruction.szOperands == sz16) {
						CPU->alu->And<word>(CPU->getReg16(CPUIntelx86::REGISTER_EAX), currentInstruction.srcValue);
					} else {
						CPU->alu->And<dword>(CPU->getReg32(CPUIntelx86::REGISTER_EAX), currentInstruction.srcValue);
					}
				}
			}
		break;

		case 0xA4:
		case 0xA5:
		case 0xA6:
		case 0xA7:
		case 0xAA:
		case 0xAB:
		case 0xAC:
		case 0xAD:
		case 0xAE:
		case 0xAF:
		case 0x6C:
		case 0x6D:
		case 0x6E:
		case 0x6F:

			CPU->do_ProceedString(currentInstruction);
		break;

		case 0xB0:
		case 0xB1:
		case 0xB2:
		case 0xB3:
		case 0xB4:
		case 0xB5:
		case 0xB6:
		case 0xB7:
		case 0xB8:
		case 0xB9:
		case 0xBA:
		case 0xBB:
		case 0xBC:
		case 0xBD:
		case 0xBE:
		case 0xBF:
			currentInstruction.isWord = opcode & 0x08;
			currentInstruction.dstAddr = opcode & 0x07;
			currentInstruction.typeDst = opReg;
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			loadImmediateValue(0);
			if (currentInstruction.executable) {
				CPU->do_mov_reg_imm(currentInstruction);
			}
		break;

		case 0xC0:
		case 0xC1:
		case 0xD0:
		case 0xD1:
		case 0xD2:
		case 0xD3:
			currentInstruction.reverseDirection = opcode & 2;
			currentInstruction.isWord = opcode & 1;

			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			this->getRM();
			if (!currentInstruction.executable) {
				break;
			}
			if ((opcode == 0xC0) || (opcode == 0xC1)) {
				imm8 = nextByte();
			} else {
				if ((opcode == 0xD0) || (opcode == 0xD1)) {
					imm8 = 1;
				} else {
					imm8 = CPU->getReg8(CPUIntelx86::REGISTER_ECX);
				}
			}
			if (currentInstruction.executable) {
				CPU->do_shift_rm(currentInstruction, imm8);
			}
		break;
		case 0xC2: CPU->do_retn_imm16(currentInstruction); break;
		case 0xC3: CPU->do_retn(currentInstruction); break;
		case 0xC4:
		case 0xC5:
			currentInstruction.isWord = 1;
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			this->getRM();
			if (currentInstruction.executable) {
				if (opcode == 0xC4) {
					CPU->do_load_eff_seg(currentInstruction, CPUIntelx86::REGISTER_ES);
				} else {
					CPU->do_load_eff_seg(currentInstruction, CPUIntelx86::REGISTER_DS);
				}
			}
		break;

		case 0xC6:
		case 0xC7:
			currentInstruction.isWord = opcode & 0x1;
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}

			this->getRM();
			if (currentInstruction.executable) {
				loadImmediateValue(0);
				opExtension = (currentInstruction.reg);

				if (currentInstruction.executable) {
					CPU->do_mov_reg_imm_ext(currentInstruction, opExtension);
				}
			}

		break;

		case 0xC8:
			imm16 = nextWord();
			if (currentInstruction.executable) {
				imm8 = nextByte();
				if (currentInstruction.executable) {
					CPU->enter_c8(currentInstruction, imm16, imm8);
				}
			}
		break;
		case 0xC9:
			CPU->leave_c9(currentInstruction);
		break;

		case 0xCA:
			currentInstruction.immediate = nextWord();
			if (currentInstruction.executable) {
				CPU->do_retf_imm16(currentInstruction);
			}
		break;

		case 0xCB:
			CPU->do_retf(currentInstruction);
		break;

		case 0xCC:	// INT 3
			CPU->interruptInternal(3, 1);
		break;

		case 0xCD:	// INT ib
			imm8 = nextByte();
			if (currentInstruction.executable) {
				CPU->interruptInternal(imm8, 1);
			}
		break;

//		case 0xCE:
//			if (CPU->eflags.eflags.flag32 & FLAG_OF)
//				CPU->oper_int(4);
//		break;

		case 0xCF:
			CPU->iret(currentInstruction);
		break;

//		case 0xD4: CPU->aam(); break;
		case 0xD5:
			imm8 = nextByte();
			if (currentInstruction.executable) {
				CPU->do_aad(imm8);
			}
		break;

		case 0xD6:
			CPU->debug_op0("SETALC");
			if (CPU->eflags.eflags.flag32 & FLAG_CF) {
				CPU->setReg8(CPUIntelx86::REGISTER_EAX, 0xFF);
			} else {
				CPU->setReg8(CPUIntelx86::REGISTER_EAX, 0);
			}
		break;

		case 0xD7:
			CPU->do_xlat(currentInstruction);
		break;

		case 0xD8:
		case 0xD9:
		case 0xDA:
		case 0xDB:
		case 0xDC:
		case 0xDD:
		case 0xDE:
		case 0xDF:
			decodeFPU();
		break;

		case 0xE0:
		case 0xE1:
		case 0xE2:
			currentInstruction.immediate = CPU->alu->byte2s32(nextByte());
			if (currentInstruction.executable) {
				CPU->do_loop((loopKind)(opcode & 0x03), currentInstruction);
			}
		break;

		case 0xE3:
			imm8 = nextByte();
			if (currentInstruction.executable) {
				CPU->do_jumpcc(jCXZ, 0, imm8, currentInstruction);
			}
		break;

		case 0xE4:
		case 0xE5:
			currentInstruction.isWord = opcode & 1;
			currentInstruction.immediate = nextByte();
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			if (currentInstruction.executable) {
				CPU->do_in_ax_imm8(currentInstruction);
			}
		break;

		case 0xE6:
		case 0xE7:
			currentInstruction.isWord = (opcode & 1);
			currentInstruction.immediate = nextByte();
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			if (currentInstruction.executable) {
				CPU->do_out_imm8_ax(currentInstruction);
			}
		break;

		case 0xE8:
			if (sz16 == operandSize[CPUIntelx86::REGISTER_CS]) {
				imm32 = CPU->alu->word2s32(nextWord());
			} else {
				imm32 = CPU->alu->dword2s32(nextDWord());
			}
			if (currentInstruction.executable) {
				CPU->do_calln_rel(currentInstruction, imm32);
			}
		break;

		case 0xEB:
		case 0xE9:
			currentInstruction.isWord = !(opcode & 0x2);
			currentInstruction.segmentPrefix = CPUIntelx86::REGISTER_CS;

			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			loadImmediateValue(0);

			if (currentInstruction.executable) {
				CPU->do_jumpn_rel(currentInstruction);
			}
		break;

		case 0xEA:
			if (sz16 == currentInstruction.szOperands) {
				ptrEIP = nextWord(); if (!currentInstruction.executable) break;
				ptrCS = nextWord(); if (!currentInstruction.executable) break;
			}
			else {
				ptrEIP = nextDWord(); if (!currentInstruction.executable) break;
				ptrCS = nextWord(); if (!currentInstruction.executable) break;
			}
			CPU->jmpf(ptrCS, ptrEIP);
		break;

		case 0xEC:
		case 0xED:
			currentInstruction.isWord = (opcode & 1);
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			CPU->do_in_ax_dx(currentInstruction);
		break;

		case 0xEE:
		case 0xEF:
			currentInstruction.isWord = (opcode & 1);
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			CPU->do_out_dx_ax(currentInstruction);
		 break;

		case 0xF4:
			//CPUStop = true;
			CPU->debug_op0("HLT");
			if (!(CPU->eflags.eflags.flag32 & FLAG_IF)) {
//				CPU->slotSetDebug(true);
				CPU->setDebugEnabled();
				//CPU->slotStop();
				CPU->stop();
				//CPU->stepAllow = 0;
				debug_critical(" -  >>> HLT without any interrupt allowed!");
			}
		break;

		case 0xF5:
			currentInstruction.cyclesTotal += 2;
			CPU->debug_op0("CMC");
			if (CPU->eflags.eflags.flag32 & FLAG_CF) {
				CPU->eflags.eflags.flag32 &= (~FLAG_CF);
			} else {
				CPU->eflags.eflags.flag32 |= FLAG_CF;
			}
		break;

		case 0xF6:
		case 0xF7:
			currentInstruction.isWord = opcode & 0x1;
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}

			this->getRM();
			if (currentInstruction.executable) {
				opExtension = currentInstruction.reg;
				if (opExtension <= 1) {
					loadImmediateValue(0);
				}

				if (currentInstruction.executable) {
					CPU->do_unary_ext(currentInstruction);
				}
			}
		break;

		case 0xF8:
			currentInstruction.cyclesTotal += 2;
			CPU->debug_op0("CLC");
			CPU->eflags.eflags.flag32 &= (~FLAG_CF);
		break;

		case 0xF9:
			currentInstruction.cyclesTotal += 2;
			CPU->debug_op0("STC");
			CPU->eflags.eflags.flag32 |= FLAG_CF;
		break;

		case 0xFA:
			CPU->do_cli();
		break;

		case 0xFB:
			CPU->do_sti();
		break;

		case 0xFC:
			currentInstruction.cyclesTotal += 2;
			CPU->debug_op0("CLD");
			CPU->eflags.eflags.flag32 &= (~FLAG_DF);
		break;

		case 0xFD:
			currentInstruction.cyclesTotal += 2;
			CPU->debug_op0("STD");
			CPU->eflags.eflags.flag32 |= FLAG_DF;
		break;

		case 0xFE:
		case 0xFF:
			currentInstruction.isWord = (opcode & 0x1);
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}

			this->getRM();
			if (currentInstruction.executable) {
				opExtension = (currentInstruction.reg);

				if (currentInstruction.executable) {
					CPU->do_arithmetic_ext(currentInstruction, opExtension);
				}
			}
		break;

		default:
			CPU->setDebugEnabled();
			//CPU->slotStop();
			CPU->debug_op0(" - UNKNOWN: " + QString::number(opcode, 16));
			CPU->debug_op0("PROTECTED = " + QString::number(CPU->cRegisters[0] & 1));
			CPU->stop();
		break;
	}

	this->advance();

	CPU->cyclesTotal += currentInstruction.cyclesTotal;
	currentInstruction.cyclesTotal = 0;

}

/**
 * @brief Decode 0x0F opcode extension
 * @short 80386 set
 */
void CPUIntelx86_decoder::decodeExtension()
{
//	CPU->slotSetDebug(true);
//	CPU->slotSuspend();
	byte opExtension = nextByte();
	if (!currentInstruction.executable) return;

	currentInstruction.cyclesTotal += 5;
	currentInstruction.extensionIndex = opExtension;
	currentInstruction.isWord = 1;

	switch (opExtension) {
		case 0x00: decodeExtension_grp00(); break;
		case 0x01: decodeExtension_grp01(); break;

		case 0x02:
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			this->getRM();
			if (currentInstruction.executable) {
				CPU->do_lar(currentInstruction);
			}
		break;

		case 0x03:
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			this->getRM();
			if (currentInstruction.executable) {
				CPU->do_lsl(currentInstruction);
			}
		break;

		case 0x08:
			currentInstruction.cyclesTotal += 4;
			CPU->debug_op0("INVD");
		break;

		case 0x09:
			currentInstruction.cyclesTotal += 4;
			CPU->debug_op0("WBINVD");
		break;

		case 0x20:
		case 0x22:
			currentInstruction.reverseDirection = (opExtension & 0x2);
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			this->getRM();
			currentInstruction.szOperands = sz32;
			currentInstruction.typeSrc = opCReg;
			if (currentInstruction.reverseDirection) {
				qSwap(currentInstruction.dstAddr, currentInstruction.srcAddr);
				qSwap(currentInstruction.typeDst, currentInstruction.typeSrc);
			}
			if (currentInstruction.executable) {
				CPU->do_mov_creg_rm(currentInstruction);
			}
		break;

		case 0x40:
		case 0x41:
		case 0x42:
		case 0x43:
		case 0x44:
		case 0x45:
		case 0x46:
		case 0x47:
		case 0x48:
		case 0x49:
		case 0x4A:
		case 0x4B:
		case 0x4C:
		case 0x4D:
		case 0x4E:
		case 0x4F:
			currentInstruction.cyclesTotal += 14;
			currentInstruction.isWord = 1;
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			this->getRM();
			if (currentInstruction.executable) {
				swapDstSrc();
				CPU->do_movcc(currentInstruction, opExtension & 0x0F);
			}
		break;

		case 0x30:
			CPU->debug_op0("WRMSR");
			CPU->MSR[CPU->getReg32(CPUIntelx86::REGISTER_ECX) & 0xFF][0] = CPU->getReg32(CPUIntelx86::REGISTER_EAX);
			CPU->MSR[CPU->getReg32(CPUIntelx86::REGISTER_ECX) & 0xFF][1] = CPU->getReg32(CPUIntelx86::REGISTER_EDX);
		break;

//		case 0x31: {
//			dbg_breakpoint();
//			if (AddDebug)
//				debuglog->addDebug("RDTSC", "");
//			SetReg32(REGISTER_EAX, CyclesTotalLo);
//			SetReg32(REGISTER_EDX, CyclesTotalHi);
//			} return;

		case 0x32:
			CPU->debug_op0("RDMSR");
			CPU->setReg32(CPUIntelx86::REGISTER_EAX, CPU->MSR[CPU->getReg32(CPUIntelx86::REGISTER_ECX) & 0xFF][0]);
			CPU->setReg32(CPUIntelx86::REGISTER_EDX, CPU->MSR[CPU->getReg32(CPUIntelx86::REGISTER_ECX) & 0xFF][1]);
		break;



		case 0xA0:
			CPU->debug_op1(DebugClosure{OperandType::opSReg, sz16, CPUIntelx86::REGISTER_FS}, "PUSH");
			CPU->do_pushSReg(currentInstruction, CPUIntelx86::REGISTER_FS);
		break;

		case 0xA1:
			CPU->debug_op1(DebugClosure{OperandType::opSReg, sz16, CPUIntelx86::REGISTER_FS}, "POP");
			CPU->do_popSReg(currentInstruction, CPUIntelx86::REGISTER_FS);
		break;

		case 0xA8:
			CPU->debug_op1(DebugClosure{OperandType::opSReg, sz16, CPUIntelx86::REGISTER_GS}, "PUSH");
			CPU->do_pushSReg(currentInstruction, CPUIntelx86::REGISTER_GS);
		break;

		case 0xA9:
			CPU->debug_op1(DebugClosure{OperandType::opSReg, sz16, CPUIntelx86::REGISTER_GS}, "POP");
			CPU->do_popSReg(currentInstruction, CPUIntelx86::REGISTER_GS);
		break;

		case 0xA2:
			CPU->debug_op0("CPUID");
			switch (CPU->getReg32(CPUIntelx86::REGISTER_EAX)) {
				case 0x00:
					CPU->setReg32(CPUIntelx86::REGISTER_EAX, 1); // Max. input value
					CPU->setReg32(CPUIntelx86::REGISTER_EBX, 0x47656E75); // Genu
					CPU->setReg32(CPUIntelx86::REGISTER_ECX, 0x6E74656C); // ntel
					CPU->setReg32(CPUIntelx86::REGISTER_EDX, 0x696E6549); //ineI
				break;
				case 0x01:
					CPU->setReg32(CPUIntelx86::REGISTER_EAX, 0x10A); // CPU type field
					CPU->setReg32(CPUIntelx86::REGISTER_EBX, 0x1010102); // CPU feature 1
					CPU->setReg32(CPUIntelx86::REGISTER_ECX, 0x00); // CPU feature 2
					CPU->setReg32(CPUIntelx86::REGISTER_EDX, 0x00); //
				break;
			}
		break;

		case 0xA4:
		case 0xA5:
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			this->getRM();
			if (currentInstruction.executable) {
				if (opExtension == 0xA4) {
					currentInstruction.immediate = nextByte();
				} else {
					currentInstruction.immediate = CPU->getReg8(CPUIntelx86::REGISTER_ECX);
				}
				if (currentInstruction.executable) {
					CPU->do_shld(currentInstruction, currentInstruction.immediate);
				}
			}
		break;


		case 0xAC:
		case 0xAD:
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			this->getRM();
			if (currentInstruction.executable) {
				if (opExtension == 0xAC) {
					currentInstruction.immediate = nextByte();
				} else {
					currentInstruction.immediate = CPU->getReg8(CPUIntelx86::REGISTER_ECX);
				}
				if (currentInstruction.executable) {
					CPU->do_shrd(currentInstruction, currentInstruction.immediate);
				}
			}
		break;

		case 0xAF:
			currentInstruction.cyclesTotal += 26;
			currentInstruction.isWord = opExtension & 0x1;
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			this->getRM();
			CPU->debug_rm(currentInstruction, "IMUL");
			if (currentInstruction.executable) {
				CPU->loadInstructionOperands(currentInstruction);
				if (currentInstruction.szOperands == sz16) {
					word op16_1 = currentInstruction.dstValue & 0xFFFF;
					word op16_2 = currentInstruction.srcValue & 0xFFFF;
					word result = (word)(CPU->alu->imul_2<word, dword>(op16_1, op16_2) & 0xFFFF);
					currentInstruction.dstValue = result;
				} else {
					dword op32_1 = currentInstruction.dstValue;
					dword op32_2 = currentInstruction.srcValue;
					dword result = (dword)(CPU->alu->imul_2<dword, qword>(op32_1, op32_2) & 0xFFFFFFFF);
					currentInstruction.dstValue = result;
				}
				CPU->saveInstructionDestination(currentInstruction, currentInstruction.dstValue);
			}
		break;


		case 0x80:
		case 0x81:
		case 0x82:
		case 0x83:
		case 0x84:
		case 0x85:
		case 0x86:
		case 0x87:
		case 0x88:
		case 0x89:
		case 0x8A:
		case 0x8B:
		case 0x8C:
		case 0x8D:
		case 0x8E:
		case 0x8F:
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			if (currentInstruction.szOperands == sz16) {
				currentInstruction.srcValue = nextWord();
			} else {
				currentInstruction.srcValue = nextDWord();
			}
			if (currentInstruction.executable) {
				CPU->do_jumpcc((jumpEval)(opExtension & 0x0F), 1, currentInstruction.srcValue, currentInstruction);
			}
		break;

		case 0x90:
		case 0x91:
		case 0x92:
		case 0x93:
		case 0x94:
		case 0x95:
		case 0x96:
		case 0x97:
		case 0x98:
		case 0x99:
		case 0x9A:
		case 0x9B:
		case 0x9C:
		case 0x9D:
		case 0x9E:
		case 0x9F:
			currentInstruction.cyclesTotal += 14;
			currentInstruction.isWord = 0;
			this->getRM();
			if (currentInstruction.executable) {
				CPU->do_setcc(currentInstruction, opExtension & 0x0F);
				CPU->saveInstructionDestination(currentInstruction, currentInstruction.dstValue);
			}
		break;

		case 0xB2:
		case 0xB4:
		case 0xB5:
			currentInstruction.isWord = 1;
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			this->getRM();
			if (currentInstruction.executable) {
				if (opExtension == 0xB2) {
					CPU->debug_rm(currentInstruction, "LSS");
					CPU->loadSegmentFarPointer(CPUIntelx86::REGISTER_SS, currentInstruction);
				} else
				if (opExtension == 0xB4) {
					CPU->debug_rm(currentInstruction, "LFS");
					CPU->loadSegmentFarPointer(CPUIntelx86::REGISTER_FS, currentInstruction);
				} else
				if (opExtension == 0xB5) {
					CPU->debug_rm(currentInstruction, "LGS");
					CPU->loadSegmentFarPointer(CPUIntelx86::REGISTER_GS, currentInstruction);
				}
			}
		break;

		case 0xB6:
		case 0xB7:
			currentInstruction.isWord = opExtension & 0x1;
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			this->getRM();
			qSwap(currentInstruction.dstAddr, currentInstruction.srcAddr);
			qSwap(currentInstruction.dstValue, currentInstruction.srcValue);
			qSwap(currentInstruction.typeDst, currentInstruction.typeSrc);
			CPU->debug_rm(currentInstruction, "MOVZX");
			if (currentInstruction.executable) {
				// Load 8/16 bit source rm
				if (currentInstruction.isWord) {
					currentInstruction.szOperands = sz16;
				} else {
					currentInstruction.szOperands = sz8;
				}
				CPU->loadInstructionOperand(currentInstruction, currentInstruction.typeSrc, currentInstruction.srcValue, currentInstruction.srcAddr);
				// Load 32/16 bit dest
				currentInstruction.szOperands = operandSize[CPUIntelx86::REGISTER_CS];
				CPU->loadInstructionOperand(currentInstruction, currentInstruction.typeDst, currentInstruction.dstValue, currentInstruction.dstAddr);
				if (currentInstruction.overrideOpSize) {
					currentInstruction.szOperands = (Size)(1 - currentInstruction.szOperands);
				}
				if (!currentInstruction.executable) {
					return;
				}
				if (!currentInstruction.isWord) {
					currentInstruction.dstValue = static_cast<dword>((byte)(currentInstruction.srcValue & 0xFF));
				} else {
					currentInstruction.dstValue = static_cast<dword>((word)(currentInstruction.srcValue & 0xFFFF));
				}
				CPU->saveInstructionDestination(currentInstruction, currentInstruction.dstValue);
			}
		break;


//		case 0xBE: {
////			dbg_breakpoint();
//			GetRMNames();
//			if (!operOK) return;
//			if (!lsizeover) {
//				if (RMName.CType == RM_REGISTER) {
//					if (AddDebug)
//						debuglog->addDebug("MOVSX", RegStrings[8 + RMName.cREG], RegStrings[RMName.cRM]);
//					SetReg16(RMName.cREG, ALU->byte2s32(GetReg8(RMName.cRM)));
//					return;
//				}
//				else {
//					if (AddDebug)
//						debuglog->addDebug("MOVSX", RegStrings[8 + RMName.cREG], "b[" + QString::number(RMName.CMemAddress) + "]");
//					byte a = GetByte(RMName.CMemAddress, RMName.segOverride); if (!operOK) return;
//					SetReg16(RMName.cREG, ALU->byte2s16(a));
//					return;
//				}
//			}
//			else {
//				ClearSizes;
//				if (RMName.CType == RM_REGISTER) {
//					if (AddDebug)
//						debuglog->addDebug("MOVSX", RegStrings32[8 + RMName.cREG] , RegStrings[RMName.cRM]);
//					SetReg32(RMName.cREG, ALU->byte2s32(GetReg8(RMName.cRM)));
//					return;
//				}
//				else {
//					if (AddDebug)
//						debuglog->addDebug("MOVSX", RegStrings32[8 + RMName.cREG], "b[" + QString::number(RMName.CMemAddress) + "]");
//					byte a = GetByte(RMName.CMemAddress, RMName.segOverride); if (!operOK) return;
//					SetReg32(RMName.cREG, ALU->byte2s32(a));
//					return;
//				}
//			}
//		} return;

		case 0xBA:
			currentInstruction.isWord = 1;
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			this->getRM();

			opExtension = currentInstruction.reg;
			switch (opExtension) {
				case 4:
					CPU->loadInstructionOperand(currentInstruction, currentInstruction.typeDst, currentInstruction.dstValue, currentInstruction.dstAddr);
					if (!currentInstruction.executable) {
						break;
					}
					currentInstruction.srcValue = nextByte();
					if (currentInstruction.executable) {
						currentInstruction.typeSrc = opImm;
						CPU->do_bt(currentInstruction);
					}
					CPU->saveInstructionDestination(currentInstruction, currentInstruction.dstValue);
				break;
				case 5:
					CPU->loadInstructionOperand(currentInstruction, currentInstruction.typeDst, currentInstruction.dstValue, currentInstruction.dstAddr);
					if (!currentInstruction.executable) {
						break;
					}
					currentInstruction.srcValue = nextByte();
					if (currentInstruction.executable) {
						currentInstruction.typeSrc = opImm;
						CPU->do_bts(currentInstruction);
					}
					CPU->saveInstructionDestination(currentInstruction, currentInstruction.dstValue);
				break;
				default:
					CPU->setDebugEnabled();
					CPU->debug_op0("0F BA " + QString("%1").arg((int)2, (int)16, opExtension) + " <---- UNKNOWN!!");
					CPU->stop();
			}
		break;

//		case 0xBF: {
//			GetRMNames();
//			if (!operOK) return;
//			ClearSizes;
//			if (RMName.CType == RM_REGISTER) {
//				if (AddDebug)
//					debuglog->addDebug("MOVSX", RegStrings[8 + RMName.cREG] , RegStrings[8 + RMName.cRM]);
//				SetReg32(RMName.cREG, ALU->word2s32(GetReg16(RMName.cRM)));
//				return;
//			}
//			else {
//				if (AddDebug)
//					debuglog->addDebug("MOVSX", RegStrings32[8 + RMName.cREG], "w[" + QString::number(RMName.CMemAddress) + "]");
//				dword C = ALU->word2s32(GetWord(RMName.CMemAddress, RMName.segOverride)); if (!operOK) return;
//				SetReg32(RMName.cREG, C);
//				return;
//			}
//		} return;

		case 0xBC:
			currentInstruction.isWord = 1;
			if (!currentInstruction.overrideOpSize) {
				currentInstruction.szOperands = getOperandSize();
			}
			this->getRM();
			qSwap(currentInstruction.dstAddr, currentInstruction.srcAddr);
			qSwap(currentInstruction.dstValue, currentInstruction.srcValue);
			qSwap(currentInstruction.typeDst, currentInstruction.typeSrc);
			CPU->loadInstructionOperands(currentInstruction);
			if (currentInstruction.executable) {
				CPU->do_bsf(currentInstruction);
			}
			CPU->saveInstructionDestination(currentInstruction, currentInstruction.dstValue);
		break;

		case 0xC8:
		case 0xC9:
		case 0xCA:
		case 0xCB:
		case 0xCC:
		case 0xCD:
		case 0xCE:
		case 0xCF:
			CPU->do_bswap(opExtension & 0x7);
		break;

		default:
			//CPU->slotStop();
			CPU->setDebugEnabled();
			CPU->debug_op0("0F " + QString("%1").arg((int)2, (int)16, opExtension) + " <---- UNKNOWN!!");
			CPU->stop();
	}


}

void CPUIntelx86_decoder::decodeExtension_grp01() {
//	int lsizeover = OpSizeOver(0);
//	GetRMNames();
//	if (!operOK) return;

	this->getRM();
	currentInstruction.isWord = 1;
	if (!currentInstruction.overrideOpSize) {
		currentInstruction.szOperands = getOperandSize();
	}
	if (!currentInstruction.executable)
		return;

	byte opExtension = currentInstruction.reg;
	word limit;
	dword base;

	switch (opExtension) {
		case 0x00:
			currentInstruction.cyclesTotal += 18;
			CPU->debug_op1({opMem, currentInstruction.szOperands, currentInstruction.dstAddr}, "SGDT");

			// Cannot be a register
			// #UD
			if (currentInstruction.typeDst != opMem) {
				CPU->debug_op0("Error: SGDT, dst != reg");
				CPU->fault_UD(currentInstruction);
				return;
			}
			CPU->setWordR(currentInstruction.dstAddr, CPU->GDTR.Limit, currentInstruction.segmentPrefix); if (!CPU->operOK) return;
			if (sz16 == currentInstruction.szOperands) {
				CPU->setDWordR(currentInstruction.dstAddr + 2, CPU->GDTR.Base & 0x00FFFFFF, currentInstruction.segmentPrefix); if (!CPU->operOK) return;
			} else {
				CPU->setDWordR(currentInstruction.dstAddr + 2, CPU->GDTR.Base, currentInstruction.segmentPrefix); if (!CPU->operOK) return;
			}
		break;

		case 0x01:
			currentInstruction.cyclesTotal += 21;
			CPU->debug_op1({opMem, currentInstruction.szOperands, currentInstruction.dstAddr}, "SIDT");

			// Cannot be a register
			// #UD
			if (currentInstruction.typeDst != opMem) {
				CPU->debug_op0("Error: SIDT, dst != reg");
				CPU->fault_UD(currentInstruction);
				return;
			}
			CPU->setWordR(currentInstruction.dstAddr, CPU->IDTR.Limit, currentInstruction.segmentPrefix); if (!CPU->operOK) return;
			if (sz16 == currentInstruction.szOperands) {
				CPU->setDWordR(currentInstruction.dstAddr + 2, CPU->IDTR.Base & 0x00FFFFFF, currentInstruction.segmentPrefix); if (!CPU->operOK) return;
			} else {
				CPU->setDWordR(currentInstruction.dstAddr + 2, CPU->IDTR.Base, currentInstruction.segmentPrefix); if (!CPU->operOK) return;
			}
		break;

		case 0x02:
			currentInstruction.cyclesTotal += 12;
			CPU->debug_op1({opMem, currentInstruction.szOperands, currentInstruction.dstAddr}, "LGDT");

			// Cannot be a register
			// #UD
			if (currentInstruction.typeDst != opMem) {
				CPU->debug_op0("Error: LGDT, src != reg");
				CPU->fault_UD(currentInstruction);
				return;
			}
			limit = CPU->getWordR(currentInstruction.dstAddr, currentInstruction.segmentPrefix); if (!CPU->operOK) return;
			base = CPU->getDWordR(currentInstruction.dstAddr + 2, currentInstruction.segmentPrefix); if (!CPU->operOK) return;
			CPU->GDTR.Limit = limit;
			CPU->GDTR.Base = base;
			if (sz16 == currentInstruction.szOperands) {
				CPU->GDTR.Base &= 0x00FFFFFF;
			}
		break;

		case 0x03:
			currentInstruction.cyclesTotal += 16;
			CPU->debug_op1({opMem, currentInstruction.szOperands, currentInstruction.dstAddr}, "LIDT");

			// Cannot be a register
			// #UD
			if (currentInstruction.typeDst != opMem) {
				CPU->debug_op0("Error: LIDT, src != reg");
				CPU->fault_UD(currentInstruction);
				return;
			}

			limit = CPU->getWordR(currentInstruction.dstAddr, currentInstruction.segmentPrefix); if (!CPU->operOK) return;
			base = CPU->getDWordR(currentInstruction.dstAddr + 2, currentInstruction.segmentPrefix); if (!CPU->operOK) return;
			CPU->IDTR.Limit = limit;
			CPU->IDTR.Base = base;
			if (sz16 == currentInstruction.szOperands)
				CPU->IDTR.Base &= 0x00FFFFFF;
		break;

		case 0x04:
			currentInstruction.typeSrc = opCReg;
			currentInstruction.srcValue = (CPU->cRegisters[0] & 0xFF);
			CPU->do_smsw(currentInstruction);
		break;

		case 0x06:
			qSwap(currentInstruction.typeDst, currentInstruction.typeSrc);
			qSwap(currentInstruction.dstAddr, currentInstruction.srcAddr);
			qSwap(currentInstruction.dstValue, currentInstruction.srcValue);
			currentInstruction.typeDst = opCReg;
			currentInstruction.dstValue = CPU->cRegisters[0];
			currentInstruction.dstAddr = 0;
			CPU->do_lmsw(currentInstruction);
		break;
		default:
			//CPU->slotSuspend();
			CPU->setDebugEnabled();
			debug_critical("0F 01 " + QString("%1").arg(opExtension, (int)2, (int)16, QChar('0')) + " <---- UNKNOWN!!");
			CPU->stop();
	}
}

void CPUIntelx86_decoder::decodeFPU() {
	byte opcode2 = 0;
	byte regIndex = 0;
	Intel387x::StackElement data;
	qword data64;
	switch (currentInstruction.opcode) {
		case 0xDB:
			opcode2 = nextByte();
			if (!currentInstruction.executable) {
				break;
			}
			switch (opcode2) {
				case 0xE3:
					CPU->debug_op0("FINIT");
					CPU->x387->finit();
				break;
				default:
					CPU->debug_op0("Unknown FPU opcode " + QString::number(currentInstruction.opcode,16) + " " + QString::number(opcode2, 16));
					qDebug() << hex << CS << ":" << hex << EIP << " - Unknown FPU opcodes: " << hex << currentInstruction.opcode << " " << opcode2;
				break;
			}
		break;
		case 0xD9:
			opcode2 = nextByte();
			if (!currentInstruction.executable) {
				break;
			}
			switch (opcode2) {
				case 0xEE:
					CPU->debug_op0("FLDZ");
					CPU->x387->fldz();
				break;
				default:
					CPU->debug_op0("Unknown FPU opcode " + QString::number(currentInstruction.opcode,16) + " " + QString::number(opcode2, 16));
					qDebug() << hex << CS << ":" << hex << EIP << " - Unknown FPU opcodes: " << hex << currentInstruction.opcode << " " << opcode2;
				break;
			}
		break;
		case 0xDD:
			opcode2 = nextByte();
			if (opcode2 >= 0xD0 && opcode2 <= 0xDF) {
				regIndex = opcode2 & 0x7;
				if (opcode2 & 0x08) {
					CPU->debug_op0("FSTP(" + QString::number(regIndex) + ")");
					CPU->x387->fstReg(regIndex, true);
				} else {
					CPU->debug_op0("FST(" + QString::number(regIndex) + ")");
					CPU->x387->fstReg(regIndex, false);
				}
			} else {
				--EIP;
				this->getRM();
				if (!currentInstruction.executable) {
					break;
				}
				CPU->loadInstructionOperand(currentInstruction, currentInstruction.typeSrc, currentInstruction.srcValue, currentInstruction.srcAddr);
				if (!currentInstruction.executable) {
					break;
				}
				switch (currentInstruction.reg) {
					case 0x00:
						CPU->debug_rm(currentInstruction, "FLD");
						data64 = CPU->readMemory64(currentInstruction);
						if (currentInstruction.executable) {
							CPU->x387->fld64(data64);
						}
					break;
					case 0x02:
					case 0x03:
						CPU->debug_rm(currentInstruction, (currentInstruction.reg & 0x01) == 0 ? "FST" : "FSTP");
						data = CPU->x387->fstRm64(currentInstruction.reg & 0x01);
						CPU->saveInstructionDestination64(currentInstruction, *reinterpret_cast<dword*>(&data.data.realExtended));
					break;
					default:
						CPU->debug_op0("Unknown FPU opcode: DD /" + QString::number(currentInstruction.reg,16));
						qDebug() << hex << CS << ":" << hex << EIP << " -Unknown FPU opcode: DD /" << hex << currentInstruction.reg;
					break;
				}
			}
		break;
		default:
			CPU->debug_op0("Unknown FPU opcode " + QString::number(currentInstruction.opcode,16));
			qDebug() << hex << CS << ":" << hex << EIP << " -Unknown FPU opcode: " << hex << currentInstruction.opcode;
		break;
	}
}

void CPUIntelx86_decoder::decodeExtension_grp00() {
	this->getRM();
	currentInstruction.isWord = 1;
	if (!currentInstruction.overrideOpSize) {
		currentInstruction.szOperands = getOperandSize();
	}
	if (!currentInstruction.executable) {
		return;
	}

	byte opExtension = currentInstruction.reg;
//	word limit;
//	dword base;
	switch (opExtension) {

		case 0x00:
			CPU->debug_rm(currentInstruction, "SLDT");
			CPU->saveInstructionDestination(currentInstruction, CPU->LDTR.selector);
		break;

		case 0x01:
			CPU->debug_rm(currentInstruction, "STR");
			CPU->saveInstructionDestination(currentInstruction, CPU->taskRegister.selector);
		break;

		case 0x02:
			CPU->loadLdtr(currentInstruction);
		break;

		case 0x03:
			CPU->loadTaskRegister(currentInstruction);
		break;

		case 0x04:
			CPU->verr(currentInstruction);
		break;

		case 0x05:
			CPU->verw(currentInstruction);
		break;


//	}
	//CPU->slotStop();

		default:
			CPU->setDebugEnabled();
			debug_critical("0F 00 " + QString("%1").arg((int)2, (int)16, opExtension) + " <---- UNKNOWN!!");
			CPU->stop();
	}
}



/**
 * @brief Decode mod.reg.rm field
 */
void CPUIntelx86_decoder::getRM() {
	int reg, rm, b1;

	byte param = nextByte();
	if (!currentInstruction.executable) return;

	reg = ((param & 0x38) >> 3);
	rm = (param & 0x07);
	b1 = ((param & 0xC0) >> 6);

	currentInstruction.dstAddr = reg;
	currentInstruction.srcAddr = rm;

	currentInstruction.reg = reg;
	currentInstruction.rm = rm;
	currentInstruction.mod = b1;

	if (b1 < 3) {
		currentInstruction.typeDst = opMem;
		currentInstruction.typeSrc = opReg;
		currentInstruction.srcAddr = reg;
		if (sz16 == currentInstruction.szAddress) {
			loadRM16();
		}
		else {
			loadRM32();
		}
	}
	else {
		currentInstruction.typeDst = opReg;
		currentInstruction.typeSrc = opReg;
		currentInstruction.dstAddr = rm;
		currentInstruction.srcAddr = reg;
	}


}

void CPUIntelx86_decoder::loadRM16() {
	dword address = 0;
	dword addr_base = 0;
	qint16 disp16;
	qint8 disp8;

	if (currentInstruction.segmentOverride) {
		addr_base = CPU->readSegmentAddressExt(currentInstruction.segmentPrefix);
	} else {
		if (0 == currentInstruction.mod) {
			currentInstruction.segmentPrefix = segRMBaseB[currentInstruction.rm];
			addr_base = CPU->readSegmentAddressExt(segRMBaseB[currentInstruction.rm]);
		} else {
			currentInstruction.segmentPrefix = segRMBaseA[currentInstruction.rm];
			addr_base = CPU->readSegmentAddressExt(segRMBaseA[currentInstruction.rm]);
		}
	}

	switch (currentInstruction.rm)
	{
		case 0:
			address = CPU->registers[CPUIntelx86::REGISTER_EBX].word.reg16;
			address += CPU->registers[CPUIntelx86::REGISTER_ESI].word.reg16;
		break;

		case 1:
			address = CPU->registers[CPUIntelx86::REGISTER_EBX].word.reg16;
			address += CPU->registers[CPUIntelx86::REGISTER_EDI].word.reg16;
		break;

		case 2:
			address = CPU->registers[CPUIntelx86::REGISTER_EBP].word.reg16;
			address += CPU->registers[CPUIntelx86::REGISTER_ESI].word.reg16;
		break;

		case 3:
			address = CPU->registers[CPUIntelx86::REGISTER_EBP].word.reg16;
			address += CPU->registers[CPUIntelx86::REGISTER_EDI].word.reg16;
		break;

		case 4:
			address = CPU->registers[CPUIntelx86::REGISTER_ESI].word.reg16;
		break;

		case 5:
			address = CPU->registers[CPUIntelx86::REGISTER_EDI].word.reg16;
		break;

		case 6:
			if (0 != currentInstruction.mod)
				address = CPU->registers[CPUIntelx86::REGISTER_EBP].word.reg16;
			else {
				disp16 = nextWord();
				address = (disp16 & 0xFFFF);
			}
		break;

		case 7:
			address = CPU->registers[CPUIntelx86::REGISTER_EBX].word.reg16;
		break;
	}

	if (1 == currentInstruction.mod) {
		disp8 = nextByte();
		address += disp8;
	} else
	if (2 == currentInstruction.mod) {
		disp16 = nextWord();
		address += disp16;
	}
	if (sz16 == currentInstruction.szAddress) {
		address &= 0xFFFF;
	}
	currentInstruction.dstAddr = addr_base + address;

}

void CPUIntelx86_decoder::loadRM32()
{
	dword addr_base = 0;

	if (currentInstruction.segmentOverride) {
		addr_base = CPU->readSegmentAddressExt(currentInstruction.segmentPrefix);
	} else {
		if ((5 == currentInstruction.rm) && (currentInstruction.mod != 0)) {
			currentInstruction.segmentPrefix = CPUIntelx86::REGISTER_SS;
			addr_base = CPU->readSegmentAddressExt(CPUIntelx86::REGISTER_SS);
		} else {
			currentInstruction.segmentPrefix = CPUIntelx86::REGISTER_DS;
			addr_base = CPU->readSegmentAddressExt(CPUIntelx86::REGISTER_DS);
		}
	}

	dword address = 0;
	qint32 disp32 = 0;
	qint8 disp8 = 0;

	if (currentInstruction.rm == 4) {

		// base + (index * scale) + disp

		byte sib = nextByte();
		if (!currentInstruction.executable) {
			return;
		}
		byte baseReg = (sib & 0x7);
		byte indexReg = (sib & 0x38) >> 3;
		byte scale = Constants::pow2[(sib & 0xC0) >> 6];

		if (baseReg == 5) {
			if (currentInstruction.mod != 0) {
				 address = CPU->registers[CPUIntelx86::REGISTER_EBP].dword.reg32;
			} else {
				disp32 = nextDWord();
				if (!currentInstruction.executable) {
					return;
				}
				address += disp32;
			}
		} else {
			address = CPU->registers[baseReg].dword.reg32;
		}

		if (indexReg != 4) {
			address += CPU->registers[indexReg].dword.reg32 * scale;
		}

		if (currentInstruction.mod == 1) {
			disp8 = nextByte();
			if (!currentInstruction.executable) {
				return;
			}
			address += disp8;
		} else
		if (currentInstruction.mod == 2) {
			disp32 = nextDWord();
			if (!currentInstruction.executable) {
				return;
			}
			address += disp32;
		}

		// Cut off most significant bytes if 16-bit addressing
		if (sz16 == CPU->getAddrSize(CPUIntelx86::REGISTER_CS)) {
			address &= 0xFFFF;
		}
	} else {
		// load disp +- ebp
		if (currentInstruction.rm == 5 && currentInstruction.mod == 0) {
			// disp32
			disp32 = nextDWord();
			address += disp32;
		} else {
			address = CPU->registers[currentInstruction.rm].dword.reg32;
			if (currentInstruction.mod == 1) {
				disp8 = nextByte();
				address += disp8;
			} else
			if (currentInstruction.mod == 2) {
				disp32 = nextDWord();
				address += disp32;
			}
		}

		if (!currentInstruction.executable) return;

		// Cut off most significant bytes if 16-bit addressing
		if (sz16 == CPU->getAddrSize(CPUIntelx86::REGISTER_CS)) {
			address &= 0xFFFF;
		}
	}
	currentInstruction.dstAddr = addr_base + address;
}

Size CPUIntelx86_decoder::getOperandSize() {
	if (!currentInstruction.isWord) {
		return sz8;
	} else {
		//return operandSize[currentInstruction.segmentPrefix];
		return operandSize[CPUIntelx86::REGISTER_CS];
	}
}

Size CPUIntelx86_decoder::getOperandSize(int segment) {
	if (!currentInstruction.isWord) {
		return sz8;
	} else {
		return operandSize[segment];
	}
}

void CPUIntelx86_decoder::loadImmediateValue(int signExt) {
	currentInstruction.typeSrc = opImm;
	if (currentInstruction.szOperands == sz8) {
		if (signExt) {
			currentInstruction.srcValue = CPU->alu->byte2s32(nextByte());
		} else {
			currentInstruction.srcValue = nextByte();
		}
	} else {
		if (currentInstruction.szOperands == sz16) {
			if (signExt) {
				currentInstruction.srcValue = CPU->alu->byte2s16(nextByte());
			} else {
				currentInstruction.srcValue = nextWord();
			}
		} else {
			if (signExt) {
				currentInstruction.srcValue = CPU->alu->byte2s32(nextByte());
			} else {
				currentInstruction.srcValue = nextDWord();
			}
		}
	}
}

void CPUIntelx86_decoder::loadImmediateOffset(int signExt) {
	if (currentInstruction.szAddress == sz16) {
		if (signExt) {
			currentInstruction.soffset = CPU->alu->byte2s16(nextByte());
		} else {
			currentInstruction.uoffset = nextWord();
		}
	} else {
		if (signExt) {
			currentInstruction.soffset = CPU->alu->byte2s32(nextByte());
		} else {
			currentInstruction.uoffset = nextDWord();
		}
	}
}

void CPUIntelx86_decoder::executeArithmetic1Accumulator(OpExtensionArithmetic operation) {
	currentInstruction.isWord = currentInstruction.opcode & 0x1;
	if (!currentInstruction.overrideOpSize) {
		currentInstruction.szOperands = getOperandSize(CPUIntelx86::REGISTER_CS);
	}
	currentInstruction.typeDst = opReg;
	currentInstruction.dstAddr = CPUIntelx86::REGISTER_EAX;
	loadImmediateValue(0);
	if (currentInstruction.executable) {
		CPU->do_arithm_rm_r(currentInstruction, operation);
	}
}

void CPUIntelx86_decoder::executeArithmetic1RegisterMemory(OpExtensionArithmetic operation) {
	currentInstruction.reverseDirection = currentInstruction.opcode & 0x2;
	currentInstruction.isWord = currentInstruction.opcode & 0x1;

	if (!currentInstruction.overrideOpSize) {
		currentInstruction.szOperands = getOperandSize();
	}
	this->getRM();

	// If 8-bit operand, then opSize doesnt matter
	if (!currentInstruction.executable) {
		return;
	}

	if (currentInstruction.reverseDirection) {
		qSwap(currentInstruction.dstAddr, currentInstruction.srcAddr);
		qSwap(currentInstruction.typeDst, currentInstruction.typeSrc);
		qSwap(currentInstruction.dstValue, currentInstruction.srcValue);
	}
	if (currentInstruction.executable) {
		CPU->do_arithm_rm_r(currentInstruction, operation);
	}
}


bool CPUIntelx86_decoder::checkCanExecute(int len)
{
	bool segCheckOK =
			(CPU->SRegRights[CPUIntelx86::REGISTER_CS].execute)
			| (CPU->isInLimits(CPUIntelx86::REGISTER_CS, EIP, len));
	currentInstruction.executable = segCheckOK;
	if (!segCheckOK) {
		CPU->fault_GP(currentInstruction, 0);
		debug("decoder CanExecute: this segment cannot be executed.");
	}
	return segCheckOK;
}

void CPUIntelx86_decoder::swapDstSrc()
{
	qSwap(currentInstruction.dstAddr, currentInstruction.srcAddr);
	qSwap(currentInstruction.typeDst, currentInstruction.typeSrc);
	qSwap(currentInstruction.dstValue, currentInstruction.srcValue);
}

/**
 * Return byte at [EIP] after checking whether it can be executed.
 * Increments EIP by 1
 */
byte CPUIntelx86_decoder::nextByte() {
	if (!checkCanExecute(1))
		return 0;
	EIP += 1;
	return CPU->byte_at(CS + EIP - 1);
}

/**
 * Return word at [EIP] after checking whether it can be executed.
 * Increments EIP by 2
 */
word CPUIntelx86_decoder::nextWord() {
	if (!checkCanExecute(2)) {
		return 0;
	}
	EIP += 2;
	return CPU->word_at(CS + EIP - 2);
}

/**
* Return doubleword at [EIP] after checking whether it can be executed.
* Increments EIP by 4
*/
dword CPUIntelx86_decoder::nextDWord() {
	if (!checkCanExecute(4))
		return 0;
	EIP += 4;
	return CPU->dword_at(CS + EIP - 4);
}

#ifdef ENABLE_DEBUG
void CPUIntelx86_decoder::debug(const QString& msg)
{
//	qDebug() << "CPU Debug";
	if ( !CPU->isDebugEnabled() ) return;

	QMap<QString, QString> params;
	params.insert("type", "cpu");
	params.insert("operandcount", "0");
	params.insert("opcode", QString::number(currentInstruction.opcode));
	params.insert("opname", msg);
	params.insert("address", QString::number(oldCS + oldEIP));
	doDebug( params );
	//emit signalDebugOutput(params);
}
#else
void CPUIntelx86_decoder::debug(const QString&)
{
}
#endif

void CPUIntelx86_decoder::debug_critical(const QString msg)
{
	QMap<QString, QString> params;
	params.insert("type", "cpu");
	params.insert("operandcount", "0");
	params.insert("opcode", QString::number(currentInstruction.opcode));
	params.insert("opname", msg);
	params.insert("address", QString::number(oldCS + oldEIP));
//	emit signalDebugOutput(params);
	doDebug( params );
}
