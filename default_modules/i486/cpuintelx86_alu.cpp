#include "cpuintelx86_alu.h"
#include "cpuintelx86.h"

CPUIntelx86_ALU::CPUIntelx86_ALU(CPUIntelx86 *myCPU, FlagsRegister *flags, Register *regAccumulator, Register *regData)
{
	this->myCPU = myCPU;
	this->flags = flags;
	this->regAccumulator = regAccumulator;
	this->regData = regData;
}

qint8 CPUIntelx86_ALU::byte2s8(byte b) {
	return static_cast<qint8>(b);
}

qint16 CPUIntelx86_ALU::byte2s16(byte b) {
	qint8 i8 = byte2s8(b);
	qint16 i16 = static_cast<qint16>(i8);
	return i16;
}

qint32 CPUIntelx86_ALU::byte2s32(byte b) {
	qint16 i16 = byte2s16(b);
	qint32 i32 = static_cast<qint32>(i16);
	return i32;
}

qint16 CPUIntelx86_ALU::word2s16(word w) {
	return static_cast<qint16>(w);
}

qint32 CPUIntelx86_ALU::word2s32(word w) {
	qint16 i16 = word2s16(w);
	qint32 i32 = static_cast<qint32>(i16);
	return i32;
}

qint32 CPUIntelx86_ALU::dword2s32(dword d) {
	return static_cast<qint32>(d);
}

qint8 CPUIntelx86_ALU::unsignedToSigned(byte val) {
	return byte2s8(val);
}

qint16 CPUIntelx86_ALU::unsignedToSigned(word val) {
	return word2s16(val);
}

qint32 CPUIntelx86_ALU::unsignedToSigned(dword val) {
	return dword2s32(val);
}

qint16 CPUIntelx86_ALU::mulSigned(qint8 val1, qint8 val2) {
	return (qint16)(val1 * val2);
}

qint32 CPUIntelx86_ALU::mulSigned(qint16 val1, qint16 val2) {
	return (qint32)(val1 * val2);
}

qint64 CPUIntelx86_ALU::mulSigned(qint32 val1, qint32 val2) {
	return (qint64)(val1 * val2);
}

void CPUIntelx86_ALU::div_8(Instruction & instr, byte divider) {
	if (0 == divider) {
//		myCPU->slotStop();
		//CPU->stop();
		myCPU->fault_DE(instr);
		return;
	}

	word op1 = myCPU->getReg16(CPUIntelx86::REGISTER_EAX);
	byte ResultAH;
	word ResultAL;

	ResultAL = op1 / divider;
	if (ResultAL > 0xFF) {
		myCPU->fault_DE(instr);
		return;
	}
	ResultAH = op1 % divider;
	myCPU->registers[CPUIntelx86::REGISTER_EAX].word.byte.low8 = (ResultAL & 0xFF);
	myCPU->registers[CPUIntelx86::REGISTER_EAX].word.byte.high8 = ResultAH;
}

void CPUIntelx86_ALU::div_16(Instruction & instr, word divider) {
	if (0 == divider) {
		myCPU->fault_DE(instr);
		return;
	}

	dword op1 = (myCPU->getReg16(CPUIntelx86::REGISTER_EDX) << 16) | myCPU->getReg16(CPUIntelx86::REGISTER_EAX);
	dword ResultAX;
	word ResultDX;

	ResultAX = op1 / divider;
	if (ResultAX > 0xFFFF) {
		myCPU->fault_DE(instr);
		return;
	}
	ResultDX = op1 % divider;

	myCPU->setReg16(CPUIntelx86::REGISTER_EAX, (ResultAX & 0xFFFF));
	myCPU->setReg16(CPUIntelx86::REGISTER_EDX, ResultDX);
}

void CPUIntelx86_ALU::div_32(Instruction & instr, dword divider) {
	if (0 == divider) {
		myCPU->fault_DE(instr);
		return;
	}
	qint64 op1 = ((static_cast<qint64>(myCPU->getReg32(CPUIntelx86::REGISTER_EDX)) << 32)
				  | myCPU->getReg32(CPUIntelx86::REGISTER_EAX));
	qint64 ResultEAX;
	dword ResultEDX;

	ResultEAX = op1 / divider;
	if (ResultEAX > 0xFFFFFFFF) {
		myCPU->fault_DE(instr);
		return;
	}
	ResultEDX = op1 % divider;

	myCPU->setReg32(CPUIntelx86::REGISTER_EAX, static_cast<dword>(ResultEAX & 0xFFFFFFFF));
	myCPU->setReg32(CPUIntelx86::REGISTER_EDX, ResultEDX);
}

void CPUIntelx86_ALU::idiv_8(Instruction & instr, byte divider) {
	if (0 == divider) {
		myCPU->fault_DE(instr);
		return;
	}

	word op1 = myCPU->getReg16(CPUIntelx86::REGISTER_EAX);
	byte ResultAH;
	word ResultAL;

	ResultAL = word2s16(op1) / byte2s8(divider);
	if (ResultAL > 0xFF) {
		myCPU->fault_DE(instr);
		return;
	}
	ResultAH = op1 % divider;
	myCPU->registers[CPUIntelx86::REGISTER_EAX].word.byte.low8 = (ResultAL & 0xFF);
	myCPU->registers[CPUIntelx86::REGISTER_EAX].word.byte.high8 = ResultAH;
}

void CPUIntelx86_ALU::idiv_16(Instruction & instr, word divider) {
	if (0 == divider) {
		myCPU->fault_DE(instr);
		return;
	}

	dword op1 = (myCPU->getReg16(CPUIntelx86::REGISTER_EDX) << 16) | myCPU->getReg16(CPUIntelx86::REGISTER_EAX);
	dword ResultAX;
	word ResultDX;

	ResultAX = dword2s32(op1) / word2s16(divider);
	if (ResultAX > 0xFFFF) {
		myCPU->fault_DE(instr);
		return;
	}
	ResultDX = op1 % divider;

	myCPU->setReg16(CPUIntelx86::REGISTER_EAX, (ResultAX & 0xFFFF));
	myCPU->setReg16(CPUIntelx86::REGISTER_EDX, ResultDX);
}

void CPUIntelx86_ALU::idiv_32(Instruction & instr, dword divider) {
	if (0 == divider) {
		myCPU->fault_DE(instr);
		return;
	}

	qint64 op1 = ((static_cast<qint64>(myCPU->getReg32(CPUIntelx86::REGISTER_EDX)) << 32)
				  | myCPU->getReg32(CPUIntelx86::REGISTER_EAX));
	qint64 ResultEAX;
	dword ResultEDX;

	ResultEAX = op1 / dword2s32(divider);
	if (ResultEAX > 0xFFFFFFFF) {
		myCPU->fault_DE(instr);
		return;
	}
	ResultEDX = op1 % divider;

	myCPU->setReg32(CPUIntelx86::REGISTER_EAX, static_cast<dword>(ResultEAX & 0xFFFFFFFF));
	myCPU->setReg32(CPUIntelx86::REGISTER_EDX, ResultEDX);
}

