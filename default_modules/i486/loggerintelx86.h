#ifndef LOGGERINTELX86_H
#define LOGGERINTELX86_H

#include <QObject>
#include <logger.h>

class LoggerIntelx86 : public Logger
{
public:
	LoggerIntelx86(const QString fileName, QObject *parent = 0);

private:




};


const QString

	// Common registers in 16-bit mode
	RegStrings[16]  =
		{"AL", "CL", "DL", "BL", "AH", "CH", "DH", "BH",
		"AX", "CX", "DX", "BX", "SP", "BP", "SI", "DI"},

	// Common registers in 32-bit mode
	RegStrings32[16] =
		{"AL", "CL", "DL", "BL", "AH", "CH", "DH", "BH",
		"EAX", "ECX", "EDX", "EBX", "ESP", "EBP", "ESI", "EDI"},

	// Control registers
	cRegStrings[8] =
		{"CR0", "CR1", "CR2", "CR3", "CR4", "CR5", "CR6", "CR7"},

	// Trap registers
	tRegStrings[8] =
		{"TR0", "TR1", "TR2", "TR3", "TR4", "TR5", "TR6", "TR7"},

	// Segment registers
	SRegStrings[6] =
		{"ES", "CS", "SS", "DS", "FS", "GS"},

	// Flags
	FlagStrings[8] =
		{"C", "-", "P", "-", "A", "-", "Z", "S"},

	// Call gates
	GateStrings[8] = {
			"Reserved segment (EXCEPTION)",
			"Free TSS",
			"LDT",
			"Non-Free TSS",
			"Call gate",
			"Task gate",
			"Interrupt gate",
			"Trap gate"
		};

const QString OpcodeNames16[256+8+8] = {"ADD", "ADD", "ADD", "ADD", "ADD", "ADD", "PUSH", "POP",           "OR", "OR", "OR", "OR", "OR", "OR", "PUSH", "0F",
									"ADC", "ADC", "ADC", "ADC", "ADC", "ADC", "PUSH", "POP",           "SBB", "SBB", "SBB", "SBB", "SBB", "SBB", "PUSH", "POP",
									"AND", "AND", "AND", "AND", "AND", "AND", "ES:", "DAA",            "SUB", "SUB", "SUB", "SUB", "SUB", "SUB", "CS:", "DAS",
									"XOR", "XOR", "XOR", "XOR", "XOR", "XOR", "SS:", "AAA",            "CMP", "CMP", "CMP", "CMP", "CMP", "CMP", "DS:", "AAS",
									"INC", "INC", "INC", "INC", "INC", "INC", "INC", "INC",            "DEC", "DEC", "DEC", "DEC", "DEC", "DEC", "DEC", "DEC",
									"PUSH", "PUSH", "PUSH", "PUSH", "PUSH", "PUSH", "PUSH", "PUSH",    "POP", "POP", "POP", "POP", "POP", "POP", "POP", "POP",
									"PUSHA", "POPA", "BOUND", "ARPL", "FS:", "GS:", "SZ:", "AD:",      "PUSH", "IMUL", "PUSH", "IMUL", "INSB", "INSW", "OUTSB", "OUTSW",
									"JO", "JNO", "JC", "JNC", "JZ", "JNZ", "JNA", "JA",                "JS", "JNS", "JP", "JNP", "JL", "JGE", "JLE", "JG",
									"*", "*", "*", "*", "TEST", "TEST", "XCHG", "XCHG",                "MOV", "MOV", "MOV", "MOV", "MOV", "LEA", "MOV", "POP",
									"NOP", "XCHG", "XCHG", "XCHG", "XCHG", "XCHG", "XCHG", "XCHG", "CBW", "CWD", "CALLF", "WAIT", "PUSHF", "POPF", "SAHF", "LAHF",
									"MOV", "MOV", "MOV", "MOV", "MOVSB", "MOVSW", "CMPSB", "CMPSW", "TEST", "TEST", "STOSB", "STOSW", "LODSB", "LODSW", "SCASB", "SCASW",
									"MOV", "MOV", "MOV", "MOV", "MOV", "MOV", "MOV", "MOV", "MOV", "MOV", "MOV", "MOV", "MOV", "MOV", "MOV", "MOV",
									"*", "*", "RETN", "RETN", "LES", "LDS", "*", "*", "ENTER", "LEAVE", "RETF", "RETF", "INT3", "INT", "INTO", "IRET",
									"*", "*", "*", "*", "AAM", "AAD", "UNKN", "XLAT", "ESC", "ESC", "ESC", "ESC", "ESC", "ESC", "ESC", "ESC",
									"LOOPNZ", "LOOPZ", "LOOP", "JCXZ", "IN", "IN", "OUT", "OUT", "CALL", "JMP", "JMP", "JMP", "IN", "IN", "OUT", "OUT",
									"LOCK", "*", "REPNE", "REPE", "HLT", "CMC", "*", "*", "CLC", "STC", "CLI", "STI", "CLD", "STD", "*", "*",
									"ROL", "ROR", "RCL", "RCR", "SHL", "SHR", "SAL", "SAR",
									"TEST", "-", "NOT", "NEG", "MUL", "IMUL", "DIV", "IDIV"};

const QString OpcodeNames32[256+8+8] = {"ADD d", "ADD d", "ADD d", "ADD d", "ADD d", "ADD d", "PUSH d", "POP d",  "OR d", "OR d", "OR d", "OR d", "OR d", "OR d", "PUSH d", "0F",
									"ADC d", "ADC d", "ADC d", "ADC d", "ADC d", "ADC d", "PUSH d", "POP d",  "SBB d", "SBB d", "SBB d", "SBB d", "SBB d", "SBB d", "PUSH d", "POP d",
									"AND d", "AND d", "AND d", "AND d", "AND d", "AND d", "ES:", "DAA d", "SUB d", "SUB d", "SUB d", "SUB d", "SUB d", "SUB d", "CS:", "DAS d",
									"XOR d", "XOR d", "XOR d", "XOR d", "XOR d", "XOR d", "SS:", "AAA d",            "CMP d", "CMP d", "CMP d", "CMP d", "CMP d", "CMP d", "DS:", "AAS d",
									"INC d", "INC d", "INC d", "INC d", "INC d", "INC d", "INC d", "INC d",            "DEC d", "DEC d", "DEC d", "DEC d", "DEC d", "DEC d", "DEC d", "DEC d",
									"PUSH d", "PUSH d", "PUSH d", "PUSH d", "PUSH d", "PUSH d", "PUSH d", "PUSH d",    "POP d", "POP d", "POP d", "POP d", "POP d", "POP d", "POP d", "POP d",
									"PUSHAD", "POPAD", "BOUND d", "ARPL", "FS:", "GS:", "SZ:", "AD:",      "PUSH d", "IMUL d", "PUSH d", "IMUL d", "INSB", "INSD", "OUTSB", "OUTSD",
									"JO", "JNO", "JC", "JNC", "JZ", "JNZ", "JNA", "JA",                "JS", "JNS", "JP", "JNP", "JL", "JGE", "JLE", "JG",
									"*", "*", "*", "*", "TEST d", "TEST d", "XCHG d", "XCHG d",                "MOV d", "MOV d", "MOV d", "MOV d", "MOV d", "LEA d", "MOV d", "POP d",
									"NOP", "XCHG d", "XCHG", "XCHG d", "XCHG d", "XCHG d", "XCHG d", "XCHG d", "CBW d", "CWD d", "CALLF", "WAIT", "PUSHFD", "POPFD", "SAHF", "LAHF",
									"MOV d", "MOV d", "MOV d", "MOV d", "MOVSB", "MOVSD", "CMPSB", "CMPSD", "TEST d", "TEST d", "STOSB", "STOSD", "LODSB", "LODSD", "SCASB", "SCASD",
									"MOV d", "MOV d", "MOV d", "MOV d", "MOV d", "MOV d", "MOV d", "MOV d", "MOV d", "MOV d", "MOV d", "MOV d", "MOV d", "MOV d", "MOV d", "MOV d",
									"*", "*", "RETN", "RETN", "LES d", "LDS d", "*", "*", "ENTERD", "LEAVED", "RETFD", "RETFD", "INT3", "INT", "INTOD", "IRETD",
									"*", "*", "*", "*", "AAM d", "AAD d", "UNKN", "XLAT d", "ESC d", "ESC d", "ESC d", "ESC d", "ESC d", "ESC d", "ESC d", "ESC d",
									"LOOPNZ", "LOOPZ", "LOOP", "JCXZ", "IN d", "IN d", "OUT d", "OUT d", "CALL", "JMP", "JMP", "JMP", "IN d", "IN d", "OUT d", "OUT d",
									"LOCK", "*", "REPNE", "REPE", "HLT", "CMC d", "*", "*", "CLC", "STC", "CLI", "STI", "CLD", "STD", "*", "*",
									"ROL d", "ROR d", "RCL d", "RCR d", "SHL d", "SHR d", "SAL d", "SAR d",
									"TEST d", "-", "NOT d", "NEG d", "MUL d", "IMUL d", "DIV d", "IDIV d"};


const QString ExtensionNames[256] = {"LAR", "LSL"};





#endif // LOGGERINTELX86_H
