#include "cpuintelx86.h"
#include "cpuintelx86_alu.h"
#include "loggerintelx86.h"
//#include "../chipset/chipsetpc.h"

/**
* 27    DAA
* Decimal adjust AL after addition
*/
void CPUIntelx86::do_daa() {
	cyclesTotal += 3;
	debug_op0();
	byte old_AL = getReg8(REGISTER_EAX);
	dword old_CF = (eflags.eflags.flag32 & FLAG_CF);
	eflags.eflags.flag32 &= FLAG_CF_CLEAR;

	if (((old_AL & 0x0F) > 9) || (eflags.eflags.flag32 & FLAG_AF)) {
		word tmp = (old_AL + 6);
		setReg8(REGISTER_EAX, (tmp & 0xFF));
		if ((tmp & 0xFF00) || old_CF) {
			eflags.eflags.flag32 |= FLAG_CF;
		}
		eflags.eflags.flag32 |= FLAG_AF;
	} else {
		eflags.eflags.flag32 &= ~FLAG_AF_CLEAR;
	}

	if ((old_AL > 0x99) || (old_CF)) {
		setReg8(REGISTER_EAX, ((getReg8(REGISTER_EAX) + 0x60) & 0xFF));
		eflags.eflags.flag32 |= FLAG_CF;
	}
}

void CPUIntelx86::do_das() {
	cyclesTotal += 3;
	debug_op0();
	byte old_AL = getReg8(REGISTER_EAX);
	dword old_CF = (eflags.eflags.flag32 & FLAG_CF);

	if (((old_AL & 0x0F) > 9) || (eflags.eflags.flag32 & FLAG_AF)) {
		word tmp = (old_AL - 6);
		setReg8(REGISTER_EAX, (tmp & 0xFF));
		if ((tmp & 0xFF00) || old_CF) eflags.eflags.flag32 |= FLAG_CF;
		eflags.eflags.flag32 |= FLAG_AF;
	}
	else {
		eflags.eflags.flag32 &= FLAG_AF_CLEAR;
	}

	if ((old_AL > 0x99) || (old_CF)) {
		setReg8(REGISTER_EAX, ((getReg8(REGISTER_EAX) - 0x60) & 0xFF));
		eflags.eflags.flag32 |= FLAG_CF;
	}
	else {
		eflags.eflags.flag32 &= FLAG_CF_CLEAR;
	}
}

/**
 * 37    AAA
 * ASCII adjust AL after correction
 */
void CPUIntelx86::do_aaa() {
	debug_op0();

	cyclesTotal += 3;

	eflags.eflags.flag32 &= FLAG_CF_CLEAR;
	eflags.eflags.flag32 &= FLAG_AF_CLEAR;

	if (((registers[REGISTER_EAX].word.byte.low8 & 0x0F) > 9) || (eflags.eflags.flag32 & FLAG_AF)) {
		registers[REGISTER_EAX].word.byte.low8 += 0x06; // Correction
		registers[REGISTER_EAX].word.byte.high8++;		// Send CF to hi
		eflags.eflags.flag32 |= FLAG_CF;					// Show decimal carry
		eflags.eflags.flag32 |= FLAG_AF;
	}
	registers[REGISTER_EAX].word.byte.low8 &= 0x0F;
}

/**
 * D5 ib    AAM
 * Adjust AX before division
 */
void CPUIntelx86::do_aad(byte base) {
	debug_op0();

	byte tempAL = registers[REGISTER_EAX].word.byte.low8;
	byte tempAH = registers[REGISTER_EAX].word.byte.high8;
	byte resultAL = tempAL + (tempAH * base);
	alu->setFlagsSZP<byte>(resultAL);

	cyclesTotal += 4;

	registers[REGISTER_EAX].word.byte.low8 = tempAL;
	registers[REGISTER_EAX].word.byte.high8 = tempAH;
}

/**
 * 3F    AAS
 * ASCII adjust AL after subtraction
 */
void CPUIntelx86::do_aas() {
	cyclesTotal += 3;
	debug_op0();
	eflags.eflags.flag32 &= FLAG_CF_CLEAR;
	eflags.eflags.flag32 &= FLAG_AF_CLEAR;

	if (((registers[REGISTER_EAX].word.byte.low8 & 0x0F) > 9) || (eflags.eflags.flag32 & FLAG_AF)) {
		registers[REGISTER_EAX].word.reg16 -= 6;
		registers[REGISTER_EAX].word.byte.high8--;		// Sub CF from hi
		eflags.eflags.flag32 |= FLAG_CF;					// Show decimal carry
		eflags.eflags.flag32 |= FLAG_AF;
	}
	registers[REGISTER_EAX].word.byte.low8 &= 0x0F;
}

void CPUIntelx86::do_bswap(byte reg) {
	debug_op1(DebugClosure{opReg, sz32, reg}, "BSWAP");
	dword op32_1 = getReg32(reg);
	dword result = 0;
	result =  ((op32_1 & 0xFF000000) >> 24);
	result |= ((op32_1 & 0x00FF0000) >> 8);
	result |= ((op32_1 & 0x0000FF00) << 8);
	result |= ((op32_1 & 0x000000FF) << 24);
	setReg32(reg, result);
}

void CPUIntelx86::do_bt(Instruction & instr) {
	debug_rm(instr, "BT");
	dword bitCount = instr.srcValue;
	dword dst = instr.dstValue;
	dword result = dst & (1 << bitCount);
	if (result) {
		eflags.eflags.flag32 |= FLAG_CF;
	} else {
		eflags.eflags.flag32 &= FLAG_CF_CLEAR;
	}

	instr.dstValue = dst;
}

void CPUIntelx86::do_bts(Instruction & instr) {
	debug_rm(instr, "BTS");
	dword bitCount = instr.srcValue;
	dword dst = instr.dstValue;
	dword result = dst & (1 << bitCount);
	if (result) {
		eflags.eflags.flag32 |= FLAG_CF;
	} else {
		eflags.eflags.flag32 &= FLAG_CF_CLEAR;
	}
	dst |= (1 << bitCount);

	instr.dstValue = dst;
}

void CPUIntelx86::do_bsf(Instruction & instr) {
	debug_rm(instr, "BSF");
	dword count = 0;
	if (instr.szOperands == sz16) {
		word src = (instr.srcValue & 0xFFFF);
		if (0 == src) {
			eflags.eflags.flag32 |= FLAG_ZF;
		} else {
			while (!(src & 1)) {
				++count;
				src = (src >> 1);
			}
		}
	} else {
		dword src = instr.srcValue;
		if (0 == src) {
			eflags.eflags.flag32 |= FLAG_ZF;
		} else {
			while (!(src & 1)) {
				++count;
				src = (src >> 1);
			}
		}
	}
	instr.dstValue = count;
}

void CPUIntelx86::do_setcc(Instruction & instr, byte code) {
	byte result = 0;
	bool testResult = false;
	switch (code) {
		case 0:
			debug_rm(instr, "SETO");
			testResult = flagTestCO();
		break;
		case 1:
			debug_rm(instr, "SETNO");
			testResult = !flagTestCO();
		break;
		case 2:
			debug_rm(instr, "SETC");
			testResult = flagTestCC();
		break;
		case 3:
			debug_rm(instr, "SETNC");
			testResult = !flagTestCC();
		break;
		case 4:
			debug_rm(instr, "SETZ");
			testResult = flagTestCZ();
		break;
		case 5:
			debug_rm(instr, "SETNZ");
			testResult = !flagTestCZ();
		break;
		case 6:
			debug_rm(instr, "SETBE");
			testResult = flagTestCBE();
		break;
		case 7:
			debug_rm(instr, "SETA");
			testResult = flagTestCA();
		break;
		case 8:
			debug_rm(instr, "SETS");
			testResult = flagTestCS();
		break;
		case 9:
			debug_rm(instr, "SETNS");
			testResult = !flagTestCS();
		break;
		case 0xa:
			debug_rm(instr, "SETP");
			testResult = flagTestCP();
		break;
		case 0xb:
			debug_rm(instr, "SETNP");
			testResult = !flagTestCP();
		break;
		case 0xc:
			debug_rm(instr, "SETL");
			testResult = flagTestCL();
		break;
		case 0xd:
			debug_rm(instr, "SETNL");
			testResult = !flagTestCL();
		break;
		case 0xe:
			debug_rm(instr, "SETLE");
			testResult = flagTestCLE();
		break;
		case 0xf:
			debug_rm(instr, "SETG");
			testResult = flagTestCG();
		break;
	}
	if (testResult) {
		result = 0x01;
	}
	instr.dstValue = result;
}

void CPUIntelx86::do_movcc(Instruction & instr, byte code) {
	bool testResult = false;
	switch (code) {
		case 0:
			debug_rm(instr, "CMOVO");
			testResult = flagTestCO();
		break;
		case 1:
			debug_rm(instr, "CMOVNO");
			testResult = !flagTestCO();
		break;
		case 2:
			debug_rm(instr, "CMOVC");
			testResult = flagTestCC();
		break;
		case 3:
			debug_rm(instr, "CMOVNC");
			testResult = !flagTestCC();
		break;
		case 4:
			debug_rm(instr, "CMOVZ");
			testResult = flagTestCZ();
		break;
		case 5:
			debug_rm(instr, "CMOVNZ");
			testResult = !flagTestCZ();
		break;
		case 6:
			debug_rm(instr, "CMOVBE");
			testResult = flagTestCBE();
		break;
		case 7:
			debug_rm(instr, "CMOVA");
			testResult = flagTestCA();
		break;
		case 8:
			debug_rm(instr, "CMOVS");
			testResult = flagTestCS();
		break;
		case 9:
			debug_rm(instr, "CMOVNS");
			testResult = !flagTestCS();
		break;
		case 0xa:
			debug_rm(instr, "CMOVP");
			testResult = flagTestCP();
		break;
		case 0xb:
			debug_rm(instr, "CMOVNP");
			testResult = !flagTestCP();
		break;
		case 0xc:
			debug_rm(instr, "CMOVL");
			testResult = flagTestCL();
		break;
		case 0xd:
			debug_rm(instr, "CMOVNL");
			testResult = !flagTestCL();
		break;
		case 0xe:
			debug_rm(instr, "CMOVLE");
			testResult = flagTestCLE();
		break;
		case 0xf:
			debug_rm(instr, "CMOVG");
			testResult = flagTestCG();
		break;
	}
	if (testResult) {
		do_mov_rm_r(instr, false);
	}
}

void CPUIntelx86::do_inc_reg(Instruction& instr) {
	cyclesTotal ++;

	dword cf = (eflags.eflags.flag32 & FLAG_CF);

	byte reg = instr.reg;
	debug_op1(DebugClosure{opReg, instr.szOperands, reg});
	if (instr.szOperands == sz16) {
		word r16 = getReg16(reg);
		word result = alu->add<word, dword>(r16, 1);
		setReg16(reg, result);
	} else {
		dword r32 = getReg32(reg);
		dword result = alu->add<dword, qword>(r32, 1);
		setReg32(reg, result);
	}
	eflags.eflags.flag32 &= (~FLAG_CF);
	eflags.eflags.flag32 |= cf;
}

void CPUIntelx86::do_dec_reg(Instruction & instr) {
	cyclesTotal ++;

	dword cf = (eflags.eflags.flag32 & FLAG_CF);

	byte reg = instr.reg;
	debug_op1(DebugClosure{opReg, instr.szOperands, reg});
	if (instr.szOperands == sz16) {
		word r16 = getReg16(reg);
		word result = alu->sub<word, dword>(r16, 1);
		setReg16(reg, result);
	} else {
		debug_op1(DebugClosure{opReg, sz32, reg});
		dword r32 = getReg32(reg);
		dword result = alu->sub<dword, qword>(r32, 1);
		setReg32(reg, result);
	}
	eflags.eflags.flag32 &= FLAG_CF_CLEAR;
	eflags.eflags.flag32 |= cf;

}

void CPUIntelx86::do_cli() {
	cyclesTotal += 5;
	debug_op0();
	if (operatingMode == opModeReal) {
		eflags.eflags.flag32 &= (~FLAG_IF);
	} else {
		if (operatingMode == opModeProtected) {
			if (cpl <= getIOPL()) {
				eflags.eflags.flag32 &= (~FLAG_IF);
			} else {
				fault_GP(decoder->currentInstruction, 0);
			}
		} else {
			if (getIOPL() == 3) {
				eflags.eflags.flag32 &= (~FLAG_IF);
			} else {
				fault_GP(decoder->currentInstruction, 0);
			}
		}
	}
}

void CPUIntelx86::do_sti() {
	cyclesTotal += 5;
	debug_op0();
	if (operatingMode == opModeReal) {
		eflags.eflags.flag32 |= FLAG_IF;
	} else {
		auto iopl = getIOPL();
		if (operatingMode == opModeProtected) {
			if (iopl == 3) {
				eflags.eflags.flag32 |= FLAG_IF;
			} else {
				if (cpl <= iopl) {
					eflags.eflags.flag32 |= FLAG_IF;
				} else {
					fault_GP(decoder->currentInstruction, 0);
				}
			}
		} else {
			if ((cpl == iopl) && (iopl == 3)) {
				eflags.eflags.flag32 |= FLAG_IF;
			} else {
				fault_GP(decoder->currentInstruction, 0);
			}
		}
	}
	if (hasPendingInterrupts) {
		ioAckInterrupt();
	}
}

byte CPUIntelx86::getIOPL() {
	return ((eflags.eflags.flag32 & BITS_IOPL) >> BITS_IOPL_SHIFT);
}

void CPUIntelx86::do_push_reg(Instruction & instr) {
	cyclesTotal += 1;

	debug_op1(DebugClosure{opReg, instr.szOperands, instr.reg});
	if (instr.szOperands == sz16) {
		push16(instr, getReg16(instr.reg));
	} else {
		push32(instr, getReg32(instr.reg));
	}
}

void CPUIntelx86::do_pop_reg(Instruction & instr) {
	cyclesTotal += 1;
	debug_op1(DebugClosure{opReg, instr.szOperands, instr.reg});
	if (instr.szOperands == sz16) {
		word A = pop16(instr); if (!operOK) return;
		setReg16(instr.reg, A);
	} else {
		dword K = pop32(instr); if (!operOK) return;
		setReg32(instr.reg, K);
	}
}

void CPUIntelx86::enter_c8(Instruction & instr, word locSize, byte lexLevel) {
	cyclesTotal += 12;

	debug_op2(DebugClosure{opImm, sz16, locSize}, DebugClosure{opImm, sz8, lexLevel});

	// addr-size of SS determines entire operation
	int stackSize = getAddrSize(REGISTER_SS);

	if (!stackSize) {
		push16(instr, getReg16(REGISTER_EBP));
	} else {
		push32(instr, getReg32(REGISTER_EBP));
	}
	if (!operOK) return;

	lexLevel %= 32;
	dword temp_sp = registers[REGISTER_ESP].dword.reg32;

	if (lexLevel > 0) {
		for (int i = 1; i < lexLevel - 1; ++i) {
			if (instr.szOperands == sz32) {
				if (stackSize) {
					registers[REGISTER_EBP].dword.reg32 -= 4;
					push32(instr, registers[REGISTER_EBP].dword.reg32);
				} else {
					registers[REGISTER_EBP].word.reg16 -= 4;
					push32(instr, registers[REGISTER_EBP].word.reg16);
				}
			} else {
				if (stackSize) {
					registers[REGISTER_EBP].dword.reg32 -= 2;
					push32(instr, registers[REGISTER_EBP].dword.reg32);
				} else {
					registers[REGISTER_EBP].word.reg16 -= 2;
					push16(instr, registers[REGISTER_EBP].word.reg16);
				}
			}
			if (!operOK) return;
		}
	}
	if (instr.szOperands == sz16) {
		push16(instr, temp_sp & 0xFFFF);
	} else {
		push32(instr, temp_sp);
	}
	if (!operOK) return;

	if (stackSize) {
		registers[REGISTER_EBP].dword.reg32 = temp_sp;
		registers[REGISTER_ESP].dword.reg32 -= locSize;
	}
	else {
		registers[REGISTER_ESP].word.reg16 -= locSize;
		registers[REGISTER_EBP].word.reg16 = (temp_sp & 0xFFFF);
	}
}

void CPUIntelx86::leave_c9(Instruction & instr) {
	cyclesTotal += 22;
	debug_op0();

	// 16 bit
	if (getAddrSize(REGISTER_SS)) {
		registers[REGISTER_ESP].word.reg16 = registers[REGISTER_EBP].word.reg16;
	} else {
		registers[REGISTER_ESP].dword.reg32 = registers[REGISTER_EBP].dword.reg32;
	}

	operOK = 1;
	if (instr.szOperands == sz16) {
		registers[REGISTER_EBP].word.reg16 = pop16(instr);
	} else {
		registers[REGISTER_EBP].dword.reg32 = pop32(instr);
	}
}

template<typename T, typename TNext> T CPUIntelx86::do_arithmetic(Instruction &instr, OpExtensionArithmetic operation) {
	//byte operation = ((instr.opcode & 0x38) >> 3);
	T src = extractBitValue<T>(instr.srcValue);
	T dst = extractBitValue<T>(instr.dstValue);
	T result = dst;
	switch (operation) {
		case OpExtensionArithmetic::Add: result = alu->add<T, TNext>(dst, src); break;
		case OpExtensionArithmetic::Or: result = alu->Or<T>(dst, src); break;
		case OpExtensionArithmetic::Adc: result = alu->adc<T, TNext>(dst, src); break;
		case OpExtensionArithmetic::Sbb: result = alu->sbb<T, TNext>(dst, src); break;
		case OpExtensionArithmetic::And: result = alu->And<T>(dst, src); break;
		case OpExtensionArithmetic::Sub: result = alu->sub<T, TNext>(dst, src); break;
		case OpExtensionArithmetic::Xor: result = alu->Xor<T>(dst, src); break;
		case OpExtensionArithmetic::Cmp: alu->cmp<T, TNext>(dst, src); break;
		default:
		{
			//dbg_breakpoint();
			this->setDebugEnabled();
			//this->slotStop();
			debug_log("Error in [ decode arithmetic ]: unknown opcode " + QString::number(instr.opcode, 16));
			stop();
		}
	}
	return result;
}

void CPUIntelx86::do_imul_tri(Instruction & instr) {
	debug_rm(instr);
	if (instr.szOperands == sz16) {
		word result = (word)(alu->imul_2<word, dword>(instr.srcValue, (word)(instr.immediate & 0xFFFF)) & 0xFFFF);
		qSwap(instr.srcAddr, instr.dstAddr);
		qSwap(instr.typeDst, instr.typeSrc);
		saveInstructionDestination(instr, result);
	} else {
		dword result = (dword)(alu->imul_2<dword, qword>(instr.srcValue, instr.immediate) & 0xFFFFFFFF);
		qSwap(instr.srcAddr, instr.dstAddr);
		qSwap(instr.typeDst, instr.typeSrc);
		saveInstructionDestination(instr, result);
	}
}


/**
 * @brief Using given decoded instruction, perform needed action
 * @warning Should not be used outsize decoder routines
 */
void CPUIntelx86::do_arithm_rm_r(Instruction &instr, OpExtensionArithmetic operation)
{
	cyclesTotal += 13;
	loadInstructionOperands(instr);	// Load operands

	debug_rm(instr);

	if (!instr.executable) return;

	dword result = 0;	// Clear all bits to prevent byte and word overflows

	// Byte operation
	if (instr.szOperands == sz8) {
		result = do_arithmetic<byte, word>(instr, operation);
	} else {
		if (instr.szOperands == sz16) {
			result = do_arithmetic<word, dword>(instr, operation);
		} else {
			result = do_arithmetic<dword, qword>(instr, operation);
		}
	}

	if (operation != OpExtensionArithmetic::Cmp) {
		saveInstructionDestination(instr, result);
	}
}

void CPUIntelx86::do_xchg_rm_r(Instruction &instr) {
	loadInstructionOperands(instr);	// Load operands

	debug_rm(instr);

	if (!instr.executable) return;

	dword temp = instr.dstValue;

	saveInstructionDestination(instr, instr.srcValue);
	instr.typeDst = instr.typeSrc;
	instr.dstAddr = instr.srcAddr;
	saveInstructionDestination(instr, temp);
}

void CPUIntelx86::do_test_rm_r(Instruction &instr) {
	loadInstructionOperands(instr);	// Load operands
	debug_rm(instr);
	if (!instr.executable) return;
	if (sz8 == instr.szOperands) {
		alu->And<byte>((byte)(instr.dstValue & 0x000000FF), (byte)(instr.srcValue & 0x000000FF));
	} else {
		if (sz16 == instr.szOperands) {
			alu->And<word>((word)(instr.dstValue & 0x0000FFFF), (word)(instr.srcValue & 0x0000FFFF));
		} else {
			alu->And<dword>(instr.dstValue, instr.srcValue);
		}
	}
}


void CPUIntelx86::do_mov_rm_r(Instruction &instr, bool )
{
	debug_rm(instr);
	loadInstructionOperand(instr, instr.typeSrc, instr.srcValue, instr.srcAddr);	// Load operands
	if (!instr.executable) return;
	saveInstructionDestination(instr, instr.srcValue);
}

void CPUIntelx86::do_mov_sr_rm(Instruction &instr)
{
	instr.typeDst = opSReg;
	loadInstructionOperand(instr, instr.typeSrc, instr.srcValue, instr.srcAddr);	// Load operands

	debug_rm(instr);

	if (!instr.executable) return;

	saveInstructionDestination(instr, instr.srcValue);
}

void CPUIntelx86::do_pop_rm(Instruction &instr)
{
	debug_op1({opMem, instr.szOperands, instr.dstAddr});

	if (instr.typeDst != opMem) {
		fault_UD(instr);
		return;
	}

	if (instr.reg != 0) {
		fault_UD(instr);
		return;
	}

	//loadInstructionOperand(instr, instr.typeDst, instr.dstValue, instr.dstAddr);
	if (!instr.executable) return;

	if (sz16 == instr.szOperands)
		instr.srcValue = pop16(instr);
	else
		instr.srcValue = pop32(instr);

	saveInstructionDestination(instr, instr.srcValue);
}

void CPUIntelx86::do_mov_rm_sr(Instruction &instr)
{
	instr.typeSrc = opSReg;
	loadInstructionOperand(instr, instr.typeSrc, instr.srcValue, instr.srcAddr);	// Load operands

	debug_rm(instr);

	if (!instr.executable) return;

	saveInstructionDestination(instr, instr.srcValue);
}

/**
 * @brief Mov reg, imm with extension code
 * @short For C6/C7 instruction
 */
void CPUIntelx86::do_mov_reg_imm_ext(Instruction &instr, byte opExtension)
{
	instr.extensionIndex = 0xA0;	// "MOV" lookup
	// This is C6/C7 opcode which now only allows to use /0 extension code
	if (opExtension != 0) {
		fault_UD(instr);
		return;
	}

	debug_rm(instr);

	saveInstructionDestination(instr, instr.srcValue);
}


void CPUIntelx86::do_xlat(Instruction &instr) {
	cyclesTotal += 15;
	debug_op0();
	byte a = getReg8(REGISTER_EAX);
	dword addr;
	dword addr_base;
	addr_base = (this->*getSegmentAddress)(instr.segmentPrefix);
	if (sz16 == instr.szAddress) {
		addr = getReg16(REGISTER_EBX);
	} else {
		addr = getReg32(REGISTER_EBX);
	}
	byte b = getByteR(addr_base + addr + a, instr.segmentPrefix); if (!operOK) return;
	setReg8(REGISTER_EAX, b);
}


/**
*	ARPL - Adjust Required Priveledge Level
*	rm16 <- correct(rm16 < reg16)
*/
void CPUIntelx86::do_arpl(Instruction &instr) {
	cyclesTotal += 2;
	loadInstructionOperands(instr);	// Load operands

	debug_rm(instr);

	if (!instr.executable) return;

	word src = instr.srcValue;
	word dest = instr.dstValue;

	word rplSrc = (src & 0x3);
	word rplDest = (dest & 0x3);

	if (rplDest < rplSrc) {
		eflags.eflags.flag32 |= FLAG_ZF;
		dest &= 0xFFFC;
		dest |= rplSrc;
		saveInstructionDestination(instr, dest);
	}
	else {
		eflags.eflags.flag32 &= FLAG_ZF_CLEAR;
	}

}


/**
 * 62 mod.reg.rm
 * BOUND
 * Check if the element is in array index [m .. m + opSize]
 * Generate #BR if not
 * @todo faults
*/
void CPUIntelx86::do_bound(Instruction &instr) {
	loadInstructionOperands(instr);
	debug_rm(instr);

	if (!instr.executable) return;

	if (instr.typeDst == opReg) {
		fault_UD(instr);
	}


	if (instr.szOperands == sz16) {
		//	16-bit
		qint16 Lo = alu->word2s16(getWordR(instr.dstAddr, instr.segmentPrefix)); if (!operOK) return;
		qint16 Hi = alu->word2s16(getWordR(instr.dstAddr+2, instr.segmentPrefix)); if (!operOK) return;
		qint16 Index = alu->word2s16(instr.srcValue & 0xFFFF);

		// Check fault conditions
		if ((Index < Lo) || (Index > Hi)) {
			exception_BR(instr);
			debug_log("BOUND: Register is " + QString::number(Index, 16) +
					  ", boundaries are [" + QString::number(Lo, 16) + ".." + QString::number(Hi, 16) + "]");
			return;
		}

		// No fault - load next decoder->Instruction
		return;
	}

	// 32-bit
	qint32 Lo = alu->dword2s32(getDWordR(instr.dstAddr, instr.segmentPrefix)); if (!operOK) return;
	qint32 Hi = alu->dword2s32(getDWordR(instr.dstAddr+4, instr.segmentPrefix)); if (!operOK) return;
	qint32 Index = alu->dword2s32(instr.srcValue);

	// Check fault conditions
	if ((Index < Lo) || (Index > Hi)) {
		exception_BR(instr);
		return;
	}
}


void CPUIntelx86::do_popSReg(Instruction & instr, const word selector)
{
	debug_op1({opSReg, sz16, selector});
	word A = pop16(instr); if (!operOK) return;
	(this->*setSegment)(selector, A);
}

void CPUIntelx86::do_pushImm(Instruction & instr) {
	debug_op1({opImm, instr.szOperands, instr.srcValue});
	if (instr.executable) {
		if (instr.szOperands == sz32) {
			push32(instr, instr.srcValue);
		} else
		if (instr.szOperands == sz16) {
			push16(instr, instr.srcValue & 0x0000FFFF);
		} else
		if (instr.szOperands == sz8 ){
			push8(instr, instr.srcValue & 0x000000FF);
		}
	}
}

void CPUIntelx86::do_pushSReg(Instruction & instr, const word selector)
{
	debug_op1({opSReg, sz16, selector});
	push16(instr, (this->*getSegment)(selector));
}

/**
 * @brief Try load given memory/register operands values
 * @param instr - instruction descriptor
 */
void CPUIntelx86::loadInstructionOperands(Instruction &instr)
{
	loadInstructionOperand(instr, instr.typeDst, instr.dstValue, instr.dstAddr);
	if (operOK) {
		loadInstructionOperand(instr, instr.typeSrc, instr.srcValue, instr.srcAddr);
	}
}

/**
 * @brief CPUIntelx86::loadInstructionOperand
 * @param instr
 * @param ot
 * @param value
 * @param address
 */
void CPUIntelx86::loadInstructionOperand(Instruction &instr, OperandType ot, dword &value, dword &address)
{
	if (!instr.executable) return;
	if (ot == opReg) {
		if (instr.szOperands == sz8) {
			value = getReg8(address);
		} else {
			if (instr.szOperands == sz16) {
				value = getReg16(address);
			} else {
				value = getReg32(address);
			}
		}
	} else
	if (ot == opMem){
		if (instr.szOperands == sz8) {
			value = getByteR(address, instr.segmentPrefix);
		} else {
			if (instr.szOperands == sz16) {
				value = getWordR(address, instr.segmentPrefix);
			} else {
				value = getDWordR(address, instr.segmentPrefix);
			}
		}
	} else
	if (ot == opSReg) {
		value = (this->*getSegment)(address);
	} else
	if (ot == opCReg) {
		value = cRegisters[address];
	}
}

qword CPUIntelx86::readMemory64(Instruction & instr)
{
	qword data = ((qword)getDWordR(instr.srcAddr + 4, instr.segmentPrefix)) << 32;
	if (operOK) {
		data |= (dword)getDWordR(instr.srcAddr, instr.segmentPrefix);
	}
	if (!operOK) {
		instr.executable = false;
	}
	return data;
}

void CPUIntelx86::saveInstructionDestination(Instruction &instr, dword value) {
	if (!instr.executable) return;
	if (instr.typeDst == opReg) {
		if (instr.szOperands == sz8) {
			setReg8(instr.dstAddr, value & 0x000000FF);
		} else {
			if (instr.szOperands == sz16) {
				setReg16(instr.dstAddr, value & 0x0000FFFF);
			} else {
				setReg32(instr.dstAddr, value);
			}
		}
	} else
	if (instr.typeDst == opMem) {
		if (instr.szOperands == sz8) {
			setByteR(instr.dstAddr, (value & 0x000000FF), instr.segmentPrefix);
		} else {
			if (instr.szOperands == sz16) {
				setWordR(instr.dstAddr, (value & 0x0000FFFF), instr.segmentPrefix);
			} else {
				setDWordR(instr.dstAddr, value, instr.segmentPrefix);
			}
		}
	} else
	if (instr.typeDst == opSReg) {
		(this->*setSegment)(instr.dstAddr, (value & 0x0000FFFF));
	} else
	if (instr.typeDst == opCReg) {
		if (instr.szOperands == sz16) {
			cRegisters[instr.dstAddr] &= 0xFFFF0000;
			cRegisters[instr.dstAddr] |= (value & 0x0000FFFF);
		} else {
			cRegisters[instr.dstAddr] = value;
		}
	}
}

void CPUIntelx86::saveInstructionDestination64(Instruction &instr, qword value) {
	if (!instr.executable) return;
	if (instr.typeDst == opMem) {
		setDWordR(instr.dstAddr, value & 0xFFFFFFFF, instr.segmentPrefix);
		if (!instr.executable) return;
		setDWordR(instr.dstAddr+4, (value & 0xFFFFFFFF00000000 >> 32), instr.segmentPrefix);
	}
}

void CPUIntelx86::do_xchg_ax_reg(Instruction & instr, byte reg) {
	cyclesTotal += 2;
	debug_op2(DebugClosure{opReg, instr.szOperands,  REGISTER_EAX}, {opReg, instr.szOperands, reg});
	if (instr.szOperands == sz16) {
		word A = getReg16(REGISTER_EAX);
		setReg16(REGISTER_EAX, getReg16(reg));
		setReg16(reg, A);
	} else {
		dword C = getReg32(REGISTER_EAX);
		setReg32(REGISTER_EAX, getReg32(reg));
		setReg32(reg, C);
	}
}

void CPUIntelx86::do_cwd() {
	cyclesTotal += 3;
	if (0 == getOpSize()) {
		debug_op0();
		qint32 wd = alu->word2s32(getReg16(REGISTER_EAX));
		setReg16(REGISTER_EDX, ((wd & 0xFFFF0000) >> 16));
	}
	else {
		debug_op0();
		qint64 L = static_cast<qint64>(getReg32(REGISTER_EAX));
		setReg32(REGISTER_EAX, (L & 0xFFFFFFFF));
		setReg32(REGISTER_EDX, ((L  & 0xFFFFFFFF00000000) >> 32));
	}

}

void CPUIntelx86::do_cbw()
{
	cyclesTotal += 3;
	debug_op0();
	if (0 == getOpSize()) {
		byte src8 = getReg8(REGISTER_EAX);
		setReg16(REGISTER_EAX, alu->byte2s16(src8));
	} else {
		word src16 = getReg16(REGISTER_EAX);
		setReg32(REGISTER_EAX, alu->word2s32(src16));
	}
}

void CPUIntelx86::do_mov_reg_imm(Instruction &instr) {
	debug_rm(instr);
	saveInstructionDestination(instr, instr.srcValue);

}

void CPUIntelx86::do_out_imm8_ax(Instruction & instr) {
	byte imm = instr.immediate & 0xFF;
	debug_op2({opImm, sz8, imm}, {opReg, instr.szOperands, REGISTER_EAX});
	if (instr.szOperands == sz8) {
		processOutput(imm, getReg8(REGISTER_EAX), 1);
	} else {
		if (instr.szOperands == sz32) {
			processOutput(imm, getReg32(REGISTER_EAX), 4);
		} else {
			processOutput(imm, getReg16(REGISTER_EAX), 2);
		}
	}
}

void CPUIntelx86::do_in_ax_imm8(Instruction& instr) {
	byte imm = instr.immediate & 0xFF;
	debug_op2({opReg, instr.szOperands, REGISTER_EAX}, {opImm, sz8, imm});
	if (instr.szOperands == sz8) {
		setReg8(REGISTER_EAX, (processInput(imm, 1) & 0xFF));
	} else {
		if (instr.szOperands == sz16) {
			setReg16(REGISTER_EAX, (processInput(imm, 2) & 0xFFFF));
		} else {
			setReg32(REGISTER_EAX, processInput(imm, 4));
		}
	}
}

void CPUIntelx86::do_out_dx_ax(Instruction& instr) {
	debug_op2({opReg, sz16, REGISTER_EDX}, {opReg, instr.szOperands, REGISTER_EAX});
	if (instr.szOperands == sz8) {
		processOutput(getReg16(REGISTER_EDX), getReg8(REGISTER_EAX), 1);
	} else {
		if (instr.szOperands == sz32) {
			processOutput(getReg16(REGISTER_EDX), getReg32(REGISTER_EAX), 4);
		} else {
			processOutput(getReg16(REGISTER_EDX), getReg16(REGISTER_EAX), 2);
		}
	}
}

void CPUIntelx86::do_in_ax_dx(Instruction & instr) {
	debug_op2({opReg, instr.szOperands, REGISTER_EAX}, {opReg, sz16, REGISTER_EDX});
	if (instr.szOperands == sz8) {
		setReg8(REGISTER_EAX, processInput(getReg16(REGISTER_EDX), 1) & 0xFF);
	} else {
		if (instr.szOperands == sz32) {
			setReg32(REGISTER_EAX, processInput(getReg16(REGISTER_EDX), 4));
		} else {
			setReg16(REGISTER_EAX, processInput(getReg16(REGISTER_EDX), 2) & 0xFFFF);
		}
	}
}

/**
 * @brief INC or DEC r/m 8/16/32
 */
void CPUIntelx86::inc_dec_rm(Instruction &instr, byte opIndex) {
	cyclesTotal += 1;

	dword cf = (eflags.eflags.flag32 & FLAG_CF);	// Preserve CF state

	if (0 == opIndex) {
		instr.extensionIndex = 0x40;
	} else {
		instr.extensionIndex = 0x48;
	}
	debug_op1({instr.typeDst, instr.szOperands, instr.dstAddr});

	loadInstructionOperand(instr, instr.typeDst, instr.dstValue, instr.dstAddr);
	if (!operOK) return;

	dword result = 0;	// Clear all bits to prevent byte and word overflows

	if (0 == opIndex) {
		if (instr.szOperands == sz8) {
			result = alu->add<byte, word>((instr.dstValue & 0x000000FF), 1);
		} else {
			if (instr.szOperands == sz16) {
				result = alu->add<word, dword>((instr.dstValue & 0x0000FFFF), 1);
			} else {
				result = alu->add<dword, qword>(instr.dstValue, 1);
			}
		}
	} else {
		if (instr.szOperands == sz8)
			result = alu->sub<byte, word>((instr.dstValue & 0x000000FF), 1);
		else
		if (instr.szOperands == sz16)
			result = alu->sub<word, dword>((instr.dstValue & 0x0000FFFF), 1);
		else
			result = alu->sub<dword, qword>(instr.dstValue, 1);
	}

	instr.typeSrc = instr.typeDst;
	instr.srcAddr = instr.dstAddr;
	saveInstructionDestination(instr, result);


	// Reset CF state
	eflags.eflags.flag32 &= FLAG_CF_CLEAR;
	eflags.eflags.flag32 |= cf;
}

void CPUIntelx86::calln_abs_rm(Instruction &instr) {
	cyclesTotal += 31;

	instr.extensionIndex = 0xE8;
	debug_op1({instr.typeDst, instr.szOperands, instr.dstAddr});
	loadInstructionOperand(instr, instr.typeDst, instr.dstValue, instr.dstAddr);
	if (!operOK) return;

	if (!isInLimits(REGISTER_CS, decoder->EIP + instr.dstValue, 1)) {
		fault_GP(instr, 0);
		return;
	}

	if (instr.szOperands == sz32) {
		push32(instr, decoder->EIP); if (!operOK) return;
	} else {
		push16(instr, decoder->EIP & 0xFFFF); if (!operOK) return;
	}
	if (instr.szOperands == sz16) {
		decoder->setEIP(instr.dstValue & 0xFFFF);
	} else {
		decoder->setEIP(instr.dstValue);
	}
}

void CPUIntelx86::callf_abs_rm(Instruction &instr) {
	cyclesTotal += 33;

	dword addr_base = 0;//(this->*getSegmentAddress)(instr.segmentPrefix);
	dword addr;
	dword tempEIP;
	word tempCS;

	instr.extensionIndex = 0xE8;
	debug_op1({instr.typeDst, instr.szOperands, instr.dstAddr});
	loadInstructionOperand(instr, instr.typeDst, instr.dstValue, instr.dstAddr);
	if (!operOK) return;

	addr = instr.dstAddr;//instr.dstValue;

	if (instr.szOperands == sz32) {
		tempEIP = getDWordR(addr_base + addr, instr.segmentPrefix); if (!operOK) return;
		tempCS = getWordR(addr_base + addr + 4, instr.segmentPrefix); if (!operOK) return;
	} else {
		tempEIP = getWordR(addr_base + addr, instr.segmentPrefix); if (!operOK) return;
		tempCS = getWordR(addr_base + addr + 2, instr.segmentPrefix); if (!operOK) return;
	}
	callf(tempCS, tempEIP);
}

void CPUIntelx86::jmpn_abs_rm(Instruction &instr) {
	cyclesTotal += 11;

	instr.extensionIndex = 0xE9;
	debug_op1({instr.typeDst, instr.szOperands, instr.dstAddr});
	loadInstructionOperand(instr, instr.typeDst, instr.dstValue, instr.dstAddr);
	if (!operOK) return;

	decoder->setEIP(instr.dstValue);
}

void CPUIntelx86::jmpf_abs_rm(Instruction &instr) {
	cyclesTotal += 16;

	dword addr_base = 0;//(this->*getSegmentAddress)(instr.segmentPrefix);
	dword addr;
	dword tempEIP;
	word tempCS;

	instr.extensionIndex = 0xE9;
//	loadInstructionOperand(instr, instr.typeDst, instr.dstValue, instr.dstAddr);
	debug_op1({instr.typeDst, instr.szOperands, instr.dstAddr});
	if (!operOK) return;

	addr = instr.dstAddr;

	if (sz32 == instr.szOperands) {
		tempEIP = getDWordR(addr_base + addr, instr.segmentPrefix); if (!operOK) return;
		tempCS = getWordR(addr_base + addr + 4, instr.segmentPrefix); if (!operOK) return;
	} else {
		tempEIP = getWordR(addr_base + addr, instr.segmentPrefix); if (!operOK) return;
		tempCS = getWordR(addr_base + addr + 2, instr.segmentPrefix); if (!operOK) return;
	}
	jmpf(tempCS, tempEIP);
}

void CPUIntelx86::push_rm(Instruction &instr) {
	cyclesTotal += 5;

	instr.extensionIndex = 0x50;
	debug_op1({instr.typeDst, instr.szOperands, instr.dstAddr});
	loadInstructionOperand(instr, instr.typeDst, instr.dstValue, instr.dstAddr);
	if (!operOK) return;

	if (instr.szOperands == sz32) {
		push32(instr, instr.dstValue);
	} else {
		push16(instr, instr.dstValue & 0xFFFF);
	}
}


bool CPUIntelx86::flagTestCZ() {
	return (eflags.eflags.flag32 & FLAG_ZF);
}

bool CPUIntelx86::flagTestCP() {
	return (eflags.eflags.flag32 & FLAG_PF);
}

bool CPUIntelx86::flagTestCC() {
	return (eflags.eflags.flag32 & FLAG_CF);
}

bool CPUIntelx86::flagTestCO() {
	return (eflags.eflags.flag32 & FLAG_OF);
}

bool CPUIntelx86::flagTestCS() {
	return (eflags.eflags.flag32 & FLAG_SF);
}

bool CPUIntelx86::flagTestCA() {
	return !flagTestCBE();
}

bool CPUIntelx86::flagTestCAE() {
	return !flagTestCC();
}

bool CPUIntelx86::flagTestCB() {
	return flagTestCC();
}

bool CPUIntelx86::flagTestCBE() {
	return (flagTestCZ() || flagTestCC());
}

bool CPUIntelx86::flagTestCG() {
	return (!flagTestCL()) && (!flagTestCZ());
}

bool CPUIntelx86::flagTestCGE() {
	return (!flagTestCL());
}

bool CPUIntelx86::flagTestCL() {
	bool temp1 = (eflags.eflags.flag32 & FLAG_OF) ? true : false;
	bool temp2 = (eflags.eflags.flag32 & FLAG_SF) ? true : false;
	return (temp1 != temp2);
}

bool CPUIntelx86::flagTestCLE() {
	return (flagTestCZ() || flagTestCL());
}




/**
 * @brief Perform different actions based on extension code
 * @short For FE/FF instruction
 */
void CPUIntelx86::do_arithmetic_ext(Instruction &instr, byte opExtension)
{
	instr.extensionIndex = 0xA0;	// "MOV" lookup
	debug_op0("EXT " + QString::number(opExtension, 16));
	if ((instr.szOperands == sz8) && (opExtension > 1)) {
		debug_log("FF /n>1 with 8-bit undefined");
		fault_UD(instr);
		return;
	}
	switch (opExtension) {
		case 0:
		case 1:
			inc_dec_rm(instr, opExtension);
		break;

		// mod.010.rm
		// CALLN rm16/32
		case 2:
			calln_abs_rm(instr);
		break;

		// mod.011.rm
		// CALLF [rm16/32]
		case 3:
			callf_abs_rm(instr);
		break;

		// mod.100.rm
		// JMP rm16/32
		case 4:
			jmpn_abs_rm(instr);
		break;

		// mod.101.rm
		// JMP mem[rm16/32]
		case 5:
			jmpf_abs_rm(instr);
		break;

		// mod.110.rm
		// PUSH rm16/32
		case 6:
			push_rm(instr);
		break;

		default:
			fault_UD(instr);
	}

}

void CPUIntelx86::do_shift_rm(Instruction &instr, byte shiftCount) {
	cyclesTotal += 7;
	byte shc = shiftCount;
	shc &= 0x1F;

	instr.typeSrc = opImm;
	instr.srcValue = shiftCount;
	instr.extensionIndex = 256 + instr.reg;
	debug_rm(instr);

	loadInstructionOperand(instr, instr.typeDst, instr.dstValue, instr.dstAddr);
	if (!operOK)
		return;

	dword result;

	if (instr.szOperands == sz8) {
		byte op8 = (instr.dstValue & 0x000000FF);
		result = alu->shift<byte>(op8, shc, (opShiftType)instr.reg);
	} else {
		if (instr.szOperands == sz16) {
			word op16 = (instr.dstValue & 0x0000FFFF);
			result = alu->shift<word>(op16, shc, (opShiftType)instr.reg);
		} else {
			dword op32 = instr.dstValue;
			result = alu->shift<dword>(op32, shc, (opShiftType)instr.reg);
		}
	}
	saveInstructionDestination(instr, result);
}

void CPUIntelx86::do_shrd(Instruction & instr, byte shiftCount) {
	shiftCount &= 0x1F;
	loadInstructionOperands(instr);
	debug_rm(instr, "SHRD");
	if (!operOK || (shiftCount == 0)) {
		return;
	}
	dword lastBit = 0;
	if (instr.szOperands == sz16) {
		if (shiftCount > 16) {
			return;
		}
		byte count = shiftCount;
		word src = instr.dstValue & 0xFFFF;
		word bitSource = instr.srcValue & 0xFFFF;
		while (count--) {
			lastBit = src & 1;
			src = (src >> 1);
			src &= 0b0111111111111111;
			src |= ((bitSource & 1) << 15);
			bitSource = (bitSource >> 1);
		}
		alu->setFlagsSZP<word>(src);
		if (src ^ (instr.dstValue & 0x8000)) {
			eflags.eflags.flag32 |= FLAG_OF;
		} else {
			eflags.eflags.flag32 &= FLAG_OF_CLEAR;
		}
		instr.dstValue = src;
	} else {
		if (shiftCount > 16) {
			return;
		}
		byte count = shiftCount;
		dword src = instr.dstValue;
		dword bitSource = instr.srcValue;
		while (count--) {
			lastBit = src & 1;
			src = (src >> 1);
			src &= 0b01111111111111111111111111111111;
			src |= ((bitSource & 1) << 31);
			bitSource = (bitSource >> 1);
		}
		alu->setFlagsSZP<dword>(src);
		if (src ^ (instr.dstValue & 0x80000000)) {
			eflags.eflags.flag32 |= FLAG_OF;
		} else {
			eflags.eflags.flag32 &= FLAG_OF_CLEAR;
		}
		instr.dstValue = src;
	}
	if (lastBit) {
		eflags.eflags.flag32 |= FLAG_CF;
	} else {
		eflags.eflags.flag32 &= FLAG_CF_CLEAR;
	}
	saveInstructionDestination(instr, instr.dstValue);
}

void CPUIntelx86::do_shld(Instruction & instr, byte shiftCount) {
	shiftCount &= 0x1F;
	loadInstructionOperands(instr);
	debug_rm(instr, "SHLD");
	if (!operOK || (shiftCount == 0)) {
		return;
	}
	dword lastBit = 0;
	if (instr.szOperands == sz16) {
		if (shiftCount > 16) {
			return;
		}
		byte count = shiftCount;
		word src = (instr.dstValue & 0xFFFF);
		word bitSource = instr.srcValue & 0xFFFF;
		while (count--) {
			lastBit = (src & 0x8000);
			src = (src << 1);
			src &= 0b1111111111111110;
			src |= ((bitSource & (0x8000) >> 15));
			bitSource = (bitSource << 1);
		}
		alu->setFlagsSZP<word>(src);
		if (src ^ (instr.dstValue & 0x8000)) {
			eflags.eflags.flag32 |= FLAG_OF;
		} else {
			eflags.eflags.flag32 &= FLAG_OF_CLEAR;
		}
		instr.dstValue = src;
	} else {
		if (shiftCount > 16) {
			return;
		}
		byte count = shiftCount;
		dword src = instr.dstValue;
		dword bitSource = instr.srcValue;
		while (count--) {
			lastBit = (src & 0x80000000);
			src = (src << 1);
			src &= 0b11111111111111111111111111111110;
			src |= ((bitSource & 0x80000000) >> 31);
			bitSource = (bitSource << 1);
		}
		alu->setFlagsSZP<dword>(src);
		if (src ^ (instr.dstValue & 0x80000000)) {
			eflags.eflags.flag32 |= FLAG_OF;
		} else {
			eflags.eflags.flag32 &= FLAG_OF_CLEAR;
		}
		instr.dstValue = src;
	}
	if (lastBit) {
		eflags.eflags.flag32 |= FLAG_CF;
	} else {
		eflags.eflags.flag32 &= FLAG_CF_CLEAR;
	}
	saveInstructionDestination(instr, instr.dstValue);
}

void CPUIntelx86::do_unary_ext(Instruction &instr) {
	cyclesTotal += 19;

	int opIndex = instr.reg;

	instr.extensionIndex = 256 + 8 + opIndex;

	loadInstructionOperand(instr, instr.typeDst, instr.dstValue, instr.dstAddr);
	if (!operOK)
		return;
	dword src = instr.srcValue, dst = instr.dstValue;
	dword tempReg = 0;

	switch (opIndex) {
		case 0x00:
		case 0x01: /* F7 /1 not valid in manual but valid in practice */
			debug_op1({instr.typeDst, instr.szOperands, instr.dstAddr});
			if (instr.szOperands == sz8) {
				alu->And<byte>((byte)(src & 0xFF), (byte)(dst & 0xFF));
			} else {
				if (instr.szOperands == sz16) {
					alu->And<word>((word)(src & 0xFFFF), (word)(dst & 0xFFFF));
				} else {
					alu->And<dword>(src, dst);
				}
			}
		break;

		// mod.010.rm
		// NOT rm8/16/32
		case 0x02:
			debug_op1({instr.typeDst, instr.szOperands, instr.dstAddr});
			dst = (~dst);
			saveInstructionDestination(instr, dst);
		break;

		// mod.011.rm
		// NEG rm8/16/32
		case 0x03:
			debug_op1({instr.typeDst, instr.szOperands, instr.dstAddr});
			tempReg = dst;
			if (sz8 == instr.szOperands) {
				dst = alu->sub<byte, word>(0, (dst & 0x000000FF));
			} else {
				if (sz16 == instr.szOperands) {
					dst = alu->sub<word, dword>(0, (dst & 0x0000FFFF));
				} else {
					dst = alu->sub<dword, qword>(0, dst);
				}
			}
			if (tempReg) {
				eflags.eflags.flag32 |= FLAG_CF;
			} else {
				eflags.eflags.flag32 &= FLAG_CF_CLEAR;
			}
			saveInstructionDestination(instr, dst);
		break;

		case 0x04:	// MUL
		case 0x05:	// IMUL
		case 0x06:	// DIV
		case 0x07:	// IDIV
			debug_op2({opReg, instr.szOperands, REGISTER_EAX}, {instr.typeDst, instr.szOperands, instr.dstAddr});
			if (sz8 == instr.szOperands) {
				cyclesTotal += 8;
				switch (opIndex) {
					case 4: alu->mul_1<byte, word>(dst); break;
					case 5: alu->imul_1<byte, word>(dst); break;
					case 6: alu->div_8(instr, dst); break;
					case 7: alu->idiv_8(instr, dst); break;
				}
			} else {
				if (sz16 == instr.szOperands) {
					cyclesTotal += 12;
					switch (opIndex) {
						case 4: alu->mul_1<word, dword>(dst); break;
						case 5: alu->imul_1<word, dword>(dst); break;
						case 6: alu->div_16(instr, dst); break;
						case 7: alu->idiv_16(instr, dst); break;
					}
				}
				else {
					cyclesTotal += 14;
					switch (opIndex) {
						case 4: alu->mul_1<dword, qword>(dst); break;
						case 5: alu->imul_1<dword, qword>(dst); break;
						case 6: alu->div_32(instr, dst); break;
						case 7: alu->idiv_32(instr, dst); break;
					}
				}
			}
		break;
	} // switch end

}


/**
 * @brief Binary grp
 * @short For 0x80 .. 0x83 instructions
 */
void CPUIntelx86::do_binary_ext_imm(Instruction &instr) {
	cyclesTotal += 16;
	int opIndex = instr.reg;
	instr.extensionIndex = opIndex;

	loadInstructionOperand(instr, instr.typeDst, instr.dstValue, instr.dstAddr);

	debug_rm(instr, OpcodeNames16[opIndex*8]);

	if (!operOK) return;

	dword result = 0;	// Clear all bits to prevent byte and word overflows

	OpExtensionArithmetic operation;
	switch (opIndex) {
		case 0: operation = OpExtensionArithmetic::Add; break;
		case 1: operation = OpExtensionArithmetic::Or; break;
		case 2: operation = OpExtensionArithmetic::Adc; break;
		case 3: operation = OpExtensionArithmetic::Sbb; break;
		case 4: operation = OpExtensionArithmetic::And; break;
		case 5: operation = OpExtensionArithmetic::Sub; break;
		case 6: operation = OpExtensionArithmetic::Xor; break;
		case 7: operation = OpExtensionArithmetic::Cmp; break;
	}

	// Byte operation
	if (instr.szOperands == sz8) {
		result = do_arithmetic<byte, word>(instr, operation);
	} else {
		if (instr.szOperands == sz16) {
			result = do_arithmetic<word, dword>(instr, operation);
		} else {
			result = do_arithmetic<dword, qword>(instr, operation);
		}
	}

	// skip CMP save
	if (operation != OpExtensionArithmetic::Cmp) {
		saveInstructionDestination(instr, result);
	}
}

void CPUIntelx86::do_lea(Instruction &instr) {
	cyclesTotal += 5;

	dword addr = 0;
	dword addr_base = 0;

	//loadInstructionOperands(instr);
	if (!operOK) return;

	if (instr.segmentOverride)
		addr_base = (this->*getSegmentAddress)(instr.segmentPrefix);
	else {
		if ((instr.rm == 2) || (instr.rm == 3) || ((instr.rm == 6) && (instr.mod < 3)))
			addr_base = (this->*getSegmentAddress)(REGISTER_SS);
		else
			addr_base =	(this->*getSegmentAddress)(REGISTER_DS);
	}
	if (!operOK) return;

	debug_rm(instr);
	if (instr.typeSrc == opMem) {
		addr = instr.srcAddr - addr_base;
	}
	else {
		addr = instr.srcValue - addr_base;
	}
	saveInstructionDestination(instr, addr);
}

/**
 * @brief LSS LDS LES LGS LFS
 */
void CPUIntelx86::loadSegmentFarPointer(word sreg, Instruction &instr) {
	dword regValue32;
	word regValue16, sregValue;
	debug_op2({opReg, instr.szOperands, instr.srcAddr}, {opMem, instr.szOperands, instr.dstAddr});
	if (sz16 == instr.szOperands) {
		regValue16 = getWordR(instr.dstAddr, instr.segmentPrefix); if (!operOK) return;
		sregValue = getWordR(instr.dstAddr + 2, instr.segmentPrefix); if (!operOK) return;
		(this->*setSegment)(sreg, sregValue);
		if (operOK) {
			setReg16(instr.srcAddr, regValue16);
		}
	} else {
		regValue32 = getDWordR(instr.dstAddr, instr.segmentPrefix); if (!operOK) return;
		sregValue = getWordR(instr.dstAddr + 4, instr.segmentPrefix); if (!operOK) return;
		(this->*setSegment)(sreg, sregValue);
		if (operOK) {
			setReg32(instr.srcAddr, regValue32);
		}
	}

}


void CPUIntelx86::do_load_eff_seg(Instruction &instr, word selector) {
	cyclesTotal += 4;
	loadInstructionOperands(instr);
	loadSegmentFarPointer(selector, instr);
}
