#include <QApplication>
#include "cpuintelx86.h"
#include "loggerintelx86.h"

#include <QDebug>

bool CPUIntelx86::isInLimits(const word selector, const dword address, int size) const {
	return ((address >= SRegRights[selector].minAddress) && (address+size-1 <= SRegRights[selector].maxAddress));
}

/**
 * Check whether current instruction can be executed
 */
bool CPUIntelx86::canExecute(const dword address, const int instructionLength) {
	if (!SRegRights[REGISTER_CS].execute) {
		fault_GP(decoder->currentInstruction, 0);
		debug_log("CanExecute: this segment cannot be executed.");
		return false;
	}
	return (isInLimits(REGISTER_CS, address, instructionLength));
}


/**
 * @brief Load all descriptor fields into structure
 * @param selector - segment selector
 * @return descriptor info
 */

DescriptorInfo CPUIntelx86::loadDescriptorInfo(const dword &selector) {
	//debug_log("Loading segment descriptor.");
	DescriptorInfo desc;
	desc.selector = ((selector & 0xFFF8) >> 3);
	dword descAddress = (selector & 0xFFF8);

	if (0 == (selector & 0x4)) {
		// Load from GDT
		//debug_log("GDT load.");
		desc.isInLimits = ((selector & 0xFFF8) < GDTR.Limit);
		descAddress += GDTR.Base;
		desc.isGdt = true;
	}
	else {
		// Load from LDT
		//debug_log("LDT load.");
		desc.isInLimits = ((selector & 0xFFF8) < LDTR.limit);
		descAddress += LDTR.base;
		desc.isGdt = false;
	}

	desc.descriptorAddress = descAddress;

//	qDebug() << "PR: before addr=" << hex << descAddress << ":" << dword_at(descAddress);

	desc.dw0 = dword_at(descAddress);
	desc.dw4 = dword_at(descAddress + 4);

	byte flagsr = byte_at(descAddress + 5);
	byte flagsl = (byte_at(descAddress + 6) & 0xF0);

	desc.base = (dword_at(descAddress + 2) & 0x00FFFFFF);
	desc.base |= (byte_at(descAddress + 7) << 24);

	desc.type = (flagsr & 0x1F);

	desc.limit = (word_at(descAddress));
	desc.limit |= ((byte_at(descAddress + 6) & 0xF) << 16);

	desc.dpl = ((flagsr & 0x60) >> 5);
	desc.present = ((flagsr & 0x80) >> 7);

	desc.avl = (flagsl & 0x10);
	desc.granularity = ((flagsl & 0x80) >> 7);

	desc.system = !((flagsr & 0x10) >> 4);
	desc.defaultSize = ((flagsl & 0x40) >> 6);
	desc.expandingDirection = 0;
	if (!desc.system) {
		desc.code = ((flagsr & 0x08) >> 3);
		desc.data = 1 - desc.code;
		desc.accessed = (flagsr & 0x1);
		if (desc.data) {
			desc.writable = ((flagsr & 0x2) >> 1);
			desc.expandingDirection = ((flagsr & 0x4) >> 2);
			desc.executable = 0;
			desc.conforming = 0;
			desc.readable = 1;
		}
		else {
			desc.conforming = ((flagsr & 0x4) >> 2);
			desc.readable = ((flagsr & 0x2) >> 1);
			desc.executable = 1;
			desc.writable = 0;
		}

		// Normal growing
		if (0 == desc.expandingDirection)
		{
			desc.minAddress = desc.base;
			desc.maxAddress = desc.base;
			if (desc.granularity) {
				if (desc.limit != 0xFFFFF)
					desc.maxAddress += (((desc.limit + 1) << 12) - 1);
				else
					desc.maxAddress = 0xFFFFFFFF;
			}
			else {
				qword maxAddress = (qword)desc.minAddress + (qword)desc.limit;
				if (maxAddress > 0xFFFFFFFF) {
					maxAddress = 0xFFFFFFFF;
				}
				desc.maxAddress = maxAddress;
			}
		}
		// Growing down
		else
		{
			desc.minAddress = desc.base;
			if (0 == desc.defaultSize)
				desc.maxAddress = 0xFFFF;
			else
				desc.maxAddress = 0xFFFFFFFF;
			if (desc.granularity) {
				desc.minAddress += (desc.limit + 1);
			}
			else {
				desc.minAddress += ((desc.limit + 1) << 12) - 1;
			}
		}

	}
	else {
		desc.code = 0;
		desc.data = 0;
	}

//	qDebug() << "PR: after addr=" << hex << descAddress << ":" << dword_at(descAddress);

	return desc;
}


void CPUIntelx86::loadLdtr(Instruction & instr) {
	cyclesTotal += 2;
	debug_rm(instr, "LLDT");
	if (cpl != 0) {
		fault_GP(instr, 0);
		return;
	}
	loadInstructionOperands(instr);	// Load operands
	if (!operOK) return;

	word src = instr.dstValue;

	auto desc = loadDescriptorInfo(src);
	if (!operOK) return;
	if (desc.type != 0x02) {
		fault_GP(instr, src);
		return;
	}
	if (!desc.isInLimits) {
		fault_GP(instr, src);
		return;
	}
	if (!desc.present) {
		fault_NP(instr, src);
		return;
	}

	LDTR.info = desc;
	LDTR.base = desc.base;
	LDTR.limit = desc.limit;
	LDTR.selector = instr.dstValue;
}

void CPUIntelx86::loadTaskRegister(Instruction & instr) {
	cyclesTotal += 2;
	debug_rm(instr, "LTR");
	if (cpl != 0) {
		fault_GP(instr, 0);
		return;
	}
	loadInstructionOperands(instr);
	if (!operOK) return;

	word src = instr.dstValue;

	auto desc = loadDescriptorInfo(src);
	if (!operOK) return;
	if (desc.type != 0x01 && desc.type != 0x09) {
		fault_GP(instr, src);
		return;
	}
	if (!desc.isInLimits) {
		fault_GP(instr, src);
		return;
	}
	if (!desc.present) {
		fault_NP(instr, src);
		return;
	}
	if (!desc.isGdt) {
		fault_GP(instr, 0);
		return;
	}

	taskRegister.info = desc;
	taskRegister.base = desc.base;
	taskRegister.limit = desc.limit;
	taskRegister.selector = instr.dstValue;

	// Modify TASK to BUSY
	dword dw4 = desc.dw4;
	dw4 |= 0x0020;
	set_word_at(desc.descriptorAddress + 4, dw4);
}

/**
 * @brief Load all descriptor fields into structure
 * @param selector - segment selector
 * @return descriptor info
 */
DescriptorInfo CPUIntelx86::loadInterruptDescriptorInfo(const word &selector) {
	DescriptorInfo desc;
	desc.selector = (selector);
	dword descAddress = selector * 8;

	desc.isInLimits = (selector  < IDTR.Limit);
	descAddress += IDTR.Base;

	desc.dw0 = dword_at(descAddress);
	desc.dw4 = dword_at(descAddress + 4);

	byte flagsr = byte_at(descAddress + 5);
	byte flagsl = (byte_at(descAddress + 6) & 0xF0);

	desc.base = (dword_at(descAddress + 2) & 0x00FFFFFF);
	desc.base |= (byte_at(descAddress + 7) << 24);

	desc.type = (flagsr & 0x1F);

	desc.limit = (word_at(descAddress));
	desc.limit |= ((byte_at(descAddress + 6) & 0xF) << 16);

	desc.dpl = ((flagsr & 0x60) >> 5);
	desc.present = ((flagsr & 0x80) >> 7);

	desc.avl = (flagsl & 0x10);
	desc.granularity = ((flagsl & 0x80) >> 7);

	desc.system = !((flagsr & 0x10) >> 4);
	desc.expandingDirection = 0;
	if (desc.system)
	{
		desc.data = ((flagsr & 0x08) >> 3);
		desc.code = 1 - desc.data;
		desc.accessed = (flagsr & 0x1);
		if (desc.data) {
			desc.writable = ((flagsr & 0x2) >> 1);
			desc.expandingDirection = ((flagsr & 0x4) >> 2);
			desc.executable = 0;
			desc.conforming = 0;
		}
		else {
			desc.conforming = ((flagsr & 0x4) >> 2);
			desc.readable = ((flagsr & 0x2) >> 1);
			desc.executable = 1;
			desc.writable = 0;
		}

		// Normal growing
		if (0 == desc.expandingDirection)
		{
			desc.minAddress = desc.base;
			desc.maxAddress = desc.base;
			if (desc.granularity) {
				desc.maxAddress += ((desc.limit + 1) << 12) - 1;
			}
			else {
				desc.maxAddress += desc.limit;
			}
		}
		// Growing down
		else
		{
			desc.minAddress = desc.base;
			if (0 == desc.defaultSize)
				desc.maxAddress = 0xFFFF;
			else
				desc.maxAddress = 0xFFFFFFFF;
			if (desc.granularity) {
				desc.minAddress += (desc.limit + 1);
			}
			else {
				desc.minAddress += ((desc.limit + 1) << 12) - 1;
			}
		}

	}
	else {
		desc.code = 0;
		desc.data = 0;
	}
	return desc;
}



dword CPUIntelx86::getSegmentAddressReal(const dword Selector) {
	operOK = 1;
	return SRegAddress[Selector];
}

dword CPUIntelx86::getSegmentReal(const dword Selector) {
	return SRegisters[Selector];
}

void CPUIntelx86::setSegmentReal(const dword Selector, const dword Value) {
	operOK = 1;
	SRegisters[Selector] = Value;
	SRegAddress[Selector] = (Value << 4);
	SRegRights[Selector].isRealModed = true;
	SRegRights[Selector].maxAddress = 0x1F0000;
	SRegRights[Selector].minAddress = 0;
	SRegRights[Selector].read = 1;
	SRegRights[Selector].write = 1;
	SRegRights[Selector].execute = 1;
	SRegRights[Selector].conforming = 0;
	SRegRights[Selector].dpl = 0;
}

void CPUIntelx86::setSegmentProtected(const dword Register, const dword Value) {
	operOK = 1;

	//segmentStatus Current = loadSegmentDescriptor(Value);
	DescriptorInfo desc = loadDescriptorInfo(Value);
	byte rpl = (Value & 0x3);

	// Protected mode checks
	switch (Register) {

		// SS
		case REGISTER_SS:
			if (desc.selector == 0) {
				debug_log(" # Load Segment # : SS value = 0");
				operOK = 0;
				fault_GP(decoder->currentInstruction, 0);
				return;
			}
			if (!desc.isInLimits || (rpl != cpl) ||
				(!desc.data) || (!desc.writable)) {
				debug_log(" # Load Segment # : SS check fail");
				fault_GP(decoder->currentInstruction, Value);
				operOK = 0;
				return;
			}
			if (!desc.present) {
				debug_log(" # Load Segmen # : Not present");
				fault_SS(decoder->currentInstruction, Value);
				operOK = 0;
				return;
			}
		break;

		// DS, ES, FS, GS
		case REGISTER_DS:
		case REGISTER_ES:
		case REGISTER_FS:
		case REGISTER_GS:
			if (desc.selector != 0) {
				if (
					!desc.isInLimits ||
					((!desc.data) && (!desc.code && !desc.readable))
					|| (!desc.data && ((desc.code && !desc.conforming)
					  && ((rpl > desc.dpl) && (cpl > desc.dpl))))
					)
				{

					debug_log(" # Load Segment # : protection GP");
					fault_GP(decoder->currentInstruction, Value);
					operOK = false;
					return;
				}
				if (!desc.present) {
					debug_log(" # Load Segment # : Not present");
					fault_NP(decoder->currentInstruction, Value);
					operOK = false;
					return;
				}
			}

		break;
	}

//	qDebug() << "Load segment " << SRegStrings[Register] << ", selector " << Value << ", addr [" << hex << desc.minAddress
//			 << "," << hex << desc.maxAddress << "], granularity " << desc.granularity;

	SRegRights[Register].opSize = desc.defaultSize;
	SRegisters[Register] = Value;
	SRegAddress[Register] = desc.minAddress;
	SRegRights[Register].minAddress = desc.minAddress;
	SRegRights[Register].maxAddress = desc.maxAddress;

	SRegRights[Register].system = desc.system;
	SRegRights[Register].read = desc.readable;
	SRegRights[Register].write = desc.writable;
	SRegRights[Register].execute = desc.executable;
	SRegRights[Register].conforming = desc.conforming;
	SRegRights[Register].isRealModed = false;
}

CPUIntelx86::PageDirEntryInfo CPUIntelx86::loadPageDirEntry(dword address) {
	dword data = dword_at(address);
	PageDirEntryInfo result;
	result.present = (data & BIT_PAGE_PRESENT);
	if (!result.present) {
		result.ok = false;
		result.present = false;
	} else {
		result.is4M = data & BIT_PAGE_SIZE;
		result.address = (data & (BIT_PAGE_PDE_ADDRESS | BIT_PAGE_PTE_ADDRESS));
		result.addressPde = ((data & (BIT_PAGE_PDE_ADDRESS)) >> BIT_PAGE_PDE_SHIFT) << 4;
		if (!result.is4M) {
			result.addressPte = ((data & BIT_PAGE_PTE_ADDRESS) >> BIT_PAGE_PTE_SHIFT) << 4;
		}
		result.rw = data & BIT_PAGE_WRITEABLE;
		result.user = data & BIT_PAGE_USER;
		result.accessed = data & BIT_PAGE_ACCESSED_SET;
		result.dirty = data & BIT_PAGE_DIRTY_SET;
		result.ok = true;
	}
	return result;
}

dword CPUIntelx86::getPDEAddress(dword address) {
	return ((address & BIT_PAGE_PDE_ADDRESS) >> BIT_PAGE_PDE_SHIFT);
}

dword CPUIntelx86::getPTEAddress(dword address) {
	return ((address & BIT_PAGE_PTE_ADDRESS) >> BIT_PAGE_PTE_SHIFT);
}


/**
 * #GP (General Protection fault)
 * Base protection fault interrupt is called
 * before access to unallowed data
 */
void CPUIntelx86::fault_GP(Instruction & instr, const dword Code) {
	//dbg_breakpoint();
	if (flagGP) {
		stop();
		debug_log("Double fault occured.");

		//this->slotSuspend();

		QApplication::processEvents();
		return;
	}
	flagGP = 1;
	debug_log(" --- #GP with code " + QString::number(Code));
	qDebug() << "#GP code " << Code;
	decoder->rollback();;
	interruptInternal(0x0D, 0);
	push16(instr, Code & 0xFFFF);
	flagGP = 0;
	operOK = 0;
}

/**
 * #SS (Stack Segment fault)
 * Generated on every stack-based operation error,
 * including loading null-pointer or non-existing segment to SS
 */
void CPUIntelx86::fault_SS(Instruction & instr, const dword Code) {
	debug_log(" -F- #SS with code " + QString::number(Code));
	if (flagSS) {
		flagSS = 0;
		debug_log(" --D #DF! Double fault reached. Processor is stopping now.");
		reset();
	}
	flagSS = 1;
	decoder->rollback();
	interruptInternal(0x0C, 0);
	push16(instr, Code & 0xFFFF);
	operOK = 0;
}

void CPUIntelx86::fault_PF(Instruction & instr, const dword code) {
	debug_log(" -F- #PF with code " + QString::number(code));
	if (flagPF) {
		flagPF = 0;
		debug_log(" --D #DF! Double fault reached. Processor is stopping now.");
		reset();
	}
	flagPF = 1;
	decoder->rollback();
	interruptInternal(0x0E, 0);
	push16(instr, code & 0xFFFF);
	operOK = 0;
}

/**
 * #NP (Not Present segment)
 * Generated on any instruction attempting to access
 * non-existing segment (bit P = 0)
 */
void CPUIntelx86::fault_NP(Instruction & instr, const dword Code) {
	debug_log(" -F- #NP with code " + QString::number(Code));
	decoder->rollback();
	interruptInternal(0x0B, 0);
	push16(instr, Code & 0xFFFF);
	operOK = 0;
}

/**
 * #UD (Unknown opcode Definition fault)
 * An unknown instruction is here or there were some wrong
 * params for the known one (LES ...REG, REG)
*/
void CPUIntelx86::fault_UD(Instruction & instr) {
	stop();
	setDebugEnabled(true);
	qDebug() << "#UD " << hex << instr.opcode;
	debug_op0("#UD");
	decoder->rollback();
	interruptInternal(0x06, 0);
	operOK = 0;
}

/**
 * #BR (out of Boundary Range)
 * Generated on a BOUND instruction that fails array boundary check
 */
void CPUIntelx86::exception_BR(Instruction & instr) {
	debug_log("#BR");
	decoder->rollback();
	interruptInternal(0x05, 0);
	operOK = 0;
}

/**
 * #DE (Division Error)
 * Generated on a division by zero or when dest value > sizeof(Target_Register)
 */
void CPUIntelx86::fault_DE(Instruction & instr) {
	debug_log("#DE");
	decoder->rollback();
	interruptInternal(0, 0);
	flagGP = 0;
	operOK = 0;
}

/**
 *	LAR - Load access rights
 *	Loads access field of segment selector specified in rm16
 *  TODO: Make it
 */
void CPUIntelx86::do_lar(Instruction &instr) {
	cyclesTotal += 2;

	debug_op1({instr.typeDst, instr.szOperands, instr.dstAddr}, "LAR");

	dword result = 0;
	loadInstructionOperand(instr, instr.typeDst, instr.dstValue, instr.dstAddr);
	if (!operOK) {
		return;
	}
	word src = instr.dstValue;

	instr.dstValue &= 0xFFFF;
	DescriptorInfo cur = loadDescriptorInfo(src);

	bool validType = true;

	if (cur.system) {
		switch (cur.type & 0xF) {
			case 0x0:
			case 0x6:
			case 0x7:
			case 0x8:
			case 0xA:
			case 0xD:
			case 0xE:
			case 0xF:
				validType = 0;
			break;

			default: validType = 1;
		}
	}

	if ((cur.selector == 0) || (!validType) || (!cur.isInLimits) ||
		(!(cur.code && cur.conforming) && ((cpl > cur.dpl) || ((src & 0x3) > cur.dpl))) ) {
		eflags.eflags.flag32 &= (~FLAG_ZF);
		return;
	}

	result |= ((cur.type & 0xF) << 8);
	result |= ((!cur.system) << 12);
	result |= (cur.dpl << 13);
	result |= (cur.present << 15);
	result |= (cur.avl << 20);
	result |= (cur.defaultSize << 22);
	result |= (cur.granularity << 23);
	eflags.eflags.flag32 |= FLAG_ZF;
//	if (!lsizeover) {
//		SetReg16(RMName.cREG, (result & 0xFFFF));
//	}
//	else {
//		SetReg32(RMName.cREG, result);
//	}
	qSwap(instr.dstAddr, instr.srcAddr);
	saveInstructionDestination(instr, result);
}

/**
*	LSL - Load segment limit
*	Loads limit field of segment selector specified in rm16
*/
void CPUIntelx86::do_lsl(Instruction &instr) {
	cyclesTotal += 2;

	instr.extensionIndex = 0;
	debug_op1({instr.typeDst, instr.szOperands, instr.dstAddr}, "LSL");

	dword result = 0;
	loadInstructionOperand(instr, instr.typeDst, instr.dstValue, instr.dstAddr);

	if (!operOK) {
		return;
	}

	instr.dstValue &= 0xFFFF;
	word src = instr.dstValue;
	DescriptorInfo cur = loadDescriptorInfo(src);

	bool validType = true;
	if (cur.system) {
		switch (cur.type & 0xF) {
		case 0x1:
		case 0x2:
		case 0x3:
		case 0x9:
		case 0xB: validType = 1; break;
		default: validType = 0;
		}
	}
	if ((0 == cur.selector) || (!validType) || (!cur.isInLimits)) {
		eflags.eflags.flag32 &= FLAG_ZF_CLEAR;
		return;
	}
	result = cur.maxAddress - cur.minAddress;
	eflags.eflags.flag32 |= FLAG_ZF;
	qSwap(instr.dstAddr, instr.srcAddr);
	saveInstructionDestination(instr, result);
}

void CPUIntelx86::do_mov_creg_rm(Instruction &instr)
{
	debug_rm(instr, "MOV");

	loadInstructionOperands(instr);
	if (!instr.executable) {
		return;
	}

	saveInstructionDestination(instr, instr.srcValue);

//	CPU->cRegisters[currentInstruction.dstAddr] = CPU->getReg32(currentInstruction.srcAddr);
	checkOperatingMode();
	checkPagingMode();
}

/**
 * @brief Check is DPL allows to execute command of required pl
 * @param required - requested operation level
 * @return true on succeed
 */
int CPUIntelx86::check_CPL(const word required) {
	int k = (required <= SRegRights[REGISTER_CS].dpl) ;
	if (!k) {
		debug_log("Check_CPL: cpl is lower.");
		fault_GP(decoder->currentInstruction, 0);
	}
	return k;
}




/**
 * Check CPU operating mode and set fast or slow operative routines
 * accordingly. Called every call or ret which changes CS
 */
void CPUIntelx86::checkOperatingMode() {
	OperatingMode temp;

	if (cRegisters[0] & 1) {
		if (eflags.eflags.flag32 & FLAG_VM) {
			temp = opModeV8086;
		} else {
			temp = opModeProtected;
		}
	} else {
		temp = opModeReal;
	}

	if (operatingMode != temp) {
		operatingMode = temp;
		if (operatingMode == opModeProtected) { // Protected
			//dbg_breakpoint();
			setSegment = &CPUIntelx86::setSegmentProtected;
			getSegment = &CPUIntelx86::getSegmentReal;
			getSegmentAddress = &CPUIntelx86::getSegmentAddressProtected;
		} else
		if (operatingMode == opModeReal)
		{ // Real
			setSegment = &CPUIntelx86::setSegmentReal;
			getSegment = &CPUIntelx86::getSegmentReal;
			getSegmentAddress = &CPUIntelx86::getSegmentAddressReal;
		} else {
			setDebugEnabled(true);
			debug_log("******** VM8086 EMU MODE ENABLED. ****");
			stop();
		}
	}

}

void CPUIntelx86::checkPagingMode() {
	int enable = cRegisters[0] & BIT_PAGING_ENABLE;
	isTable4k = (!(cRegisters[4] & BIT_CR_PAGE_4K));
	if (enable) {
		debug_log("** Enable paging **");
		getByte = &CPUIntelx86::getBytePaged;
		getWord = &CPUIntelx86::getWordPaged;
		getDWord = &CPUIntelx86::getDWordPaged;
		setByte = &CPUIntelx86::setBytePaged;
		setWord = &CPUIntelx86::setWordPaged;
		setDWord = &CPUIntelx86::setDWordPaged;
	} else {
		getByte = &CPUIntelx86::getByteFlat;
		getWord = &CPUIntelx86::getWordFlat;
		getDWord = &CPUIntelx86::getDWordFlat;
		setByte = &CPUIntelx86::setByteFlat;
		setWord = &CPUIntelx86::setWordFlat;
		setDWord = &CPUIntelx86::setDWordFlat;
	}
}

dword CPUIntelx86::getSegmentAddressProtected(const dword Selector) {
	operOK = 1;
	if ((Selector == REGISTER_SS) && (!SRegRights[Selector].isRealModed)) {
		if (SRegisters[Selector] != 0) {
			return SRegAddress[Selector];
		} else {
			operOK = 0;
			fault_GP(decoder->currentInstruction, 0);
			debug_log("GetSAddr: SS segment is NULL.");
			return 0;
		}
	} else return SRegAddress[Selector];
}



void CPUIntelx86::do_smsw(Instruction &instr) {
	cyclesTotal += 2;
	debug_rm(instr, "SMSW");

	if (instr.szOperands == sz32) {
		if (instr.typeDst == opMem) {
			instr.szOperands = sz16;
		}
	}

	instr.srcValue = cRegisters[0] & 0x000000FF;
	saveInstructionDestination(instr, instr.srcValue);
}

void CPUIntelx86::do_lmsw(Instruction &instr) {
	cyclesTotal += 2;
	debug_rm(instr, "LMSW");
	loadInstructionOperands(instr);
	if (!operOK) return;
	cRegisters[0] &= 0xFFFFFFF0;
	cRegisters[0] |= (instr.srcValue & 0x0000000F);
	checkOperatingMode();
}

void CPUIntelx86::verr(Instruction &instr) {
	cyclesTotal += 7;
	debug_rm(instr, "VERR");
	loadInstructionOperands(instr);
	if (!operOK) return;

	word selector = instr.dstValue;
	DescriptorInfo desc = loadDescriptorInfo(selector);
	bool cond = true;

	if ((0 == desc.selector) ||
		(!desc.isInLimits) ||
		(desc.system) ||
		(!desc.readable))
	{
		cond = false;
	} else
	if ((!desc.conforming && desc.code) &&
		((cpl > desc.dpl) || ((selector & 0x3) > desc.dpl)) )
	{
		cond = false;
	}
	if (cond) {
		eflags.eflags.flag32 |= FLAG_ZF;
	} else {
		eflags.eflags.flag32 &= FLAG_ZF_CLEAR;
	}
}


void CPUIntelx86::verw(Instruction &instr) {
	cyclesTotal += 7;
	debug_rm(instr, "VERW");
	loadInstructionOperands(instr);
	if (!operOK) return;

	word selector = instr.dstValue;
	DescriptorInfo desc = loadDescriptorInfo(selector);
	bool cond = true;

	if ((0 == desc.selector) ||
		(!desc.isInLimits) ||
		(desc.system) ||
		(!desc.writable))
	{
		cond = false;
	} else
	if ((!desc.conforming && desc.code) &&
		((cpl > desc.dpl) || ((selector & 0x3) > desc.dpl)) )
	{
		cond = false;
	}
	if (cond) {
		eflags.eflags.flag32 |= FLAG_ZF;
	} else {
		eflags.eflags.flag32 &= FLAG_ZF_CLEAR;
	}
}


