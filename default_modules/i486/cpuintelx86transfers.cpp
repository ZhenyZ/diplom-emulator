#include "cpuintelx86.h"
#include "cpuintelx86_alu.h"

#include <QDebug>

qint32 CPUIntelx86::getDisplacement(dword value, int isLong) {
	qint32 disp;
	if (!isLong)
		disp = alu->byte2s32(value & 0x000000FF);
	else
	if (0 == getOpSize())
		disp = alu->word2s32(value & 0x0000FFFF);
	else {
		disp = alu->dword2s32(value);
	}
	//if (!isLong) disp &= 0xFFFF;
	return disp;
}

void CPUIntelx86::do_jumpcc(jumpEval jumpKind, int isLong, dword displacement, Instruction &instr) {
	cyclesTotal += 3;
	qint32 disp = getDisplacement(displacement, isLong);
	if (!operOK) return;
	bool testResult = false;
	switch (jumpKind) {
		case jC: {
			testResult = flagTestCC();
		} break;
		case jNC: {
			testResult = !flagTestCC();
		} break;
		case jO: {
			testResult = flagTestCO();
		} break;
		case jNO: {
			testResult = !flagTestCO();
		} break;
		case jZ: {
			testResult = flagTestCZ();
		} break;
		case jNZ: {
			testResult = !flagTestCZ();
		} break;
		case jP: {
			testResult = flagTestCP();
		} break;
		case jNP: {
			testResult = !flagTestCP();
		} break;
		case jA: {
			testResult = flagTestCA();
		} break;
		case jS: {
			testResult = flagTestCS();
		} break;
		case jNS: {
			testResult = !flagTestCS();
		} break;
		case jG: {
			testResult = flagTestCG();
		} break;
		case jGE: {
			testResult = flagTestCGE();
		} break;
		case jBE: {
			testResult = flagTestCBE();
		} break;
		case jLE: {
			testResult = flagTestCLE();
		} break;
		case jL: {
			testResult = flagTestCL();
		} break;
		case jCXZ: {
			if (instr.szOperands == sz32) {
				testResult = (registers[REGISTER_ECX].dword.reg32 == 0);
			} else {
				testResult = (registers[REGISTER_ECX].word.reg16 == 0);
			}
		} break;
	}
	debug_jump(jumpKind, testResult, disp);

	dword newEIP = decoder->EIP + disp;
	if (0 == getOpSize()) {
		newEIP &= 0xFFFF;
	}

	if (testResult) {
		if (!isInLimits(REGISTER_CS, decoder->CS + newEIP, 1)) {
			fault_GP(decoder->currentInstruction, 0);
			debug_log("jCC: Dest [" + QString::number(decoder->CS + newEIP, 16) + "] is not in limits.");
			operOK = 0;
			return;
		}
		decoder->setEIP(newEIP);
	}
}

void CPUIntelx86::callf(const word &newCS, const dword &newEIP) {
	debug_op2({opImm, sz16, newCS}, {opImm, (getOpSize() == 0) ? sz16 : sz32, newEIP});
	if (operatingMode == opModeProtected)
		callfProtected(decoder->currentInstruction, newCS, newEIP);
	else
		callfReal(decoder->currentInstruction, newCS, newEIP);
}

void CPUIntelx86::jmpfReal(const word &newCS, const dword &newEIP) {
	decoder->setEIP(newEIP);
	(this->*setSegment)(REGISTER_CS, newCS);
	decoder->setCS((this->*getSegmentAddress)(REGISTER_CS));
}

void CPUIntelx86::jmpfProtected(const word &newCS, const dword &newEIP) {
	//AddDebug = 1;
	operOK = 1;
	DescriptorInfo descr = loadDescriptorInfo(newCS);

	if (!descr.isInLimits) {
		debug_log(QString::number(newCS, 16) + " is not in GDT limits.");
		fault_GP(decoder->currentInstruction, newCS);
		operOK = 0;
		return;
	}

	gateInfo gate = loadGateInfo(descr);

	if (!descr.code && !gate.callGate) {
		debug_log("Not a code or call gate.");
		fault_GP(decoder->currentInstruction, newCS);
		operOK = 0;
		return;
	}

	if (descr.code) {
		debug_log("Loading jump to code segment.");
		dword tempEIP = newEIP;
		if (0 == descr.defaultSize) {
			tempEIP &= 0x0000FFFF;
		}

		// Conforming
		if (descr.conforming) {
			if (descr.dpl > cpl) {
				debug_log("Conforming segment DPL > CPL");
				fault_GP(decoder->currentInstruction, newCS);
				operOK = 0;
				return;
			}

		}

		// Non-conforming
		if (!descr.conforming) {
			if ((descr.dpl != cpl) || ((newCS & 0x3) > cpl)) {
				debug_log("Non-Conformint segment DPL != CPL || RPL > CPL");
				fault_GP(decoder->currentInstruction, newCS);
				operOK = 0;
				return;
			}
		}

		if (!descr.present) {
			debug_log("Segment is not present.");
			fault_NP(decoder->currentInstruction, newCS);
			operOK = 0;
			return;
		}
		if (!isInCallLimits(tempEIP, descr)) {
			debug_log("EIP is not in loading segment limits");
			fault_GP(decoder->currentInstruction, 0);
			operOK = 0;
			return;
		}
		(this->*setSegment)(REGISTER_CS, newCS); if (!operOK) return;
		decoder->setCS((this->*getSegmentAddress)(REGISTER_CS));
		decoder->setEIP(tempEIP);
		if (0 == descr.defaultSize) {
			decoder->setOpSize(REGISTER_CS, sz16);
			decoder->setAddressSize(REGISTER_CS, sz16);
		} else {
			decoder->setOpSize(REGISTER_CS, sz32);
			decoder->setAddressSize(REGISTER_CS, sz32);
		}
		return;
	}

	// Call gate
	if (gate.callGate)
	{
		debug_log("Loading call gate.");
		if ((gate.descr.dpl < cpl) || ((newCS & 0x3) > gate.descr.dpl))
		{
			debug_log("Call gate DPL < CPL || RPL > DPL");
			fault_GP(decoder->currentInstruction, newCS);
			operOK = 0;
			return;
		}
		if (!gate.descr.present) {
			debug_log("Call gate segment not present");
			fault_NP(decoder->currentInstruction, newCS);
			operOK = 0;
			return;
		}

		DescriptorInfo gdesc = loadDescriptorInfo(gate.selector);

		if (0 == gdesc.selector) {
			debug_log("Gate selector is NULL");
			fault_GP(decoder->currentInstruction, 0);
			operOK = 0;
			return;
		}

		if ((!gdesc.isInLimits) ||
			(!gdesc.code) || (gdesc.dpl > cpl))
		{
			debug_log("Gate segment not in limits || not code || DPL > CPL");
			fault_GP(decoder->currentInstruction, gdesc.selector);
			operOK = 0;
			return;
		}

		if (!gdesc.present) {
			debug_log("Gate segment not present");
			fault_NP(decoder->currentInstruction, gdesc.selector);
			operOK = 0;
			return;
		}

		// more-privilege
		if (!gdesc.conforming && (gdesc.dpl < cpl))
		{
			stop();
			debug_log(" *** JMPF Call gate for more privilege level. (page 3-95) ***");
			operOK = 0;
			return;
		}
		else // same-privilege
		{
			dword tempEIP = gate.destination;
			if (0 == getOpSize())
				tempEIP &= 0xFFFF;
			if (!isInCallLimits(tempEIP, gdesc)) {
				debug_log("Call gate target EIP is not in segment limits");
				fault_GP(decoder->currentInstruction, 0);
				operOK = 0;
				return;
			}
			word newCS = gate.selector;
			decoder->setEIP(tempEIP);
			(this->*setSegment)(REGISTER_CS, newCS); if (!operOK) return;
			decoder->setCS((this->*getSegmentAddress)(REGISTER_CS));
		}
		return;
	}

	if (gate.taskGate) {
		stop();
		debug_log(" *** JMPF Task gate (page 3-97) ***");
		operOK = 0;
		return;
	}

	stop();
	debug_log ("*** FATAL: Protected-Mode JMPF to something strange. ***");

}

void CPUIntelx86::jmpf(const word &newCS, const dword &newEIP) {
	debug_op2({opImm, sz16, newCS}, {opImm, sz32, newEIP});
	if (operatingMode == opModeProtected) {
		jmpfProtected(newCS, newEIP);
	} else {
		jmpfReal(newCS, newEIP);
	}
}


void CPUIntelx86::callfReal(Instruction & instr, const word &newCS, const dword &newEIP) {
	operOK = true;
	if (0 != getOpSize()) {
		checkStackSize(8); if (!operOK) return;
		if (newEIP & 0xFFFF0000) {
			fault_GP(instr, 0);
			return;
		}
		push32(instr, (this->*getSegment)(REGISTER_CS)); if (!operOK) return;
		push32(instr, decoder->EIP); if (!operOK) return;

		(this->*setSegment)(REGISTER_CS, newCS); if (!operOK) return;
		decoder->setCS((this->*getSegmentAddress)(REGISTER_CS));
		decoder->setEIP(newEIP);
	} else {
		checkStackSize(4); if (!operOK) return;
		push16(instr, (this->*getSegment)(REGISTER_CS)); if (!operOK) return;
		push16(instr, decoder->EIP & 0xFFFF); if (!operOK) return;

		(this->*setSegment)(REGISTER_CS, newCS); if (!operOK) return;
		decoder->setCS((this->*getSegmentAddress)(REGISTER_CS));
		decoder->setEIP(newEIP);
	}
}

bool CPUIntelx86::isInCallLimits(dword newEIP, DescriptorInfo &descr) {
	dword baseEIP = descr.minAddress + newEIP;
	if ((baseEIP < descr.minAddress) || (baseEIP > descr.maxAddress))
		return false;
	return true;
}

void CPUIntelx86::callfProtected(Instruction & instr, const word &newCS, const dword &newEIP) {
	operOK = 1;
	DescriptorInfo desc = loadDescriptorInfo(newCS);
	gateInfo gate = loadGateInfo(desc);

	if (0 == desc.selector) {
		debug("Jump to zero selector");
		fault_GP(instr, 0);
		operOK = 0;
		return;
	}

	if ((!desc.isInLimits) ||
		(!desc.code && !gate.callGate && !gate.taskGate))
	{
		fault_GP(instr, newCS);
		operOK = 0;
		return;
	}

	// Switch to a code segment
	if (desc.code) {
		if (!desc.present) {
			fault_NP(instr, newCS);
			operOK = 0;
			return;
		}
		checkStackSize(4 * (1 + (0 != getOpSize())));
		if (!operOK) return;

		// Conforming
		if (desc.conforming) {
			if (desc.dpl > cpl) {
				fault_GP(instr, newCS);
				operOK = 0;
				return;
			}
		}

		// Non-conforming
		if (!desc.conforming)
		{
			if ((desc.dpl != cpl) || ((newCS & 0x3) > cpl)) {
				fault_GP(instr, newCS);
				operOK = 0;
				return;
			}

		}

		dword tempEIP = newEIP;
		if (0 == getOpSize()) {
			tempEIP &= 0xFFFF;
		}
		if (!isInCallLimits(tempEIP, desc)) {
			fault_GP(instr, 0);
			operOK = 0;
			return;
		}
		if (0 != getOpSize()) {
			push32(instr, SRegisters[REGISTER_CS]);
			push32(instr, decoder->EIP);
		}
		else {
			push16(instr, SRegisters[REGISTER_CS]);
			push16(instr, decoder->EIP & 0xFFFF);
		}
		(this->*setSegment)(REGISTER_CS, newCS); if (!operOK) return;
		decoder->setCS((this->*getSegmentAddress)(REGISTER_CS));
		decoder->setEIP(tempEIP);
		return;
	}

	// Call gate
	if (gate.callGate) {
		if ((gate.descr.dpl < cpl) || ((newCS & 0x3) > gate.descr.dpl)) {
			fault_GP(instr, newCS);
			operOK = 0;
			return;
		}
		if (!gate.descr.present) {
			fault_NP(instr, newCS);
			operOK = 0;
			return;
		}

		DescriptorInfo gdesc = loadDescriptorInfo(gate.selector);

		if (0 == gdesc.selector) {
			fault_GP(instr, 0);
			operOK = 0;
			return;
		}

		if ((!gdesc.isInLimits) ||
			(!gdesc.code) || (gdesc.dpl > cpl))
		{
			fault_GP(instr, gdesc.selector);
			operOK = 0;
			return;
		}

		if (!gdesc.present) {
			fault_NP(instr, gdesc.selector);
			operOK = 0;
			return;
		}


		if (!gdesc.conforming && (gdesc.dpl < cpl)) {
			// more-privilege
			stop();
			debug_log(" *** CALLF Call gate for more privilege level. (page 3-95) ***");
			operOK = 0;
			return;
		} else {
			// same-privilege
			checkStackSize(4 + (4 * gate.descr.defaultSize));
			if (!operOK) return;
			dword tempEIP = gate.destination;
			if (0 == getOpSize()) {
				tempEIP &= 0xFFFF;
			}
			if (!isInCallLimits(tempEIP, gdesc)) {
				fault_GP(instr, 0);
				operOK = 0;
				return;
			}
			word newCS = gate.selector;
			if (gate.descr.defaultSize) {
				push32(instr, SRegisters[REGISTER_CS]);
				push32(instr, decoder->EIP);
			}
			else {
				push16(instr, SRegisters[REGISTER_CS]);
				push16(instr, decoder->EIP & 0xFFFF);
			}
			decoder->setEIP(tempEIP);
			(this->*setSegment)(REGISTER_CS, newCS);
			decoder->setCS((this->*getSegmentAddress)(REGISTER_CS));
		}
		return;
	}

	if (gate.taskGate)
	{
		stop();
		debug_log(" *** CALLF Task gate (page 3-97) ***");
		operOK = 0;
		return;
	}

	stop();
	debug_log ( " *** FATAL : Protected-mode CALLF to something strange. *** ");

}

void CPUIntelx86::do_retn_imm16(Instruction &instr) {
	cyclesTotal += 5;
	word popcnt = (instr.immediate & 0xFFFF);

	debug_op1({opImm, sz16, popcnt});

	if (0 == getOpSize()) {
		word tempEIP16 = pop16(instr); if (!operOK) return;
		decoder->setEIP(tempEIP16);
		registers[REGISTER_ESP].word.reg16 += popcnt;
	}
	else {
		dword tempEIP32 = pop32(instr); if (!operOK) return;
		decoder->setEIP(tempEIP32);
		registers[REGISTER_ESP].dword.reg32 += popcnt;
	}
}

void CPUIntelx86::do_retn(Instruction & instr) {
	cyclesTotal += 5;
	debug_op0();
	if (instr.szOperands == sz16) {
		word tempEIP16 = pop16(instr); if (!operOK) return;
		decoder->setEIP(tempEIP16);
	}
	else {
		dword tempEIP32 = pop32(instr); if (!operOK) return;
		decoder->setEIP(tempEIP32);
	}
}

void CPUIntelx86::do_jumpn_rel(Instruction &instr) {

	cyclesTotal += 8;
	qint32 rel;
	if (instr.szOperands == sz8) {
		rel = alu->byte2s32((instr.srcValue & 0x000000FF));
	} else {
		if (instr.szOperands == sz16) {
			rel = alu->word2s16((instr.srcValue & 0x0000FFFF));
		} else {
			rel = alu->word2s32(instr.srcValue);
		}
	}

	debug_op1({opImm, instr.szOperands, decoder->CS + decoder->EIP + rel});

	decoder->EIP += rel;
	if (instr.szOperands == sz16) {
		decoder->EIP &= 0xFFFF;
	}
}


void CPUIntelx86::signalExternalInterrupt(word level) {
	this->hasPendingInterrupts = true;
	if (0 == (eflags.eflags.flag32 & FLAG_IF)) {
		return;
	}
	debug("External int " + QString::number(level));
	interruptInternal(level, 0);
}

void CPUIntelx86::stop() {
	this->halted = true;
}

void CPUIntelx86::interruptInternal(word level, int isSoftware) {
	Instruction instr;
	if (getOpSize()) {
		instr.szOperands = sz32;
	} else {
		instr.szOperands = sz16;
	}
	if (getAddrSize(REGISTER_SS)) {
		instr.szAddress = sz32;
	} else {
		instr.szAddress = sz16;
	}
	if (operatingMode == opModeReal) {
		intReal(instr, level, isSoftware);
	} else {
		intProtected(instr, level, isSoftware);
	}
}

/**
 * @brief Load gate info from existing descriptor
 * @param descr - gate descriptor
 * @return Gate info on success
 */
gateInfo CPUIntelx86::loadGateInfo(DescriptorInfo &descr) {
	//dbg_breakpoint();
	gateInfo gate;
	gate.descr = descr;
	gate.busy = 0;
	gate.callGate = 0;
	gate.interruptGate = 0;
	gate.trapGate = 0;
	gate.taskGate = 0;

	switch (descr.type) {

		// Call Gate
		case 0x0C:
			gate.paramCount = (descr.dw4 & 0x1F);
			gate.destination = (descr.dw0 & 0xFFFF) | (descr.dw4 & 0xFFFF0000);
			gate.selector = ((descr.dw0 & 0xFFFF0000) >> 16);
			gate.callGate = 1;
		break;

		// Interrupt gate
		case 0x0E:
		case 0x06:
			gate.descr.defaultSize = ((descr.type & 0x8) >> 3);
			gate.destination = (descr.dw0 & 0xFFFF) | (descr.dw4 & 0xFFFF0000);
			gate.selector = ((descr.dw0 & 0xFFFF0000) >> 16);
			gate.interruptGate = 1;
		break;

		// Trap gate
		case 0x0F:
		case 0x07:
			gate.descr.defaultSize = ((descr.type & 0x8) >> 3);
			gate.destination = (descr.dw0 & 0xFFFF) | (descr.dw4 & 0xFFFF0000);
			gate.selector = ((descr.dw0 & 0xFFFF0000) >> 16);
			gate.trapGate = 1;
		break;

		// TSS
		case 0x05:
		case 0x09:
		case 0x0B:
			stop();
			debug_log("Load Gate: requested TSS.");
		break;

		default:
			debug_log("Load gate : === Type = " + QString::number(descr.type, 16));

	}
	return gate;
}

void CPUIntelx86::checkStackSize(dword count) {
	dword curesp = (this->*getSegmentAddress)(REGISTER_SS);
	if (0 == getAddrSize(REGISTER_SS))
		curesp += registers[REGISTER_ESP].word.reg16;
	else
		curesp += registers[REGISTER_ESP].dword.reg32;
	if (!isInLimits(REGISTER_SS, curesp - count, count)) {
		fault_SS(decoder->currentInstruction, 0);
		operOK = false;
	}
}

void CPUIntelx86::do_calln_rel(Instruction & instr, qint32 disp) {
	cyclesTotal += 16;

	debug_op1({opImm, instr.szOperands, disp});
	if (instr.szOperands == sz16) {
		push16(instr, decoder->EIP & 0xFFFF); if (!operOK) return;
		decoder->EIP = ((decoder->EIP + disp) & 0xFFFF);
	} else {
		debug_op1({opImm, sz32, disp});
		push32(instr, decoder->EIP); if (!operOK) return;
		decoder->EIP += disp;
	}
}


void CPUIntelx86::intProtected(Instruction & instr, dword Number, int isSoftware) {
	operOK = 1;
//	dbg_breakpoint();
	cyclesTotal += 112;

	debug_op1({opImm, sz8, Number});

	int z = Number;

	DescriptorInfo descr = loadInterruptDescriptorInfo(z);
	gateInfo gate = loadGateInfo(descr);

	if (!descr.isInLimits || !descr.system ||
		(!gate.interruptGate && !gate.taskGate && !gate.trapGate)) {
		fault_GP(instr, Number);
		return;
	}

	if (isSoftware) {
		if (descr.dpl < cpl) {
			fault_GP(instr, Number);
			return;
		}
	}

	if (!descr.present) {
		fault_NP(instr, Number);
		return;
	}

	if (gate.taskGate) {
		stop();
		debug_log( " *** INT PM : task gate  *** ");
		return;
	}

	// Call gate
	if (gate.trapGate || gate.interruptGate)
	{

		DescriptorInfo gdesc = loadDescriptorInfo(gate.selector);

		if (0 == gdesc.selector) {
			fault_GP(instr, 0);
			return;
		}

		if ((!gdesc.isInLimits))
		{
			fault_GP(instr, gdesc.selector);
			return;
		}

		if (!gdesc.code || gdesc.dpl > cpl) {
			fault_GP(instr, gdesc.selector);
			return;
		}

		if (!gdesc.present) {
			fault_NP(instr, gdesc.selector);
			return;
		}

		// more-privilege
		if (!gdesc.conforming && (gdesc.dpl < cpl)) {
			stop();
			debug_log(" *** CALLF Call gate for more privilege level. (page 3-95) ***");
			return;
		}
		else // same-privilege
		{
			if (eflags.eflags.flag32 & FLAG_VM) {
				fault_GP(instr, gdesc.selector);
				return;
			}

			if (!gdesc.conforming && (gdesc.dpl != cpl)) {
				fault_GP(instr, gdesc.selector);
				return;
			}

			// Intra-privilege level

			if (gate.descr.defaultSize) {
				checkStackSize(12);
				if (!operOK) return;
			} else {
				checkStackSize(6);
				if (!operOK) return;
			}

			qDebug() << "Loading INT protected";
			dword tempEIP = gate.destination;
			if (0 == getOpSize()) {
				tempEIP &= 0xFFFF;
			}
			if (!isInCallLimits(tempEIP, gdesc)) {
				fault_GP(instr, 0);
				return;
			}
			word newCS = gate.selector;
			if (gate.descr.defaultSize) {
				push32(instr, eflags.wflag.f16);
				push16(instr, SRegisters[REGISTER_CS]);
				push32(instr, decoder->EIP);
			}
			else {
				push16(instr, eflags.wflag.f16);
				push16(instr, SRegisters[REGISTER_CS]);
				push16(instr, decoder->EIP & 0xFFFF);
			}
			decoder->setEIP(tempEIP);
			(this->*setSegment)(REGISTER_CS, newCS);
			decoder->setCS((this->*getSegmentAddress)(REGISTER_CS));
		}
	}

	if (gate.interruptGate) {
		eflags.eflags.flag32 &= ~FLAG_IF;
	}
	eflags.eflags.flag32 &= ~FLAG_VM;
}

void CPUIntelx86::iretReal(Instruction & instr) {
	cyclesTotal += 15;

	debug_op0();

	if (instr.szOperands == sz16) {
		// 16-bit
		word tempEIP = pop16(instr); if (!operOK) return;
		word tempCS = pop16(instr); if (!operOK) return;
		pop_flags(instr); if (!operOK) return;
		decoder->setEIP(tempEIP);
		(this->*setSegment)(REGISTER_CS, tempCS); if (!operOK) return;
	} else {
		// 32-bit
		dword tempEIP = pop32(instr); if (!operOK) return;
		if (tempEIP & 0xFFFF0000) {
			push32(instr, tempEIP); // prevent changes on #GP
			fault_GP(instr, 0);
			return;
		}
		dword tempCS = pop32(instr);
		if (!operOK) {
			push32(instr, tempEIP); // prevent changes on #GP
			return;
		}
		pop_flags(instr);
		if (!operOK) {
			// prevent changes on #GP
			push32(instr, tempCS);
			push32(instr, tempEIP);
			return;
		}
		decoder->setEIP(tempEIP);
		(this->*setSegment)(REGISTER_CS, (tempCS & 0xFFFF));
		if (!operOK) return;
	}
	decoder->setCS((this->*getSegmentAddress)(REGISTER_CS));
}

void CPUIntelx86::iretProtected(Instruction & instr) {
	//dbg_breakpoint();
	cyclesTotal += 15;
	debug_op0();

	if (instr.szOperands == sz16) {
		word newIP = pop16(instr);
		if (!operOK) return;
		word newCS = pop16(instr);
		if (!operOK) return;
		pop_flags(instr);
		if (!operOK) return;
		decoder->setEIP(newIP);
		(this->*setSegment)(REGISTER_CS, newCS);
		decoder->setCS((this->*getSegmentAddress)(REGISTER_CS));
	}
	else {
		dword newEIP = pop32(instr);
		if (!operOK) return;
		dword newCS = pop32(instr);
		if (!operOK) return;
		pop_flags(instr);
		if (!operOK) return;
		decoder->setEIP(newEIP);
		(this->*setSegment)(REGISTER_CS, (newCS & 0xFFFF));
		decoder->setCS((this->*getSegmentAddress)(REGISTER_CS));
	}
}

void CPUIntelx86::iret(Instruction & instr) {
	if (operatingMode == opModeReal)
		iretReal(instr);
	else
		iretProtected(instr);
}

void CPUIntelx86::intReal(Instruction & instr, const dword Number, int isSoftware) {
	Q_UNUSED(isSoftware);


//	if (Number == 0x13) {
//		slotSuspend();
//		slotSetDebug(true);
//	}

	cyclesTotal += 77;
	word z = Number;

	debug_op1({opImm, sz16, Number});
	push_flags(instr); if (!operOK) return;
	push16(instr, (this->*getSegment)(REGISTER_CS)); if (!operOK) return;
	push16(instr, decoder->EIP & 0xFFFF); if (!operOK) return;
	eflags.eflags.flag32 &= (~FLAG_IF);

	const dword address = IDTR.Base + z*4;

	(this->*setSegment)(REGISTER_CS, word_at(address + 2));
	decoder->setCS((this->*getSegmentAddress)(REGISTER_CS));
	decoder->setEIP(word_at(address));
}



/**
 * @brief LOOP / LOOPNZ / LOOPZ function
 * @short Cycle program with specified condition
 * @param lc - condition type
 */
void CPUIntelx86::do_loop(loopKind lc, Instruction& instr) {
	cyclesTotal += 10;
	qint32 disp = alu->byte2s32(instr.immediate & 0xFF);
	qint32 counter;

	bool branchCondition;

	if (0 == getAddrSize(REGISTER_CS)) {
		counter = --registers[REGISTER_ECX].word.reg16;
	}
	else {
		counter = --registers[REGISTER_ECX].dword.reg32;
	}

	bool condition = 0;
	if (lc == lkLoop) {
		condition = 1;
	} else {
		if (lc == lkLoopz) {
			condition = ((eflags.eflags.flag32 & FLAG_ZF) == 0);
		} else {
			condition = ((eflags.eflags.flag32 & FLAG_ZF) != 0);
		}
	}

	if ((0 != counter) && condition) {
		branchCondition = true;
	} else {
		branchCondition = false;
	}

	debug_op1({opImm, instr.szOperands, disp});

	if (branchCondition) {
		decoder->EIP += disp;
		if (instr.szOperands == sz16) {
			decoder->EIP &= 0xFFFF;
		} else {
			if (decoder->EIP < SRegRights[REGISTER_CS].minAddress ||
				decoder->EIP > SRegRights[REGISTER_CS].maxAddress)
			{
				fault_GP(instr, 0);
				return;
			}
		}
	}
}

void CPUIntelx86::do_retf(Instruction & instr) {
	cyclesTotal += 13;
	debug_op0();
	if (instr.szOperands == sz16) {
		word tempEIP16 = pop16(instr); if (!operOK) return;
		word tempCS = pop16(instr); if (!operOK) return;
		(this->*setSegment)(REGISTER_CS, tempCS); if (!operOK) return;
		decoder->setEIP(tempEIP16);
		decoder->setCS((this->*getSegmentAddress)(REGISTER_CS));
	}
	else {
		dword tempEIP32 = pop32(instr); if (!operOK) return;
		dword tempCS = (pop32(instr) & 0xFFFF); if (!operOK) return;
		(this->*setSegment)(REGISTER_CS, tempCS); return;
		decoder->setEIP(tempEIP32);
		decoder->setCS((this->*getSegmentAddress)(REGISTER_CS));
	}
}

void CPUIntelx86::do_retf_imm16(Instruction & instr) {
	cyclesTotal += 14;

	word PopCount = instr.immediate & 0xFFFF;
	debug_op1({opImm, instr.szOperands, PopCount});

	if (instr.szOperands == sz16) {
		word tempEIP16 = pop16(instr); if (!operOK) return;
		word tempCS = pop16(instr); if (!operOK) return;
		(this->*setSegment)(REGISTER_CS, tempCS); if (!operOK) return;
		registers[REGISTER_ESP].word.reg16 += PopCount;
		decoder->setEIP(tempEIP16);
		decoder->setCS((this->*getSegmentAddress)(REGISTER_CS));
	}
	else {
		dword tempEIP32 = pop32(instr); if (!operOK) return;
		dword tempCS = (pop32(instr) & 0xFFFF); if (!operOK) return;
		(this->*setSegment)(REGISTER_CS, tempCS); if (!operOK) return;
		registers[REGISTER_ESP].dword.reg32 += PopCount;
		decoder->setEIP(tempEIP32);
		decoder->setCS((this->*getSegmentAddress)(REGISTER_CS));
	}
}

