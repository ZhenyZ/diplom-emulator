#include "cpuintelx86.h"
#include <QDebug>

void CPUIntelx86::loadBIOS(const QString fileName) {
	if (biosMemory) {
		delete [] biosMemory;
	}
	FILE *file;
	file = fopen(fileName.toStdString().c_str(), "rb");
	if (file != NULL) {
		fseek(file, 0, SEEK_END);

		int fSize = ftell(file);
		rewind(file);

		biosMemory = new byte[fSize];
		biosMemSize = fSize;

		//address = 0x100000-fSize;	// start BIOS address
		fread(biosMemory, 1, fSize, file);
		//fread(memory + address, 1, fSize, file);
		fclose(file);
		debug_log("BIOS loaded from file " + fileName);
	}
	else {
		biosMemory = nullptr;
		biosMemSize = 0;
		debug_log("Error loading BIOS from file " + fileName);
	}

	memcpy(memory + (0x100000 - biosMemSize), biosMemory, biosMemSize);
}

//void CPUIntelx86::setChipset(GenericChipset *c) {
//	chipset = c;
//}


byte CPUIntelx86::getReg8(const dword reg) const {
	return ((registers[RegTable8[reg].Reg].word.reg16 & RegTable8[reg].Mask) >> RegTable8[reg].Shor);
}

word CPUIntelx86::getReg16(const dword reg) const {
	return registers[reg].word.reg16;
}

dword CPUIntelx86::getReg32(const dword reg) const {
	return registers[reg].dword.reg32;
}

void CPUIntelx86::setReg8(const dword reg, const byte value) {
	registers[RegTable8[reg].Reg].word.reg16 =
		(registers[RegTable8[reg].Reg].word.reg16 & (~RegTable8[reg].Mask)) +
		((value & 0xFF) << RegTable8[reg].Shor);
}

void CPUIntelx86::setReg16(const dword reg, const word value) {
	registers[reg].word.reg16 = (value & 0xFFFF);
}

void CPUIntelx86::setReg32(const dword reg, const dword value) {
	registers[reg].dword.reg32 = value;
}


byte CPUIntelx86::byte_at(dword addr) const {
	dword low_bios = 0xFFFFFFFF - biosMemSize + 1;
	dword min_bios = 0x100000 - biosMemSize;

	// Redirect to BIOS
	if (addr >= low_bios) {
		static dword mask = 0xFFFFF;
		addr = (addr & mask);
		if (addr >= min_bios && addr <= 0xFFFFF) {
			return *reinterpret_cast<byte*>(biosMemory + (addr - min_bios));
		}
	}

	if (addr >= eMemSize) return 0xFF;
	return *reinterpret_cast<byte*>(memory+addr);
}

word CPUIntelx86::word_at(dword addr) const {
	dword low_bios = 0xFFFFFFFF - biosMemSize + 1;
	dword min_bios = 0x100000 - biosMemSize;

	// Redirect to BIOS
	if (addr >= low_bios) {
		static dword mask = 0xFFFFF;
		addr = (addr & mask);
		if (addr >= min_bios && addr <= 0xFFFFE) {
			return *reinterpret_cast<word*>(biosMemory + (addr - min_bios));
		}
	}

	if (addr > eMemSize) return 0xFFFF;
	return *reinterpret_cast<word*>(memory+addr);
}

dword CPUIntelx86::dword_at(dword addr) const {
	dword low_bios = 0xFFFFFFFF - biosMemSize + 1;
	dword min_bios = 0x100000 - biosMemSize;

	// Redirect to BIOS
	if (addr >= low_bios) {
		static dword mask = 0xFFFFF;
		addr = (addr & mask);
		if (addr >= min_bios && addr <= 0xFFFFC) {
			return *reinterpret_cast<dword*>(biosMemory + (addr - min_bios));
		}
	}

	if (addr >= eMemSize) return 0xFFFFFFFF;
	return *reinterpret_cast<dword*>(memory+addr);
}

void CPUIntelx86::set_byte_at(dword addr, byte value) {
	dword low_bios = 0xFFFFFFFF - biosMemSize + 1;
	dword min_bios = 0x100000 - biosMemSize;

	// Redirect to BIOS
	if (addr >= low_bios) {
		static dword mask = 0xFFFFF;
		addr = (addr & mask);
		if (addr >= min_bios && addr <= 0xFFFFF) {
			*reinterpret_cast<byte*>(biosMemory + (addr - min_bios)) = value;
			return;
		}
	}

	if (addr >= eMemSize) return;
	*reinterpret_cast<byte*>(memory+addr) = value;
}

void CPUIntelx86::set_word_at(dword addr, word value) {
	dword low_bios = 0xFFFFFFFF - biosMemSize + 1;
	dword min_bios = 0x100000 - biosMemSize;

	// Redirect to BIOS
	if (addr >= low_bios) {
		static dword mask = 0xFFFFF;
		addr = (addr & mask);
		if (addr >= min_bios && addr <= 0xFFFFE) {
			*reinterpret_cast<word*>(biosMemory + (addr - min_bios)) = value;
			return;
		}
	}

	if (addr >= eMemSize) return;
	*reinterpret_cast<word*>(memory+addr) = value;
}

void CPUIntelx86::set_dword_at(dword addr, dword value) {
	dword low_bios = 0xFFFFFFFF - biosMemSize + 1;
	dword min_bios = 0x100000 - biosMemSize;

	// Redirect to BIOS
	if (addr >= low_bios) {
		static dword mask = 0xFFFFF;
		addr = (addr & mask);
		if (addr >= min_bios && addr <= 0xFFFFC) {
			*reinterpret_cast<dword*>(biosMemory + (addr - min_bios)) = value;
			return;
		}
	}


	if (addr >= eMemSize) return;
	*reinterpret_cast<dword*>(memory+addr) = value;
}

/**
 * @brief Reads far pointer from specified address
 * @brief Does not perform memory access checks
 * @param address - source
 */
CPUIntelx86::FarPointer CPUIntelx86::readFarPointer(dword address, bool is32bit)
{
	FarPointer ptr;
	if (is32bit) {
		ptr.address = this->dword_at(address);
		ptr.selector = this->word_at(address + 4);
	}
	else {
		ptr.address = this->word_at(address);
		ptr.selector = this->word_at(address + 2);
	}
	return ptr;
}

void CPUIntelx86::writeFarPointer(dword , FarPointer )
{

}

dword CPUIntelx86::readSegmentAddressExt(const dword selector)
{
	return (this->*getSegmentAddress)(selector);
}

int CPUIntelx86::getOpSize(const word selector) const {
	return (SRegRights[selector].opSize);
}

int CPUIntelx86::getOpSize() const {
	return getOpSize(REGISTER_CS);
}

/**
 * @brief Errorneous nut!
 * @param
 * @return
 * @warning Possibly useless
 */
int CPUIntelx86::getAddrSize(const word selector) const {
	return (SRegRights[selector].opSize);
}

/**
 * Return byte at ADDRESS and check if segment has read access
 */
byte CPUIntelx86::getByteFlat(const dword address, const word Segment) {
	cyclesTotal += 3;
	if (!SRegRights[Segment].read) {
		fault_GP(decoder->currentInstruction, 0);
		debug("GetByte : segment " + QString::number(Segment, 16) + " is not readable.");
		operOK = 0;
		return 0;
	}
	if (!isInLimits(Segment, address, 1)) {
		fault_GP(decoder->currentInstruction, 0);
		debug("GetByte : address " + QString::number(address, 16) + " is not within limits.");
		operOK = 0;
		return 0;
	}
	operOK = 1;
	return byte_at(address);
}

/**
 * Return word at ADDRESS and check if segment has read access
 */
word CPUIntelx86::getWordFlat(const dword address, const word Segment) {
	cyclesTotal += 3;
	if (!SRegRights[Segment].read) {
		operOK = 0;
		fault_GP(decoder->currentInstruction, 0);
		//logger("GetWord : segment " << SRegStrings[Segment] << " is not readable.");
		return 0;
	}
	if (!isInLimits(Segment, address, 2)) {
		fault_GP(decoder->currentInstruction, 0);
		//logger("GetWord : address " << address << " is not within limits.");
		operOK = 0;
		return 0;
	}
	return word_at(address);
}

/**
 * Return doubleword at ADDRESS and check if segment has read access
 */
dword CPUIntelx86::getDWordFlat(const dword address, const word Segment) {
	cyclesTotal += 3;
	if (!SRegRights[Segment].read) {
		operOK = 0;
		fault_GP(decoder->currentInstruction, 0);
		//debug_log("GetDWord : segment " + SRegStrings[Segment] << " is not readable.");
		return 0;
	}
	if (!isInLimits(Segment, address, 4)) {
		fault_GP(decoder->currentInstruction, 0);
		//logger("GetDWord : address " << address << " is not within limits.");
		operOK = 0;
		return 0;
	}
	return dword_at(address);
}


/**
 * Check if the segment has write access and then set byte at address
 */
void CPUIntelx86::setByteFlat(const dword address, const byte Byte, const word Segment) {
	cyclesTotal += 3;
	operOK = 1;
	if (!SRegRights[Segment].write) {
		fault_GP(decoder->currentInstruction, 0);
		//logger("SetByte : segment " << SRegStrings[Segment] << " is not writable.");
		operOK = 0;
		return;
	}
	if (!isInLimits(Segment, address, 1)) {
		fault_GP(decoder->currentInstruction, 0);
		//logger("SetByte : address " << address << " is not within limits.");
		operOK = 0;
		return;
	}
	set_byte_at(address, Byte);
}

/**
 * Check if the segment has write access and then set word at address
 */
void CPUIntelx86::setWordFlat(const dword address, const word Word, const word Segment) {
	cyclesTotal += 3;
	operOK = 1;
	if (!SRegRights[Segment].write) {
		fault_GP(decoder->currentInstruction, 0);
		//logger("SetWord : segment " << SRegStrings[Segment] << " is not writable.");
		operOK = 0;
		return;
	}
	if (!isInLimits(Segment, address, 2)) {
		fault_GP(decoder->currentInstruction, 0);
		//logger("SetWord : address " << address << " is not within limits.");
		operOK = 0;
		return;
	}
	set_word_at(address, Word);
}

/**
 * Check if the segment has write access and then set doubleword at address
 */
void CPUIntelx86::setDWordFlat(const dword address, const dword DWord, const word Segment) {
	cyclesTotal += 3;
	operOK = 1;
	if (!SRegRights[Segment].write) {
		fault_GP(decoder->currentInstruction, 0);
		//logger("SetDWord : segment " << SRegStrings[Segment] << " is not writable.");
		operOK = 0;
		return;
	}
	if (!isInLimits(Segment, address, 4)) {
		fault_GP(decoder->currentInstruction, 0);
		//logger("SetDWord : address " << address << " is not within limits.");
		operOK = 0;
		return;
	}
	set_dword_at(address, DWord);
}



// ---------------------

/**
 * Return byte at ADDRESS and check if segment has read access
 */
byte CPUIntelx86::getBytePaged(const dword address, const word Segment) {
	cyclesTotal += 3;
	if (!SRegRights[Segment].read) {
		fault_GP(decoder->currentInstruction, 0);
		debug("GetByte : segment " + QString::number(Segment, 16) + " is not readable.");
		operOK = 0;
		return 0;
	}
	if (!isInLimits(Segment, address, 1)) {
		fault_GP(decoder->currentInstruction, 0);
		debug("GetByte : address " + QString::number(address, 16) + " is not within limits.");
		operOK = 0;
		return 0;
	}
	operOK = 1;
	dword resultAddress = getBaseAddressForPage(address, false);
	if (operOK) {
		return byte_at(resultAddress);
	}
	return 0xFF;
}

/**
 * Return word at ADDRESS and check if segment has read access
 */
word CPUIntelx86::getWordPaged(const dword address, const word Segment) {
	cyclesTotal += 3;
	if (!SRegRights[Segment].read) {
		operOK = 0;
		fault_GP(decoder->currentInstruction, 0);
		//logger("GetWord : segment " << SRegStrings[Segment] << " is not readable.");
		return 0;
	}
	if (!isInLimits(Segment, address, 2)) {
		fault_GP(decoder->currentInstruction, 0);
		//logger("GetWord : address " << address << " is not within limits.");
		operOK = 0;
		return 0;
	}

	operOK = 1;
	dword resultAddress = getBaseAddressForPage(address, false);
	if (operOK) {
		return word_at(resultAddress);
	}
	return 0xFF;
}

/**
 * Return doubleword at ADDRESS and check if segment has read access
 */
dword CPUIntelx86::getDWordPaged(const dword address, const word Segment) {
	cyclesTotal += 3;
	if (!SRegRights[Segment].read) {
		operOK = 0;
		fault_GP(decoder->currentInstruction, 0);
		//debug_log("GetDWord : segment " + SRegStrings[Segment] << " is not readable.");
		return 0;
	}
	if (!isInLimits(Segment, address, 4)) {
		fault_GP(decoder->currentInstruction, 0);
		//logger("GetDWord : address " << address << " is not within limits.");
		operOK = 0;
		return 0;
	}

	operOK = 1;

	dword resultAddress = getBaseAddressForPage(address, false);
	if (operOK) {
		return dword_at(resultAddress);
	}
	return 0xFF;
}


/**
 * Check if the segment has write access and then set byte at address
 */
void CPUIntelx86::setBytePaged(const dword address, const byte Byte, const word Segment) {
	cyclesTotal += 3;
	operOK = 1;
	if (!SRegRights[Segment].write) {
		fault_GP(decoder->currentInstruction, 0);
		//logger("SetByte : segment " << SRegStrings[Segment] << " is not writable.");
		operOK = 0;
		return;
	}
	if (!isInLimits(Segment, address, 1)) {
		fault_GP(decoder->currentInstruction, 0);
		//logger("SetByte : address " << address << " is not within limits.");
		operOK = 0;
		return;
	}

	dword resultAddress = getBaseAddressForPage(address, true);
	if (operOK) {
		set_byte_at(resultAddress, Byte);
	}
}

/**
 * Check if the segment has write access and then set word at address
 */
void CPUIntelx86::setWordPaged(const dword address, const word Word, const word Segment) {
	cyclesTotal += 3;
	operOK = 1;
	if (!SRegRights[Segment].write) {
		fault_GP(decoder->currentInstruction, 0);
		//logger("SetWord : segment " << SRegStrings[Segment] << " is not writable.");
		operOK = 0;
		return;
	}
	if (!isInLimits(Segment, address, 2)) {
		fault_GP(decoder->currentInstruction, 0);
		//logger("SetWord : address " << address << " is not within limits.");
		operOK = 0;
		return;
	}

	dword resultAddress = getBaseAddressForPage(address, true);
	if (operOK) {
		set_word_at(resultAddress, Word);
	}
}

/**
 * Check if the segment has write access and then set doubleword at address
 */
void  CPUIntelx86::setDWordPaged(const dword address, const dword DWord, const word Segment) {
	cyclesTotal += 4;
	operOK = 1;
	if (!SRegRights[Segment].write) {
		fault_GP(decoder->currentInstruction, 0);
		//logger("SetDWord : segment " << SRegStrings[Segment] << " is not writable.");
		operOK = 0;
		return;
	}
	if (!isInLimits(Segment, address, 4)) {
		fault_GP(decoder->currentInstruction, 0);
		//logger("SetDWord : address " << address << " is not within limits.");
		operOK = 0;
		return;
	}

	dword resultAddress = getBaseAddressForPage(address, true);
	if (operOK) {
		set_dword_at(resultAddress, DWord);
	}
}

dword CPUIntelx86::getBaseAddressForPage(const dword address, bool write) {
	dword basePagingAddress = cRegisters[3];
	dword pdeAddress = getPDEAddress(address);
	dword pteAddress = getPTEAddress(address);
	basePagingAddress += pdeAddress;
	PageDirEntryInfo resultEntry;

	auto pdeInfo = loadPageDirEntry(basePagingAddress);

	dword resultAddress = 0;
	dword offset = (address & BIT_PAGE_OFFSET);

	if (!isTable4k) {
		// 4MB page
		// page info stored right away
		if (!pdeInfo.ok) {
			// Not Present
			if (!pdeInfo.present) {
				cRegisters[2] = pdeAddress;
				fault_PF(decoder->currentInstruction, pdeAddress);
				return 0;
			}
		}
		if (!pdeInfo.user && (cpl != 3)) {
			fault_GP(decoder->currentInstruction, 0);
			return 0;
		}
		if (write) {
			if (!pdeInfo.rw) {
				fault_GP(decoder->currentInstruction, 0);
				return 0;
			}
		}

		dword pdeStatus = dword_at(pdeAddress);
		if (!pdeInfo.accessed || !pdeInfo.dirty) {
			pdeStatus |= BIT_PAGE_ACCESSED_SET;
			if (write) {
				pdeStatus |= BIT_PAGE_DIRTY_SET;
			}
			set_dword_at(pdeAddress, pdeStatus);
		}
		resultEntry = pdeInfo;

	} else {
		// 4KB page
		dword basePteAddress = pdeInfo.address + pteAddress * 4;
		auto pteInfo = loadPageDirEntry(basePteAddress);
		if (!pteInfo.ok) {
			if (!pteInfo.present) {
				cRegisters[2] = pdeAddress;
				fault_PF(decoder->currentInstruction, basePteAddress);
				return 0;
			}
		}
		if (!pteInfo.user && (cpl != 3)) {
			fault_GP(decoder->currentInstruction, 0);
			return 0;
		}
		if (write) {
			if (!pdeInfo.rw) {
				fault_GP(decoder->currentInstruction, 0);
				return 0;
			}
		}
		if (!pteInfo.accessed || !pteInfo.dirty) {
			dword pteStatus = dword_at(basePteAddress);
			pteStatus |= BIT_PAGE_ACCESSED_SET;
			if (write) {
				pteStatus |= BIT_PAGE_DIRTY_SET;
			}
			set_dword_at(basePteAddress, pteStatus);
		}
		if (!pdeInfo.accessed || !pdeInfo.dirty) {
			dword pdeStatus = dword_at(pdeAddress);
			pdeStatus |= BIT_PAGE_ACCESSED_SET;
			pdeStatus |= BIT_PAGE_DIRTY_SET;
			set_dword_at(pdeAddress, pdeStatus);
		}
		resultEntry = pteInfo;
	}

	resultAddress = resultEntry.address + offset;
	return resultAddress;
}


