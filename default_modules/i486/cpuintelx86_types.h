#ifndef CPUINTELX86_TYPES
#define CPUINTELX86_TYPES

#include <QObject>

#include <constants.h>

#define ENABLE_DEBUG
//#undef ENABLE_DEBUG

constexpr dword
	// Flags
	FLAG_CF = 0x0001,
	FLAG_PF = 0x0004,
	FLAG_AF = 0x0010,
	FLAG_ZF = 0x0040,
	FLAG_SF = 0x0080,
	FLAG_TF = 0x0100,
	FLAG_IF = 0x0200,
	FLAG_DF = 0x0400,
	FLAG_OF	= 0x0800;

constexpr dword
	FLAG_CF_CLEAR = ~FLAG_CF,
	FLAG_PF_CLEAR = ~FLAG_PF,
	FLAG_AF_CLEAR = ~FLAG_AF,
	FLAG_ZF_CLEAR = ~FLAG_ZF,
	FLAG_SF_CLEAR = ~FLAG_SF,
	FLAG_OF_CLEAR = ~FLAG_OF;


const dword
	FLAG_VM			= 0x00020000;


const int
	FLAG_AF_SHIFT	= 4,
	FLAG_ZF_SHIFT	= 6,
	FLAG_CF_SHIFT	= 0,
	FLAG_PF_SHIFT	= 2;


// Register closure
typedef struct {
	union {
		struct {
			union {
				word reg16;		// low word
				struct {
					byte low8;
					byte high8;
				} byte;
			};
			word word_hi;		// filler
		} word;

		struct {			// OR
			dword reg32;		// single 32-bit evaluator
		} dword;
	};
} Register;

// Same as above with EFLAGS correction
typedef struct {
	union {
		struct {
		union {
			word f16;			// No flag 00x0 xx00
			struct {
				byte Flags_Lo;
				byte Flags_Hi;
			} flags;
		};
		word word_hi;
		} wflag;
		struct {
		dword flag32;
		} eflags;
	};
} FlagsRegister;


struct DescriptorInfo {
	byte	type;
	dword	base;
	dword	limit;
	byte	dpl;
	byte	present;
	byte	avl;
	byte	granularity;
	byte	defaultSize;
	byte	expandingDirection;
	byte	readable;
	byte	executable;
	byte	writable;
	byte	conforming;
	byte	accessed;
	word	selector;
	bool	isInLimits;
	bool	isGdt;
	byte	system;
	byte	code;
	byte	data;
	dword	minAddress;
	dword	maxAddress;

	dword	dw0, dw4;

	dword	descriptorAddress;
};

struct gateInfo {
	DescriptorInfo descr;
	dword	destination;
	word	selector;
	byte	paramCount;
	byte	callGate;
	byte	taskGate;
	byte	interruptGate;
	byte	trapGate;
	byte	busy;
};


/**
 * @brief Enum for handling JMPcc
 */
enum jumpEval {
	jO = 0,
	jNO = 1,
	jC = 2,
	jNC = 3,
	jZ = 4,
	jNZ = 5,
	jBE = 6,
	jA = 7,
	jS = 8,
	jNS = 9,
	jP = 10,
	jNP = 11,
	jL = 12,
	jGE = 13,
	jLE = 14,
	jG = 15,
	jCXZ = 16,
	jECXZ = 17
};

/**
 * @brief Evaluates loop kind on LOOPx instructions
 */
enum loopKind {
	lkLoopnz = 0, lkLoopz = 1, lkLoop = 2
};

enum repeatPrefix {repNone, repEqual, repNonEqual};



constexpr byte
	// parity table (fast-evaluator)
	Parity[256] = {
		0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0,
		1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1,
		1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1,
		0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0,
		1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1,
		0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0,
		0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0,
		1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1,
		1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1,
		0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0,
		0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0,
		1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1,
		0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0,
		1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1,
		1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1,
		0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0
	};



#endif // CPUINTELX86_TYPES

