#ifndef INTEL387X_H
#define INTEL387X_H

#include <cpuintelx86_types.h>

class CPUIntelx86;

class Intel387x
{
public:

	typedef word xWord;
	typedef dword xShort;
	typedef qword xLong;
	typedef float xSingleReal;
	typedef double xDoubleReal;
	typedef long double xExtendedReal;

	enum DataType {
		dtWord,
		dtShort,
		dtLong,
		dtSingle,
		dtDouble,
		dtExtended,
		dtPackedDecimal
	};

	typedef struct {
		union {
			xWord intWord;
			xShort intShort;
			xLong intLong;
			byte packedDecimal[10];
			xSingleReal realSingle;
			xDoubleReal realDouble;
			xExtendedReal realExtended;
		} data;
		DataType type;
	} StackElement;

	Intel387x(CPUIntelx86* cpu);

	void finit();
	void fldz();
	void fstReg(byte index, bool pop);
	StackElement fstRm32(bool pop);
	StackElement fstRm64(bool pop);
	StackElement fstRm80(bool pop);
	void fld64(qword data);

	void setIP(const dword ip) {
		this->instructionPointer = ip;
	}

	void setDataPointer(const dword ptr) {
		this->dataPointer = ptr;
	}

private:
	StackElement invalidElement;

	CPUIntelx86* cpu = nullptr;

	void push(StackElement el);
	StackElement pop();

	int stackTop;
	StackElement stack[8];
	byte tags[8];
	word controlReg;
	word statusReg;
	word tagReg;

	qword instructionPointer;
	qword dataPointer;

	void convertStackElementToExt(StackElement &el);
	void convertStackElementToSingle(StackElement &el);
	void convertStackElementToDouble(StackElement &el);

	static constexpr word
		BIT_INVALID_OPERATION = 0b00000001,
		BIT_DENORMALIZED_OPERAND = 0b00000010,
		BIT_ZERO_DIVIDE = 0b00000100,
		BIT_OVERFLOW = 0b00001000,
		BIT_UNDERFLOW = 0b00010000,
		BIT_PRECISION = 0b00100000,
		BIT_STACK_FAULT = 0b01000000,
		BIT_ERROR_SUMMARY = 0b10000000,
		BIT_STACK_TOP_CLEAR = 0b1100011111111111,
		BIT_STACK_TOP_SHIFT = 11,
		BIT_COND_SIGN = 0b0000000100000000,
		BIT_COND_AF = 0b0000001000000000,
		BIT_COND_CARRY = 0b0000010000000000,
		BIT_COND_ZERO = 0b0100000000000000,
		BIT_BUSY  = 0b1000000000000000
	;

	static constexpr byte
		TAG_VALID = 0b00,
		TAG_ZERO = 0b01,
		TAG_INVALID = 0b10,
		TAG_EMPTY = 0b11;

	friend class CPUIntelx86;

};

#endif // INTEL387X_H
