#ifndef STATUS_H
#define STATUS_H

#include <QFrame>
#include <QTime>
#include <QMap>
#include <QString>

namespace Ui {
	class Status;
}

class StatusWidget : public QFrame
{
	Q_OBJECT

public:
	explicit StatusWidget(QWidget *parent = 0);
	~StatusWidget();

public slots:
	void slotUpdateInfo(QMap<QString,QString> params);

signals:
	void signalPullCX();

private slots:
	void on_pushButton_clicked();

private:
	Ui::Status *ui;
	QTime time;
};

#endif // STATUS_H
