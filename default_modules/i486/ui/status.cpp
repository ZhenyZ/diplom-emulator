#include "status.h"
#include "ui_status.h"
#include <QDebug>

StatusWidget::StatusWidget(QWidget *parent) :
	QFrame(parent),
	ui(new Ui::Status)
{
	ui->setupUi(this);
}

StatusWidget::~StatusWidget() {
	delete ui;
}

void StatusWidget::slotUpdateInfo(QMap<QString,QString> params) {

	//qDebug() << "StatusWidget update in thread " << this->thread();

	ui->lax->setText(params.value("eax"));
	ui->lcx->setText(params.value("ecx"));
	ui->ldx->setText(params.value("edx"));
	ui->lbx->setText(params.value("ebx"));
	ui->lsp->setText(params.value("esp"));
	ui->lbp->setText(params.value("ebp"));
	ui->lsi->setText(params.value("esi"));
	ui->ldi->setText(params.value("edi"));

	ui->les->setText(params.value("es"));
	ui->lcs->setText(params.value("cs"));
	ui->lss->setText(params.value("ss"));
	ui->lds->setText(params.value("ds"));
	ui->lfs->setText(params.value("fs"));
	ui->lgs->setText(params.value("gs"));

	ui->label->setText(params.value("flags"));

	ui->lmode->setText(params.value("opmode"));

}

void StatusWidget::on_pushButton_clicked()
{
	emit signalPullCX();
}
