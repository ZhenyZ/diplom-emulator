#include "cpuguiwrapper.h"

#include <QApplication>
#include <QDebug>

CpuGuiWrapper::CpuGuiWrapper(QObject *parent)
	: QObject(parent)
{
	this->moveToThread(QApplication::instance()->thread());
}

CpuGuiWrapper::~CpuGuiWrapper() {
	refreshTimer->deleteLater();
	statusWidget->deleteLater();
}

void CpuGuiWrapper::slotInit()
{
	refreshTimer = new QTimer();
	refreshTimer->setSingleShot(false);
	refreshTimer->setInterval(250);

	connect(refreshTimer, SIGNAL(timeout()), this, SIGNAL(signalRequestUpdateStatus()), Qt::QueuedConnection);

	statusWidget = new StatusWidget();
	connect(this, SIGNAL(signalHide()), statusWidget, SLOT(hide()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalShow()), statusWidget, SLOT(show()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalUpdateStatus(QMap<QString,QString>)), statusWidget, SLOT(slotUpdateInfo(QMap<QString,QString>)), Qt::QueuedConnection);
	connect(statusWidget, SIGNAL(signalPullCX()), this, SIGNAL(signalPullCX()), Qt::QueuedConnection);

	refreshTimer->start();
}
