#ifndef CPUGUIWRAPPER_H
#define CPUGUIWRAPPER_H

#include <QObject>
#include <QTimer>

#include "status.h"

class CpuGuiWrapper : public QObject
{
	Q_OBJECT
public:
	explicit CpuGuiWrapper(QObject *parent = 0);
	virtual ~CpuGuiWrapper();

signals:
	void signalShow();
	void signalHide();
	void signalUpdateStatus(QMap<QString,QString> data);
	void signalRequestUpdateStatus();

	void signalPullCX();

public slots:
	void slotInit();

private:
	StatusWidget* statusWidget = nullptr;
	QTimer* refreshTimer = nullptr;
};

#endif // CPUGUIWRAPPER_H
