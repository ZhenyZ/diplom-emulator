#ifndef DISPLAYUIWRAPPER_H
#define DISPLAYUIWRAPPER_H

#include <QObject>
#include <QDebug>

#include "displayui.h"
#include "display_global.h"

class DISPLAYSHARED_EXPORT DisplayUiWrapper : public QObject
{
	Q_OBJECT
public:
	explicit DisplayUiWrapper(QObject *parent = 0);

signals:
	void signalSetVideoMemory(byte**, qword*);
	void signalShowDisplayUi();
	void signalHideDisplayUi();
	void signalRedraw();
	void signalSetVideModeText();
	void signalSetVideModeGraphical();
	void signalSetDisplaySwitchedOff();
	void signalSetDisplaySwitchedOn();
	void signalSetDimensions(dword height, dword width);

	void signalKeyPress(dword scancode, int sz);
	void signalKeyRelease(dword scancode, int sz);

public slots:
	void slotInit();
	void slotReturn(QThread* thr);

private:
	DisplayUi* displayUi = nullptr;
};

#endif // DISPLAYUIWRAPPER_H
