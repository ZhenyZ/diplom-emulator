#include "displayui.h"

#include "vgafonts.h"

#include <GL/gl.h>
#include <QOpenGLFunctions>
#include <QOpenGLFunctions_ES2>
#include <QtWidgets>
#include <QtGui>
#include <omp.h>
#include <QApplication>

#include <QDebug>

DisplayUi::DisplayUi(QWidget* parent)
	: QGLWidget(parent)
{
	this->moveToThread(QApplication::instance()->thread());
//	qDebug() << "DisplayGui thread " << this->thread();
	memory = nullptr;
	memSize = 0;
	memPtrTransit = &memory;
	memSizeTransit = &memSize;

	w = 320;
	h = 200;
	scaleWidth = 1;
	scaleHeight = 1;
	switchedOn = true;
	redrawTimer = new QTimer();
	redrawTimer->setSingleShot(false);
	redrawTimer->setInterval(200);
	desktopRect = QApplication::desktop()->screenGeometry();
//	qDebug() << "DisplayUi created in thread " << this->thread();
	connect(redrawTimer, SIGNAL(timeout()), this, SLOT(slotRepaint()));
	redrawTimer->start();
	this->setGeometry(0,0,200,200);
}

dword DisplayUi::getScanCode(int key, qint32 nativeScanCode) {
	dword result = 0;
	switch (key) {
		case Qt::Key_Home:
			result = 0x47E02AE0;
		break;
		case Qt::Key_PageUp:
			result = 0x49E02AE0;
		break;
		case Qt::Key_PageDown:
			result = 0x51E02AE0;
		break;
		case Qt::Key_End:
			result = 0x4FE02AE0;
		break;
		case Qt::Key_Right:
			result = 0x4DE02AE0;
		break;
		case Qt::Key_Down:
			result = 0x50E02AE0;
		break;
		case Qt::Key_Left:
			result = 0x4BE02AE0;
		break;
		case Qt::Key_Up:
			result = 0x48E02AE0;
		break;
		default:
			result = nativeScanCode;
		break;
	}
	return result;
}

void DisplayUi::keyPressEvent(QKeyEvent *e)
{
//	dword key = getScanCode(e->key(), e->nativeScanCode());
//	int size = 0;
//	if (key & 0xFF000000) {
//		size = 4;
//	} else
//	if (key & 0x00FF0000) {
//		size = 3;
//	} else
//	if (key & 0x0000FF00) {
//		size = 2;
//	} else {
//		size = 1;
//	}
//	qDebug() << "Press " << hex << key;
//	emit signalKeyPress(key, size);
	emit signalKeyPress(e->nativeScanCode() & 0xFF, 1);
}

void DisplayUi::keyReleaseEvent(QKeyEvent *e)
{
//	dword key = getScanCode(e->key(), e->nativeScanCode());
//	int size = 0;
//	if (key & 0xFF000000) {
//		size = 4;
//		key |= 0x80008000;
//	} else
//	if (key & 0x00FF0000) {
//		size = 3;
//		key |= 0x00008000;
//	} else
//	if (key & 0x0000FF00) {
//		size = 2;
//		key |= 0x00008000;
//	} else {
//		size = 1;
//		key |= 0x00000080;
//	}
//	qDebug() << "Release " << hex << key;
//	emit signalKeyRelease(key, size);
	emit signalKeyPress((e->nativeScanCode() & 0xFF) | 0x80, 1);
}

void DisplayUi::setVideoMemory(byte ** mem, qword * size) {
	*this->memPtrTransit = *mem;
	*this->memSizeTransit = *size;
}

DisplayUi::~DisplayUi() {
	glDeleteTextures(256, texid);
}

void DisplayUi::slotGuiUpdate() {
	//	this->memory = *this->memPtrTransit;
//	this->memSize = *this->memSizeTransit;

	if (displayMode == DisplayMode::Text) {
		w = (displayedWidth)*8;
		h = (displayedHeight)*14;
	}
	else {
		w = displayedWidth;
		h = displayedHeight;
	}

	qreal origW = w;
	qreal origH = h;

	qreal maxW = this->width();
	qreal maxH = this->height();

//	qreal maxW = desktopRect.width() / 2.0;
//	qreal maxH = desktopRect.height() / 2.0;

	qreal aspect = maxW / origW;

	qreal newW = origW * aspect;
	qreal newH = origH * aspect;

	if (newW < 100 || newH < 100) {
		newW = 100;
		newH = 100;
	}

	if (newH > maxH) {
		aspect = maxH / origH;
		newW = origW * aspect;
		newH = origH * aspect;
	}

	scaleToDisplay = aspect;

	this->resizeGL(newW, newH);
}

void DisplayUi::resizeGL(int nWidth, int nHeight) {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glViewport(0, 0, nWidth, nHeight);
	QGLWidget::resizeGL(nWidth, nHeight);
	this->repaint();
}

void DisplayUi::loadFonts() {

	const int H = 14;
	const int W = 8;

	GLfloat curchar[H][W][4];
	glEnable(GL_TEXTURE_2D);

	for (int n = 0; n < 256; n++) {
		int charindex = n * H;
		for (int y = 0; y < H; y++) {
			int strindex = charindex + y;
			for (int x = 0; x < W; x++) {
				GLfloat val;
				if ((vgafont14[strindex] & Constants::pow2[x]) == 0) {
					val = 0;
				} else {
					val = 1;
				}
				curchar[y][x][0] = val;
				curchar[y][x][1] = val;
				curchar[y][x][2] = val;
				curchar[y][x][3] = val;
			}
		}
		glGenTextures(1, &texid[n]);
		glBindTexture(GL_TEXTURE_2D, texid[n]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, W, H, 0, GL_RGBA, GL_FLOAT, curchar);
	}
	glDisable(GL_TEXTURE_2D);
}

void DisplayUi::paintGL() {

	if (memory == nullptr) {
		QGLWidget::paintGL();
		return;
	}

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();

	if (displayMode == DisplayMode::Text) {
		glOrtho(0, displayedWidth, 0, displayedHeight, -1, 1);
	} else {
		glOrtho(0, w, 0, h, -1 ,1);
	}

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glClear(GL_COLOR_BUFFER_BIT);
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glLoadIdentity(); // Сброс просмотра
	glTranslatef(0, 0, -1);
	glPushAttrib(GL_ALL_ATTRIB_BITS);


	if (switchedOn) {
		if (displayMode == DisplayMode::Text) {
			drawTexts();
		}
		else {
			qword address;
			const int bitTable[4] = {0xC0, 0x30, 0x0C, 0x03};
			const int shorTable[4] = {6, 4, 2, 0};

			glPointSize(1.8f * this->scaleToDisplay);
			for (unsigned x = 0; x < displayedWidth; x++) {
				for (unsigned y = 0; y < displayedHeight; y++) {
					//E = 0xB8000 + 0x2000 * (k%2) + 80 * (k/2) + (i/4);
					auto currentBit = x % 4;
					auto byteIndexInRow = x / 4;
					address = 0x2000 * (y % 2) + 80 * (y / 2) + byteIndexInRow;
					if (address >= memSize) {
						continue;
					}
					auto colorIndex = (memory[address] & bitTable[currentBit]) >> shorTable[currentBit];
					glColor3f(Colors[colorIndex][0], Colors[colorIndex][1], Colors[colorIndex][2]);
					glBegin(GL_POINTS);
					glVertex2f(x, displayedHeight-y);
					glEnd();
				}
			}
		}
		//callOutputByIndex(0);
		//callOutputByIndex(1);
		//VGA->cursorX = (VGA->cursorPos) % (VGA->width);
		//VGA->cursorY = (VGA->cursorPos) / (VGA->width);

	}

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glFlush();
}

void DisplayUi::drawTexts() {

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	unsigned ht = displayedHeight;

	for (dword y = 0; y < displayedHeight; y++) {
		dword E = 2 * y * displayedWidth;
		for (unsigned k = 0; k < displayedWidth; k++) {
			byte color, bgcolor;

			qword addressColor = E+k*2+1;
			if (addressColor >= memSize) {
				continue;
			}
			color = (memory[addressColor] & 0x0F);
			bgcolor = ((memory[addressColor] & 0x70) >> 4);
			glColor3f(Colors[bgcolor][0], Colors[bgcolor][1], Colors[bgcolor][2]);
			glBegin(GL_QUADS);
				glVertex2f(k, ht-y-1);
				glVertex2f(k, ht-y);
				glVertex2f(k+1, ht-y);
				glVertex2f(k+1, ht-y-1);
			glEnd();

			glEnable(GL_TEXTURE_2D);
			glColor3f(Colors[color][0], Colors[color][1], Colors[color][2]);
			glBindTexture(GL_TEXTURE_2D, texid[*reinterpret_cast<byte*>(memory+E+k*2)]);
			glBegin(GL_QUADS);
				glTexCoord2i(1, 1); glVertex2f(k, ht-y-1);
				glTexCoord2i(1, 0); glVertex2f(k, ht-y);
				glTexCoord2i(0, 0); glVertex2f(k+1, ht-y);
				glTexCoord2i(0, 1); glVertex2f(k+1, ht-y-1);
			glEnd();
			glDisable(GL_TEXTURE_2D);
		}
	}
}

void DisplayUi::slotRepaint() {
	this->slotGuiUpdate();
}

void DisplayUi::slotSetVideoModeText() {
	displayMode = DisplayMode::Text;
	slotGuiUpdate();
}

void DisplayUi::slotSetVideoModeGraphics() {
	displayMode = DisplayMode::Graphical;
	qDebug() << "*** *** Set graphical *** ***";
	slotGuiUpdate();
}

void DisplayUi::slotSetDisplaySwitchedOff() {
	switchedOn = false;
}

void DisplayUi::slotSetDisplaySwitchedOn() {
	switchedOn = true;
}

void DisplayUi::slotSetVideoMemory(byte ** mem, qword * size) {
	setVideoMemory(mem, size);
}

void DisplayUi::slotSetDimensions(dword height, dword width) {
	displayedHeight = height;
	displayedWidth = width;
	qDebug() << "DisplayUi set Dimensions = " << width << "x" << height;
}

void DisplayUi::initializeGL() {
	qglClearColor(Qt::black);
	loadFonts();
}

