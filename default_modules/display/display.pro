QT += core gui widgets opengl

TARGET = displaygeneric
TEMPLATE = lib
CONFIG += c++11 opengl

windows {
	LIBS += -lopengl32
}

linux {
	LIBS += -lGL
}

DEFINES += DISPLAY_LIBRARY

INCLUDEPATH += $$PWD/../lib/

SOURCES += \ 
    displaygl.cpp \
    displayui.cpp \
    displayuiwrapper.cpp

CONFIG(debug, debug|release) {
	LIBS += -L$$PWD/../lib/debug -lEMUESDebugLib -lEMUESDevicesLib
}
CONFIG(release, debug|release) {
	LIBS += -L$$PWD/../lib/release -lEMUESDebugLib -lEMUESDevicesLib
}

HEADERS += display_global.h \ 
    displaygl.h \
    displayui.h \
    displayuiwrapper.h \
    vgafonts.h


