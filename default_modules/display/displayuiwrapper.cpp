#include "displayuiwrapper.h"

#include <QDebug>

DisplayUiWrapper::DisplayUiWrapper(QObject *parent) : QObject(parent)
{
	this->moveToThread(QApplication::instance()->thread());
//	qDebug() << "DisplayWrap thread " << this->thread();
}

void DisplayUiWrapper::slotInit()
{
	displayUi = new DisplayUi();
	connect(this, SIGNAL(signalHideDisplayUi()), displayUi, SLOT(hide()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalRedraw()), displayUi, SLOT(slotRepaint()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalSetDisplaySwitchedOff()), displayUi, SLOT(slotSetDisplaySwitchedOff()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalSetDisplaySwitchedOn()), displayUi, SLOT(slotSetDisplaySwitchedOn()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalSetVideModeGraphical()), displayUi, SLOT(slotSetVideoModeGraphics()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalSetVideModeText()), displayUi, SLOT(slotSetVideoModeText()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalSetVideoMemory(byte**, qword*)), displayUi, SLOT(slotSetVideoMemory(byte**, qword*)), Qt::QueuedConnection);
	connect(this, SIGNAL(signalSetDimensions(dword,dword)), displayUi, SLOT(slotSetDimensions(dword,dword)), Qt::QueuedConnection);
	connect(this, SIGNAL(signalShowDisplayUi()), displayUi, SLOT(show()), Qt::QueuedConnection);

	connect(displayUi, SIGNAL(signalKeyPress(dword, int)), this, SIGNAL(signalKeyPress(dword, int)), Qt::QueuedConnection);
	connect(displayUi, SIGNAL(signalKeyRelease(dword, int)), this, SIGNAL(signalKeyRelease(dword, int)), Qt::QueuedConnection);
}

void DisplayUiWrapper::slotReturn(QThread * thr) {
	this->moveToThread(thr);
	//		qDebug() << "DisplayWrap slotReturn in thread " << thr;
}
