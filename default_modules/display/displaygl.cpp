#include "displaygl.h"
#include "vgafonts.h"

#include <QWidget>
#include <QDebug>

DisplayGL::DisplayGL(QObject * parent)
	: QObject(parent),
	  ModuleDevice("Display", "Eugene Shestakov", 1)
{
	currentScanCode = 0;
	currentScanSize = 1;
	memSizeTransit = new qword*[1];
	initIo();
}

void DisplayGL::init() {

}

void DisplayGL::tick() {
	emit signalRedraw();
}

dword DisplayGL::getScanCode(int) {
	return this->currentScanCode;
}

void DisplayGL::showUi() {
	emit signalShowDisplayUi();
	QApplication::processEvents();
}

void DisplayGL::hideUi() {
	emit signalHideDisplayUi();
	QApplication::processEvents();
}

void DisplayGL::createUi() {
	displayUiWrapper = new DisplayUiWrapper();
	connect(this, SIGNAL(signalHideDisplayUi()), displayUiWrapper, SIGNAL(signalHideDisplayUi()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalShowDisplayUi()), displayUiWrapper, SIGNAL(signalShowDisplayUi()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalRedraw()), displayUiWrapper, SIGNAL(signalRedraw()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalSetDisplaySwitchedOff()), displayUiWrapper, SIGNAL(signalSetDisplaySwitchedOff()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalSetDisplaySwitchedOn()), displayUiWrapper, SIGNAL(signalSetDisplaySwitchedOn()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalSetVideModeGraphical()), displayUiWrapper, SIGNAL(signalSetVideModeGraphical()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalSetVideModeText()), displayUiWrapper, SIGNAL(signalSetVideModeText()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalSetVideoMemory(byte**, qword*)), displayUiWrapper, SIGNAL(signalSetVideoMemory(byte**, qword*)), Qt::QueuedConnection);
	connect(this, SIGNAL(signalWrapperInit()), displayUiWrapper, SLOT(slotInit()), Qt::QueuedConnection);
	connect(this, SIGNAL(signalWrapperReturn(QThread*)), displayUiWrapper, SLOT(slotReturn(QThread*)), Qt::QueuedConnection);
	connect(this, SIGNAL(signalSetDimensions(dword,dword)), displayUiWrapper, SIGNAL(signalSetDimensions(dword,dword)), Qt::QueuedConnection);

	connect(displayUiWrapper, SIGNAL(signalKeyPress(dword, int)), this, SLOT(slotKeyPress(dword, int)), Qt::QueuedConnection);
	connect(displayUiWrapper, SIGNAL(signalKeyRelease(dword, int)), this, SLOT(slotKeyRelease(dword, int)), Qt::QueuedConnection);

	emit signalWrapperInit();
	emit signalWrapperReturn(this->thread());
}

void DisplayGL::moduleReset() {
	init();
}

void DisplayGL::moduleShutdown() {
	//delete displayUiWrapper;
}

void DisplayGL::onBeforeStart() {
	emit signalSetVideoMemory(this->memPtrTransit, *this->memSizeTransit);
	emit signalRedraw();
}

void DisplayGL::initIo() {
	registerReceiveSignalFunction(0, "Set text mode", std::bind(&DisplayGL::slotSetVideoModeText, this));
	registerReceiveSignalFunction(1, "Set graphical mode", std::bind(&DisplayGL::slotSetVideoModeGraphics, this));
	registerReceiveSignalFunction(2, "Switch off", std::bind(&DisplayGL::slotSetDisplaySwitchedOff, this));
	registerReceiveSignalFunction(3, "Switch on", std::bind(&DisplayGL::slotSetDisplaySwitchedOn, this));
	registerInput(0, "Height input port", std::bind(&DisplayGL::slotSetHeight, this, std::placeholders::_1, std::placeholders::_2));
	registerInput(1, "Width input port", std::bind(&DisplayGL::slotSetWidth, this, std::placeholders::_1, std::placeholders::_2));

	ModuleDevice::DataMappingBlock videoMemBlock = std::make_pair(&this->memPtrTransit, this->memSizeTransit);
	registerDataMapConsumer(0, "Video Memory block transfer", videoMemBlock);

	registerOutput(OUT_KEY_PRESS, "Keyboard press signal", std::bind(&DisplayGL::getScanCode, this, std::placeholders::_1));
	registerOutput(OUT_KEY_RELEASE, "Keyboard release signal", std::bind(&DisplayGL::getScanCode, this, std::placeholders::_1));
}

DisplayGL::~DisplayGL() {
	delete [] memSizeTransit;
}

void DisplayGL::slotKeyPress(dword scancode, int size) {
	this->currentScanCode = scancode;
	this->currentScanSize = 1;
	callOutputByIndex(OUT_KEY_PRESS, 1);
}

void DisplayGL::slotKeyRelease(dword scancode, int size) {
	this->currentScanCode = scancode;
	this->currentScanSize = 1;
	callOutputByIndex(OUT_KEY_RELEASE, 1);
}

void DisplayGL::slotSetVideoModeText() { emit signalSetVideModeText(); }

void DisplayGL::slotSetVideoModeGraphics() { emit signalSetVideModeGraphical(); }

void DisplayGL::slotSetDisplaySwitchedOff() { emit signalSetDisplaySwitchedOff(); }

void DisplayGL::slotSetDisplaySwitchedOn() { emit signalSetDisplaySwitchedOn(); }

void DisplayGL::slotSetWidth(const dword val, int) {
	width = val;
	emit signalSetDimensions(height, width);
}

void DisplayGL::slotSetHeight(const dword val, int) {
	height = val;
	emit signalSetDimensions(height, width);
}

ModuleDevice* createModule() {
	return new DisplayGL();
}
