#ifndef DISPLAY_H
#define DISPLAY_H

#include <QGLWidget>
#include <QtOpenGL>
#include <QTimer>
#include <QDebug>

#include <moduledevice.h>
#include "displayuiwrapper.h"

#include "display_global.h"

class DISPLAYSHARED_EXPORT DisplayGL : public QObject, public ModuleDevice
{
	Q_OBJECT

private:
	const int
		OUT_KEY_PRESS = 0,
		OUT_KEY_RELEASE = 1;

public:
	DisplayGL(QObject* parent = nullptr);
	virtual ~DisplayGL();

	void init();

	virtual void tick() override;

	dword getScanCode(int);

	virtual void showUi() override;
	virtual void hideUi() override;
	virtual void createUi() override;
	virtual void moduleReset() override;
	virtual void moduleShutdown() override;

	virtual void onBeforeStart() override;

signals:
	void signalSetVideoMemory(byte**, qword*);
	void signalShowDisplayUi();
	void signalHideDisplayUi();
	void signalRedraw();
	void signalSetVideModeText();
	void signalSetVideModeGraphical();
	void signalSetDisplaySwitchedOff();
	void signalSetDisplaySwitchedOn();
	void signalSetDimensions(dword height, dword width);

	void signalWrapperInit();
	void signalWrapperReturn(QThread*);

public slots:
	void slotKeyPress(dword scancode, int size);
	void slotKeyRelease(dword scancode, int size);

protected:

	dword currentScanCode;
	int currentScanSize;

	DisplayUiWrapper * displayUiWrapper = nullptr;

	void slotSetVideoModeText();
	void slotSetVideoModeGraphics();
	void slotSetDisplaySwitchedOff();
	void slotSetDisplaySwitchedOn();
	void slotSetWidth(const dword val, int);
	void slotSetHeight(const dword val, int);

	bool highHeight = false;
	bool highWidth = false;

//	byte* memory = nullptr;
//	qword memSize = 0;
	ModuleMemPtr memPtrTransit = nullptr;
	qword** memSizeTransit = nullptr;

	dword height = 0;
	dword width = 0;

private:

	void initIo();
};

extern "C" DISPLAYSHARED_EXPORT ModuleDevice* createModule();

#endif
