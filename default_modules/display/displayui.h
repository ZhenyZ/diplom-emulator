#ifndef DISPLAYUI_H
#define DISPLAYUI_H

#include <QGLWidget>
#include <QtOpenGL>

#include "display_global.h"
#include <constants.h>

const
   float Colors [0x10][3]=
		{{0,0,0}, {0,0,1}, {0,0.5f,0},
		{0, 0.5f, 0.5f}, {1,0,0}, {0.5f,0,0.5f},
		{0.5f,0.5f,0}, {0.7f,0.7f,0.7f}, {0.5f,0.5f,0.5f},
		{0,1,1}, {0,1,0}, {0,0.5f,1},
		{1,0,1}, {1,0,1}, {1,1,0}, {1,1,1}};

class DISPLAYSHARED_EXPORT DisplayUi : public QGLWidget
{
	Q_OBJECT
public:
	DisplayUi(QWidget * parent = nullptr);
	~DisplayUi();

protected:
	virtual void initializeGL() override;
	virtual void resizeGL(int nWidth, int nHeight) override;
	virtual void paintGL() override;

	void showInfo();

	void drawTexts();
	void loadFonts();

	virtual void keyPressEvent(QKeyEvent* e) override;
	virtual void keyReleaseEvent(QKeyEvent* e) override;

private:
	GLuint texid[256];
	GLuint listid[256];
	dword w = 0, h = 0;
	qreal scaleWidth, scaleHeight;
	qreal scaleToDisplay;

	byte* memory = nullptr;
	byte** memPtrTransit = nullptr;
	qword memSize = 0;
	qword* memSizeTransit = nullptr;

	QTimer* redrawTimer = nullptr;

	enum DisplayMode {
		Graphical,
		Text
	};

	DisplayMode displayMode = DisplayMode::Text;
	bool switchedOn = false;	// VGA->DIsplayOn

	dword displayedHeight = 0, displayedWidth = 0;	// VGA->height, width

	void setVideoMemory(byte ** mem, qword* size);

	QRect desktopRect;
	dword getScanCode(int key, qint32 nativeScanCode);

public slots:
	void slotGuiUpdate();
	void slotRepaint();
	void slotSetVideoModeText();
	void slotSetVideoModeGraphics();
	void slotSetDisplaySwitchedOff();
	void slotSetDisplaySwitchedOn();
	void slotSetVideoMemory(byte ** mem, qword* size);

	void slotSetDimensions(dword height, dword width);

signals:
	void signalKeyPress(dword scancode, int size);
	void signalKeyRelease(dword scancode, int size);

};

#endif // DISPLAYUI_H
