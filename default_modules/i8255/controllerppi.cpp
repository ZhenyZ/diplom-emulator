#include "controllerppi.h"

#include <QDebug>

ModulePPI::ModulePPI(QObject * parent)
	: Loggable("PPI", parent),
	  ModuleDevice("Intel 8255 Peripherial interface", "Eugene Shestakov", 11)
{
	init();
	initIo();
}

ModulePPI::~ModulePPI() {}

void ModulePPI::tick() {}

void ModulePPI::initIo() {
	registerInput(INPUT_KEYBOARD_DATA, "Write keyboard data", std::bind(&ModulePPI::io_writeKeyboardData, this, std::placeholders::_1, std::placeholders::_2));
	registerInput(INPUT_PORT, "Set port data", std::bind(&ModulePPI::io_setPort, this, std::placeholders::_1, std::placeholders::_2));
	registerInput(INPUT_CONTROL_REGISTER, "Set control register", std::bind(&ModulePPI::io_setControlRegister, this, std::placeholders::_1, std::placeholders::_2));
	registerInput(INPUT_COMMAND, "Write command", std::bind(&ModulePPI::io_writeCommand, this, std::placeholders::_1, std::placeholders::_2));
	registerInput(INPUT_TIMER2CHANNEL, "Timer channel 2 value", std::bind(&ModulePPI::io_writeTimerChannel2Counter, this, std::placeholders::_1, std::placeholders::_2));
	registerInput(INPUT_KEYPRESS, "Key Press receiver", std::bind(&ModulePPI::keyPress, this, std::placeholders::_1, std::placeholders::_2));
	registerInput(INPUT_KEYRELEASE, "Key Release receiver", std::bind(&ModulePPI::keyRelease, this, std::placeholders::_1, std::placeholders::_2));

	registerOutput(0, "Read keyboard byte", std::bind(&ModulePPI::io_getKeybByte, this, std::placeholders::_1));
	registerOutput(1, "Read port B", std::bind(&ModulePPI::io_getPortB, this, std::placeholders::_1));
	registerOutput(2, "Read port C", std::bind(&ModulePPI::io_getPortC, this, std::placeholders::_1));
	registerOutput(3, "Read Control Register", std::bind(&ModulePPI::io_getControlRegister, this, std::placeholders::_1));
	registerOutput(4, "Read status", std::bind(&ModulePPI::io_getStatus, this, std::placeholders::_1));
	registerOutput(5, "Read system reg", std::bind(&ModulePPI::io_getSystemReg, this, std::placeholders::_1));

//	registerReceiveSignalFunction(0, "Channel 0 signal receiver", std::bind(&ModulePIT::channel0SignalEmit, this) );
	registerEmitSignalFunction(SIGNAL_INTERRUPT, "Interrupt signal");
	registerEmitSignalFunction(SIGNAL_REBOOT, "CPU Reset signal");
	registerEmitSignalFunction(SIGNAL_CLEAR_IRQ, "Clear IRQ");

}

void ModulePPI::moduleReset() {
	init();
}

void ModulePPI::moduleShutdown()
{

}


void ModulePPI::io_setControlRegister(dword value, int) {
	debug("Set CR = " + QString::number(value, 16) + "h");
	controlReg = value;
}

void ModulePPI::flushBuffer() {
	keybStatusReg &= CLEAR_INPUT_DATA;
	inBuffer = 0;
}

void ModulePPI::io_setPort(dword value, int) {
	debug("Set portB = " + QString::number(value, 16) + "h");
	if (!(regB & 0x40) && (value & 0x40)) {
		debug("[ Keyboard reset ]");
		flushBuffer();
		regC = (regC | 0x20);
		callSignalEmitByIndex(SIGNAL_INTERRUPT);
		hotStart = 1;
	}
	regB = value;
}

dword ModulePPI::io_getPortB(int) {
	//debug("Read PortB");
	int k = 0;

	clockData--;
	if (clockData > 3) {
		k = 1;
	} else {
		if (clockData == 0)
			clockData = 7;
		k = 0;
	}

	byte qA = ((regB & 0x2F) | (k * 0x10));
	return qA;
}

void ModulePPI::io_writeTimerChannel2Counter(dword value, int) {
	debug("PPI write timer channel2 out");
	this->timer2out = value;
}

dword ModulePPI::io_getPortC(int sz) {
	debug("Read PortC");

	callInputByIndex(INPUT_TIMER2CHANNEL, sz);
	if (regB & 0x80) {
		return (0x0E + 0x20 * timer2out);
	} else {
		return(0x20 * timer2out);
	}
}

dword ModulePPI::io_getControlRegister(int) {
	debug("Read Control register");
	return controlReg;
}

dword ModulePPI::io_getStatus(int) {
	debug("[Keyboard] : Read Status register");
	byte t = keybStatusReg;

	t |= (hotStart << 2);
	t |= 0x10;
	readInputByte();
	if (outTemp.size()) {
		tempCnt--;
		if (tempCnt <= 0) {
			writeOutputByte(outTemp.dequeue());
			tempCnt = 4;
		}
	}
	return t;
}

dword ModulePPI::io_getSystemReg(int) {
	dword result = systemReg;
	// Simulate mem refresh
	if (systemReg & 0x10) {
		systemReg &= (~0x10);
	} else {
		systemReg |= 0x10;
	}
	return result;
}

void ModulePPI::keyPress(dword key, int) {
	qDebug() << "PPI kp " << hex << key;
	debug("Keyboard press detected " + QString::number(key, 16) + "h");
	writeOutputByte(key & 0xFF);
	regA = (key & 0xFF);
	callSignalEmitByIndex(SIGNAL_INTERRUPT);
}

void ModulePPI::outTmp(byte b) {
	outTemp.enqueue(b);
}

void ModulePPI::keyRelease(dword key, int) {
	qDebug() << "PPI kr " << hex << key;
	debug("Keyboard release detected " + QString::number(key, 16) + "h");
	writeOutputByte((key & 0xFF) | 0x80);
	regA = ((key & 0xFF)  | 0x80);
	callSignalEmitByIndex(SIGNAL_INTERRUPT);
}

dword ModulePPI::io_getKeybByte(int) {
	debug("[Keyboard] : Read byte");
	byte k = readOutputByte();
	callSignalEmitByIndex(SIGNAL_CLEAR_IRQ);
	return k;
}

void ModulePPI::init() {
	this->inBuffer = 0;
	this->keybStatusReg = 0;
	this->command = 0;
	this->commandByte = 0;
	this->commandNext = 0;
	this->hotStart = 0;
	this->keybClockOn = 1;
	this->keybGenerateInterrupts = 1;
	this->keyboardON = 1;
	this->clockData = 7;
	this->tempCnt = 4;
}

void ModulePPI::io_writeKeyboardData(dword value, int) {

	byte tmpClkOn;

	if (command == 1) { // Issue command to 8042
		switch(commandNext) {
			case 0x60:
				debug(" [Keyboard] : Set CMD register");
				readInputByte();

				keybGenerateInterrupts = (value & 1);
				keybClockOn = !((value & 0x10) >> 4);
				tmpClkOn = !((commandByte & 0x10) >> 4);

				if (keybClockOn != tmpClkOn) {
					if (!tmpClkOn) {
						writeOutputByte(0xAA);
					}
				}

				commandByte = value;
			break;

			case 0xD1:
				debug(" [Keyboard] : Set output byte");
				command = 0;
				commandNext = 0;
			break;

		}
	}
	else {
		// Issue command to 8048
		switch (value) {

			case 0xF2:
				debug(" [Keyboardc] : Read id");
				command = 0;
				keybStatusReg |= SET_COMMAND;
				writeOutputByte(0xAB);
				writeOutputByte(0x83);
				if (keybGenerateInterrupts) {
					callSignalEmitByIndex(SIGNAL_INTERRUPT);
				}
			break;

			default:
				regA = value;
			break;
		}
	}
}

void ModulePPI::writeOutputByte(byte b) {
	keybStatusReg |= SET_OUTPUT_DATA;
	outBuffer.enqueue(b);
}

byte ModulePPI::readInputByte() {
	keybStatusReg &= CLEAR_INPUT_DATA;
	return inBuffer;
}

void ModulePPI::writeInputByte(byte b) {
	keybStatusReg |= SET_INPUT_DATA;
	inBuffer = b;
}

byte ModulePPI::readOutputByte() {
	byte b = 0;
	if (outBuffer.size()) {
		b = outBuffer.dequeue();
		debug(" [Keyboard] : respond [" + QString::number(b, 16) + "h].");
	}
	if (outBuffer.size() == 0) {
		keybStatusReg &= CLEAR_OUTPUT_DATA;
		debug(" [Keyboard] : No more output.");
	}
	else {
		debug(" [Keyboard] : " + QString::number(outBuffer.size()) + " more bytes to out.");
		if (keybGenerateInterrupts) {
			callSignalEmitByIndex(SIGNAL_INTERRUPT);
		}
	}
	return b;
}


void ModulePPI::io_writeCommand(dword value, int) {
	writeInputByte(value);
	switch (value) {
		case 0x20:
			debug(" [Keyboard] : Read CMD byte");
			regC = (regC | 0x20);
			outTmp(commandByte);
			keybStatusReg |= SET_COMMAND;
			command = 0;
		break;

		case 0x60:
			debug(" [Keyboard] : Prepare CMD");
			command = 1;
			commandNext = 0x60;
			keybStatusReg &= CLEAR_OUTPUT_DATA;
		break;

		case 0xA1:
			debug(" [Keyboard] : Read Controller version");
			command = 0;
			keybStatusReg |= SET_COMMAND;
			outTmp(0x2);
		break;

		case 0xAA: {
			debug(" [Keyboard] : Do Self-Test");
			command = 0;
			outBuffer.clear();
			outTmp(0x55);
			keybStatusReg |= SET_COMMAND;
			regC = (regC | 0x20);
			if (keybGenerateInterrupts) {
				callSignalEmitByIndex(SIGNAL_INTERRUPT);
			}
			hotStart = 1;
		} break;
		case 0xAB: {
			debug(" [Keyboard] : Do Extended Self-Test");
			command = 0;
			outBuffer.clear();
			outTmp(0x00);
			keybStatusReg |= SET_COMMAND;
			if (keybGenerateInterrupts) {
				callSignalEmitByIndex(SIGNAL_INTERRUPT);
			}
			regC = (regC | 0x20);
			hotStart = 1;
		} break;
		case 0xAD: {
			debug(" [Keyboard] : Switch OFF");
			command = 0;
			flushBuffer();
		} break;
		case 0xAE: {
			debug(" [Keyboard] : Switch ON");
			keyboardON = 1;
			command = 0;
			hotStart = 1;
		} break;
		case 0xC0: {
			debug(" [Keyboard] : Read InState");
			command = 0;
			keybStatusReg |= SET_COMMAND;
			outTmp(0x00);
		} break;
		case 0xD0: {
			debug(" [Keyboard] : Read OutPort");
			command = 0;
			keybStatusReg |= SET_COMMAND;
			outTmp(0);
		} break;
		case 0xD1: {
			debug(" [Keyboard] : Write output byte");
			command = 1;
			commandNext = 0xD1;
		} break;
		case 0xFE: {
			debug(" *** Reboot ***");
			callSignalEmitByIndex(SIGNAL_REBOOT);
		} break;
		case 0xFF: {
			debug(" [Keyboard] : Reset and do Self-Test");
			command = 0;
			outTmp(0xFA);
			keybStatusReg |= SET_COMMAND;
			regC = (regC | 0x20);
			if (keybGenerateInterrupts) {
				callSignalEmitByIndex(SIGNAL_INTERRUPT);
			}
			hotStart = 1;
		} break;
		default: {
			debug(" [Keyboard] : command " + QString::number(value, 16) + "h is not recognized");
		}
	}
}


void ModulePPI::debug(const QString & str)
{
	QMap<QString, QString> params;
	params.insert("type", "device");
	params.insert("message", str);
	doDebug(params);
}

ModuleDevice* createModule() {
	return new ModulePPI();
}

