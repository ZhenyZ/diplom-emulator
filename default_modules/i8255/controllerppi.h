#ifndef CONTROLLERPPI_H
#define CONTROLLERPPI_H

#include <QQueue>

#include <moduledevice.h>
#include <loggable.h>

#include "i8255_global.h"

class I8255SHARED_EXPORT ModulePPI : public Loggable, public ModuleDevice
{
	Q_OBJECT
public:
	ModulePPI(QObject * parent = nullptr);
	virtual ~ModulePPI();

	virtual void tick() override;

	void keyPress(dword key, int);
	void keyRelease(dword key, int);

	virtual void moduleReset() override;
	virtual void moduleShutdown() override;
	virtual void debug(const QString &str) override;

	void io_setControlRegister(dword value, int);
	void io_setPort(dword value, int);
	void io_writeKeyboardData(dword value, int);
	void io_writeCommand(dword value, int);
	void io_writeTimerChannel2Counter(dword value, int);

	dword io_getPortB(int);
	dword io_getPortC(int);
	dword io_getControlRegister(int);
	dword io_getKeybByte(int);
	dword io_getStatus(int);
	dword io_getSystemReg(int);

private:

	const byte
		CLEAR_OUTPUT_DATA	= 0xFE,
		CLEAR_INPUT_DATA	= 0xFD,
		SET_OUTPUT_DATA		= 0x01,
		SET_INPUT_DATA		= 0x02,
		SET_COMMAND			= 0x08,
		SET_DATA			= 0xF7;

	const int
		INPUT_KEYBOARD_DATA = 0,
		INPUT_PORT = 1,
		INPUT_CONTROL_REGISTER = 2,
		INPUT_COMMAND = 3,
		INPUT_TIMER2CHANNEL = 4,
		INPUT_KEYPRESS = 5,
		INPUT_KEYRELEASE = 6;

	const int
		SIGNAL_INTERRUPT = 0,
		SIGNAL_REBOOT = 1,
		SIGNAL_CLEAR_IRQ = 2;

	byte inBuffer;
	QQueue<byte> outBuffer;
	QQueue<byte> outTemp;

	byte regA = 0;
	byte regB = 0;
	byte regC = 0;
	byte controlReg = 0;
	byte systemReg = 0;


	void init();
	void initIo();



	word systemBit = 0;
	word command = 0;
	word keybGenerateInterrupts = 0;
	word commandNext = 0;
	word keybClockOn = 0;
	word hotStart = 0;

	byte timer2out = 0;

	byte clockData = 0;

	byte commandByte = 0;
	byte keybStatusReg = 0;
	byte keybOutputReg = 0;

	int keyboardON = 0;

	byte readOutputByte();
	byte readInputByte();
	void writeOutputByte(byte b);
	void writeInputByte(byte b);

	void outTmp(byte);

	int tempCnt = 0;

	void flushBuffer();

};

extern "C" I8255SHARED_EXPORT ModuleDevice* createModule();


#endif // CONTROLLERPPI_H
