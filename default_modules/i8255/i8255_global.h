#ifndef I8255_GLOBAL_H
#define I8255_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(I8255_LIBRARY)
#  define I8255SHARED_EXPORT Q_DECL_EXPORT
#else
#  define I8255SHARED_EXPORT Q_DECL_IMPORT
#endif

#endif 
