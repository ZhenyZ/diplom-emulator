QT -= gui

TARGET = i8255
TEMPLATE = lib
CONFIG += c++11

DEFINES += I8255_LIBRARY

INCLUDEPATH += $$PWD/../lib/

SOURCES += \ 
    controllerppi.cpp

HEADERS += \ 
    controllerppi.h \
    i8255_global.h

CONFIG(debug, debug|release) {
	LIBS += -L$$PWD/../lib/debug -lEMUESDebugLib -lEMUESDevicesLib
}
CONFIG(release, debug|release) {
	LIBS += -L$$PWD/../lib/release -lEMUESDebugLib -lEMUESDevicesLib
}

