#include "modulepit.h"
#include <QDebug>

#define PredefRead1(A) std::bind( [this](int sz)->byte { return A(sz); }))
#define PredefWrite1(A) std::bind( [this](byte val, int sz) { A(val, sz); }, std::placeholders::_1, std::placeholders::_2)
#define PredefReadThis(A) std::bind<dword>( A, this, std::placeholders::_1 )
#define PredefWriteThis(A) std::bind( A, this, std::placeholders::_1, std::placeholders::_2)

ModulePIT::ModulePIT(QObject * parent)
	: Loggable("PIT", parent),
	  ModuleDevice("Intel 8253 Programmable timer", "Eugene Shestakov", 1)
{
	init();
	initIo();
}

ModulePIT::~ModulePIT() {
	for (int i = 0; i < 4; ++i) {
		delete channels[i];
	}
}

void ModulePIT::tick() {
	for (int i = 0; i < 4; i++) {
		channels[i]->tick();
	}
}

void ModulePIT::init() {
	for (int i = 0; i < 4; i++) {
		channels[i] = new ChannelPIT(this);
		connect(channels[i], SIGNAL(signalDebugOutput(const QMap<QString,QString> &)), this, SIGNAL(signalDebugOutput(const QMap<QString,QString> &)), Qt::QueuedConnection);
		channels[i]->init();
	}
	channels[0]->setSignalInterruptCallback(std::bind(&ModulePIT::channel0SignalEmit, this));
}

void ModulePIT::initIo() {
	this->registerInput(0, "Set channel 1 counter", PredefWriteThis( & ModulePIT::io_writeChannel1Counter) );
	this->registerInput(1, "Set channel 2 counter", PredefWriteThis( & ModulePIT::io_writeChannel2Counter) );
	this->registerInput(2, "Set channel 3 counter", PredefWriteThis( & ModulePIT::io_writeChannel3Counter) );
	this->registerInput(3, "Set Mode register", PredefWriteThis( & ModulePIT::io_setModeReg ) );

	this->registerOutput(0, "Read channel 1 counter", PredefReadThis( & ModulePIT::io_readChannel1Counter ) );
	this->registerOutput(1, "Read channel 2 counter", PredefReadThis( & ModulePIT::io_readChannel2Counter ) );
	this->registerOutput(2, "Read channel 3 counter", PredefReadThis( & ModulePIT::io_readChannel3Counter ) );
	this->registerOutput(3, "Read channel 2 out value", PredefReadThis( & ModulePIT::io_readChannel2OutValue ) );

//	this->registerReceiveSignalFunction(0, "Channel 0 signal receiver", std::bind(&ModulePIT::channel0SignalEmit, this) );
	this->registerEmitSignalFunction(0, "Channel 0 signal");
}

void ModulePIT::io_writeChannel1Counter(dword value, int sizeInBytes) {
	channels[0]->io_setCounter(value);
}

void ModulePIT::io_writeChannel2Counter(dword value, int sizeInBytes) {
	channels[1]->io_setCounter(value);
}

void ModulePIT::io_writeChannel3Counter(dword value, int sizeInBytes) {
	channels[2]->io_setCounter(value);
}

void ModulePIT::io_setModeReg(dword value, int sizeInBytes) {
	byte lval = ((value & 0xC0) >> 6);

	// write [lval] channel mode
	if (lval != 0x3) {
		debug("Set " + QString::number(lval) + " Mode = " + QString::number((value & 0x0E) >> 1, 16) + "h");
		channels[lval]->io_setModeReg(value);
	}
}

dword ModulePIT::io_readChannel1Counter(int sizeInBytes) {
	return channels[0]->io_getCounter();
}

dword ModulePIT::io_readChannel2Counter(int sizeInBytes) {
	return channels[1]->io_getCounter();
}

dword ModulePIT::io_readChannel3Counter(int sizeInBytes) {
	return channels[2]->io_getCounter();
}

dword ModulePIT::io_readChannel2OutValue(int sizeInBytes) {
	return channels[1]->getOutputBit();
}

void ModulePIT::debug(const QString & str)
{
	QMap<QString, QString> params;
	params.insert("type", "device");
	params.insert("message", str);
	doDebug(params);
}

void ModulePIT::channelInterrupt() {
	this->callSignalEmitByIndex(0);
}

ModuleDevice* createModule() {
	return new ModulePIT();
}
