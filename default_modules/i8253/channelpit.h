#ifndef CHANNEL_PIT_H
#define CHANNEL_PIT_H

#include <QObject>
#include <constants.h>
#include <loggable.h>

#include <functional>

class ModulePIT;

class ChannelPIT : public Loggable
{
	Q_OBJECT
public:
	ChannelPIT(ModulePIT*);
	virtual ~ChannelPIT();

	void	init();

	byte	io_getCounter();
	void	io_setCounter(byte);

	void	io_setModeReg(byte);

	void	setInnerCounter(word);
	word	getInnerCounter() const;

	void	setInnerGate(int);
	int		getInnerGate() const;

	void	tick();
	byte getOutputBit() const {
		return this->outputBit;
	}

	void	callInterrupt() {
		if (this->interruptCallback)
			this->interruptCallback();
	}

	void	setSignalInterruptCallback(std::function<void()> cb) {
		this->interruptCallback = cb;
	}

	virtual void debug(const QString & str);

private:

	byte outputBit;
	byte allow;

	ModulePIT* myPIT;
	int Gate;
	word Counter;
	byte ModeReg;
	word Buffer;
	word rdMask, rdShor, wrMask, wrShor;
	word StartValue;
	word Mode;
	word ChannelMode;
	bool Pause;
	bool OnlyLow;
	bool Latch;
	bool Start;

	std::function<void()> interruptCallback = nullptr;
};

#endif
