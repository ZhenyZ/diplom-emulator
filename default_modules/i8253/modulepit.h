#ifndef MODULEPIT_H
#define MODULEPIT_H

#include <moduledevice.h>
#include <loggable.h>
#include "channelpit.h"
#include <QDebug>

#include "i8253_global.h"

class I8253SHARED_EXPORT ModulePIT : public Loggable, public ModuleDevice
{
	Q_OBJECT
public:
	ModulePIT(QObject * parent = nullptr);
	virtual ~ModulePIT();

	virtual void tick() override;

	void channelInterrupt();

	virtual void moduleReset() override {
		for (int i = 0; i < 4; i++) {
			channels[i]->init();
		}
	}
	virtual void moduleShutdown() override {
		for (int i = 0; i < 4; ++i) {
			disconnect(channels[i], SIGNAL(signalDebugOutput(QMap<QString,QString>)));
		}
	}

private:
	void init();
	void initIo();
	void channel0SignalEmit() {
		callSignalEmitByIndex(0);
	}


	void io_writeChannel1Counter(dword value, int sizeInBytes);
	void io_writeChannel2Counter(dword value, int sizeInBytes);
	void io_writeChannel3Counter(dword value, int sizeInBytes);
	void io_setModeReg(dword value, int sizeInBytes);

	dword io_readChannel1Counter(int sizeInBytes);
	dword io_readChannel2Counter(int sizeInBytes);
	dword io_readChannel3Counter(int sizeInBytes);

	dword io_readChannel2OutValue(int sizeInBytes);

	ChannelPIT* channels[3];

	virtual void debug(const QString &str);

};

extern "C" I8253SHARED_EXPORT ModuleDevice* createModule();

#endif // MODULEPIT_H
