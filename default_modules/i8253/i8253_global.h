#ifndef I8253_GLOBAL_H
#define I8253_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(I8253_LIBRARY)
#  define I8253SHARED_EXPORT Q_DECL_EXPORT
#else
#  define I8253SHARED_EXPORT Q_DECL_IMPORT
#endif

#endif 
