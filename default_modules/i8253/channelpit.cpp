#include "channelpit.h"
#include "modulepit.h"

ChannelPIT::ChannelPIT(ModulePIT * mypit)
	:Loggable("Channel PIT", mypit)
{
	myPIT = mypit;
}

ChannelPIT::~ChannelPIT()
{

}

void ChannelPIT::init() {
	Buffer = 0;
	ModeReg = 0;
	Counter = 0;
	StartValue = 0;
	rdMask = 0xFF;
	rdShor = 0;
	wrMask = 0xFF;
	wrShor = 0;
	Pause = true;
	OnlyLow = false;
	Latch = false;
	outputBit = 0;
	Gate = 1;
	Start = true;
	allow = 0;
}


void ChannelPIT::tick() {
	if (Pause || (outputBit && (ChannelMode != 3))) return;
	if (!allow) return;
	if (Start) {
		Start = false;
		return;
	}

	switch (ChannelMode) {

		// ====== Mode 0 =======
		case 0:
			if ((!Pause) && (!outputBit)) {
				if (Counter >= 4)
					Counter -= 4;
				else
					--Counter;
				if (Counter == 0) {
					outputBit = 1;
					callInterrupt();
				}
			}
		break;

		// ====== Mode 1 =======
		case 1:
			if ((!Pause) && (!outputBit)) {
				if (Counter >= 4)
					Counter -= 4;
				else
					--Counter;
			}
			if (Counter == 0) {
				outputBit = true;
				callInterrupt();
			}
		break;

		// ====== Mode 2 =======
		case 2:
			if ((!Pause) && (!outputBit)) {
				--Counter;
			}
			if (Counter == 0) {
				outputBit = 1;
				callInterrupt();
				Counter = StartValue;
				if (Counter) outputBit = 0;
			}
		break;

		// ====== Mode 3 =======
		case 3:
			if (!Pause) {
				if (Counter >= 4)
					Counter -= 4;
				else
					--Counter;
			}
			if (Counter < ((StartValue-1) / 2)) {
				outputBit = 0;
			} else outputBit = 1;
			if (Counter == 0) {
				outputBit = 1;
				callInterrupt();
				Counter = StartValue;
			}
		break;

	}
	//printf(" - - - Timer ctr = %d\n", Counter);
}

byte ChannelPIT::io_getCounter() {
	byte k;
	if (Latch) {
		k = ((Buffer & rdMask) >> rdShor);
		rdMask = (~rdMask);
		rdShor = 8 - rdShor;
		if (rdMask == 0x00FF) {
			Latch = false;
			rdMask = 0xFF00;
		}
		return k;
	}
	k = ((Counter & rdMask) >> rdShor);
	rdMask = (~rdMask);
	rdShor = 8 - rdShor;
//	debug("Read counter -> [" + QString::number(k, 16) + "h]");

	return k;
}

void ChannelPIT::io_setCounter(byte Value) {
	if (OnlyLow) {
		allow = 1;
		StartValue = Value;
		Counter = Value;
		wrMask = 0xFF00;
		wrShor = 8;
	} else {
		allow = 0;
		StartValue = (StartValue & (~wrMask)) + (Value << wrShor);
		Counter = (Counter & (~wrMask)) + (Value << wrShor);
		wrShor = 8 - wrShor;
		wrMask = (~wrMask);
	}
	if ((Gate) && (wrMask == 0xFF00)) {
		Pause = false;
		outputBit = 0;
	}
	if (wrMask == 0x00FF || OnlyLow) {
		Start = true;
		allow = 1;
	}
	//debug("Write counter <- [" + QString::number(Counter, 16) + "h]");
}

void ChannelPIT::io_setModeReg(byte Value) {
	if ( ((Value & 0x30) >> 4) == 0) {
		Buffer = Counter;
		Latch = true;
		rdMask = 0x00FF;
		rdShor = 0;
		wrMask = 0x00FF;
		wrShor = 0;
		return;
	}
	ModeReg = Value;
	ChannelMode = ((Value & 0x0E) >> 1);
	OnlyLow = false;

	Mode = ((Value & 0x30) >> 4);

	if (Mode == 1)
		OnlyLow = true;
	if (Mode == 2) {
		rdMask = 0xFF00;
		rdShor = 8;
	}
	if (Mode == 3)
		outputBit = 1;
}

void ChannelPIT::setInnerGate(int Value) {
	bool kPause = Pause;
	Gate = Value;
	Pause = !((((ModeReg & 0x0E) >> 1) != 1) && Value);
	if (kPause && (!Pause)) {
		switch (Mode) {
			case 0:
				if (StartValue == Counter)
					outputBit = 0;
			break;

			case 1:
				Counter = StartValue;
				outputBit = 0;
			break;
		}
	}
}

void ChannelPIT::debug(const QString & str) {
	QMap<QString, QString> params;
	params.insert("type", "device");
	params.insert("message", str);
	doDebug(params);
}
