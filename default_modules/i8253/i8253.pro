QT -= gui

TARGET = i8253
TEMPLATE = lib
CONFIG += c++11

DEFINES += I8253_LIBRARY

INCLUDEPATH += $$PWD/../lib/

SOURCES += \ 
    channelpit.cpp \
    modulepit.cpp

HEADERS += \ 
    channelpit.h \
    i8253_global.h \
    modulepit.h

CONFIG(debug, debug|release) {
	LIBS += -L$$PWD/../lib/debug -lEMUESDebugLib -lEMUESDevicesLib
}
CONFIG(release, debug|release) {
	LIBS += -L$$PWD/../lib/release -lEMUESDebugLib -lEMUESDevicesLib
}

