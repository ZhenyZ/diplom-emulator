#ifndef CS8230_H
#define CS8230_H

#include "cs8230_global.h"

#include <moduledevice.h>
#include <loggable.h>

class CS8230SHARED_EXPORT CS8230 : public Loggable, public ModuleDevice
{
	Q_OBJECT
public:
	CS8230(QObject* parent = nullptr);
	virtual ~CS8230();

	virtual void tick() override;
	virtual void debug(const QString &str) override;
	virtual void moduleReset() override;
	virtual void moduleShutdown() override;

	static constexpr int
		INPUT_INDEX = 0,
		INPUT_DATA = 1;

	static constexpr int
		OUTPUT_DATA = 0;

private:

	dword ioReadData(int);
	void ioSetIndex(dword val, int);
	void ioSetData(dword val, int);

	void init();

	byte index;

};

#endif // CS8230_H
