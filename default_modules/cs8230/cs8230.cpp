#include "cs8230.h"


CS8230::CS8230(QObject * parent)
	: Loggable("CS8230", parent),
	  ModuleDevice("CS8230", "Eugene Shestakov", 1)
{
	index = 0;
}

CS8230::~CS8230() {

}

void CS8230::tick() {

}

void CS8230::debug(const QString & str) {
	QMap<QString, QString> params;
	params.insert("type", "device");
	params.insert("message", str);
	doDebug(params);
}

void CS8230::moduleReset() {
	index = 0;
}

void CS8230::moduleShutdown() {

}

dword CS8230::ioReadData(int) {

}

void CS8230::ioSetIndex(dword val, int) {
	this->index = val;
}

void CS8230::ioSetData(dword val, int) {

}

void CS8230::init() {
	registerInput(INPUT_DATA, "Write data", std::bind(&CS8230::ioSetData, this, std::placeholders::_1, std::placeholders::_2));
	registerInput(INPUT_INDEX, "Set index", std::bind(&CS8230::ioSetIndex, this, std::placeholders::_1, std::placeholders::_2));

	registerOutput(OUTPUT_DATA, "Read data", std::bind(&CS8230::ioReadData, this, std::placeholders::_1));
}
