#ifndef CS8230_GLOBAL_H
#define CS8230_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(CS8230_LIBRARY)
#  define CS8230SHARED_EXPORT Q_DECL_EXPORT
#else
#  define CS8230SHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // CS8230_GLOBAL_H
