QT -= gui

TARGET = cs8230
TEMPLATE = lib

DEFINES += CS8230_LIBRARY

DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += $$PWD/../lib/

CONFIG(debug, debug|release) {
	LIBS += -L$$PWD/../lib/debug -lEMUESDebugLib -lEMUESDevicesLib
}
CONFIG(release, debug|release) {
	LIBS += -L$$PWD/../lib/release -lEMUESDebugLib -lEMUESDevicesLib
}

SOURCES += cs8230.cpp

HEADERS += cs8230.h\
        cs8230_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
