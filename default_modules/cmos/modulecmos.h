#ifndef CONTROLLERCMOS_H
#define CONTROLLERCMOS_H

#include <moduledevice.h>
#include <loggable.h>
#include "cmos_global.h"

class CMOSSHARED_EXPORT ModuleCMOS : public Loggable, public ModuleDevice
{
	Q_OBJECT
public:
	ModuleCMOS(QObject * parent = nullptr);
	virtual ~ModuleCMOS();

	dword io_read(int);
	void io_write(dword, int);
	void io_setAddr(dword value, int);

	void io_diagPoint(dword value, int);
	dword io_getDiagValue(int);

	void init();

	void load();
	byte diagPoint;

	virtual void tick() override;
	virtual void debug(const QString &str) override;
	virtual void moduleReset() override;
	virtual void moduleShutdown() override;

private:

	// CMOS is always in ready state

	int size;
	void save();

	word CMOSAddr;
	byte* CMOSMemory = nullptr;

};

extern "C" CMOSSHARED_EXPORT ModuleDevice * createModule();

#endif // CONTROLLERCMOS_H
