#ifndef CMOS_GLOBAL_H
#define CMOS_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(CMOS_LIBRARY)
#  define CMOSSHARED_EXPORT Q_DECL_EXPORT
#else
#  define CMOSSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // CMOS_GLOBAL_H
