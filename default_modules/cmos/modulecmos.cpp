#include "modulecmos.h"

#include <QDebug>
#include <QFile>
#include <QDataStream>
#include <QTime>

const QString FILE_NAME = "CMOS.BIN";

ModuleCMOS::ModuleCMOS(QObject * parent)
	: Loggable("CMOS", parent),
	ModuleDevice("CMOS", "Eugene Shestakov", 1)
{
	CMOSAddr = 0;
	size = 16384;
	CMOSMemory = new byte[size];
	memset(CMOSMemory, (byte)0x00, size);
	init();
}

void ModuleCMOS::init() {
	load();

	registerInput(0, "Set Address", std::bind(&ModuleCMOS::io_setAddr, this, std::placeholders::_1, std::placeholders::_2));
	registerInput(1, "Write data", std::bind(&ModuleCMOS::io_write, this, std::placeholders::_1, std::placeholders::_2));
	registerInput(2, "Write diagnostic point number", std::bind(&ModuleCMOS::io_diagPoint, this, std::placeholders::_1, std::placeholders::_2));

	registerOutput(0, "Read data", std::bind(&ModuleCMOS::io_read, this, std::placeholders::_1));
	registerOutput(1, "Read diagnostic point value", std::bind(&ModuleCMOS::io_getDiagValue, this, std::placeholders::_1));

	registerEmitSignalFunction(0, "Next diagnostic point signal");
}

void ModuleCMOS::io_diagPoint(dword val32, int) {
	byte value = val32 & 0xFF;
	diagPoint = value;
	debug("Diagnostic point " + QString::number(diagPoint, 16));
	//qDebug() << "Diag point " << hex << value;
	callSignalEmitByIndex(0);
}

dword ModuleCMOS::io_getDiagValue(int) {
	qDebug() << "Read diag value = " << diagPoint;
	return diagPoint;
}

ModuleCMOS::~ModuleCMOS() {
	save();
	delete [] CMOSMemory;
}

void ModuleCMOS::debug(const QString & str) {
	QMap<QString, QString> params;
	params.insert("type", "device");
	params.insert("message", str);
	doDebug(params);
}

void ModuleCMOS::moduleReset() {}

void ModuleCMOS::moduleShutdown() {
	save();
}

dword ModuleCMOS::io_read(int) {
	if (CMOSAddr == 0x00) {
		debug("RD Sec");
		return (QTime::currentTime().second() & 0xFF);
	}
	if (CMOSAddr == 0x02) {
		debug("RD Min");
		return (QTime::currentTime().minute() & 0xFF);
	}
	if (CMOSAddr == 0x04) {
		debug("RD Hr");
		return (QTime::currentTime().hour() & 0xFF);
	}
	if (CMOSAddr == 0x06) {
		debug("RD Dow");
		return (QDate::currentDate().dayOfWeek() & 0xFF);
	}
	if (CMOSAddr == 0x07) {
		debug("RD Day");
		return (QDate::currentDate().day() & 0xFF);
	}
	if (CMOSAddr == 0x08) {
		debug("RD Month");
		return (QDate::currentDate().month() & 0xFF);
	}
	if (CMOSAddr == 0x09) {
		debug("RD Year");
		return (QDate::currentDate().year() & 0xFF);
	}
	if (CMOSAddr == 0x10) {
		qDebug() << "RD FloppyType";
		debug("RD FloppyType");
		return 0x40;
	}
	if (CMOSAddr == 0x14) {
		qDebug() << "RD InstalledEquipment byte";
		debug("RD InstalledEquipment byte");
		return 0x2D;
	}
	debug("RD [" + QString::number(CMOSAddr, 16) + "] -> (" + QString::number(CMOSMemory[CMOSAddr], 16) + ")");
	return CMOSMemory[CMOSAddr];
}

void ModuleCMOS::io_write(dword val32, int) {
	byte value = val32 & 0xFF;
	debug("WR [" + QString::number(CMOSAddr, 16) + "] <- (" + QString::number(value, 16) + ")");
	if (CMOSAddr == 0x00) {
		debug("WR Sec");
		qDebug() << "WR Sec";
	} else
	if (CMOSAddr == 0x02) {
		debug("WD Min");
		qDebug() << "WR Min";
	} else
	if (CMOSAddr == 0x10) {
		qDebug() << "WR FloppyType";
		debug("RD FloppyType");
	} else
	if (CMOSAddr == 0x14) {
		qDebug() << "WR InstalledEquipment byte";
		debug("WR InstalledEquipment byte");
	}
	CMOSMemory[CMOSAddr] = value;
	save();
}

void ModuleCMOS::io_setAddr(dword value, int) {
	CMOSAddr = (value & 0x7F);
	debug("Set address = " + QString::number(CMOSAddr, 16) + "h");
}

void ModuleCMOS::load() {
	//FILE *File = fopen("CMOS.BIN","rb");
	QFile file(FILE_NAME);
	if (file.open(QFile::ReadOnly)) {
		file.read(reinterpret_cast<char*>(CMOSMemory), size);
		file.close();
		debug("CMOS table laded");
	} else {
		debug("Error loading CMOS table");
	}
}

void ModuleCMOS::tick() {}

void ModuleCMOS::save() {
	QFile file(FILE_NAME);
	if (file.open(QFile::WriteOnly)) {
		file.write(reinterpret_cast<char*>(CMOSMemory), size);
		file.close();
		debug("Save OK");
	} else {
		debug("Save error");
	}
}

ModuleDevice *createModule() {
	return new ModuleCMOS();
}
