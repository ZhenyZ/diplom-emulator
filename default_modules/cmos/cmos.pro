QT       -= gui

TARGET = cmosgeneric
TEMPLATE = lib

DEFINES += CMOS_LIBRARY

INCLUDEPATH += $$PWD/../lib/

CONFIG(debug, debug|release) {
	LIBS += -L$$PWD/../lib/debug -lEMUESDebugLib -lEMUESDevicesLib
}
CONFIG(release, debug|release) {
	LIBS += -L$$PWD/../lib/release -lEMUESDebugLib -lEMUESDevicesLib
}



SOURCES += modulecmos.cpp


HEADERS += cmos_global.h \
	modulecmos.h

