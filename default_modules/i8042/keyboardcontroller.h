#ifndef KEYBOARDCONTROLLER_H
#define KEYBOARDCONTROLLER_H

#include <moduledevice.h>
#include <loggable.h>
#include "i8042_global.h"
#include <QList>

class I8042SHARED_EXPORT KeyboardController : public Loggable, public ModuleDevice
{
	Q_OBJECT
public:
	KeyboardController(QObject * parent = nullptr);
	virtual ~KeyboardController();

	virtual void tick() override;
	virtual void moduleReset() override;
	virtual void moduleShutdown() override;

	dword io_readDataPort(int);
	dword io_readStatusReg(int);
	dword io_readPortB(int);
	void io_writeDataPort(dword, int);
	void io_writeCommangReg(dword, int);
	void io_writePortB(dword, int);
	void io_writeTimerChannel2Out(dword value, int);
	void onKeyPress(dword key, int length);
	void onKeyRelease(dword key, int length);

	static constexpr byte
		BIT_OUTPUT_FULL_SET = 0x1,
		BIT_OUTPUT_FULL_CLEAR = ~BIT_OUTPUT_FULL_SET,
		BIT_INPUT_FULL_SET = 0x2,
		BIT_INPUT_FULL_CLEAR = ~BIT_INPUT_FULL_SET,
		BIT_HOT_START = 0x4,
		BIT_WRITING_TO_CONTROLLER_SET = 0x8,
		BIT_WRITING_TO_CONTROLLER_CLEAR = ~BIT_WRITING_TO_CONTROLLER_SET,
		BIT_KEYBD_UNLOCKED = 0x10;

	static constexpr int
		SIGNAL_EMIT_INT = 0,
		SIGNAL_CPU_RESET = 1,
		SIGNAL_INT_ABORT = 2;

protected:
	virtual void debug(const QString &str) override;

private:
	void init();
	void initIO();

	QList<byte> dataOutputQueue;
	QList<byte> dataOutputPublicQueue;
	QList<byte> dataInputQueue;
	bool isWritingToController;
	bool hotStart;

	static constexpr byte
		BIT_KBD_STATUS_UNLOCK = 0x80,
		BIT_KBD_STATUS_SYSLOCK = 0x20,
		BIT_KBD_STATUS_CLOCK = 0x40,

		BIT_CMD_INT = 0x01,
		BIT_CMD_HOTSTART = 0x04,
		BIT_CMD_DISABLE_KBD = 0x10,
		BIT_CMD_TRANSLATEXT = 0x40,

		BIT_SYS_RESET = 0x01,
		BIT_SYS_A20 = 0x02,
		BIT_SYS_OUT_FULL = 0x10,
		BIT_SYS_IN_EMPTY = 0x20,
		BIT_SYS_KBD_CLOCK = 0x40;

	byte keyboardStatusByte;

	byte sysByte;

	byte commandByte;
	int commandByteIndex;
	byte generateInterrupts;
	byte keyboardOn;
	byte translateXT;
	bool isCommand;
	bool isKbdCommand;
	byte kbdCommandByte;

	byte timer2OutValue;
	byte refreshRequestStatus;

	void enqueue(byte value);

};

extern "C" I8042SHARED_EXPORT ModuleDevice * createModule();

#endif // KEYBOARDCONTROLLER_H
