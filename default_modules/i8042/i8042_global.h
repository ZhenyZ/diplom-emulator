#ifndef I8042_GLOBAL_H
#define I8042_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(I8042_LIBRARY)
#  define I8042SHARED_EXPORT Q_DECL_EXPORT
#else
#  define I8042SHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // I8042_GLOBAL_H
