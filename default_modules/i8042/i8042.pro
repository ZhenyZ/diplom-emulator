QT -= gui

CONFIG += c++14

TARGET = i8042
TEMPLATE = lib

DEFINES += I8042_LIBRARY

INCLUDEPATH += $$PWD/../lib/

CONFIG(debug, debug|release) {
	LIBS += -L$$PWD/../lib/debug -lEMUESDebugLib -lEMUESDevicesLib
}
CONFIG(release, debug|release) {
	LIBS += -L$$PWD/../lib/release -lEMUESDebugLib -lEMUESDevicesLib
}

SOURCES += keyboardcontroller.cpp

HEADERS += keyboardcontroller.h\
			i8042_global.h
