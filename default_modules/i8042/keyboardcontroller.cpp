#include "keyboardcontroller.h"
#include <QDebug>

KeyboardController::KeyboardController(QObject* parent)
	: Loggable("Keybd", parent),
	ModuleDevice("8042 PS/2 Controller", "Eugene Shestakov", 2)
{
	initIO();
	init();
}

KeyboardController::~KeyboardController() {

}

void KeyboardController::tick() {
	if (dataOutputQueue.size()) {
		dataOutputPublicQueue.append(dataOutputQueue);
		dataOutputQueue.clear();
	}
	if (dataOutputPublicQueue.size()) {
		callSignalEmitByIndex(SIGNAL_EMIT_INT);
	}
	if (dataInputQueue.size()) {
		byte value = dataInputQueue.takeFirst();
		if (isCommand) {
			if (commandByte == 0x60) {
				++commandByteIndex;
				generateInterrupts = (value & BIT_CMD_INT);
				if (value & BIT_CMD_DISABLE_KBD) {
					if (!keyboardOn) {
						keyboardOn = true;
						enqueue(0xAA);
					}
				} else {
					if (keyboardOn) {
						keyboardOn = false;
					}
				}
				hotStart = (value & BIT_CMD_HOTSTART);
				translateXT = (value & BIT_CMD_TRANSLATEXT);
				isCommand = false;
			} else
			if (commandByte == 0xD1) {
				if (!value & 0x01) {
					callSignalEmitByIndex(SIGNAL_CPU_RESET);
					value |= 0x01;
				}
				sysByte = value;
				isCommand = false;
			}
		} else {
			if (!isKbdCommand) {
				isKbdCommand = true;
				kbdCommandByte = value;
				switch (value) {
					case 0xED:
						debug("Set lights");
						enqueue(0xFA);
					break;
					case 0xEE:
						debug("Keyboard echo");
						isKbdCommand = false;
						enqueue(0xEE);
					break;
					case 0xF4:
						enqueue(0xFA);
						debug("Keyboard resume");
						dataOutputQueue.clear();
						dataOutputPublicQueue.clear();
						isKbdCommand = false;
					break;
					case 0xFF:
						debug("Reset kbd");
						enqueue(0xAA);
						isKbdCommand = false;
					break;
					default:
						qDebug() << "Sent byte to KBD Inner Controller: " << hex << value;
					break;
				}
			} else {
				qDebug() << "Keyb about to receive next byte, queue size= " << dataInputQueue.size();
				switch (kbdCommandByte) {
					case 0xED:
						qDebug() << "Kbd set lights = " << hex << value;
						enqueue(0xFA);
						isKbdCommand = false;
					break;
					default:
						qDebug() << "Unknown kbd opcode " << hex << value;
					break;
				}
			}
		}
	}
}

void KeyboardController::moduleReset() {
	init();
}

void KeyboardController::moduleShutdown() {

}

dword KeyboardController::io_readDataPort(int) {
	if (dataOutputPublicQueue.size()) {
		qDebug() << "Kbd: read data " << hex << dataOutputPublicQueue.first();
		return dataOutputPublicQueue.takeFirst();
	}
	return 0;
}

dword KeyboardController::io_readStatusReg(int) {
	dword result = 0;
	if (dataInputQueue.size()) {
		result |= BIT_INPUT_FULL_SET;
	}
	if (dataOutputPublicQueue.size()) {
		result |= BIT_OUTPUT_FULL_SET;
	}
	if (hotStart) {
		result |= BIT_HOT_START;
	}
	if (isWritingToController) {
		result |= BIT_WRITING_TO_CONTROLLER_SET;
	}
	result |= BIT_KEYBD_UNLOCKED;
	return result;
}

dword KeyboardController::io_readPortB(int) {
	dword result = timer2OutValue;
	if (refreshRequestStatus) {
		refreshRequestStatus = 0;
	} else {
		refreshRequestStatus = 0x10;
	}
	result |= refreshRequestStatus;
	return result;
}

void KeyboardController::io_writeDataPort(dword d, int) {
	dataInputQueue.push_back(d & 0xFF);
}

void KeyboardController::io_writeCommangReg(dword value, int) {
	byte command = (value & 0xFF);
	isWritingToController = true;
	isCommand = true;
	byte tempOut;
	switch (command) {
		case 0x20:
			debug("Read command byte");
			dataOutputQueue.push_back(commandByte);
			isCommand = false;
		break;
		case 0x60:
			debug("Write command byte");
			commandByte = command;
			commandByteIndex = 0;
		break;
		case 0xA4:
			debug("Read password status");
			commandByte = command;
			enqueue(0xF1);
			isCommand = false;
		break;
		case 0xA8:
			debug("Disable mouse");
			commandByte = command;
			isCommand = false;

		break;
		case 0xA9:
			debug("Test aux device");
			commandByte = command;
			enqueue(0x00);
			isCommand = false;
		break;
		case 0xAA:
			debug("Self-test");
			commandByte = command;
			enqueue(0x55);
			isCommand = false;
		break;
		case 0xAB:
			debug("Test kbd device");
			commandByte = command;
			enqueue(0x00);
			isCommand = false;
		break;
		case 0xAD:
			debug("Switch kbd off");
			commandByte = command;
			keyboardOn = false;
			isCommand = false;
		break;
		case 0xAE:
			debug("Switch kbd on");
			commandByte = command;
			keyboardOn = true;
			isCommand = false;
		break;
		case 0xAF:
			debug("Read version");
			commandByte = command;
			enqueue(0x02);
			isCommand = false;
		break;
		case 0xC0:
			debug("Read Status port");
			commandByte = command;
			enqueue(keyboardStatusByte);
			isCommand = false;
		break;
		case 0xC1:
			debug("Read Status port Lo");
			commandByte = command;
			enqueue(keyboardStatusByte & 0x0F);
			isCommand = false;
		break;
		case 0xC2:
			debug("Read Status port Hi");
			commandByte = command;
			enqueue((keyboardStatusByte & 0xF0) >> 4);
			isCommand = false;
		break;
		case 0xD0:
			debug("Read SysOutput port");
			if (sysByte & BIT_SYS_KBD_CLOCK) {
				sysByte &= (~BIT_SYS_KBD_CLOCK);
			} else {
				sysByte |= (~BIT_SYS_KBD_CLOCK);
			}
			commandByte = command;
			enqueue(sysByte);
			isCommand = false;
		break;
		case 0xD1:
			debug("Write SysOutput port");
			commandByte = command;
			commandByteIndex = 0;
		break;
		case 0xFE:
			debug("Issue reboot signal");
			commandByte = command;
			isCommand = false;
			callSignalEmitByIndex(SIGNAL_CPU_RESET);
		break;
		default:
			qDebug() << "Keybd: unknown command " << hex << command;
		break;
	}
}

void KeyboardController::io_writePortB(dword value, int) {
	if (value & 0x80) {
		callSignalEmitByIndex(SIGNAL_INT_ABORT);
	}
}

void KeyboardController::io_writeTimerChannel2Out(dword value, int) {
	timer2OutValue = value & 0xFF;
}

void KeyboardController::debug(const QString & str) {
	QMap<QString, QString> params;
	params.insert("type", "device");
	params.insert("message", str);
	doDebug(params);
//	qDebug() << "Keybd: " << str;
}

void KeyboardController::init()
{
	dataInputQueue.clear();
	dataOutputPublicQueue.clear();
	dataOutputQueue.clear();
	isWritingToController = true;
	hotStart = 0;
	commandByteIndex = 0;
	keyboardStatusByte = BIT_KBD_STATUS_SYSLOCK | BIT_KBD_STATUS_UNLOCK;
	commandByte = 0;
	generateInterrupts = 0;
	keyboardOn = 0;
	isCommand = false;
	sysByte = BIT_SYS_IN_EMPTY | BIT_SYS_RESET | BIT_SYS_KBD_CLOCK;
	isKbdCommand = false;
	kbdCommandByte = 0;
	refreshRequestStatus = 0;
	timer2OutValue = 0;
}

void KeyboardController::initIO() {
	registerInput(0, "Write Data", std::bind(&KeyboardController::io_writeDataPort, this, std::placeholders::_1, std::placeholders::_2));
	registerInput(1, "Write Command", std::bind(&KeyboardController::io_writeCommangReg, this, std::placeholders::_1, std::placeholders::_2));
	registerInput(2, "Keyboard Press", std::bind(&KeyboardController::onKeyPress, this, std::placeholders::_1, std::placeholders::_2));
	registerInput(3, "Keyboard Release", std::bind(&KeyboardController::onKeyRelease, this, std::placeholders::_1, std::placeholders::_2));
	registerInput(4, "Write timer2 out value", std::bind(&KeyboardController::io_writeTimerChannel2Out, this, std::placeholders::_1, std::placeholders::_2));
	registerInput(5, "Write portB", std::bind(&KeyboardController::io_writePortB, this, std::placeholders::_1, std::placeholders::_2));

	registerOutput(0, "Read Data", std::bind(&KeyboardController::io_readDataPort, this, std::placeholders::_1));
	registerOutput(1, "Read Status", std::bind(&KeyboardController::io_readStatusReg, this, std::placeholders::_1));
	registerOutput(2, "Read portB", std::bind(&KeyboardController::io_readPortB, this, std::placeholders::_1));

	registerEmitSignalFunction(SIGNAL_EMIT_INT, "Interrupt");
	registerEmitSignalFunction(SIGNAL_CPU_RESET, "Reset CPU");
	registerEmitSignalFunction(SIGNAL_INT_ABORT, "Abort INT rq");
}

void KeyboardController::enqueue(byte value)
{
	dataOutputQueue.push_back(value);
}

void KeyboardController::onKeyPress(dword key, int length) {
	if (!isKbdCommand && !isCommand) {
		debug("Keyboard press detected " + QString::number(key, 16) + "h");
//		while (length--) {
			dataOutputQueue.push_back(key & 0xFF);
//			key = key >> 8;
//		}
		callSignalEmitByIndex(SIGNAL_EMIT_INT);
	} else {
		debug("Keyboard press skipped: busy");
	}
}

void KeyboardController::onKeyRelease(dword key, int length) {
	if (!isKbdCommand && !isCommand) {
		qDebug() << "Keyb release " << hex << key;
		debug("Keyboard release detected " + QString::number(key, 16) + "h");
		key |= 0x80;
//		while (length--) {
			dataOutputQueue.push_back(key & 0xFF);
//			key = key >> 8;
//		}
		callSignalEmitByIndex(SIGNAL_EMIT_INT);
	} else {
		debug("Keyboard release skipped: busy");
	}
}

ModuleDevice * createModule() {
	return new KeyboardController();
}
