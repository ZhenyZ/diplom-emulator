QT		+= core gui widgets
CONFIG	+= c++11

TARGET = VirtualMain
CONFIG -= app_bundle

GIT_REV_NUMBER = $$system(git rev-list --count HEAD)
GIT_REV_COMMIT = $$system(git rev-parse --short HEAD)

TEMPLATE = app

CONFIG(debug, debug|release) {
	QMAKE_CXXFLAGS_DEBUG += -pg
	QMAKE_CXXFLAGS_MT_DBG += -pg
	QMAKE_LFLAGS_DEBUG += -pg
	LIBS += -L$$PWD/lib/debug -lEMUESDebugLib -lEMUESDevicesLib
}
CONFIG(release, debug|release) {
	LIBS += -L$$PWD/lib/release -lEMUESDebugLib -lEMUESDevicesLib
}

INCLUDEPATH += $$PWD/lib

SOURCES += main.cpp \
    core/modulebuilder.cpp \
    core/repository/modulerepository.cpp \
    core/compiledsystem.cpp \
    ui/systembuilder.cpp \
    ui/mainwindow.cpp \
    ui/genericdataform.cpp \
    ui/moduleconfigwindow.cpp \
    ui/moduleconfigdelegate.cpp \
    ui/moduleconfigmodel.cpp \
    ui/defaultconfigdelegate.cpp \
    ui/statisticswidget.cpp \
    ui/timingwidget.cpp

HEADERS += \
    core/modulebuilder.h \
    core/repository/modulerepository.h \
    core/compiledsystem.h \
    core/repository/modulenotfoundexception.h \
    ui/mainwindow.h \
    ui/systembuilder.h \
    ui/genericdataform.h \
    ui/helper/genericformbuilder.h \
    ui/moduleconfigwindow.h \
    ui/moduleconfigdelegate.h \
    ui/moduleconfigmodel.h \
    ui/defaultconfigdelegate.h \
    ui/statisticswidget.h \
    ui/timingwidget.h

FORMS += \
    ui/mainwindow.ui \
    ui/systembuilder.ui \
    ui/genericdataform.ui \
    ui/moduleconfigwindow.ui \
    ui/statisticswidget.ui \
    ui/timingwidget.ui

RESOURCES += \
    resorces.qrc
