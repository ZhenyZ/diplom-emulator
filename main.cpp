#include <QApplication>

#include <ui/mainwindow.h>

#include <moduledevice.h>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	qRegisterMetaType<QMap<QString,QString> >("DebugMap");
	qRegisterMetaType<byte>("byte");
	qRegisterMetaType<word>("word");
	qRegisterMetaType<dword>("dword");
	qRegisterMetaType<qword>("qword");
	qRegisterMetaType<QList<ModuleDevice *> >("ModuleList");
	qRegisterMetaType<ModuleMap>("ModuleMap");

	MainWindow w;
	w.show();

	return a.exec();
}
